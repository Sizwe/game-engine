#pragma once

#include <SDL.h>
#include <Vector3.h>
#include <Vector2.h>
#include <Ray.h>
#include <string>

class Window {
public:
	SDL_Window* window;
	SDL_GLContext glContext;

	Window();
	Window(const std::string& title, int width, int height, Uint32 SDLFlags = 0);
	void init(const std::string& title, int width, int height, Uint32 SDLFlags = 0);

	Vector2 toLocal(const Vector3& worldPos);
	Ray toGlobal(const Vector2& windowPos);

	int getWidth() const;
	int getHeight() const;
	int getMinWidth() const;
	int getMinHeight() const;
	int getMaxWidth() const;
	int getMaxHeight() const;
	int getPosX() const;
	int getPosY() const;
	void setSize(int width, int height);
	void setMinSize(int width, int height);
	void setMaxSize(int width, int height);
	void setPos(int width, int height);

	void setTitle(const std::string& title);
	void setBrightness(float brightness);
	void setOpacity(float opacity);
	void setResizable(bool resizable);
	void setFullscreen(bool fullscreen);
	void setBordered(bool bordered);

	std::string getTitle() const;
	float getBrightness() const;
	float getOpacity() const;
	bool resizable() const;
	bool hidden() const;
	bool shown() const;
	bool minimized() const;
	bool maximized() const;
	bool focused() const;
	bool fullscreen() const;

	void hide();
	void show();
	void maximize();
	void minimize();
	void restore();
	void focus();
	void swap();
	void del();
};
