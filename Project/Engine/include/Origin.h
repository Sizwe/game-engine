#pragma once

class Origin : public GameObject {
public:
	Origin() = default;
	Origin(const Origin&) = delete;
	Origin& operator= (const Origin&) = delete;

	void setEnabled(bool boolean) = delete;
	void doom() = delete;
};