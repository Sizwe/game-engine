#pragma once
#include <Line.h>
#include <Ray.h>
#include <Vector4.h>
#include <Octree.h>
#include <MonoSprite.h>
#include <MonoCube.h>

class Debug {
public:
	static void Draw(const Line& line, int duration = 0, const Vector4& colour = Vector4(0.5f, 0.5f, 0.5f, 1.0f));
	static void Draw(const Ray& ray, float dist = 1000.0f, int duration = 0, const Vector4& colour = Vector4(0.5f, 0.5f, 0.5f, 1.0f));
	static void Draw(const MonoSprite& monoSprite, int duration = 0);
	static void Draw(const MonoCube& monoCube, int duration = 0);

	static void Reset();
	static void SetEnabled(bool enabled);
	static bool Enabled();

	static void Render();
};