#pragma once
#include <IVector2.h>
#include <Vector3.h>
#include <Ray.h>

enum class MouseButton {
	Left = 0,
	Middle = 1,
	Right = 2
};

class Mouse {
private:
	friend class Engine;
	static bool Clicked[3];
	static bool Down[3];
	static IVector2 Scroll;
	static IVector2 Motion;

public:
	static bool ButtonDown(MouseButton button);
	static bool ButtonClicked(MouseButton button);
	static IVector2 GetScroll();
	static IVector2 GetMotion();
	static Ray GetRay();
};