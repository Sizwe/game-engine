#pragma once
#include <string>
#include <unordered_map>
#include <stdexcept>
#include <Font.h>
#include <Texture.h>
#include <CubeMap.h>
#include <Shader.h>
#include <iostream>

template<typename T>
class ResourceMap {
private:
	std::unordered_map<std::string, T*> map;

public:
	ResourceMap() = default;
	ResourceMap(const ResourceMap& rescMap) = delete;
	ResourceMap& operator=(const ResourceMap& rescMap) = delete;

	T* get(const std::string& key) {
		auto it = map.find(key);
		if (it != map.end()) {
			return it->second;
		}
		else {
			try {
				T* newResc = new T(key);
				map[key] = newResc;
				return newResc;
			}
			catch (const std::runtime_error& e) {
				std::cerr << e.what() << std::endl;
				return nullptr;
			}
		}
	}
	void set(const std::string& key, T* val) {
		auto it = map.find(key);
		if (it != map.end()) {
			delete it->second;
			it->second = val;
		}
		else
			map[key] = val;
	}
	bool exists(const std::string& key) {
		return map.find(key) != map.end();
	}
	void erase(const std::string& key) {
		auto it = map.find(key);
		if (it != map.end()) {
			delete it->second;
			map.erase(it);
		}
	}
	T* eject(const std::string& key) {
		auto it = map.find(key);
		if (it != map.end()) {
			T* val = it->second;
			map.erase(it);
			return val;
		}
		return nullptr;
	}
	size_t size() {
		return map.size();
	}
	void clear() {
		for (auto& it : map)
			delete it.second;
		map.clear();
	}
};

class Resources {
public:
	static ResourceMap<Shader> Shaders;
	static ResourceMap<Texture> Textures;
	static ResourceMap<CubeMap> CubeMaps;
	static ResourceMap<Font> Fonts;

	static void Clear();
};