#pragma once
#include <string>

void glClearError();
bool glLogCall(const std::string& function, const std::string& file, int line);

#define glCall(x) glClearError();\
	x;\
	if(!(glLogCall(#x, __FILE__, __LINE__)))\
		__debugbreak();