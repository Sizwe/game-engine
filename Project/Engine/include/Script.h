#pragma once

#include <functional>

class GameObject;
class Script {
	friend class GameObject;
	friend class Engine;
private:
	GameObject* parent;
	bool _enabled;
	bool started;

public:
	virtual Script* clone() const;

	int tag;

	Script();
	Script(int tag);
	Script(const Script& script);
	Script& operator=(const Script& script);
	virtual ~Script();

	GameObject* getParent();
	const GameObject* getParent() const;
	void setEnabled(bool boolean);
	bool enabled() const;
	virtual void start();
	virtual void update();
	virtual void lateUpdate();

	void startOnce();
};

template<typename T>
class FunctionalScript : public Script {
private:
	struct Functions {
		std::function<void(FunctionalScript&)> start = nullptr;
		std::function<void(FunctionalScript&)> update = nullptr;
		std::function<void(FunctionalScript&)> lateUpdate = nullptr;
		std::function<void(FunctionalScript&)> destructor = nullptr;
		std::function<void(FunctionalScript&)> copyModifier = nullptr;
	};
public:
	FunctionalScript* clone() const override { return new FunctionalScript(*this); }

	Functions funcs;
	T data = T();

	FunctionalScript(std::function<void(FunctionalScript&)> updateF = nullptr) { funcs.update = updateF; }
	FunctionalScript(std::function<void(FunctionalScript&)> startF, std::function<void(FunctionalScript&)> updateF, std::function<void(FunctionalScript&)> lateUpdateF = nullptr, std::function<void(FunctionalScript&)> copyModifierF = nullptr, std::function<void(FunctionalScript&)> destructorF = nullptr) { funcs.start = startF; funcs.update = updateF; funcs.lateUpdate = lateUpdateF; funcs.destructor = destructorF; funcs.copyModifier = copyModifierF; }
	FunctionalScript(const FunctionalScript& lambdaScript) : Script(lambdaScript) { data = lambdaScript.data; funcs = lambdaScript.funcs; if (funcs.copyModifier) funcs.copyModifier(*this); }
	FunctionalScript& operator=(const FunctionalScript& lambdaScript) { Script::operator=(lambdaScript); data = lambdaScript.data; funcs = lambdaScript.funcs; if (funcs.copyModifier) funcs.copyModifier(*this); return *this; }

	void start() override { if (funcs.start) funcs.start(*this); }
	void update() override { if (funcs.update) funcs.update(*this); }
	void lateUpdate() override { if (funcs.lateUpdate) funcs.lateUpdate(*this); }
	~FunctionalScript() { if (funcs.destructor) funcs.destructor(*this); }
};
