#pragma once
#include <Shader.h>
#include <GameObject.h>
#include <DirLights.h>
#include <PointLights.h>
#include <CubeMap.h>
#include <array>
#include <map>

struct DirShadow {
	Texture* map = nullptr;
	Matrix4 lightLocalMatrix;
};

struct PointShadow {
	CubeMap* map = nullptr;
	Vector3 lightPos;
	float span = 0.0f;
};

struct GlobalContext {
	Vector3 camPos;
	std::map<const GameObject*, Matrix4>* modelMatrices = nullptr;
};

struct LocationContext : public GlobalContext {
	Matrix4 projMatrix;
	Matrix4 viewMatrix;
};

struct MaterialContext : public LocationContext {
	std::array<DirShadow, DirLights.size()> dirShadows;
	std::array<PointShadow, PointLights.size()> pointShadows;
};

struct PointShadowContext : public GlobalContext {
	std::array<Matrix4, 6> lightSpacePVs;
	float span = 0.0f;
};