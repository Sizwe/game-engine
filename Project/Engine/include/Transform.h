#pragma once
#include <Vector3.h>
#include <Quaternion.h>

class Transform {
public:
	Vector3 position;
	Quaternion rotation;
	Vector3 scale;

	bool operator == (const Transform& t) const;
	bool operator != (const Transform& t) const;
	bool operator < (const Transform& t) const;

	Transform(const Vector3& position = Vector3(), const Quaternion& rotation = Quaternion(), const Vector3& scale = Vector3(1.0f));
};