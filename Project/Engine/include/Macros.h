#pragma once
#include <cstdint>

using schar = int8_t;
using sshort = int16_t;
using sint = int32_t;
using slong = int64_t;

using uchar = uint8_t;
using ushort = uint16_t;
using uint = uint32_t;
using ulong = uint64_t;

using byte = uchar;
using ubyte = uchar;
using ibyte = schar;
using sbyte = schar;

using u8  = byte;
using u16 = ushort;
using u32 = uint;
using u64 = ulong;

using i8 = ibyte;
using i16 = sshort;
using i32 = sint;
using i64 = slong;