#pragma once

#include <Script.h>
#include <set>
#include <stack>
#include <functional>
#include <vector>
#include <Transform.h>

class GameObject {
public:
	template<class T>
	class FieldSet : protected std::vector<T*> {
	private:
		GameObject& gameObject;

	public:
		class iterator : public std::vector<T*>::iterator {
		public:
			iterator() = default;
			template<typename IT>
			iterator(const IT& super) : std::vector<T*>::iterator(super) {}
			T* operator->() { return *std::vector<T*>::iterator::operator->(); }
			T& operator*() { return *std::vector<T*>::iterator::operator*(); }
			operator T* () const { return std::vector<T*>::iterator::operator*(); }
		};

		class const_iterator : public std::vector<T*>::const_iterator {
		public:
			const_iterator() = default;
			template<typename IT>
			const_iterator(const IT& super) : std::vector<T*>::const_iterator(super) {}
			const T* operator->() { return *std::vector<T*>::const_iterator::operator->(); }
			const T& operator*() { return *std::vector<T*>::const_iterator::operator*(); }
			operator const T* () const { return std::vector<T*>::const_iterator::operator*(); }
		};

	public:
		FieldSet(GameObject& gameObject) : gameObject(gameObject) {}
		virtual ~FieldSet() { for (size_t i = 0, s = std::vector<T*>::size(); i < s; ++i) delete std::vector<T*>::at(i); }

		void add(T* value) {
			value->parent = &gameObject;
			std::vector<T*>::push_back(value);
		}
		void add(const std::initializer_list<T*>& values) {
			std::vector<T*>::reserve(std::vector<T*>::size() + values.size());
			for (auto v : values)
				if (v) add(*v);
		}
		iterator remove(const iterator& it) { const auto end = std::vector<T*>::end();  if (it != end) { delete (T*)it; return std::vector<T*>::erase(it); } else return end; }
		iterator remove(const T* val) { return remove(std::find(std::vector<T*>::begin(), std::vector<T*>::end(), val)); }
		size_t count() const { return std::vector<T*>::size(); }
		void clear() {
			for (size_t i = 0, s = std::vector<T*>::size(); i < s; delete std::vector<T*>::at(i), ++i);
			std::vector<T*>::clear();
		}

		iterator begin() { return std::vector<T*>::begin(); }
		iterator end() { return std::vector<T*>::end(); }
		const_iterator cbegin() const { return std::vector<T*>::cbegin(); }
		const_iterator cend() const { return std::vector<T*>::cend(); }
		const_iterator begin() const { return cbegin(); }
		const_iterator end() const { return cend(); }

		void copy(const FieldSet& memberSet) {
			clear();
			std::vector<T*>::reserve(memberSet.size());
			for (size_t i = 0, s = memberSet.size(); i < s; ++i) {
				T* copy = memberSet[i]->clone();
				copy->parent = &gameObject;
				std::vector<T*>::push_back(copy);
			}
		}
	};

	class ChildTree {
	private:
		GameObject& gameObject;

	public:
		class iterator {
		protected:
			std::stack<int> path;
			GameObject* current;

			void advance();
			void recede();

		public:
			iterator(GameObject& root);

			operator GameObject* ();
			GameObject* operator->();

			iterator operator+(int index) const;
			iterator operator-(int index) const;

			iterator& operator+=(int index);
			iterator& operator-=(int index);

			iterator& operator++();
			iterator& operator--();

			iterator operator++(int);
			iterator operator--(int);

			iterator& operator=(GameObject* gameObject);

			GameObject& operator *();
			bool operator==(const iterator& it) const;
			bool operator!=(const iterator& it) const;
			friend bool operator< (const iterator& a, const iterator& b);

			static iterator End(GameObject& root);
			static bool Ended(const iterator& it);
		};
		class const_iterator : public iterator {
		public:
			const_iterator(const GameObject& root);

			operator const GameObject* ();
			const GameObject* operator->();
			const GameObject& operator *();

			static const_iterator End(const GameObject& root);
		};

		ChildTree(GameObject& gameObject) : gameObject(gameObject) {}

		const GameObject* getByID(int tag) const;
		GameObject* getByID(int tag);

		template <class T>
		const T* getByType() const;
		template <class T>
		T* getByType();

		const std::set<GameObject*> getAllByID(int tag) const;
		std::set<GameObject*> getAllByID(int tag);

		template <class T>
		const std::set<T*> getAllByType() const;
		template <class T>
		std::set<T*> getAllByType();

		iterator begin() { return iterator(gameObject); }
		iterator end() { return iterator::End(gameObject); }
		const_iterator begin() const { return const_iterator(gameObject); }
		const_iterator end() const { return const_iterator::End(gameObject); }
		const_iterator cbegin() const { return const_iterator(gameObject); }
		const_iterator cend() const { return const_iterator::End(gameObject); }
	};
	class ScriptTree {
	private:
		GameObject& gameObject;

	public:
		class iterator {
		protected:
			ChildTree::iterator eIt;
			int index;

			void advance();
			void recede();

		public:
			iterator(GameObject& root);

			operator Script* ();
			Script* operator->();

			iterator operator+(int index) const;
			iterator operator-(int index) const;

			iterator& operator+=(int index);
			iterator& operator-=(int index);

			iterator& operator++();
			iterator& operator--();

			iterator operator++(int);
			iterator operator--(int);

			iterator& operator=(GameObject* gameObject);

			Script& operator *();
			bool operator==(const iterator& it) const;
			bool operator!=(const iterator& it) const;
			friend bool operator< (const iterator& a, const iterator& b);

			static iterator End(GameObject& root);
			static bool Ended(const iterator& it);
		};
		class const_iterator : public iterator {
		public:
			const_iterator(const GameObject& root);

			operator const Script* ();
			const Script* operator->();
			const Script& operator *();

			static const_iterator End(const GameObject& root);
		};

		ScriptTree(GameObject& gameObject) : gameObject(gameObject) {}

		const Script* getByID(int tag) const;
		Script* getByID(int tag);

		template <class T>
		const T* getByType() const;
		template <class T>
		T* getByType();

		const std::set<Script*> getAllByID(int tag) const;
		std::set<Script*> getAllByID(int tag);

		template <class T>
		const std::set<T*> getAllByType() const;
		template <class T>
		std::set<T*> getAllByType();

		iterator begin() { return iterator(gameObject); }
		iterator end() { return iterator::End(gameObject); }
		const_iterator begin() const { return const_iterator(gameObject); }
		const_iterator end() const { return const_iterator::End(gameObject); }
		const_iterator cbegin() const { return const_iterator(gameObject); }
		const_iterator cend() const { return const_iterator::End(gameObject); }
	};

	class ChildSet : public FieldSet<GameObject> {
	public:
		friend class ChildTree::iterator;
		friend class ChildTree::const_iterator;
		ChildSet(GameObject& gameObject) : FieldSet<GameObject>(gameObject) {}

		using FieldSet<GameObject>::add;
		GameObject* add(std::function<void()> constructor, std::function<void()> destructor = nullptr);
		template<typename attrib>
		GameObject& add(std::function<void(attrib&)> constructor, std::function<void(attrib&)> copyModifier = nullptr, std::function<void(attrib&)> destructor = nullptr);

		const GameObject* getByID(int tag) const;
		GameObject* getByID(int tag);

		template <class T>
		const T* getByType() const;
		template <class T>
		T* getByType();

		const std::set<GameObject*> getAllByID(int tag) const;
		std::set<GameObject*> getAllByID(int tag);

		template <class T>
		const std::set<T*> getAllByType() const;
		template <class T>
		std::set<T*> getAllByType();
	};
	class ScriptSet : public FieldSet<Script> {
	public:
		friend class ScriptTree::iterator;
		friend class ScriptTree::const_iterator;

		ScriptSet(GameObject& gameObject) : FieldSet<Script>(gameObject) {}

		using FieldSet<Script>::add;
		Script* add(std::function<void()> update);
		Script* add(std::function<void()> start, std::function<void()> update, std::function<void()> lateUpdate = nullptr);

		template<typename attrib>
		Script& add(std::function<void(attrib&)> update)
		{
			struct Data { attrib data; std::function<void(attrib&)> u; };
			FunctionalScript<Data> ls;
			ls.funcs.update = [](FunctionalScript<Data>& le) { if (le.data.u) le.data.u(le.data.data); };
			ls.data = { attrib(), update };
			return FieldSet<Script>::add(ls);
		}
		template<typename attrib>
		Script* add(std::function<void(attrib&)> start, std::function<void(attrib&)> update, std::function<void(attrib&)> lateUpdate = nullptr, std::function<void(attrib&)> destructor = nullptr)
		{
			class Data { attrib data; std::function<void(attrib&)> s, u, lu, d; };
			FunctionalScript<Data>* ls = new FunctionalScript<Data>();
			ls->funcs.start = [](FunctionalScript<Data>& le) { if (le.data.s) le.data.s(le.data.data); };
			ls->funcs.update = [](FunctionalScript<Data>& le) { if (le.data.u) le.data.u(le.data.data); };
			ls->funcs.lateUpdate = [](FunctionalScript<Data>& le) { if (le.data.lu) le.data.lu(le.data.data); };
			ls->funcs.destructor = [](FunctionalScript<Data>& le) { if (le.data.d) le.data.d(le.data.data); };
			ls->data = { attrib(), start, update, lateUpdate, destructor };
			FieldSet<Script>::add(ls);
			return ls;
		}

		const Script* getByID(int tag) const;
		Script* getByID(int tag);

		template <class T>
		const T* getByType() const;
		template <class T>
		T* getByType();

		const std::set<Script*> getAllByID(int tag) const;
		std::set<Script*> getAllByID(int tag);

		template <class T>
		const std::set<T*> getAllByType() const;
		template <class T>
		std::set<T*> getAllByType();
	};

	class LocalTransformer {
	private:
		GameObject& gameObject;

	public:
		LocalTransformer(GameObject& gameObject);

		Vector3 pos(const Vector3& globalPoint) const;
		Vector3 scale(const Vector3& globalScale) const;
		Quaternion rotation(const Quaternion& globalRotation) const;

		Vector3 operator()(const Vector3& globalPoint) const { return pos(globalPoint); }

		Matrix4 matrix() const;
	};
	class GlobalTransformer {
	private:
		GameObject& gameObject;

	public:
		GlobalTransformer(GameObject& gameObject);

		Vector3 pos(const Vector3& localPoint) const;
		Vector3 scale(const Vector3& localScale) const;
		Quaternion rotation(const Quaternion& localRotation) const;

		Vector3 operator()(const Vector3& localPoint) const { return pos(localPoint); }

		Matrix4 matrix() const;
	};

	class GlobalTransform {
	private:
		GameObject& gameObject;

	public:
		GlobalTransform(GameObject& gameObject);

		Vector3 getPos() const;
		Vector3 getScale() const;
		Quaternion getRotation() const;
		void setPos(const Vector3& globalPos);
		void setScale(const Vector3& globalScale);
		void setRotation(const Quaternion& globalRotation);
	};

private:
	bool _enabled;
	bool _doomed;
	GameObject* parent;

public:
	virtual GameObject* clone() const;

	int tag;

	Transform local;
	GlobalTransform global;
	LocalTransformer toLocal;
	GlobalTransformer toGlobal;

	ChildSet children;
	ScriptSet scripts;
	ChildTree tree;
	ScriptTree scriptTree;

	GameObject();
	GameObject(const Transform& localTransform);
	GameObject(const GameObject& gameObject);
	GameObject& operator=(const GameObject& gameObject);
	virtual ~GameObject();

	void setEnabled(bool boolean);
	bool enabled() const;

	GameObject* getParent();
	const GameObject* getParent() const;

	void doom();
	bool doomed() const;
};
 
using attrib = int;
template<typename attrib>
class FunctionalGameObject : public GameObject {
private:
	struct Functions {
		std::function<void(FunctionalGameObject&)> constructor = nullptr;
		std::function<void(FunctionalGameObject&)> copyModifier = nullptr;
		std::function<void(FunctionalGameObject&)> destructor = nullptr;
	};
public:
	FunctionalGameObject* clone() const override { return new FunctionalGameObject(*this); }

	Functions funcs;
	attrib data = attrib();

	FunctionalGameObject(const std::function<void(FunctionalGameObject&)>& constructor = nullptr, const std::function<void(FunctionalGameObject&)>& copyModifier = nullptr, const std::function<void(FunctionalGameObject&)>& destructor = nullptr) : GameObject() { funcs.constructor = constructor; funcs.copyModifier = copyModifier; funcs.destructor = destructor; if (constructor) constructor(*this); }
	FunctionalGameObject(const Transform& local, const std::function<void(FunctionalGameObject&)>& constructor = nullptr, const std::function<void(FunctionalGameObject&)>& copyModifier = nullptr, const std::function<void(FunctionalGameObject&)>& destructor = nullptr) : GameObject(local) { funcs.constructor = constructor; funcs.copyModifier = copyModifier; funcs.destructor = destructor; if (funcs.constructor) funcs.constructor(*this); }
	FunctionalGameObject(const FunctionalGameObject& lambdaGameObject) : GameObject(lambdaGameObject) { data = lambdaGameObject.data; funcs = lambdaGameObject.funcs; if (funcs.copyModifier) funcs.copyModifier(*this); }
	FunctionalGameObject& operator=(const FunctionalGameObject& lambdaGameObject) { GameObject::operator=(lambdaGameObject); data = lambdaGameObject.data; funcs = lambdaGameObject.funcs; if (funcs.copyModifier) funcs.copyModifier(*this); return *this; }
	~FunctionalGameObject() override { if (funcs.destructor) funcs.destructor(*this); }
};

template <class T>
const std::set<T*> GameObject::ChildSet::getAllByType() const { std::set<T*> out; T* t; for (auto& v : *this) if (!v.doomed() && t = (T*)dynamic_cast<const T*>(&v)) out.insert(t); return out; }
template <class T>
std::set<T*> GameObject::ChildSet::getAllByType() { std::set<T*> out; T* t; for (auto& v : *this) if (!v.doomed() && t = dynamic_cast<T*>(&v)) out.insert(t); return out; }

template <class T>
const T* GameObject::ChildSet::getByType() const { const T* t; for (auto& v : *this) if (!v.doomed() && t = dynamic_cast<const T*>(&v)) return t; return nullptr; }
template <class T>
T* GameObject::ChildSet::getByType() { T* t; for (auto& v : *this) if (!v.doomed() && t = dynamic_cast<T*>(&v)) return t; return nullptr; }

template <class T>
const T* GameObject::ScriptSet::getByType() const { const T* t; for (auto& v : *this) if (t = dynamic_cast<const T*>(&v)) return t; return nullptr; }
template <class T>
T* GameObject::ScriptSet::getByType() { T* t; for (auto& v : *this) if (t = dynamic_cast<T*>(&v)) return t; return nullptr; }

template <class T>
const std::set<T*> GameObject::ScriptSet::getAllByType() const { std::set<T*> out; T* t; for (auto& v : *this) if (t = (T*)dynamic_cast<const T*>(&v)) out.insert(t); return out; }
template <class T>
std::set<T*> GameObject::ScriptSet::getAllByType() { std::set<T*> out; T* t; for (auto& v : *this) if (t = dynamic_cast<T*>(&v)) out.insert(t); return out; }

//////////////////////////////////////

template <class T>
const std::set<T*> GameObject::ChildTree::getAllByType() const { std::set<T*> out; T* t; for (auto& v : *this) if (!v.doomed() && t = (T*)dynamic_cast<const T*>(&v)) out.insert(t); return out; }
template <class T>
std::set<T*> GameObject::ChildTree::getAllByType() { std::set<T*> out; T* t; for (auto& v : *this) if (!v.doomed() && t = dynamic_cast<T*>(&v)) out.insert(t); return out; }

template <class T>
const T* GameObject::ChildTree::getByType() const { const T* t; for (auto& v : *this) if (!v.doomed() && t = dynamic_cast<const T*>(&v)) return t; return nullptr; }
template <class T>
T* GameObject::ChildTree::getByType() { T* t; for (auto& v : *this) if (!v.doomed() && t = dynamic_cast<T*>(&v)) return t; return nullptr; }

template <class T>
const T* GameObject::ScriptTree::getByType() const { const T* t; for (auto& v : *this) if (t = dynamic_cast<const T*>(&v)) return t; return nullptr; }
template <class T>
T* GameObject::ScriptTree::getByType() { T* t; for (auto& v : *this) if (t = dynamic_cast<T*>(&v)) return t; return nullptr; }

template <class T>
const std::set<T*> GameObject::ScriptTree::getAllByType() const { std::set<T*> out; T* t; for (auto& v : *this) if (t = (T*)dynamic_cast<const T*>(&v)) out.insert(t); return out; }
template <class T>
std::set<T*> GameObject::ScriptTree::getAllByType() { std::set<T*> out; T* t; for (auto& v : *this) if (t = dynamic_cast<T*>(&v)) out.insert(t); return out; }

template<typename attrib>
GameObject& GameObject::ChildSet::add(std::function<void(attrib&)> constructor, std::function<void(attrib&)> copyModifier, std::function<void(attrib&)> destructor)
{
	struct Data { attrib data; std::function<void(attrib&)> c, cm, d; };
	FunctionalGameObject<Data>* le = new FunctionalGameObject<Data>();
	le->funcs.constructor = [](FunctionalGameObject<Data>& le) { if (le.data.c) le.data.c(le.data.data); };
	le->funcs.copyModifier = [](FunctionalGameObject<Data>& le) { if (le.data.cm) le.data.cm(le.data.data); };
	le->funcs.destructor = [](FunctionalGameObject<Data>& le) { if (le.data.d) le.data.d(le.data.data); };
	le->data = { attrib(), constructor, copyModifier, destructor };
	FieldSet<GameObject>::add(le);
	return le;
}