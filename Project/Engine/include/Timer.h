#pragma once
#include <Macros.h>
#include <unordered_set>
#include <ostream>
#include <string>

class AbsoluteTime {
private:
	friend class Engine;
	static int SumT;
	static int LastT;
	static int DeltaT;

public:
	static int Elapsed();
	static int DeltaTime();
};

class Time {
private:
	friend class Engine;
	static int SumT;
	static int LastT;
	static int DeltaT;
	static float ResidualT;
	static float Speed;

public:
	static int Elapsed();
	static int DeltaTime();

	static void SetSpeed(float speed);
	static float GetSpeed();
};

class AbsoluteTimer {
protected:
	int lastT;
	int sumT;
	float residualT;
	float speed;
	bool paused;
public:

	AbsoluteTimer(float speed = 1.0f);
	AbsoluteTimer(int milli);

	void start();
	void setPaused(bool pause);
	void setSpeed(float speed);
	float getSpeed() const;
	bool getPaused() const;

	void setElapsed(int milliseconds);
	int elapsed();

	AbsoluteTimer operator+(int ms);
	AbsoluteTimer& operator+=(int ms);
	AbsoluteTimer& operator=(int ms);
	AbsoluteTimer operator-(int ms);
	AbsoluteTimer operator-=(int ms);

	friend std::ostream& operator<<(std::ostream& output, AbsoluteTimer& timer);
};

class Timer {
protected:
	int lastT;
	int sumT;
	float residualT;
	float speed;
	bool paused;
public:

	Timer(float speed = 1.0f);
	Timer(int milli);

	void start();
	void setPaused(bool pause);
	void setSpeed(float speed);
	float getSpeed() const;
	bool getPaused() const;

	void setElapsed(int milliseconds);
	int elapsed();

	Timer operator+(int ms);
	Timer& operator+=(int ms);
	Timer& operator=(int ms);
	Timer operator-(int ms);
	Timer operator-=(int ms);

	friend std::ostream& operator<<(std::ostream& output, Timer& timer);
};
