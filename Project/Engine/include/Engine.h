#pragma once
#include <GameObject.h>
#include <Origin.h>
#include <Window.h>

class Engine {
public:
	static Origin Origin;
	static Window Window;

private:
	static void MainLoop();
	static void HandleEvents();

public:
	static void Init();
	static void Run();
	static void Halt();
	static void Quit();

	static float FPS();
	static void ShowMessage(const std::string& title, const std::string& message);
};