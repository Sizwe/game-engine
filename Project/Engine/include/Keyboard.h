#pragma once
#include <Key.h>
#include <unordered_set>
#include <string>

class Keyboard {
private:
	friend class Engine;
	static std::unordered_set<Key> KeysPressed;
	static std::unordered_set<Key> KeysClicked;
	static std::string TextInput;

public:
	static bool KeyDown();
	static bool KeyDown(Key key);

	static bool KeyClicked(Key key);
	static bool KeyClicked();

	static std::string GetInputTex();
};