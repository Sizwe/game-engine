#include <Script.h>
#include <GameObject.h>
#include <iostream>


Script* Script::clone() const
{
	return new Script(*this);
}

Script::Script() : Script(int()) {}

Script::Script(int tag)
{
	parent = nullptr;
	_enabled = true;
	started = false;
	this->tag = tag;
}

Script::Script(const Script& script)
{
	parent = nullptr;
	tag = script.tag;
	_enabled = script._enabled;
	started = false;
}

Script& Script::operator=(const Script& script)
{
	tag = script.tag;
	_enabled = script._enabled;
	started = false;
	return *this;
}

Script::~Script() {}

GameObject* Script::getParent()
{
	return parent;
}

const GameObject* Script::getParent() const
{
	return parent;
}

void Script::setEnabled(bool enabled) {
	_enabled = enabled;
}

bool Script::enabled() const {
	if (_enabled) {
		if (const GameObject* parent = getParent())
			return parent->enabled();
		return true;
	}
	return false;
}

void Script::startOnce() {
	if (!started) {
		start();
		started = true;
	}
}

void Script::start() {}
void Script::update() {}
void Script::lateUpdate() {}