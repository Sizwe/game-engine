#include "..\include\Mouse.h"
#include <Engine.h>

bool Mouse::Clicked[3] = { false, false, false };
bool Mouse::Down[3] = { false, false, false };
IVector2 Mouse::Scroll;
IVector2 Mouse::Motion;

bool Mouse::ButtonDown(MouseButton button)
{
	return Clicked[(size_t)button];
}

bool Mouse::ButtonClicked(MouseButton button)
{
	return Down[(size_t)button];
}

IVector2 Mouse::GetScroll()
{
	return Scroll;
}

IVector2 Mouse::GetMotion()
{
	return Motion;
}

Ray Mouse::GetRay() {
	int x, y;
	SDL_GetMouseState(&x, &y);

	float width = (float)Engine::Window.getWidth();
	float height = (float)Engine::Window.getHeight();
	Vector2 screenPos(2.0f * (x / width) - 1.0f, 1.0f - (y / height) * 2.0f);
	return Engine::Window.toGlobal(screenPos);
}
