#include <Error.h>
#include <iostream>
#include <glad/glad.h>

void glClearError() {
	long error;
	while ((error = glGetError()) != GL_NO_ERROR) {
		std::cerr << "Untracked Error!: " << error << "\n";
	}
}

bool glLogCall(const std::string& function, const std::string& file, int line) {
	while (GLenum error = glGetError()) {
		std::cerr << "[OpenGl_Error](" << error << ")" << " function: " << function << " file: " << file << "\n";
		return false;
	}
	return true;
}
