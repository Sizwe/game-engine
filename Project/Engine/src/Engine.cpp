#include <Engine.h>
#include <Renderer.h>
#include <Resources.h>
#include <SDL.h>
#include <Keyboard.h>
#include <Mouse.h>
#include <Timer.h>
#include <Debug.h>
#include <glad/glad.h>
#include <iostream>
#include <PointLights.h>
#include <DirLights.h>
#include <Reflections.h>
#include <TextTexturer.h>

Origin Engine::Origin;
Window Engine::Window;

namespace {
	bool engineRunning;
	int frameStartTime = 0;
	int absFrameStartTime = 0;

	void handleMouseDown(bool& click, bool& down) {
		if (!click && !down)
			click = true;
		else if (click && down)
			click = false;
		down = true;
	}

	inline void tryCall(Script& s, void (Script::* func)()) {
		try {
			(s.*func)();
		}
		catch (const std::exception & e) {
			std::cerr << e.what() << std::endl;
		}
	}

	inline void updateTree(GameObject& go, void(Script::* func)()) {
		if (go.enabled()) {
			for (auto& s : go.scripts) {
				if (s.enabled()) {
					tryCall(s, &Script::startOnce);
					tryCall(s, func);
				}
			}
			for (auto& g : go.children)
				updateTree(g, func);
		}
	}

	inline void startTree(GameObject& go) {
		if (go.enabled()) {
			for (auto& s : go.scripts) {
				if (s.enabled())
					tryCall(s, &Script::startOnce);
			}
			for (auto& g : go.children)
				startTree(g);
		}
	}
}

inline void garbageCollect(GameObject& gameObject) {
	for (auto goIt = gameObject.children.begin(), end = gameObject.children.end(); goIt != end; ++goIt) {
		if (goIt->doomed()) {
			goIt->scripts.clear();
			goIt->children.clear();
			goIt = goIt->children.remove(goIt);
		}
		else {
			garbageCollect(*goIt);
		}
	}
}

void Engine::MainLoop()
{
	frameStartTime = Time::Elapsed();
	absFrameStartTime = AbsoluteTime::Elapsed();

	while (engineRunning) {
		int currTime = Time::Elapsed();
		Time::DeltaT = currTime - frameStartTime;
		frameStartTime = Time::Elapsed();

		int absCurrTime = AbsoluteTime::Elapsed();
		AbsoluteTime::DeltaT = absCurrTime - absFrameStartTime;
		absFrameStartTime = AbsoluteTime::Elapsed();

		HandleEvents();

		garbageCollect(Engine::Origin);
		startTree(Engine::Origin);
		updateTree(Engine::Origin, &Script::update);
		updateTree(Engine::Origin, &Script::lateUpdate);

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		Debug::Render();
		Renderer::RenderToScreen();

		Engine::Window.swap();
	}
}

void Engine::HandleEvents()
{
	Mouse::Motion = IVector2(0, 0);
	Mouse::Scroll = IVector2(0, 0);
	Keyboard::KeysClicked.clear();
	Keyboard::TextInput = "";

	SDL_Event currentEvent;
	while (SDL_PollEvent(&currentEvent)) {
		switch (currentEvent.type) {
		case SDL_QUIT:
			engineRunning = false;
			break;
		case SDL_KEYDOWN:
			Keyboard::KeysPressed.insert((Key)currentEvent.key.keysym.sym);
			if (Keyboard::KeysClicked.count((Key)currentEvent.key.keysym.sym) == 0 && currentEvent.key.repeat == 0)
				Keyboard::KeysClicked.insert((Key)currentEvent.key.keysym.sym);
			break;
		case SDL_KEYUP:
			Keyboard::KeysPressed.erase((Key)currentEvent.key.keysym.sym);
			break;
		case SDL_TEXTINPUT:
			Keyboard::TextInput = currentEvent.text.text;
			break;
		case SDL_MOUSEBUTTONDOWN:
			switch (currentEvent.button.button) {
			case SDL_BUTTON_LEFT:
				handleMouseDown(Mouse::Clicked[(size_t)MouseButton::Left], Mouse::Down[(size_t)MouseButton::Left]);
				break;
			case SDL_BUTTON_MIDDLE:
				handleMouseDown(Mouse::Clicked[(size_t)MouseButton::Middle], Mouse::Down[(size_t)MouseButton::Middle]);
				break;
			case SDL_BUTTON_RIGHT:
				handleMouseDown(Mouse::Clicked[(size_t)MouseButton::Right], Mouse::Down[(size_t)MouseButton::Right]);
				break;
			}
			break;
		case SDL_MOUSEBUTTONUP:
			switch (currentEvent.button.button) {
			case SDL_BUTTON_LEFT:
				Mouse::Clicked[(size_t)MouseButton::Left] = Mouse::Down[(size_t)MouseButton::Left] = false;
				break;
			case SDL_BUTTON_MIDDLE:
				Mouse::Clicked[(size_t)MouseButton::Middle] = Mouse::Down[(size_t)MouseButton::Middle] = false;
				break;
			case SDL_BUTTON_RIGHT:
				Mouse::Clicked[(size_t)MouseButton::Right] = Mouse::Down[(size_t)MouseButton::Right] = false;
				break;
			}
			break;
		case SDL_MOUSEWHEEL:
			Mouse::Scroll = IVector2(currentEvent.wheel.x, currentEvent.wheel.y);
			break;
		case SDL_MOUSEMOTION:
			Mouse::Motion = IVector2(currentEvent.motion.xrel, currentEvent.motion.yrel);
			break;
		}
	}
}

void Engine::Quit()
{
	Engine::Origin.children.clear();
	Engine::Origin.scripts.clear();
	TextTexturer::Clear();
	Renderer::Reset();
	Debug::Reset();
	Resources::Clear();
	Engine::Window.del();
	SDL_Quit();
	engineRunning = false;
}

float Engine::FPS()
{
	int delta = AbsoluteTime::DeltaTime();
	return (delta) ? 1000.0f / delta : 0.0f;
}

void Engine::Init()
{
	Engine::Quit();
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		std::cerr << "Unable to initialize SDL " << std::string(SDL_GetError()) << std::endl;
	}
	Engine::Window.init("Game", 720, 480);
	Engine::Window.setResizable(true);
	if (!gladLoadGL()) {
		std::cerr << "Unable to initialize GLAD " << std::string(SDL_GetError()) << std::endl;
	}
	if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE)) {
		std::cerr << "Unable to initialize OpenGl" << std::string(SDL_GetError()) << std::endl;
	}

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//glEnable(GL_CULL_FACE);
	glEnable(GL_PROGRAM_POINT_SIZE);
	glEnable(0x8861); //GL_POINT_SPRITE
}

void Engine::Run()
{
	engineRunning = true;
	Engine::Window.show();
	MainLoop();
	Engine::Window.hide();
}

void Engine::Halt()
{
	engineRunning = false;
}

void Engine::ShowMessage(const std::string& title, const std::string& message)
{
	if (SDL_ShowSimpleMessageBox(SDL_MessageBoxFlags::SDL_MESSAGEBOX_INFORMATION, title.c_str(), message.c_str(), Engine::Window.window) < 0) {
		std::cerr << "Error: could not display message box" << std::endl;
	}
}
