#include <GameObject.h>

#include <Script.h>

#include <Engine.h>

#include <iostream>
#include <sstream> 
#include <string>

GameObject * GameObject::clone() const
{
	return new GameObject(*this);
}

GameObject::GameObject() : tag(0), parent(nullptr), _enabled(true), _doomed(false), children(*this), scripts(*this), tree(*this), scriptTree(*this), toLocal(*this), toGlobal(*this), global(*this)
{
}

GameObject::GameObject(const Transform& localTransform) : GameObject()
{
	local = localTransform;
}

GameObject::GameObject(const GameObject& gameObject) : parent(nullptr), children(*this), scripts(*this), tree(*this), scriptTree(*this), toLocal(*this), toGlobal(*this), global(*this)
{
	children.clear();
	scripts.clear();
	children.copy(gameObject.children);
	scripts.copy(gameObject.scripts);
	_doomed = gameObject._doomed;
	_enabled = gameObject._enabled;
	tag = gameObject.tag;
	local = gameObject.local;
}

GameObject& GameObject::operator=(const GameObject& gameObject)
{
	children.clear();
	scripts.clear();
	children.copy(gameObject.children);
	scripts.copy(gameObject.scripts);
	_doomed = gameObject._doomed;
	_enabled = gameObject._enabled;
	tag = gameObject.tag;
	local = gameObject.local;
	return *this;
}

GameObject::~GameObject() {}

void GameObject::setEnabled(bool boolean)
{
	if (!doomed()) {
		_enabled = boolean;
	}
}

bool GameObject::enabled() const
{
	for (const GameObject* e = this; e; e = e->parent) {
		if (!e->_enabled || e->_doomed)
			return false;
	}
	return true;
}

GameObject* GameObject::getParent()
{
	return parent;
}

const GameObject * GameObject::getParent() const
{
	return parent;
}

void GameObject::doom()
{
	_enabled = false;
	_doomed = true;
}

bool GameObject::doomed() const
{
	for (const GameObject* e = this; e; e = e->parent) {
		if (e->_doomed)
			return false;
	}
	return false;
}

GameObject::LocalTransformer::LocalTransformer(GameObject& gameObject) : gameObject(gameObject) {}

Vector3 GameObject::LocalTransformer::pos(const Vector3& globalPoint) const
{
	return matrix() * globalPoint;
}

Vector3 GameObject::LocalTransformer::scale(const Vector3& globalScale) const
{
	Vector3 newScale = globalScale;
	for (const GameObject* curr = &gameObject; curr; curr = curr->getParent()) {
		Vector3 scl = curr->local.scale;
		newScale = newScale * Vector3(1.0f / scl.x, 1.0f / scl.y, 1.0f / scl.z);
	}
	return newScale;
}

Quaternion GameObject::LocalTransformer::rotation(const Quaternion& globalRotation) const
{
	Quaternion newRot = globalRotation;
	for (const GameObject* curr = &gameObject; curr; curr = curr->getParent()) {
		newRot = newRot / curr->local.rotation;
	}
	return newRot;
}

Matrix4 GameObject::LocalTransformer::matrix() const
{
	Matrix4 finalMx;
	finalMx.data[0] = finalMx.data[5] = finalMx.data[10] = finalMx.data[15] = 1.0f;
	for (const GameObject* curr = &gameObject; curr; curr = curr->getParent()) {
		finalMx = finalMx * Matrix4::Scale(1.0f / curr->local.scale.x, 1.0f / curr->local.scale.y, 1.0f / curr->local.scale.z)
			* curr->local.rotation.inv().toMatrix4()
			* Matrix4::Shift(-curr->local.position.x, -curr->local.position.y, -curr->local.position.z);
	}
	return finalMx;
}

GameObject::GlobalTransformer::GlobalTransformer(GameObject& gameObject) : gameObject(gameObject) {}

Vector3 GameObject::GlobalTransformer::pos(const Vector3& localPoint) const
{
	return matrix() * localPoint;
}

Vector3 GameObject::GlobalTransformer::scale(const Vector3& localScale) const
{
	Vector3 newScale = localScale;
	for (const GameObject* curr = &gameObject; curr; curr = curr->getParent()) {
		newScale = newScale * curr->local.scale;
	}
	return newScale;
}

Quaternion GameObject::GlobalTransformer::rotation(const Quaternion& localRotation) const
{
	Quaternion newRot = localRotation;
	for (const GameObject* curr = &gameObject; curr; curr = curr->getParent()) {
		newRot = curr->local.rotation * newRot;
	}
	return newRot;
}

Matrix4 GameObject::GlobalTransformer::matrix() const
{
	Matrix4 finalMx;
	finalMx.data[0] = finalMx.data[5] = finalMx.data[10] = finalMx.data[15] = 1.0f;
	for (const GameObject* curr = &gameObject; curr; curr = curr->getParent()) {
		finalMx = Matrix4::Shift(curr->local.position.x, curr->local.position.y, curr->local.position.z)
				* curr->local.rotation.toMatrix4()
				* Matrix4::Scale(curr->local.scale.x, curr->local.scale.y, curr->local.scale.z)
				* finalMx;
	}
	return finalMx;
}

GameObject* GameObject::ChildSet::add(std::function<void()> constructor, std::function<void()> destructor)
{
	struct Calls { std::function<void()> c, d; };
	FunctionalGameObject<Calls>* fgo = new FunctionalGameObject<Calls>();
	fgo->funcs.constructor = [](FunctionalGameObject<Calls>& le) { if (le.data.c) le.data.c(); };
	fgo->funcs.destructor = [](FunctionalGameObject<Calls>& le) { if (le.data.d) le.data.d(); };
	fgo->data = { constructor, destructor };
	GameObject::FieldSet<GameObject>::add(fgo);
	return fgo;
}

const GameObject* GameObject::ChildSet::getByID(int id) const { for (auto& v : *this) if (!v.doomed() && v.tag == id) return &v; return nullptr; }
GameObject* GameObject::ChildSet::getByID(int id) { for (auto& v : *this) if (!v.doomed() && v.tag == id) return &v; return nullptr; }

const std::set<GameObject*> GameObject::ChildSet::getAllByID(int id) const { std::set<GameObject*> out; for (auto& v : *this) if (!v.doomed() && v.tag == id) out.insert((GameObject*)& v); return out; }
std::set<GameObject*> GameObject::ChildSet::getAllByID(int id) { std::set<GameObject*> out; for (auto& v : *this) if (!v.doomed() && v.tag == id) out.insert(&v); return out; }

const Script* GameObject::ScriptSet::getByID(int id) const { for (auto& v : *this) if (v.tag == id) return &v; return nullptr; }
Script* GameObject::ScriptSet::getByID(int id) { for (auto& v : *this) if (v.tag == id) return &v; return nullptr; }

const std::set<Script*> GameObject::ScriptSet::getAllByID(int id) const { std::set<Script*> out; for (auto& v : *this) if (v.tag == id) out.insert((Script*)& v); return out; }
std::set<Script*> GameObject::ScriptSet::getAllByID(int id) { std::set<Script*> out; for (auto& v : *this) if (v.tag == id) out.insert(&v); return out; }

Script* GameObject::ScriptSet::add(std::function<void()> update)
{
	using LSF = FunctionalScript<std::function<void()>>;
	LSF* ls = new LSF([](LSF& s) { if (s.data) s.data(); });
	ls->data = update;
	GameObject::FieldSet<Script>::add(ls);
	return ls;
}
Script* GameObject::ScriptSet::add(std::function<void()> start, std::function<void()> update, std::function<void()> lateUpdate)
{
	struct Calls { std::function<void()> s, u, lu; };
	FunctionalScript<Calls>* ls = new FunctionalScript<Calls>(
		[](FunctionalScript<Calls>& s) { if (s.data.s) s.data.s(); },
		[](FunctionalScript<Calls>& u) { if (u.data.u) u.data.u(); },
		[](FunctionalScript<Calls>& lu) { if (lu.data.lu) lu.data.lu(); }
	);
	ls->data = { start, update, lateUpdate };
	GameObject::FieldSet<Script>::add(ls);
	return ls;
}

GameObject::GlobalTransform::GlobalTransform(GameObject& gameObject) : gameObject(gameObject) {}

Vector3 GameObject::GlobalTransform::getPos() const
{
	if (auto parent = gameObject.getParent()) {
		return parent->toGlobal(gameObject.local.position);
	}
	return gameObject.local.position;
}

Vector3 GameObject::GlobalTransform::getScale() const
{
	Vector3 point = gameObject.local.scale;
	for (const GameObject* curr = &gameObject; curr; curr = curr->getParent()) {
		point = point * curr->local.scale;
	}
	return point;
}

Quaternion GameObject::GlobalTransform::getRotation() const
{
	Quaternion newRot = gameObject.local.rotation;
	for (const GameObject* curr = &gameObject; curr; curr = curr->getParent()) {
		newRot = curr->local.rotation * newRot;
	}
	return newRot;
}

void GameObject::GlobalTransform::setPos(const Vector3& globalPos)
{
	gameObject.local.position = gameObject.getParent()->toLocal(globalPos);
}

void GameObject::GlobalTransform::setScale(const Vector3& globalScale)
{
	gameObject.local.scale = gameObject.getParent()->toLocal.scale(globalScale);
}

void GameObject::GlobalTransform::setRotation(const Quaternion& globalRotation)
{
	gameObject.local.rotation = gameObject.getParent()->toLocal.rotation(globalRotation);
}

void GameObject::ChildTree::iterator::advance()
{
	if (path.size() == 1) {
		if (path.top() == -1) {
			path.pop();
			return;
		}
	}
	if (current->children.count()) {
		current = current->children[0];
		path.push(0);
	}
	else {
		while (!path.empty()) {
			GameObject* parent = current->getParent();
			if (path.top() + 1 < (int)parent->children.count()) {
				int index = ++path.top();
				current = parent->children[index];
				return;
			}
			else {
				current = parent;
				path.pop();
			}
		}
		path.push(-1);
	}
}

void GameObject::ChildTree::iterator::recede()
{
	if (path.size() == 1) {
		if (path.top() == -1) {
			path.pop();
			while (int count = (int)current->children.count()) {
				int index = count - 1;
				path.push(index);
				current = current->children[index];
			}
			return;
		}
	}
	if (path.empty()) {
		path.push(-1);
	}
	else {
		if (path.top() - 1 < 0) {
			path.pop();
			current = current->getParent();
		}
		else {
			int index = --path.top();
			current = current->getParent()->children[index];
			while (int count = (int)current->children.count()) {
				index = count - 1;
				path.push(index);
				current = current->children[index];
			}
		}
	}
}

GameObject::ChildTree::iterator::iterator(GameObject& root)
{
	current = &root;
}

GameObject::ChildTree::iterator::operator GameObject* ()
{
	return current;
}

GameObject* GameObject::ChildTree::iterator::operator->()
{
	return current;
}

GameObject::ChildTree::iterator GameObject::ChildTree::iterator::operator+(int index) const
{
	auto copy = *this;
	if (index > 0) {
		for (; index > 0; --index) {
			copy.advance();
		}
	}
	else {
		for (; index < 0; ++index) {
			copy.recede();
		}
	}
	return copy;
}

GameObject::ChildTree::iterator GameObject::ChildTree::iterator::operator-(int index) const
{
	auto copy = *this;
	if (index > 0) {
		for (; index > 0; --index) {
			copy.recede();
		}
	}
	else {
		for (; index < 0; ++index) {
			copy.advance();
		}
	}
	return copy;
}

GameObject::ChildTree::iterator& GameObject::ChildTree::iterator::operator++()
{
	advance();
	return *this;
}

GameObject::ChildTree::iterator& GameObject::ChildTree::iterator::operator--()
{
	recede();
	return *this;
}

GameObject::ChildTree::iterator GameObject::ChildTree::iterator::operator++(int)
{
	GameObject::ChildTree::iterator temp = *this;
	advance();
	return temp;
}

GameObject::ChildTree::iterator GameObject::ChildTree::iterator::operator--(int)
{
	GameObject::ChildTree::iterator temp = *this;
	recede();
	return temp;
}

GameObject::ChildTree::iterator& GameObject::ChildTree::iterator::operator=(GameObject* gameObject)
{
	path = std::stack<int>();
	current = gameObject;
	return *this;
}

GameObject::ChildTree::iterator& GameObject::ChildTree::iterator::operator+=(int index)
{
	if (index > 0) {
		for (; index > 0; --index) {
			advance();
		}
	}
	else {
		for (; index < 0; ++index) {
			recede();
		}
	}
	return *this;
}

GameObject::ChildTree::iterator& GameObject::ChildTree::iterator::operator-=(int index)
{
	if (index > 0) {
		for (; index > 0; --index) {
			recede();
		}
	}
	else {
		for (; index < 0; ++index) {
			advance();
		}
	}
	return *this;
}

GameObject& GameObject::ChildTree::iterator::operator*()
{
	return *current;
}

bool GameObject::ChildTree::iterator::operator==(const GameObject::ChildTree::iterator& it) const
{
	return path == it.path && current == it.current;
}

bool GameObject::ChildTree::iterator::operator!=(const GameObject::ChildTree::iterator& it) const
{
	return path != it.path || current != it.current;
}

bool GameObject::ChildTree::iterator::Ended(const GameObject::ChildTree::iterator& it)
{
	return (it.path.size() == 1) ? (it.path.top() == -1) ? true : false : false;
}

GameObject::ChildTree::iterator GameObject::ChildTree::iterator::End(GameObject& root)
{
	auto it = GameObject::ChildTree::iterator(root);
	it.path.push(-1);
	return it;
}

bool operator<(const GameObject::ChildTree::iterator& a, const GameObject::ChildTree::iterator& b)
{
	return a.path < b.path;
}


bool operator<(const GameObject::ScriptTree::iterator& a, const GameObject::ScriptTree::iterator& b)
{
	return a.eIt < b.eIt;
}

void GameObject::ScriptTree::iterator::advance()
{
	if (!GameObject::ChildTree::iterator::Ended(eIt)) {
		++index;
		while (index >= eIt->scripts.count()) {
			index = 0;
			if (GameObject::ChildTree::iterator::Ended(++eIt)) {
				return;
			}
		}
	}
}

void GameObject::ScriptTree::iterator::recede()
{
	if (!GameObject::ChildTree::iterator::Ended(eIt)) {
		--index;
		while (index < 0) {
			if (GameObject::ChildTree::iterator::Ended(--eIt)) {
				index = 0;
				return;
			}
			else {
				index = int(eIt->scripts.count()) - 1;
			}
		}
	}
}

GameObject::ScriptTree::iterator::iterator(GameObject& start) : eIt(start)
{
	index = 0;
	while (!GameObject::ChildTree::iterator::Ended(eIt)) {
		if (eIt->scripts.count()) {
			break;
		}
		else {
			++eIt;
		}
	}
}

GameObject::ScriptTree::iterator::operator Script* ()
{
	GameObject* e = eIt;
	if (eIt) {
		return e->scripts[index];
	}
	else {
		return nullptr;
	}
}

Script* GameObject::ScriptTree::iterator::operator->()
{
	GameObject* e = eIt;
	if (eIt) {
		return e->scripts[index];
	}
	else {
		return nullptr;
	}
}

GameObject::ScriptTree::iterator GameObject::ScriptTree::iterator::operator+(int index) const
{
	auto copy = *this;
	if (index > 0) {
		for (; index > 0; --index) {
			copy.advance();
		}
	}
	else {
		for (; index < 0; ++index) {
			copy.recede();
		}
	}
	return copy;
}

GameObject::ScriptTree::iterator GameObject::ScriptTree::iterator::operator-(int index) const
{
	auto copy = *this;
	if (index > 0) {
		for (; index > 0; --index) {
			copy.recede();
		}
	}
	else {
		for (; index < 0; ++index) {
			copy.advance();
		}
	}
	return copy;
}

GameObject::ScriptTree::iterator& GameObject::ScriptTree::iterator::operator+=(int index)
{
	if (index > 0) {
		for (; index > 0; --index) {
			advance();
		}
	}
	else {
		for (; index < 0; ++index) {
			recede();
		}
	}
	return *this;
}

GameObject::ScriptTree::iterator& GameObject::ScriptTree::iterator::operator-=(int index)
{
	if (index > 0) {
		for (; index > 0; --index) {
			recede();
		}
	}
	else {
		for (; index < 0; ++index) {
			advance();
		}
	}
	return *this;
}

GameObject::ScriptTree::iterator& GameObject::ScriptTree::iterator::operator++()
{
	advance();
	return *this;
}

GameObject::ScriptTree::iterator& GameObject::ScriptTree::iterator::operator--()
{
	recede();
	return *this;
}

GameObject::ScriptTree::iterator GameObject::ScriptTree::iterator::operator++(int)
{
	GameObject::ScriptTree::iterator temp = *this;
	advance();
	return temp;
}

GameObject::ScriptTree::iterator GameObject::ScriptTree::iterator::operator--(int)
{
	GameObject::ScriptTree::iterator temp = *this;
	recede();
	return temp;
}

GameObject::ScriptTree::iterator& GameObject::ScriptTree::iterator::operator=(GameObject* gameObject)
{
	eIt = gameObject;
	index = 0;
	while (eIt) {
		if (((GameObject*)eIt)->scripts.count())
			break;
		else {
			++eIt;
		}
	}
	return *this;
}

Script& GameObject::ScriptTree::iterator::operator*()
{
	return *eIt->scripts[index];
}

bool GameObject::ScriptTree::iterator::operator==(const GameObject::ScriptTree::iterator& it) const
{
	return eIt == it.eIt && index == it.index;
}

bool GameObject::ScriptTree::iterator::operator!=(const GameObject::ScriptTree::iterator& it) const
{
	return eIt != it.eIt || index != it.index;
}

GameObject::ScriptTree::iterator GameObject::ScriptTree::iterator::End(GameObject& root)
{
	auto it = GameObject::ScriptTree::iterator(root);
	it.eIt = GameObject::ChildTree::iterator::End(root);
	return it;
}

bool GameObject::ScriptTree::iterator::Ended(const GameObject::ScriptTree::iterator& it)
{
	return GameObject::ChildTree::iterator::Ended(it.eIt);
}

GameObject::ChildTree::const_iterator::const_iterator(const GameObject& root) : GameObject::ChildTree::iterator(const_cast<GameObject&>(root)) {}

GameObject::ChildTree::const_iterator::operator const GameObject* ()
{
	return current;
}

const GameObject* GameObject::ChildTree::const_iterator::operator->()
{
	return current;
}

const GameObject& GameObject::ChildTree::const_iterator::operator*()
{
	return *current;
}

GameObject::ChildTree::const_iterator GameObject::ChildTree::const_iterator::End(const GameObject& root)
{
	auto it = GameObject::ChildTree::const_iterator(root);
	it.path.push(-1);
	return it;
}

GameObject::ScriptTree::const_iterator::const_iterator(const GameObject& root) : GameObject::ScriptTree::iterator(const_cast<GameObject&>(root)) {}

GameObject::ScriptTree::const_iterator::operator const Script* ()
{
	GameObject* e = eIt;
	if (e) {
		return e->scripts[index];
	}
	else {
		return nullptr;
	}
}

const Script* GameObject::ScriptTree::const_iterator::operator->()
{
	GameObject* e = eIt;
	if (e) {
		return e->scripts[index];
	}
	else {
		return nullptr;
	}
}

const Script& GameObject::ScriptTree::const_iterator::operator*()
{
	return *eIt->scripts[index];
}

GameObject::ScriptTree::const_iterator GameObject::ScriptTree::const_iterator::End(const GameObject& root)
{
	auto it = GameObject::ScriptTree::const_iterator(root);
	it.eIt = GameObject::ChildTree::iterator::End(const_cast<GameObject&>(root));
	return it;
}

const GameObject* GameObject::ChildTree::getByID(int id) const { for (auto& v : *this) if (!v.doomed() && v.tag == id) return &v; return nullptr; }
GameObject* GameObject::ChildTree::getByID(int id) { for (auto& v : *this) if (!v.doomed() && v.tag == id) return &v; return nullptr; }

const std::set<GameObject*> GameObject::ChildTree::getAllByID(int id) const { std::set<GameObject*> out; for (auto& v : *this) if (!v.doomed() && v.tag == id) out.insert((GameObject*)& v); return out; }
std::set<GameObject*> GameObject::ChildTree::getAllByID(int id) { std::set<GameObject*> out; for (auto& v : *this) if (!v.doomed() && v.tag == id) out.insert(&v); return out; }

const Script* GameObject::ScriptTree::getByID(int id) const { for (auto& v : *this) if (v.tag == id) return &v; return nullptr; }
Script* GameObject::ScriptTree::getByID(int id) { for (auto& v : *this) if (v.tag == id) return &v; return nullptr; }

const std::set<Script*> GameObject::ScriptTree::getAllByID(int id) const { std::set<Script*> out; for (auto& v : *this) if (v.tag == id) out.insert((Script*)& v); return out; }
std::set<Script*> GameObject::ScriptTree::getAllByID(int id) { std::set<Script*> out; for (auto& v : *this) if (v.tag == id) out.insert(&v); return out; }