#pragma once
#include <Debug.h>
#include <Renderer.h>
#include <Engine.h>
#include <Timer.h>
#include <list>
#include <iostream>

namespace {
	bool debugEnabled = true;

	class DrawSession {
	public:
		RenderSession renderSession;
		int duration = 0;
	};
	std::list<DrawSession> drawSessions;
}

void Debug::Draw(const Line& line, int duration, const Vector4& colour)
{
	if (debugEnabled) {
		auto rs = Renderer::Draw(Stripe(line, colour), &Engine::Origin);
		drawSessions.push_front({ rs, duration });
	}
}

void Debug::Draw(const Ray& ray, float dist, int duration, const Vector4& colour)
{
	Draw(Line(ray.start, ray.start + ray.dir.normalise() * dist), duration, colour);
}

void Debug::Draw(const MonoSprite& monoSprite, int duration)
{
	if (debugEnabled) {
		auto rs = Renderer::Draw(monoSprite, &Engine::Origin);
		drawSessions.push_front({ rs, duration });
	}
}

void Debug::Draw(const MonoCube& monoCube, int duration)
{
	if (debugEnabled) {
		auto rs = Renderer::Draw(monoCube, &Engine::Origin);
		drawSessions.push_front({ rs, duration });
	}
}

void Debug::Reset()
{
	for (auto d : drawSessions) {
		d.renderSession.close();
	}
	drawSessions.clear();
}

void Debug::SetEnabled(bool enabled)
{
	debugEnabled = enabled;
}

bool Debug::Enabled()
{
	return debugEnabled;
}

void Debug::Render()
{
	if (debugEnabled) {
		for (auto it = drawSessions.begin(); it != drawSessions.end();) {
			if (it->duration < 0) {
				it->renderSession.close();
				it = drawSessions.erase(it);
			}
			else {
				it->duration -= Time::DeltaTime();
				++it;
			}
		}
	}
}
