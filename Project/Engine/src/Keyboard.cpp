#include "..\include\Keyboard.h"

std::unordered_set<Key> Keyboard::KeysPressed;
std::unordered_set<Key> Keyboard::KeysClicked;
std::string Keyboard::TextInput;

bool Keyboard::KeyDown()
{
	return !KeysPressed.empty();
}

bool Keyboard::KeyDown(Key key)
{
	return KeysPressed.count(key);
}

bool Keyboard::KeyClicked()
{
	return !KeysClicked.empty();
}

bool Keyboard::KeyClicked(Key key)
{
	return KeysClicked.count(key);
}

std::string Keyboard::GetInputTex()
{
	return TextInput;
}
