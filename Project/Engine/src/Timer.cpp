#include <Timer.h>
#include <SDL.h>

int AbsoluteTime::SumT = 0;
int AbsoluteTime::LastT = 0;
int AbsoluteTime::DeltaT = 0;

int Time::SumT = 0;
int Time::LastT = 0;
int Time::DeltaT = 0;
float Time::ResidualT = 0.0f;
float Time::Speed = 1.0f;

int AbsoluteTime::Elapsed()
{
	int sdlTicks = (int)SDL_GetTicks();
	int delta = (sdlTicks - LastT);
	LastT = sdlTicks;
	SumT += delta;
	return SumT;
}

int AbsoluteTime::DeltaTime()
{
	return DeltaT;
}

int Time::Elapsed()
{
	int sdlTicks = (int)SDL_GetTicks();
	float delta = (sdlTicks - LastT) * Speed;
	ResidualT = modff(delta + ResidualT, &delta);
	LastT = sdlTicks;
	SumT += (int)delta;
	return SumT;
}

int Time::DeltaTime()
{
	return DeltaT;
}

void Time::SetSpeed(float speed)
{
	Elapsed(); //updates
	Speed = speed;
}

float Time::GetSpeed()
{
	return Speed;
}

Timer::Timer(float speed)
{
	start();
	this->speed = speed;
}

Timer::Timer(int milli)
{
	start();
	sumT = milli;
	this->speed = 1.0f;
}

void Timer::start()
{
	paused = false;
	lastT = Time::Elapsed();
	residualT = 0.0f;
	sumT = 0;
}

void Timer::setPaused(bool pause)
{
	if (pause != paused) {
		elapsed();
	}
	paused = pause;
}

void Timer::setSpeed(float speed)
{
	elapsed();
	this->speed = speed;
}

float Timer::getSpeed() const
{
	return speed;
}

bool Timer::getPaused() const
{
	return paused;
}

void Timer::setElapsed(int milliseconds)
{
	sumT = milliseconds;
}

int Timer::elapsed()
{
	if (paused) {
		lastT = Time::Elapsed();
	}
	else {
		int ticks = Time::Elapsed();
		float delta = (ticks - lastT) * speed;
		float intTicks;
		residualT = modf(delta + residualT, &intTicks);
		sumT += (int)intTicks;
		lastT = ticks;
	}
	return sumT;
}

Timer Timer::operator+(int ms)
{
	Timer t;
	t.speed = speed;
	t.paused = paused;
	t.sumT = this->elapsed() + ms;
	return t;
}

Timer& Timer::operator+=(int ms)
{
	this->sumT += ms;
	return *this;
}

Timer& Timer::operator=(int ms)
{
	start();
	this->sumT = ms;
	return *this;
}

Timer Timer::operator-(int ms)
{
	Timer t;
	t.speed = speed;
	t.paused = paused;
	t.sumT = this->elapsed() - ms;
	return t;
}

Timer Timer::operator-=(int ms)
{
	this->sumT -= ms;
	return *this;
}

AbsoluteTimer::AbsoluteTimer(float speed)
{
	start();
	this->speed = speed;
}

AbsoluteTimer::AbsoluteTimer(int milli)
{
	start();
	sumT = milli;
	this->speed = 1.0f;
}

void AbsoluteTimer::start()
{
	paused = false;
	lastT = AbsoluteTime::Elapsed();
	residualT = 0.0f;
	sumT = 0;
}

void AbsoluteTimer::setPaused(bool pause)
{
	if (pause != paused) {
		elapsed();
	}
	paused = pause;
}

void AbsoluteTimer::setSpeed(float speed)
{
	elapsed();
	this->speed = speed;
}

float AbsoluteTimer::getSpeed() const
{
	return speed;
}

bool AbsoluteTimer::getPaused() const
{
	return paused;
}

void AbsoluteTimer::setElapsed(int milliseconds)
{
	sumT = milliseconds;
}

int AbsoluteTimer::elapsed()
{
	if (paused) {
		lastT = AbsoluteTime::Elapsed();
	}
	else {
		int ticks = AbsoluteTime::Elapsed();
		float delta = (ticks - lastT) * speed;
		float intTicks;
		residualT = modf(delta + residualT, &intTicks);
		sumT += (int)intTicks;
		lastT = ticks;
	}
	return sumT;
}

AbsoluteTimer AbsoluteTimer::operator+(int ms)
{
	AbsoluteTimer t;
	t.speed = speed;
	t.paused = paused;
	t.sumT = this->elapsed() + ms;
	return t;
}

AbsoluteTimer& AbsoluteTimer::operator+=(int ms)
{
	this->sumT += ms;
	return *this;
}

AbsoluteTimer& AbsoluteTimer::operator=(int ms)
{
	start();
	this->sumT = ms;
	return *this;
}

AbsoluteTimer AbsoluteTimer::operator-(int ms)
{
	AbsoluteTimer t;
	t.speed = speed;
	t.paused = paused;
	t.sumT = this->elapsed() - ms;
	return t;
}

AbsoluteTimer AbsoluteTimer::operator-=(int ms)
{
	this->sumT -= ms;
	return *this;
}

std::ostream& operator<<(std::ostream& output, AbsoluteTimer& timer)
{
	return output << "[ " + std::to_string(timer.elapsed() * 0.001f) + " sec. ]";
}

std::ostream& operator<<(std::ostream& output, Timer& timer)
{
	return output << "[ " + std::to_string(timer.elapsed() * 0.001f) + " sec. ]";
}
