#include <Resources.h>

ResourceMap<Shader> Resources::Shaders;
ResourceMap<Texture> Resources::Textures;
ResourceMap<CubeMap> Resources::CubeMaps;
ResourceMap<Font> Resources::Fonts;

void Resources::Clear()
{
	Shaders.clear();
	Textures.clear();
	CubeMaps.clear();
	Fonts.clear();
}
