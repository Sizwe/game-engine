#include <Transform.h>

Transform::Transform(const Vector3& position, const Quaternion& rotation, const Vector3& scale) :
position(position),
rotation(rotation),
scale(scale) {}

bool Transform::operator==(const Transform& t) const {
	return position == t.position && rotation == t.rotation && scale == t.scale;;
}

bool Transform::operator!=(const Transform& t) const {
	return position != t.position || rotation != t.rotation || scale != t.scale;;
}

bool Transform::operator<(const Transform& t) const {
	return (position < t.position) ? true : (t.position < position) ? false : 
		(rotation < t.rotation) ? true : (t.rotation < rotation) ? false : 
		(scale < t.scale);
}
