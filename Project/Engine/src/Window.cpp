#include <Window.h>
#include <glad/glad.h>
#include <iostream>
#include <algorithm>
#include <Renderer.h>
#include <Matrix4.h>
#undef near
#undef far

Window::Window()
{
	window = nullptr;
	glContext = nullptr;
}

Window::Window(const std::string & title, int width, int height, Uint32 SDLFlags) : Window()
{
	init(title, width, height, SDLFlags);
	glContext = nullptr;
}

void Window::init(const std::string & title, int width, int height, Uint32 SDLFlags)
{
	del();
	window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDLFlags | SDL_WINDOW_OPENGL | SDL_WINDOW_HIDDEN);
	if (!window) std::cerr << "Could not create window" << std::endl;

	glContext = SDL_GL_CreateContext(window);
	if (!glContext) std::cerr << "OpenGL conext could not be created!" << std::endl;
}

Vector2 Window::toLocal(const Vector3& worldPos)
{
	if (Renderer::Camera) {
		float width = (float)getWidth();
		float height = (float)getHeight();
		Matrix4 viewMx = Renderer::Camera->toLocal.matrix();
		Matrix4 projectionMx = Matrix4::Persp(Renderer::projection.fov, width / height, Renderer::projection.near, Renderer::projection.far);
		Vector4 gPos = projectionMx * viewMx * Vector4(worldPos, 1.0f);
		return gPos.xy() / gPos.w;
	}
	return Vector2();
}

Ray Window::toGlobal(const Vector2& windowPos)
{
	if (Renderer::Camera) {
		float width = (float)getWidth();
		float height = (float)getHeight();

		Matrix4 viewMx = Renderer::Camera->toLocal.matrix();
		Matrix4 projectionMx = Matrix4::Persp(Renderer::projection.fov, width / height, Renderer::projection.near, Renderer::projection.far);
		Matrix4 projViewMx = projectionMx * viewMx;
		Matrix4 pvInv = projViewMx.inv();
		Vector4 pos1 = pvInv * Vector4(windowPos, 0.0f, 1.0f);
		Vector4 pos2 = pvInv * Vector4(windowPos, 1.0f, 1.0f);
		Vector3 start = pos1.xyz() / pos1.w;
		Vector3 end = pos2.xyz() / pos2.w;
		return Ray(start, start.to(end));
	}
	return Ray();
}

int Window::getWidth() const
{
	int w;
	SDL_GetWindowSize(window, &w, nullptr);
	return w;
}

int Window::getHeight() const
{
	int h;
	SDL_GetWindowSize(window, nullptr, &h);
	return h;
}

int Window::getMinWidth() const
{
	int w;
	SDL_GetWindowMinimumSize(window, &w, nullptr);
	return w;
}

int Window::getMinHeight() const
{
	int h;
	SDL_GetWindowMinimumSize(window, nullptr, &h);
	return h;
}

int Window::getMaxWidth() const
{
	int w;
	SDL_GetWindowMaximumSize(window, &w, nullptr);
	return w;
}

int Window::getMaxHeight() const
{
	int h;
	SDL_GetWindowMaximumSize(window, nullptr, &h);
	return h;
}

int Window::getPosX() const
{
	int x;
	SDL_GetWindowPosition(window, &x, nullptr);
	return x;
}

int Window::getPosY() const
{
	int y;
	SDL_GetWindowPosition(window, nullptr, &y);
	return y;
}

void Window::setSize(int width, int height)
{
	SDL_SetWindowSize(window, width, height);
}

void Window::setMinSize(int width, int height)
{
	SDL_SetWindowMinimumSize(window, width, height);
}

void Window::setMaxSize(int width, int height)
{
	SDL_SetWindowMaximumSize(window, width, height);
}

void Window::setPos(int width, int height)
{
	SDL_SetWindowPosition(window, width, height);
}

std::string Window::getTitle() const
{
	return std::string(SDL_GetWindowTitle(window));
}

void Window::setTitle(const std::string & title)
{
	SDL_SetWindowTitle(window, title.c_str());
}

float Window::getBrightness() const
{
	return SDL_GetWindowBrightness(window)*100.0f;
}

void Window::setResizable(bool resizable)
{
	SDL_SetWindowResizable(window, (SDL_bool)resizable);
}

void Window::setFullscreen(bool fullscreen)
{
	SDL_SetWindowFullscreen(window, (SDL_bool)fullscreen);
}

void Window::setBordered(bool bordered)
{
	SDL_SetWindowBordered(window, (SDL_bool)bordered);
}

bool Window::resizable() const
{
	return SDL_GetWindowFlags(window) & SDL_WINDOW_RESIZABLE;
}

bool Window::hidden()  const
{
	return SDL_GetWindowFlags(window) & SDL_WINDOW_HIDDEN;
}

bool Window::shown()  const
{
	return SDL_GetWindowFlags(window) & SDL_WINDOW_SHOWN;
}

bool Window::minimized()  const
{
	return SDL_GetWindowFlags(window) & SDL_WINDOW_MINIMIZED;
}

bool Window::maximized()  const
{
	return SDL_GetWindowFlags(window) & SDL_WINDOW_MAXIMIZED;
}

bool Window::focused()  const
{
	return SDL_GetWindowFlags(window) & (SDL_WINDOW_INPUT_FOCUS | SDL_WINDOW_MOUSE_FOCUS);
}

bool Window::fullscreen()  const
{
	return SDL_GetWindowFlags(window) & SDL_WINDOW_FULLSCREEN;
}

void Window::setBrightness(float brightness)
{
	SDL_SetWindowBrightness(window, brightness);
}

float Window::getOpacity() const
{
	float opacity;
	SDL_GetWindowOpacity(window, &opacity);
	return opacity*100.0f;
}

void Window::setOpacity(float opacity)
{
	SDL_SetWindowOpacity(window, opacity);
	
}

void Window::swap()
{
	SDL_GL_SwapWindow(window);
}

void Window::del()
{
	if (window) {
		SDL_DestroyWindow(window);
		SDL_GL_DeleteContext(glContext);
		window = nullptr;
		glContext = nullptr;
	}
}

void Window::hide()
{
	SDL_HideWindow(window);
	
}

void Window::show()
{
	SDL_ShowWindow(window);
}

void Window::maximize()
{
	SDL_MaximizeWindow(window);
}

void Window::minimize()
{
	SDL_MinimizeWindow(window);
}

void Window::restore()
{
	SDL_RestoreWindow(window);
}

void Window::focus()
{
	SDL_SetWindowInputFocus(window);
	SDL_RaiseWindow(window);
}
