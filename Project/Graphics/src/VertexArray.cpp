#include <VertexArray.h>
#include <Error.h>
#include <glad/glad.h>

VertexArray::VertexArray()
{
	glCall(glGenVertexArrays(1, &ID));
}

uint VertexArray::getID() const
{
	return ID;
}

void VertexArray::bind() const
{
	glCall(glBindVertexArray(ID));
}

#include <iostream>

void VertexArray::setConfig(uint index, int count, GLenum type, bool normalised, int stride, size_t byteOffset)
{
	glCall(glEnableVertexAttribArray(index));
	if (type >= GL_BYTE && type <= GL_UNSIGNED_INT) {
		glCall(glVertexAttribIPointer(index, count, type, stride, (void*)byteOffset));
	}
	else {
		glCall(glVertexAttribPointer(index, count, type, normalised, stride, (void*)byteOffset));
	}
}

void VertexArray::setDivisor(uint index, uint divisor)
{
	glVertexAttribDivisor(index, divisor);
}

VertexArray::~VertexArray()
{
	if (ID) {
		glCall(glDeleteVertexArrays(1, &ID));
		ID = 0;
	}
}
