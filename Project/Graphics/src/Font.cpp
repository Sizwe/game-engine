#include <iostream>
#include <fstream>
#include <Font.h>
#include <exception>

Font::Font(const std::string& directory)
{
	std::ifstream file(directory, std::ios_base::binary);
	if (!file.fail()) {
		std::vector<byte> buffer((std::istreambuf_iterator<char>(file)), (std::istreambuf_iterator<char>()));
		data.swap(buffer);
	}
	else {
		throw std::runtime_error("failed to read font from file: " + directory);
	}
}

Font::Font(const byte* data, size_t size)
{
	std::vector<byte> buffer(data, data + size);
	this->data.swap(buffer);
}

const std::vector<byte>& Font::getData() const
{
	return data;
}
