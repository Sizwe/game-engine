#pragma once
#include <Text.h>

Text::Text() : 
	font(nullptr), size(20),
	colour(Vector4(1.0f)), align(Align::Left),
	rect(Vector2(0.0), Vector2(FLT_MAX, -FLT_MAX))
	{}

Text::Text(const std::wstring& string, Font* font) : Text() {
	this->string = string;
	this->font = font;
}

Text::Text(const std::string& string, Font* font) : Text() {
	this->string.assign(string.begin(), string.end());
	this->font = font;
}
