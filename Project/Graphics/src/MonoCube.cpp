#include "MonoCube.h"

MonoCube::MonoCube(const Box& box, const Colour& colour) : box(box), colour(colour) {}

MonoCube::Colour::Colour(const Vector4& left, const Vector4& right, const Vector4& up, const Vector4& down, const Vector4& front, const Vector4& back)
 : left(left), right(right), top(up), bottom(down), front(front), back(back) {
}
