#include <CubeMap.h>
#include <Error.h>
#include <iostream>
#include <GraphicsUtils.h>
#include <glad/glad.h>
#include <SDL_image.h>

CubeMap::CubeMap()
{
	glGenTextures(1, &ID);
	bind();
	wrap[0] = wrap[1] = wrap[2] = TexWrap::ClampToEdge;
	minFilter = magFilter = TexFilter::Nearest;
	setFilter(TexFilter::Nearest);
	setWrap(TexWrap::ClampToEdge);
}

CubeMap::CubeMap(int width, int height, TexFormat format, TexDataType type)
{
	glGenTextures(1, &ID);
	bind();
	for (int i = 0; i < 6; ++i) {
		setFace(CubeFace(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i), width, height, format, type);
	}
	wrap[0] = wrap[1] = wrap[2] = TexWrap::ClampToEdge;
	minFilter = magFilter = TexFilter::Nearest;
	setFilter(TexFilter::Nearest);
	setWrap(TexWrap::ClampToEdge);
}

CubeMap::CubeMap(const byte* data, int width, int height, TexFormat format, TexDataType type)
{
	glGenTextures(1, &ID);
	bind();
	for (int i = 0; i < 6; ++i) {
		setFace(CubeFace(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i), data, width, height, format, type);
	}
	glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
	wrap[0] = wrap[1] = wrap[2] = TexWrap::ClampToEdge;
	minFilter = magFilter = TexFilter::Nearest;
	setFilter(TexFilter::Nearest);
	setWrap(TexWrap::ClampToEdge);
}

CubeMap::CubeMap(std::string textureDir)
{
	glGenTextures(1, &ID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, ID);
	for (int i = 0; i < 6; ++i) {
		setFace(CubeFace(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i), textureDir);
	}
	glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
	wrap[0] = wrap[1] = wrap[2] = TexWrap::Repeat;
	minFilter = magFilter = TexFilter::Nearest;
	setFilter(TexFilter::Nearest);
	setWrap(TexWrap::ClampToEdge);
}

void CubeMap::setFace(CubeFace face, int width, int height, TexFormat format, TexDataType type)
{
	int faceIndex = (int)face - GL_TEXTURE_CUBE_MAP_POSITIVE_X;
	glCall(glTexImage2D(GLenum(face), 0, GLint(format), width, height, 0, GLenum(format), GLenum(type), nullptr));
	faceData[faceIndex].format = format;
	faceData[faceIndex].width = width;
	faceData[faceIndex].height = height;
}

CubeMap::CubeMap(std::initializer_list<std::string> textureDirectories)
{
	glGenTextures(1, &ID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, ID);
	auto dirIt = textureDirectories.begin(), dirEnd = textureDirectories.end();
	for (int i = 0; i < 6 && dirIt != dirEnd; ++i, ++dirIt) {
		setFace(CubeFace(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i), *dirIt);
	}
	glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
	wrap[0] = wrap[1] = wrap[2] = TexWrap::Repeat;
	minFilter = magFilter = TexFilter::Nearest;
	setFilter(TexFilter::Nearest);
	setWrap(TexWrap::ClampToEdge);
}

CubeMap::~CubeMap()
{
	glCall(glDeleteTextures(1, &ID));
}

void CubeMap::setFace(CubeFace face, const std::string& directory)
{
	int faceIndex = (int)face - GL_TEXTURE_CUBE_MAP_POSITIVE_X;
	SDL_Surface* img = IMG_Load(directory.c_str());
	if (img) {
		faceData[faceIndex].width = img->w;
		faceData[faceIndex].height = img->h;
		flipSurfaceY(img);
		glCall(glTexImage2D(GLenum(face), 0, img->format->BytesPerPixel, faceData[faceIndex].width, faceData[faceIndex].height, 0, (GLenum)toTexFormat(*img->format), (GLenum)TexDataType::UByte, img->pixels));
		faceData[faceIndex].format = toTexFormat(*img->format);
		SDL_FreeSurface(img);
	}
	else {
		faceData[faceIndex].width = faceData[faceIndex].height = 0;
		throw std::runtime_error("Failed to load CubeMap Texture: " + directory);
	}
}

void CubeMap::setFace(CubeFace face, const byte* data, int width, int height, TexFormat format, TexDataType type)
{
	int faceIndex = (int)face - GL_TEXTURE_CUBE_MAP_POSITIVE_X;

	uint bps = bytesPerPixel(format);
	size_t imageSize = calcImageSize(width, height, bps);
	byte* copy = new byte[imageSize];
	memcpy(copy, data, imageSize);
	flipImageY(copy, width, height, bps);

	glCall(glTexImage2D(uint(face), 0, bps, width, height, 0, GLenum(format), GLenum(type), (void*)copy));
	delete[] copy;

	faceData[faceIndex].format = format;
	faceData[faceIndex].width = width;
	faceData[faceIndex].height = height;
}
void CubeMap::setFace(CubeFace face, const Texture& texture)
{
	auto data = texture.getData();
	setFace(face, data.data(), texture.getWidth(), texture.getHeight(), texture.getFormat());
}

void CubeMap::Bind(uint ID)
{
	glBindTexture(GL_TEXTURE_CUBE_MAP, ID);
}

void CubeMap::SetActive(int index)
{
	glActiveTexture(GL_TEXTURE0 + index);
}

uint CubeMap::getID() const
{
	return ID;
}

void CubeMap::bind() const
{
	glBindTexture(GL_TEXTURE_CUBE_MAP, ID);
}

void CubeMap::bindToUnit(uint index) const
{
	glActiveTexture(GL_TEXTURE0 + index);
	bind();
	glActiveTexture(GL_TEXTURE0);
}

void CubeMap::setWrap(const TexWrap mode)
{
	glCall(glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, (int)mode));
	glCall(glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, (int)mode));
	glCall(glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, (int)mode));
	wrap[0] = wrap[1] = wrap[2] = mode;
}

void CubeMap::setWrap(const TexWrapAxis wrapAxis, const TexWrap mode)
{
	glCall(glTexParameteri(GL_TEXTURE_CUBE_MAP, GLenum(wrapAxis), (int)mode));
	switch (wrapAxis) {
	case TexWrapAxis::S:
		wrap[0] = mode;
		break;
	case TexWrapAxis::T:
		wrap[1] = mode;
		break;
	case TexWrapAxis::R:
		wrap[2] = mode;
	}
}

void CubeMap::setFilter(const TexFilter mode)
{
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, (int)mode);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, (int)mode);
	minFilter = magFilter = mode;
}

void CubeMap::setFilter(const TexFilterOperation operation, const TexFilter mode)
{
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GLenum(operation), (int)mode);
	switch (operation) {
	case TexFilterOperation::Minimize:
		minFilter = mode;
		break;
	case TexFilterOperation::Magnify:
		magFilter = mode;
		break;
	}
}

TexWrap CubeMap::getWrap(const TexWrapAxis wrapAxis) const
{
	return (TexWrapAxis::S == wrapAxis) ? wrap[0] : (TexWrapAxis::T == wrapAxis) ? wrap[1] : (TexWrapAxis::R == wrapAxis) ? wrap[2] : (TexWrap)-1;
}

TexFilter CubeMap::getFilter(const TexFilterOperation operation) const
{
	return (TexFilterOperation::Minimize == operation) ? minFilter : (TexFilterOperation::Magnify == operation) ? magFilter : (TexFilter)-1;
}

TexFormat CubeMap::getFormat(CubeFace face) const
{
	return faceData[(int)face - GL_TEXTURE_CUBE_MAP_POSITIVE_X].format;
}

int CubeMap::getWidth(CubeFace face) const
{
	return faceData[(int)face - GL_TEXTURE_CUBE_MAP_POSITIVE_X].width;
}

int CubeMap::getHeight(CubeFace face) const
{
	return faceData[(int)face - GL_TEXTURE_CUBE_MAP_POSITIVE_X].height;
}

std::vector<byte> CubeMap::getData(CubeFace face) const
{
	const auto& cf = faceData[(int)face - GL_TEXTURE_CUBE_MAP_POSITIVE_X];
	if (uint bpp = (uint)bytesPerPixel(cf.format)) {
		bind();
		std::vector<byte> data(calcImageSize(cf.width, cf.height, bpp));
		glGetTexImage(GLenum(face), 0, GLenum(cf.format), GL_UNSIGNED_BYTE, data.data());
		flipImageY(data.data(), cf.width, cf.height, bpp);
		return data;
	}
	return std::vector<byte>();
}

Texture* CubeMap::createTexture(CubeFace face) const
{
	const auto& cf = faceData[(int)face - GL_TEXTURE_CUBE_MAP_POSITIVE_X];
	return new Texture(getData(face).data(), cf.width, cf.height, cf.format);
}
