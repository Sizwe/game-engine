#include <Texture.h>
#include <Error.h>
#include <iostream>
#include <GraphicsUtils.h>
#include <glad/glad.h>
#include <SDL_image.h>
#include <array>

Texture::Texture()
{
	ID = 0;
	width = height = 0;
	format = TexFormat::RGBA;
	wrap[0] = wrap[1] = TexWrap::Repeat;
	minFilter = magFilter = TexFilter::Nearest;
}

Texture::Texture(int width, int height, TexFormat format, TexDataType type)
{
	glCall(glGenTextures(1, &ID));
	bind();

	this->width = width;
	this->height = height;
	wrap[0] = wrap[1] = TexWrap::Repeat;
	minFilter = magFilter = TexFilter::Nearest;

	glTexImage2D(GL_TEXTURE_2D, 0, (int)format, width, height, 0, GLenum(format), GLenum(type), nullptr);
	setFilter(TexFilter::Nearest);
	setWrap(TexWrap::Repeat);
	this->format = format;
}

Texture::Texture(const std::string& directory)
{
	SDL_Surface* img = IMG_Load(directory.c_str());
	if (img){
		glCall(glGenTextures(1, &ID));
		width = img->w;
		height = img->h;
		bind();
		flipSurfaceX(img);
		glCall(glTexImage2D(GL_TEXTURE_2D, 0, img->format->BytesPerPixel, width, height, 0, (GLenum)toTexFormat(*img->format), (GLenum)TexDataType::UByte, img->pixels));
		glCall(glGenerateMipmap(GL_TEXTURE_2D));
		SDL_FreeSurface(img);
		setFilter(TexFilter::Nearest);
		setWrap(TexWrap::Repeat);
	}
	else {
		ID = 0;
		width = height = 0;
		throw std::runtime_error("Failed to load Texture: " + directory);
	}
	wrap[0] = wrap[1] = TexWrap::Repeat;
	minFilter = magFilter = TexFilter::Nearest;
	this->format = format;
}


Texture::Texture(const byte * data, int width, int height, TexFormat format, TexDataType type) 
{
	glCall(glGenTextures(1, &ID));
	bind();

	uint bps = bytesPerPixel(format);
	size_t imageSize = calcImageSize(width, height, bps);
	byte* copy = new byte[imageSize];
	memcpy(copy, data, imageSize);
	flipImageX(copy, width, height, bps);

	glCall(glTexImage2D(GL_TEXTURE_2D, 0, (GLenum)format, width, height, 0, (GLenum)format, GLenum(type), (void*)copy));
	glCall(glGenerateMipmap(GL_TEXTURE_2D));
	wrap[0] = wrap[1] = TexWrap::Repeat;
	minFilter = magFilter = TexFilter::Nearest;
	this->format = format;
	this->width = width;
	this->height = height;
	delete[] copy;
}

void Texture::bind() const
{
	glCall(glBindTexture(GL_TEXTURE_2D, ID));
}

void Texture::bindToUnit(int index) const
{
	glActiveTexture(GL_TEXTURE0 + index);
	bind();
	glActiveTexture(GL_TEXTURE0);
}

void Texture::setWrap(const TexWrap mode){
	glCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, (int)mode));
	glCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, (int)mode));
	wrap[0] = wrap[1] = mode;
}

void Texture::setWrap(TexWrapAxis wrapAxis, TexWrap mode)
{
	glCall(glTexParameteri(GL_TEXTURE_2D, GLenum(wrapAxis), (int)mode));
	if (wrapAxis == TexWrapAxis::T)
		wrap[1] = mode;
	else if (wrapAxis == TexWrapAxis::S)
		wrap[0] = mode;
}

void Texture::setFilter(const TexFilter mode)
{
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, (int)mode);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, (int)mode);
	minFilter = magFilter = mode;
}

void Texture::setFilter(const TexFilterOperation operation, const TexFilter mode)
{
	glTexParameteri(GL_TEXTURE_2D, GLenum(operation), (int)mode);
	if (operation == TexFilterOperation::Minimize)
		minFilter = mode;
	else if (operation == TexFilterOperation::Magnify)
		magFilter = mode;
}

TexFilter Texture::getFilter(const TexFilterOperation operation) const
{
	return (TexFilterOperation::Minimize == operation) ? minFilter : (TexFilterOperation::Magnify == operation) ? magFilter : (TexFilter)-1;
}

TexWrap Texture::getWrap(const TexWrapAxis wrapAxis) const
{
	return (TexWrapAxis::T == wrapAxis) ? wrap[1] : (TexWrapAxis::S == wrapAxis) ? wrap[0] : (TexWrap)-1;
}

uint Texture::getID() const
{
	return ID;
}

TexFormat Texture::getFormat() const
{
	return format;
}

namespace {
	void copySettings(Texture& dest, const Texture& from) {
		dest.setFilter(TexFilterOperation::Minimize, from.getFilter(TexFilterOperation::Minimize));
		dest.setFilter(TexFilterOperation::Magnify, from.getFilter(TexFilterOperation::Magnify));
		dest.setWrap(TexWrapAxis::T, from.getWrap(TexWrapAxis::T));
		dest.setWrap(TexWrapAxis::S, from.getWrap(TexWrapAxis::S));
	}
}

int Texture::getWidth() const
{
	return width;
}

int Texture::getHeight() const
{
	return height;
}

std::vector<byte> Texture::getData() const
{
	if (uint bpp = bytesPerPixel(format)) {
		bind();
		std::vector<byte> data(calcImageSize(width, height, bpp), 0);
		glGetTexImage(GL_TEXTURE_2D, 0, GLenum(format), GL_UNSIGNED_BYTE, &data[0]);
		flipImageX(data.data(), width, height, bpp);
		return data;
	}
	return std::vector<byte>();
}

Texture::~Texture()
{
	glCall(glDeleteTextures(1,&ID));
}