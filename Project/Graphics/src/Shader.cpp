#include <Shader.h>
#include <glad/glad.h>
#include <Error.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <exception>

namespace {
	void compileShader(uint ID, const std::string& code)
	{
		const char* c_str = code.c_str();
		glCall(glShaderSource(ID, 1, &c_str, nullptr));
		glCall(glCompileShader(ID));

		int success;
		glCall(glGetShaderiv(ID, GL_COMPILE_STATUS, &success));
		if (!success) {
			char infoLog[512];
			glCall(glGetShaderInfoLog(ID, 512, nullptr, infoLog));
			std::cerr << "Error compiling Shader " << code << ":\n" << infoLog;

		}
	}

	void link(uint ID)
	{
		glCall(glLinkProgram(ID));
		int success;
		glCall(glGetProgramiv(ID, GL_LINK_STATUS, &success));
		if (!success) {
			char infoLog[512];
			glCall(glGetProgramInfoLog(ID, 512, nullptr, infoLog));
			std::cerr << "Shader Program " << std::to_string(ID) << " could not be linked" << infoLog;

		}
	}
}

Shader::Shader()
{
	ID = glCreateProgram();
}

Shader::Shader(const std::string & directory) : Shader() 
{
	std::ifstream ifs(directory);
	if (ifs.fail()) {
		throw std::runtime_error("Error: " + directory + " could not be read");
	}
	std::string fileContent((std::istreambuf_iterator<char>(ifs)), (std::istreambuf_iterator<char>()));
	compile(fileContent);
}

Shader::~Shader()
{
	glCall(glDeleteProgram(ID));
}

Shader& Shader::compile(const std::string& code) {
	std::istringstream input(code);
	std::stringstream vsSrc, gsSrc, fsSrc, ignore;
	std::stringstream* stream = &ignore;

	for (std::string line; getline(input, line);) {
		if (line.find("#VERTEX") != std::string::npos)
			stream = &vsSrc;
		else if (line.find("#GEOMETRY") != std::string::npos)
			stream = &gsSrc;
		else if (line.find("#FRAGMENT") != std::string::npos)
			stream = &fsSrc;
		else
			*stream << line << std::endl;
	}

	uint _vs = glCreateShader(GL_VERTEX_SHADER);
	uint _fs = glCreateShader(GL_FRAGMENT_SHADER);
	compileShader(_vs, vsSrc.str());
	compileShader(_fs, fsSrc.str());
	glCall(glAttachShader(ID, _vs));
	glCall(glAttachShader(ID, _fs));

	std::string gsStr = gsSrc.str();
	if (gsStr.empty()) {
		link(ID);
	}
	else {
		uint _gs = glCreateShader(GL_GEOMETRY_SHADER);
		compileShader(_gs, gsSrc.str());
		glCall(glAttachShader(ID, _gs));
		link(ID);
		glCall(glDeleteShader(_gs));
	}
	glCall(glDeleteShader(_vs));
	glCall(glDeleteShader(_fs));
	return *this;
}

void Shader::use()
{
	glCall(glUseProgram(getID()));
}

void Shader::setMatrix4(const std::string & name, const float * matrix, size_t count)
{
	glCall(glUniformMatrix4fv(glGetUniformLocation(ID, name.c_str()), (GLsizei)count, GL_FALSE, matrix));
}

void Shader::setMatrix3(const std::string & name, const float * matrix, size_t count)
{
	glCall(glUniformMatrix3fv(glGetUniformLocation(ID, name.c_str()), (GLsizei)count, GL_FALSE, matrix));
}

void Shader::setMatrix2(const std::string & name, const float * matrix, size_t count)
{
	glCall(glUniformMatrix2fv(glGetUniformLocation(ID, name.c_str()), (GLsizei)count, GL_FALSE, matrix));
}

void Shader::setFloat4(const std::string & name, float v1, float v2, float v3, float v4)
{
	glCall(glUniform4f(glGetUniformLocation(ID, name.c_str()), v1,v2,v3,v4));
}

void Shader::setFloat3(const std::string & name, float v1, float v2, float v3)
{
	glCall(glUniform3f(glGetUniformLocation(ID, name.c_str()), v1, v2, v3));
}

void Shader::setFloat2(const std::string & name, float v1, float v2)
{
	glCall(glUniform2f(glGetUniformLocation(ID, name.c_str()), v1, v2));
}

void Shader::setFloat(const std::string & name, float value)
{
	glCall(glUniform1f(glGetUniformLocation(ID, name.c_str()), value));
}

void Shader::setFloat3(const std::string& name, const float* values, size_t count)
{
	glCall(glUniform3fv(glGetUniformLocation(ID, name.c_str()), (GLsizei)count, values));
}

void Shader::setFloat4(const std::string& name, const float* values, size_t count)
{
	glCall(glUniform4fv(glGetUniformLocation(ID, name.c_str()), (GLsizei)count, values));
}

void Shader::setInt(const std::string& name, const int* values, size_t count)
{
	glCall(glUniform1iv(glGetUniformLocation(ID, name.c_str()), (GLsizei)count, values));
}

void Shader::setInt2(const std::string& name, const int* values, size_t count)
{
	glCall(glUniform2iv(glGetUniformLocation(ID, name.c_str()), (GLsizei)count, values));
}

void Shader::setInt3(const std::string& name, const int* values, size_t count)
{
	glCall(glUniform3iv(glGetUniformLocation(ID, name.c_str()), (GLsizei)count, values));
}

void Shader::setInt4(const std::string& name, const int* values, size_t count)
{
	glCall(glUniform4iv(glGetUniformLocation(ID, name.c_str()), (GLsizei)count, values));
}

void Shader::setMatrix4(const std::string& name, const Matrix4& matrix)
{
	setMatrix4(name, matrix.data);
}

void Shader::setMatrix3(const std::string& name, const Matrix3& matrix)
{
	setMatrix3(name, matrix.data);
}

void Shader::setMatrix2(const std::string& name, const Matrix2& matrix)
{
	setMatrix2(name, matrix.data);
}

void Shader::setFloat4(const std::string& name, const Vector4& vector)
{
	setFloat4(name, vector.x, vector.y, vector.z, vector.w);
}

void Shader::setFloat3(const std::string& name, const Vector3& vector)
{
	setFloat3(name, vector.x, vector.y, vector.z);
}

void Shader::setFloat2(const std::string& name, const Vector2& vector)
{
	setFloat2(name, vector.x, vector.y);
}

void Shader::setUniformBlockBinding(const std::string& name, uint index)
{
	glCall(glUniformBlockBinding(ID, glGetUniformBlockIndex(ID, name.c_str()), index));
}

void Shader::setInt(const std::string & name, int value)
{
	glCall(glUniform1i(glGetUniformLocation(ID, name.c_str()), value));
}

void Shader::setInt2(const std::string & name, int v1, int v2)
{
	glCall(glUniform2i(glGetUniformLocation(ID, name.c_str()), v1, v2));
}

void Shader::setInt3(const std::string & name, int v1, int v2, int v3)
{
	glCall(glUniform3i(glGetUniformLocation(ID, name.c_str()), v1, v2, v3));
}

void Shader::setInt4(const std::string & name, int v1, int v2, int v3, int v4)
{
	glCall(glUniform4i(glGetUniformLocation(ID, name.c_str()), v1, v2, v3, v4));
}

void Shader::setTex(const std::string & name, const Texture* texture, int unit)
{
	glCall(glUseProgram(getID()));
	glCall(glUniform1i(glGetUniformLocation(ID, name.c_str()), unit));
	texture->bindToUnit(unit);
}

void Shader::setCubeMap(const std::string& name, const CubeMap* cubeMap, int unit)
{
	glCall(glActiveTexture(GL_TEXTURE0 + unit));
	glCall(glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMap->getID()));
	glCall(glUniform1i(glGetUniformLocation(ID, name.c_str()), unit));
	glActiveTexture(GL_TEXTURE0);
}

void Shader::setBool(const std::string & name, bool value)
{
	glCall(glUniform1i(glGetUniformLocation(ID, name.c_str()), value));
}

void Shader::setFloat(const std::string& name, const float* values, size_t count)
{
	glCall(glUniform1fv(glGetUniformLocation(ID, name.c_str()), (GLsizei)count, values));
}

void Shader::setFloat2(const std::string& name, const float* values, size_t count)
{
	glCall(glUniform2fv(glGetUniformLocation(ID, name.c_str()), (GLsizei)count, values));
}
