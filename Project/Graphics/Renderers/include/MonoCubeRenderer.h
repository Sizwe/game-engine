#pragma once
#include <Renderer.h>
#include <MonoCube.h>

class RenderableMonoCube : public Renderable, public MonoCube {
public:
	std::vector<RenderSession> sessions;

	RenderableMonoCube(const MonoCube& cube, const GameObject* gameObject, RenderSession::Binding* binding);
	~RenderableMonoCube();

	void setPaused(bool pause) override;
};

class MonoCubeRenderer {
private:
	static Texture* Base;
	static std::set<RenderableMonoCube*> RCubes;

	static void AllocSprites(RenderableMonoCube* monoCube);
	static void AllocStripes(RenderableMonoCube* monoCube);

public:
	static void Insert(RenderableMonoCube* monoCube);
	static void Erase(RenderableMonoCube* monoCube);
	static size_t DrawCount();
	static void Clear();
};