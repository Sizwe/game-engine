#pragma once
#include <Renderer.h>
#include <MonoSprite.h>

class RenderableMonoSprite : public Renderable, public MonoSprite {
public:
	std::vector<RenderSession> sessions;

	RenderableMonoSprite(const MonoSprite& sprite, const GameObject* gameObject, RenderSession::Binding* binding);
	~RenderableMonoSprite();

	void setPaused(bool pause) override;
};

class MonoSpriteRenderer {
private:
	static Texture* Base;
	static std::set<RenderableMonoSprite*> RSprites;

	static void AllocSprite(RenderableMonoSprite* rMonoSprite);
	static void AllocStripes(RenderableMonoSprite* rMonoSprite);

public:
	static void Insert(RenderableMonoSprite* rMonoSprite);
	static void Erase(RenderableMonoSprite* rMonoSprite);
	static size_t DrawCount();
	static void Clear();

};