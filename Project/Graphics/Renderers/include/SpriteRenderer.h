#pragma once
#include <Atlas.h>
#include <Renderer.h>
#include <DataBuffer.h>
#include <VertexArray.h>
#include <RenderContext.h>

class RenderableSprite : public Renderable, public Sprite {
public:
	Matrix4 tMatrix;
	bool translucent;

	RenderableSprite(const Sprite& sprite, const GameObject* gameObject, RenderSession::Binding* binding);
	~RenderableSprite() override;
};

class SpriteRenderer {
public:
	static void Insert(RenderableSprite* renderable);
	static void Erase(RenderableSprite* renderable);
	static size_t DrawCount();
	static void Clear();
	static void Update(const LocationContext& context);
	static void RenderDirShadow(const LocationContext& context);
	static void RenderPointShadows(const PointShadowContext& context);
	static void Render(const MaterialContext& context);

private:
	struct Vertex {
		struct Rect {
			Vector2 min;
			Vector2 max;
		};

		struct DynamicSeg {
			Matrix4 model;
		} dynamicSeg;

		struct StaticSeg {
			Rect worldRect;
			Rect texRect;
			Vector4 colour;
			int vFlags;
			Vector3 distortion;
			Vector3 ambient;
			Vector3 diffuse;
			Vector3 specular;
			float shininess;
		} staticSeg;
	};

	enum class StateType {
		OpaqueStatic,
		OpaqueDynamic,
		Translucent
	};

	class RenderState {
	public:
		Atlas atlas;
		Texture* atlasTex;
		VertexArray* vertexArray = nullptr;
		VertexBuffer<StaticDraw>* staticVertBuff = nullptr;
		VertexBuffer<DynamicDraw>* dynamicVertBuff = nullptr;
		bool resetVertices = false;
		size_t vertexCount = 0;

		RenderState();
		virtual ~RenderState();

		virtual void update(const LocationContext& context) = 0;
		virtual void render(Shader* shader) = 0;
	};

	class OpaqueRenderState : public RenderState {
	public:
		Drawable::Lifetime lifetime;

		OpaqueRenderState(Drawable::Lifetime lifetime);

		void update(const LocationContext& context) override;
		void render(Shader* shader) override;
	};

	class TranslucentRenderState : public RenderState {
	public:
		ElementBuffer<DynamicDraw>* elementBuffer = nullptr;

		TranslucentRenderState();
		~TranslucentRenderState() override;

		void update(const LocationContext& context) override;
		void render(Shader* shader) override;
	};

	struct _RenderData {
		std::set<RenderState*> renderStates;
		std::map<Texture*, std::pair<RenderState*, std::set<RenderableSprite*>> > texMap;
	};
	static std::array<_RenderData, 3> RenderData;
};
