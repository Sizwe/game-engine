#pragma once
#include <Text.h>
#include <Atlas.h>
#include <FrameBuffer.h>
#include <DataBuffer.h>
#include <VertexArray.h>
#include <RenderContext.h>
#include <ft2build.h>
#include <freetype/freetype.h>
#include <set>
#include <map>
#include <deque>

class TextTexturer {
private:
	struct Vertex {
		Vector2 wPos;
		Vector2 tPos;
	};

	struct Char {
		Texture* tex = nullptr;
		int left = 0; // Offset from baseline to left of glyph
		int top = 0; // Offset from baseline to top of glyph
		uint advance = 0; // Offset to advance to next glyph
	};

	struct Word : public std::deque<Char> {
		int width = 0;
		int offset = 0;
	};

	struct Line : public std::deque<Word> {
		int width = 0;
	};

	struct Passage : std::deque<Line> {
		int width = 0;
		int height = 0;
		size_t charCount = 0;

		Line* append(const Line& line);
		Word* append(const Word& word);
	};

	class RenderState {
	public:
		std::map<wchar_t, Char> charMap;
		int lineSpacing = 0;
		int spaceWidth = 0;

		Atlas atlas;
		Texture* atlasTex = nullptr;
		VertexArray* vertexArray = nullptr;
		VertexBuffer<DynamicDraw>* vertexBuffer = nullptr;
		ElementBuffer<DynamicDraw>* elementBuffer = nullptr;
		FrameBuffer* frameBuffer = nullptr;
		FT_Library ft = nullptr;
		FT_Face face = nullptr;

		RenderState(const Font* font, uint size);
		~RenderState();

		bool validateTex();

		Char genChar(wchar_t c);
		Char getChar(wchar_t c);
		void truncate(Passage& passage);
		void parse(const Text& text, Passage& passage);

		Texture* genTexture(const Text& text);
	};

private:
	static std::map<std::pair<const Font*, uint>, RenderState*> FontMap;
	static std::set<RenderState*> States;

public:
	static Texture* Create(const Text& text);
	static void Clear();
};