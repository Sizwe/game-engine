#pragma once
#include <Renderer.h>
#include <array>
#include <map>

class RenderableCubeSprite : public Renderable {
public:
	std::array<RenderSession, 6> sessions;
	CubeSprite cubeSprite;

	RenderableCubeSprite(const CubeSprite& cubeSprite, const GameObject* gameObject, RenderSession::Binding* binding);
	~RenderableCubeSprite();

	void setPaused(bool pause) override;
};

class CubeSpriteRenderer {
private:
	struct CubeMapData {
		std::array<Texture*, 6> textures;
		std::set<RenderableCubeSprite*> sprites;
	};
	static std::map<CubeMap*, CubeMapData*> TexMap;

public:
	static void Insert(RenderableCubeSprite* renderable);
	static void Erase(RenderableCubeSprite* renderable);
	static size_t DrawCount();
	static void Clear();
};
