#pragma once
#include <Atlas.h>
#include <Renderer.h>
#include <DataBuffer.h>
#include <VertexArray.h>
#include <RenderContext.h>
#include <Shader.h>

class RenderableStripe : public Renderable, public Stripe {
public:
	RenderableStripe(const Stripe& stripe, const GameObject* gameObject, RenderSession::Binding* binding);
	~RenderableStripe() override;
};

class StripeRenderer {
private:
	class RenderState;
	static std::map<RenderableStripe*, RenderState*> StripeMap;
	static std::set<RenderState*> States;

public:
	static void Insert(RenderableStripe* renderable);
	static void Erase(RenderableStripe* renderable);
	static size_t DrawCount();
	static void Clear();
	static void Render(const LocationContext& context);

private:
	struct Vertex {
		struct DynamicSeg {
			Matrix4 model;
		} dynamicSeg;

		struct StaticSeg {
			Vector3 start;
			Vector3 end;
			Vector4 colour;
		} staticSeg;
	};

	class RenderState {
	public:
		Drawable::Lifetime lifetime;
		std::set<RenderableStripe*> stripes;
		VertexArray* vertexArray = nullptr;
		VertexBuffer<StaticDraw>* staticVertBuff = nullptr;
		VertexBuffer<DynamicDraw>* dynamicVertBuff = nullptr;
		bool resetVertices = false;
		size_t vertexCount = 0;

		RenderState(Drawable::Lifetime lifetime);
		~RenderState();

		void render(const LocationContext& context);
	};
};
