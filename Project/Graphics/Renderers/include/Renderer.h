#pragma once
#include <functional>
#include <GameObject.h>
#include <IVector2.h>
#include <Sprite.h>
#include <Stripe.h>
#include <CubeSprite.h>
#include <MonoSprite.h>
#include <MonoCube.h>
#include <SDL_messagebox.h>
#undef near
#undef far

class Renderable; /* forward decleration */

class RenderSession {
public:
	friend class Renderer;

	struct Binding {
		Renderable* renderable = nullptr;
		size_t refCount = 0;
	};

private:
	Binding* binding;

public:
	void setPaused(bool paused);
	bool paused() const;

	void close();
	bool closed() const;

	RenderSession(RenderSession::Binding* binding = nullptr);
	RenderSession(const RenderSession& renderSession);
	RenderSession& operator=(const RenderSession& renderSession);
	~RenderSession();
};

class Renderable {
private:
	RenderSession::Binding* binding;
	bool _paused;

public:
	const GameObject* const gameObject;

	Renderable(RenderSession::Binding* binding, const GameObject* gameObject);
	virtual ~Renderable();

	virtual void setPaused(bool pause);
	virtual bool paused() const;
};

class Renderer {
private:
	friend class Engine;
	static void RenderToScreen();

public:
	enum class ProjectionType {
		Orthographic,
		Perspective
	};

	struct Background {
		Vector4 colour;

		Background(const Vector4& colour = Vector4(0.1f, 0.1f, 0.1f, 1.0f)) : colour(colour) {}
	};

	struct ProjectionSettings {
		ProjectionType type;
		float near;
		float far;
		float fov;

		ProjectionSettings(float near, float far);
		ProjectionSettings(float near, float far, float fov);
	};

	struct DirectionalShadows {
		UVector2 resolution;
		Vector3 span;
		DirectionalShadows(const UVector2& resolution = UVector2(1000), const Vector3& span = Vector3(2500.0f));
	};

	struct PointShadows {
		UVector2 resolution;
		float span;
		PointShadows(const UVector2& resolution = UVector2(1000), float span = 2000.0f);
	};

	struct Fog {
		bool enabled;
		Vector4 colour;
		float startDepth;
		float endDepth;

		Fog();
		Fog(float startDepth, float endDepth, const Vector4& colour = Vector4(0.75f));
	};

	struct DepthOfField {
		bool enabled;
		Vector3 focalPoint;
		float blurDist;
		float blurSize;
		float blurQuality;

		DepthOfField();
		DepthOfField(const Vector3& focalPoint, float blurDist, float blurSize = 1.0f, float blurQuality = 0.5f);
	};

	struct Filters {
		bool enabled;
		Vector4 colourise;
		float brightness;
		float saturation;
		float vibrance;
		float contrast;
		float hue;

		Filters();
		Filters(float brightness, float contrast = 1.0f, float hue = 0.0f, float vibrance = 1.0f, float saturation = 1.0f, const Vector4& colourise = Vector4());
	};

public:
	static GameObject* Camera;
	static Background background;
	static ProjectionSettings projection;
	static DirectionalShadows dirShadows;
	static PointShadows pointShadows;
	static DepthOfField depthOfField;
	static Filters filters;
	static Fog fog;

	static RenderSession Draw(const Sprite& sprite, const GameObject* gameObject = nullptr);
	static RenderSession Draw(const Stripe& stripe, const GameObject* gameObject = nullptr);
	static RenderSession Draw(const CubeSprite& cube, const GameObject* gameObject = nullptr);
	static RenderSession Draw(const MonoSprite& monoSprite, const GameObject* gameObject = nullptr);
	static RenderSession Draw(const MonoCube& monoCube, const GameObject* gameObject = nullptr);

	static size_t ObjectCount();

	static Texture* Render(const UVec2& resolution);
	static void Reset();
};