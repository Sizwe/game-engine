#pragma once

#include <FrameBuffer.h>
#include <Sprite.h>
#include <IVector2.h>
#include <vector>
#include <map>

class Atlas {
private:
	UVector2 _res;
	std::map<Texture*, Rect> textures;
	FrameBuffer* frameBuffer;

	void rearrange();
	bool validate();

public:
	class texture_iterator : public std::map<Texture*, Rect>::iterator {
	public:
		texture_iterator() = default;
		texture_iterator(std::map<Texture*, Rect>::iterator it) : std::map<Texture*, Rect>::iterator(it) { };

		Texture** operator -> () { return (Texture** const) & (std::map<Texture*, Rect>::iterator::operator -> ()->first); }
		Texture* operator * () { return std::map<Texture*, Rect>::iterator::operator * ().first; }
	};

	class const_texture_iterator : public std::map<Texture*, Rect>::const_iterator {
	public:
		const_texture_iterator() = default;
		const_texture_iterator(std::map<Texture*, Rect>::const_iterator it) : std::map<Texture*, Rect>::const_iterator(it) { };

		const Texture** operator -> () { return (const Texture** const) & (std::map<Texture*, Rect>::const_iterator::operator -> ()->first); }
		const Texture* operator * () { return std::map<Texture*, Rect>::const_iterator::operator * ().first; }
	};

public:
	Atlas();
	Atlas(const std::initializer_list<Texture*> textures);
	~Atlas();

	void insert(Texture* texture);
	void erase(const Texture* texture);
	bool exists(const Texture* texture) const;
	size_t size() const;
	UVec2 resolution();
	void clear();

	Texture* create(TexFormat format = TexFormat::RGBA);
	Rect getCrop(const Texture* texture);
	Rect getCrop(const Texture* texture, const Rect& subcrop);

	texture_iterator begin() { return textures.begin(); };
	texture_iterator end() { return textures.end(); };
	const_texture_iterator cbegin() const { return textures.cbegin(); };
	const_texture_iterator cend() const { return textures.cend(); };
};
