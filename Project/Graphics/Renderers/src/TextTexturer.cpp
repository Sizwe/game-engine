#include <TextTexturer.h>
#include <freetype/freetype.h>
#include <Resources.h>
#include <iostream>
#include <GraphicsUtils.h>

std::map<std::pair<const Font*, uint>, TextTexturer::RenderState*> TextTexturer::TextTexturer::FontMap;
std::set<TextTexturer::RenderState*> TextTexturer::TextTexturer::States;

namespace {
	byte* genRGBA(FT_Bitmap bm) {
		if (!bm.width || !bm.rows)
			return nullptr;

		byte* rgba = new byte[bm.width * bm.rows * 4];
		for (byte y = 0; y < bm.rows; ++y) {
			for (uint x = 0; x < bm.width; ++x) {
				byte val = bm.buffer[(bm.pitch * y) + x];
				size_t base = ((bm.width * y) + x) * 4;
				rgba[base + 0] = 0xFF;
				rgba[base + 1] = 0xFF;
				rgba[base + 2] = 0xFF;
				rgba[base + 3] = val;
			}
		}
		return rgba;
	}

	inline int alignUp(int x, int m) {
		return x + m - (x % m);
	}

	template<typename T>
	void updateBuffer(T* buff, size_t size, void* data) {
		buff->bind();
		if (buff->getSize() < size)
			buff->setData(size, data);
		else
			buff->setData(size, data);
	}
}

Texture* TextTexturer::TextTexturer::Create(const Text& text)
{
	auto fmIt = FontMap.find({ text.font, text.size });
	if (fmIt != FontMap.end()) {
		return fmIt->second->genTexture(text);
	}
	else {
		RenderState* state = new RenderState(text.font, text.size);
		States.insert(state);
		FontMap[{ text.font, text.size }] = state;
		return state->genTexture(text);
	}
}

void TextTexturer::TextTexturer::Clear()
{
	for (auto& s : States) {
		delete s;
	}
	FontMap.clear();
	States.clear();
}

TextTexturer::Char TextTexturer::RenderState::genChar(wchar_t c)
{
	Char chr;
	if (FT_Load_Char(face, c, FT_LOAD_RENDER)) {
		std::wcerr << L"FreeType Error: Failed to load char: " << c << std::endl;
		return chr;
	}
	byte* imgData = face->glyph->bitmap.buffer;
	int width = face->glyph->bitmap.width;
	int height = face->glyph->bitmap.rows;
	chr.left = face->glyph->bitmap_left;
	chr.top = face->glyph->bitmap_top;
	chr.advance = face->glyph->advance.x >> 6;
	byte* rgba = genRGBA(face->glyph->bitmap);
	chr.tex = new Texture(rgba, width, height, TexFormat::RGBA);
	delete[] rgba;
	chr.tex->bind();
	chr.tex->setFilter(TexFilter::Linear);
	chr.tex->setWrap(TexWrap::ClampToBorder);
	charMap[c] = chr;
	atlas.insert(chr.tex);
	atlasTex = nullptr;
	return chr;
}

TextTexturer::Char TextTexturer::RenderState::getChar(wchar_t c) {
	auto mapIt = charMap.find(c);
	if (mapIt != charMap.end()) {
		return mapIt->second;
	}
	return Char();
}

void TextTexturer::RenderState::truncate(Passage& passage)
{
	while (passage.size() > 0) {
		Line& line = passage.back();
		if (line.size() == 1) {
			if (line.back().size() == 0)
				passage.pop_back();
			else break;
		}
		else if (line.size() == 0)
			passage.pop_back();
		else break;
	}
}

TextTexturer::Line* TextTexturer::Passage::append(const Line& line) {
	push_back(line);
	return &back();
}

TextTexturer::Word* TextTexturer::Passage::append(const Word& word) {
	Line& line = back();
	line.push_back(word);
	return &line.back();
}

void TextTexturer::RenderState::parse(const Text& text, Passage& passage)
{
	Line* line = passage.append(Line());
	Word* word = passage.append(Word());

	float maxWidth = text.rect.width();
	float maxHeight = text.rect.height();

	for (auto it = text.string.begin(); it != text.string.end(); ++it) {
		if (iswspace(*it)) {
			if (*it == L'\n') {
				passage.width = std::max(passage.width, line->width);
				if ((passage.size() + 1) * lineSpacing > maxHeight)
					return;
				line = passage.append(Line());
				word = passage.append(Word());
			}
			else {
				word = passage.append(Word());
				if (*it == L'\t') {
					word->offset = alignUp(line->width, spaceWidth * 4) - line->width;
					line->width += word->offset;
				}
				else {
					word->offset += spaceWidth;
					line->width += spaceWidth;
				}
			}
			continue;
		}

		Char chr = getChar(*it);
		if (!chr.tex) {
			chr = genChar(*it);
			if (!chr.tex)
				continue;
		}
		if (line->width + chr.advance > maxWidth) {
			if ((passage.size() + 1) * lineSpacing > maxHeight) {
				passage.width = std::max(passage.width, line->width);
				return;
			}
			if (line->size() == 1 && word->size() > 0) {
				passage.width = std::max(passage.width, line->width);
				line = passage.append(Line());
				word = passage.append(Word());
			}
			else if (line->size() > 1) {
				Word oldWord = *word;
				line->pop_back();
				line->width -= oldWord.width + oldWord.offset;
				passage.width = std::max(passage.width, line->width);
				line = passage.append(Line());
				word = passage.append(oldWord);
				word->offset = 0;
			}
		}
		word->push_back(chr);
		word->width += chr.advance;
		line->width += chr.advance;
		passage.charCount++;
	}
	passage.width = std::max(passage.width, line->width);
	truncate(passage);
	passage.height = (int)passage.size() * lineSpacing;
}


bool TextTexturer::RenderState::validateTex() {
	if (!atlasTex) {
		atlasTex = atlas.create();
	}
	return atlasTex;
}

Texture* TextTexturer::RenderState::genTexture(const Text& text)
{
	Passage passage;
	parse(text, passage);
	if (!validateTex() || passage.width <= 0 || passage.height <= 0) {
		return nullptr;;
	}
	frameBuffer->bind();
	frameBuffer->insertColourBuff(new Texture(passage.width, passage.height));
	glClearColor(text.colour.x, text.colour.y, text.colour.z, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	glViewport(0, 0, passage.width, passage.height);

	Shader* shader = Resources::Shaders.get("assets/renderText.shader");
	shader->setTex("tex", atlasTex, 1);

	Vector2 afres = Vector2((float)atlasTex->getWidth(), (float)atlasTex->getHeight());
	Vector2 pfres = Vector2((float)passage.width, (float)passage.height);

	Vertex* vertices = new Vertex[passage.charCount * 4];
	size_t vc = 0;

	int x = 0, y = passage.height;
	float alignMult = 0.0f;
	if (text.align == Text::Align::Right)
		alignMult = 1.0f;
	else if (text.align == Text::Align::Centre)
		alignMult = 0.5f;

	for (Line& line : passage) {
		x = int((passage.width - line.width) * alignMult);
		y -= lineSpacing;

		for (Word& word : line) {
			x += word.offset;

			for (Char& tChar : word) {
				float w = float(tChar.tex->getWidth());
				float h = float(tChar.tex->getHeight());
				float xpos = float(x + tChar.left);
				float ypos = float(y - (tChar.tex->getHeight() - tChar.top));
				Vector2 min = Vector2(xpos, ypos);
				Vector2 max = min + Vector2(w, h);

				Vector2 ptMin = Vec2((min.x / pfres.x) * 2.0f - 1.0f, (min.y / pfres.y) * 2.0f - 1.0f);
				Vector2 ptMax = Vec2((max.x / pfres.x) * 2.0f - 1.0f, (max.y / pfres.y) * 2.0f - 1.0f);

				Rect crop = atlas.getCrop(tChar.tex);
				Vector2 atMin = Vector2(crop.min().x / afres.x, crop.min().y / afres.y);
				Vector2 atMax = Vector2(crop.max().x / afres.x, crop.max().y / afres.y);

				vertices[vc + 0] = { {ptMin.x, ptMin.y}, {atMin.x, atMin.y} };
				vertices[vc + 1] = { {ptMax.x, ptMin.y}, {atMax.x, atMin.y} };
				vertices[vc + 2] = { {ptMax.x, ptMax.y}, {atMax.x, atMax.y} };
				vertices[vc + 3] = { {ptMin.x, ptMax.y}, {atMin.x, atMax.y} };

				x += tChar.advance;
				vc += 4;
			}
		}
	}
	vertexArray->bind();
	updateBuffer(vertexBuffer, passage.charCount * 4 * sizeof(Vertex), vertices);
	updateBuffer(elementBuffer, passage.charCount * sizeof(uint) * 6, getQuadIndices(passage.charCount));
	delete[] vertices;

	glCall(glDrawElements(GL_TRIANGLES, (GLsizei)(passage.charCount * 6), GL_UNSIGNED_INT, 0));

	return frameBuffer->ejectColourBuff();
}

TextTexturer::RenderState::RenderState(const Font* font, uint size)
{
	if (FT_Init_FreeType(&ft)) {
		std::cerr << "FreeType Error: Could not init FreeType Library" << std::endl;
		return;
	}
	if (FT_New_Memory_Face(ft, font->getData().data(), (FT_Long)font->getData().size(), 0, &face)) {
		std::cerr << "FreeType Error: Failed to load font" << std::endl;
		FT_Done_FreeType(ft);
		return;
	}
	if (FT_Select_Charmap(face, ft_encoding_unicode)) {
		std::cerr << "FreeType Error: Failed to initialise unicode encoding" << std::endl;
		FT_Done_FreeType(ft);
		return;
	}
	if (FT_Set_Pixel_Sizes(face, 0, size)) {
		std::cerr << "FreeType Error: Failed to set font size to " << size << std::endl;
		FT_Done_FreeType(ft);
		return;
	}

	lineSpacing = face->size->metrics.height >> 6;

	if (FT_Load_Char(face, ' ', FT_LOAD_RENDER)) {
		std::cerr << "FreeType Error: Failed to load space width" << std::endl;
		spaceWidth = 32;
	}
	else {
		spaceWidth = face->glyph->advance.x >> 6;
	}

	atlasTex = nullptr;
	vertexArray = new VertexArray();
	vertexArray->bind();

	vertexBuffer = new VertexBuffer<DynamicDraw>();
	vertexBuffer->bind();
	vertexArray->setConfig(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0 * sizeof(float));
	vertexArray->setConfig(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 2 * sizeof(float));

	elementBuffer = new ElementBuffer<DynamicDraw>();
	elementBuffer->bind();

	frameBuffer = new FrameBuffer();
}

TextTexturer::RenderState::~RenderState() {
	delete atlasTex;
	delete vertexBuffer;
	delete elementBuffer;
	delete vertexArray;
	delete frameBuffer;
	FT_Done_Face(face);
	FT_Done_FreeType(ft);
	for (auto& c : charMap) {
		delete c.second.tex;
	}
}