#include <Atlas.h>
#include <Error.h>
#include <FrameBuffer.h>
#include <Shader.h>
#include <VertexArray.h>
#include <DataBuffer.h>
#include <Resources.h>
#include <iostream>
#include <GraphicsUtils.h>

namespace {
#pragma pack(push, 1)
	struct Vertex {
		Vector2 to;
		Vector2 from;
		int texIndex;
	};
#pragma pack(pop)

	inline uint nextPowTwo(uint n){
		--n;
		for(uint i = 0; i < 5; ++i)
			n |= n >> (1 << i);
		return n + 1;
	}
}

//https://observablehq.com/@mourner/simple-rectangle-packing
void Atlas::rearrange()
{
	_res = UVector2();

	std::vector<Texture*> tvec;
	int area = 0, maxWidth = 0;

	for (auto& val : textures) {
		tvec.emplace_back(val.first);
		area += val.first->getWidth() * val.first->getHeight();
		maxWidth = std::max(maxWidth, val.first->getWidth());
	}

	std::sort(tvec.begin(), tvec.end(), [](const Texture* a, const Texture* b) -> bool {
		return a->getHeight() > b->getHeight();
	});

	const int startWidth = nextPowTwo((int)fmaxf(ceilf(sqrtf(area / 0.95f)), (float)maxWidth));
	std::vector<Rect> spaces = { Rect(0.0f, (float)startWidth, 0.0f, FLT_MAX) };

	for (const auto tex : tvec) {
		for(int s = (int)spaces.size() - 1; s >= 0; --s) {
			if (tex->getWidth() <= spaces[s].width() && tex->getHeight() <= spaces[s].height()) {

				textures[tex] = Rect(spaces[s].min().x, spaces[s].min().x + tex->getWidth(), spaces[s].min().y, spaces[s].min().y + tex->getHeight());
				if (textures[tex].max().x > _res.x)
					_res.x = (uint)textures[tex].max().x;
				if (textures[tex].max().y > _res.y)
					_res.y = (uint)textures[tex].max().y;

				if (tex->getWidth() == spaces[s].width() && tex->getHeight() == spaces[s].height()) {
					spaces.erase(spaces.begin() + s);
				}
				else if (tex->getHeight() == spaces[s].height()) {
					spaces[s] = Rect(spaces[s].min() + Vec2((float)tex->getWidth(), 0), spaces[s].max());
				}
				else if (tex->getWidth() == spaces[s].width()) {
					spaces[s] = Rect(spaces[s].min() + Vec2(0, (float)tex->getHeight()), spaces[s].max());
				}
				else {
					spaces.push_back(Rect(spaces[s].min() + Vec2(0, (float)tex->getHeight()), spaces[s].max()));
					spaces[s] = Rect(spaces[s].min() + Vec2((float)tex->getWidth(), 0), Vec2(spaces[s].max().x, spaces[s].min().y + tex->getHeight()));
				}
				std::sort(spaces.begin(), spaces.end(), [](const Rect& a, const Rect& b) -> bool {
					return (double)a.width() * (double)a.height() > (double)b.width() * (double)b.height();
				});
				break;
			}
		}
	}
	_res = UVector2(nextPowTwo(_res.x), nextPowTwo(_res.y));
}

bool Atlas::validate()
{
	if (_res == UVec2(0,0)) {
		rearrange();
		if (_res == UVec2(0, 0))
			return false;
	}
	return true;
}


Texture* Atlas::create(TexFormat format)
{
	if (!validate())
		return nullptr;

	GLint viewport[4], oldfbo, maxImageUnits = 16;
	glGetIntegerv(GL_VIEWPORT, viewport);
	glGetIntegerv(GL_FRAMEBUFFER_BINDING, &oldfbo);
	//glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &maxImageUnits);

	frameBuffer->bind();
	frameBuffer->insertColourBuff(new Texture((uint)_res.x, (uint)_res.y, format));
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	glViewport(0, 0, (GLsizei)_res.x, (GLsizei)_res.y);

	Shader* shader = Resources::Shaders.get("assets/genAtlas.shader");	
	shader->use();

	Vertex* vertexBuffer = new Vertex[textures.size() * 4];
	int tIndex = 0;
	for (auto& t : textures) {
		Vector2 min = (Vector2)t.second.min();
		Vector2 max = (Vector2)t.second.max();
		min = Vector2(min.x / _res.x, min.y / _res.y) * 2.0f - Vector2(1.0f);
		max = Vector2(max.x / _res.x, max.y / _res.y) * 2.0f - Vector2(1.0f);

		int fragRefIndex = tIndex % maxImageUnits;
		vertexBuffer[tIndex * 4 + 0] = Vertex{ min, Vector2(0.0f, 0.0f), fragRefIndex };
		vertexBuffer[tIndex * 4 + 1] = Vertex{ Vector2(max.x, min.y), Vector2(1.0f, 0.0f), fragRefIndex };
		vertexBuffer[tIndex * 4 + 2] = Vertex{ max, Vector2(1.0f, 1.0f), fragRefIndex };
		vertexBuffer[tIndex * 4 + 3] = Vertex{ Vector2(min.x,	max.y), Vector2(0.0f, 1.0f), fragRefIndex };
		++tIndex;
	}
	VertexArray* vao = new VertexArray();
	vao->bind();
	VertexBuffer<StaticDraw>* vbo = new VertexBuffer<StaticDraw>();
	vbo->bind();
	vbo->setData(textures.size() * 4 * sizeof(Vertex), vertexBuffer);
	vao->setConfig(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	vao->setConfig(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), 2 * sizeof(float));
	vao->setConfig(2, 1, GL_INT, GL_FALSE, sizeof(Vertex), 4 * sizeof(float));
	ElementBuffer<StaticDraw>* ebo = new ElementBuffer<StaticDraw>();
	ebo->bind();
	ebo->setData(textures.size() * 6 * sizeof(uint), getQuadIndices(textures.size()));
	delete[] vertexBuffer;

	tIndex = 0;
	int div = 0, mod = 0;

	for (auto& t : textures) {
		div = tIndex / maxImageUnits;
		mod = tIndex % maxImageUnits;
		shader->setTex("textures[" + std::to_string(tIndex) + "]", t.first, mod);
		if (mod + 1 == maxImageUnits) {
			glDrawElements(GL_TRIANGLES, maxImageUnits * 6, GL_UNSIGNED_INT, (void*)((long long)(div * maxImageUnits) * 6 * sizeof(uint)));
		}
		++tIndex;
	}
	if (mod + 1 != maxImageUnits) {
		glDrawElements(GL_TRIANGLES, (mod + 1) * 6, GL_UNSIGNED_INT, (void*)((long long)(div * maxImageUnits) * 6 * sizeof(uint)));
	}

	glViewport(0, 0, viewport[2], viewport[3]);
	
	delete vbo;
	delete ebo;
	delete vao;
	Texture* tex = frameBuffer->ejectColourBuff();
	return tex;
}

Atlas::Atlas() {
	frameBuffer = new FrameBuffer();
}

Atlas::Atlas(const std::initializer_list<Texture*> textures) : Atlas()
{
	for (auto t : textures)
		insert(t);
}

Atlas::~Atlas()
{
	delete frameBuffer;
}

void Atlas::insert(Texture* texture)
{
	if (texture) {
		if (!textures.count(texture)) {
			textures[texture] = Rect();
			_res = UVec2();
		}
	}
}

void Atlas::erase(const Texture* texture)
{
	if (textures.erase(const_cast<Texture*>(texture))) {
		_res = UVec2();
	}
}

bool Atlas::exists(const Texture* texture) const
{
	return textures.count(const_cast<Texture*>(texture));
}

size_t Atlas::size() const
{
	return textures.size();
}

UVec2 Atlas::resolution()
{
	if (!validate())
		return UVec2();
	return _res;
}

void Atlas::clear()
{
	textures.clear();
	_res = UVec2();
}

Rect Atlas::getCrop(const Texture* texture)
{
	if (!validate())
		return Rect();

	Texture* input = const_cast<Texture*>(texture);
	if (textures.count(input)) {
		return textures[input];
	}
	return Rect();
}

Rect Atlas::getCrop(const Texture* texture, const Rect& subcrop)
{
	if (!validate())
		return Rect();

	Texture* input = const_cast<Texture*>(texture);
	if (textures.count(input)) {
		Rect tRect = textures[input];
		return Rect(tRect.min() + subcrop.min(), tRect.min() + subcrop.max());
	}
	return Rect();
}
