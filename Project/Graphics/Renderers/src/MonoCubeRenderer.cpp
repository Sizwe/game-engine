#include <MonoCubeRenderer.h>
#include <GraphicsUtils.h>
#include <Sprite.h>

Texture* MonoCubeRenderer::Base = nullptr;
std::set<RenderableMonoCube*> MonoCubeRenderer::RCubes;

namespace {
	inline Stripe makeStripe(const RenderableMonoCube* rmc, const Vector4* colours, const Vector3* positions, size_t start, size_t end) {
		Stripe stripe = Stripe(Line(positions[start], positions[end]), (colours[start] + colours[end]) / 2.0f);
		stripe.lifetime = rmc->lifetime;
		return stripe;
	}
}

RenderableMonoCube::RenderableMonoCube(const MonoCube& cube, const GameObject* gameObject, RenderSession::Binding* binding)
	: Renderable(binding, gameObject), MonoCube(cube) {
	MonoCubeRenderer::Insert(this);
}

RenderableMonoCube::~RenderableMonoCube()
{
	MonoCubeRenderer::Erase(this);
}

void RenderableMonoCube::setPaused(bool pause)
{
	Renderable::setPaused(pause);
	for (auto& s : sessions) {
		s.setPaused(pause);
	}
}

void MonoCubeRenderer::AllocSprites(RenderableMonoCube* rmc)
{
	Matrix4 spriteLocal = Matrix4::Shift(rmc->transform.position) * rmc->transform.rotation.toMatrix4() * Matrix4::Scale(rmc->transform.scale);

	if (!Base) {
		byte white[3] = { 0xFF, 0xFF, 0xFF };
		Base = new Texture(white, 1, 1, TexFormat::RGB);
	}
	Vector4 colours[6] = { rmc->colour.left, rmc->colour.right, rmc->colour.top, rmc->colour.bottom, rmc->colour.front, rmc->colour.back };
	float w = rmc->box.width();
	float h = rmc->box.height();
	float d = rmc->box.depth();

	Transform transformc[6] = {
		Transform(Vec3(-w / 2, 0, 0), Quat(Vec3(0, 90, 0)), Vec3(d, h, 1)),
		Transform(Vec3(w / 2, 0, 0), Quat(Vec3(0, -90, 0)), Vec3(d, h, 1)),
		Transform(Vec3(0,  h / 2, 0), Quat(Vec3(90, 0, 0)), Vec3(w, d, 1)),
		Transform(Vec3(0, -h / 2, 0), Quat(Vec3(-90, 0, 0)), Vec3(w, d, 1)),
		Transform(Vec3(0, 0, -d / 2), Quat(Vec3(0, 0, 0)), Vec3(w, h, 1)),
		Transform(Vec3(0, 0,  d / 2), Quat(Vec3(0, 180, 0)), Vec3(w, h, 1))
	};

	rmc->sessions.reserve(6);
	for (int i = 0; i < 6; ++i) {
		Matrix4 cubeLocal = Matrix4::Shift(rmc->transform.position) * rmc->transform.rotation.toMatrix4() * Matrix4::Scale(rmc->transform.scale);
		Sprite sprite(Base);
		sprite.facing = rmc->facing;
		sprite.colour = colours[i];
		sprite.material = rmc->material;
		sprite.transform.scale = rmc->transform.scale * transformc[i].scale;
		sprite.transform.rotation = rmc->transform.rotation * transformc[i].rotation;
		sprite.transform.position = cubeLocal * transformc[i].position;
		sprite.lifetime = rmc->lifetime;
		rmc->sessions.push_back(Renderer::Draw(sprite, rmc->gameObject));
	}
}

void MonoCubeRenderer::AllocStripes(RenderableMonoCube* rmc)
{
	Matrix4 spriteLocal = Matrix4::Shift(rmc->transform.position) * rmc->transform.rotation.toMatrix4() * Matrix4::Scale(rmc->transform.scale);
	MonoCube::Colour& colour = rmc->colour;

	Vector3 pos[8] = {
	rmc->box.bbl(), rmc->box.bbr(),  rmc->box.btr(), rmc->box.btl(),
	rmc->box.fbl(), rmc->box.fbr(),  rmc->box.ftr(), rmc->box.ftl()
	};
	for (int i = 0; i < 8; ++i)
		pos[i] = spriteLocal * pos[i];

	Vector4 colours[8] = {
		(colour.back + colour.bottom + colour.left) / 3.0f,	 (colour.back + colour.bottom + colour.right) / 3.0f,
		(colour.back + colour.top + colour.right) / 3.0f,	 (colour.back + colour.top + colour.left) / 3.0f,
		(colour.front + colour.bottom + colour.left) / 3.0f, (colour.front + colour.bottom + colour.right) / 3.0f,
		(colour.front + colour.top + colour.right) / 3.0f,	 (colour.front + colour.top + colour.left) / 3.0f
	};

	if (rmc->mode == MonoDrawable::Mode::Wireframe) {
		rmc->sessions.reserve(24);
		for (size_t i = 0; i < 4; ++i) {
			size_t j = (i / 2) * 4;
			size_t k = (i / 2) * 2;
			size_t m = i % 2;
			rmc->sessions.push_back(Renderer::Draw(makeStripe(rmc, colours, pos, j + m, j + m + 2), rmc->gameObject));
			rmc->sessions.push_back(Renderer::Draw(makeStripe(rmc, colours, pos, i, 7 - i), rmc->gameObject));
			rmc->sessions.push_back(Renderer::Draw(makeStripe(rmc, colours, pos, k + m, k + 5 - m), rmc->gameObject));
		}
	}
	else
		rmc->sessions.reserve(12);

	for (size_t i = 0; i < 4; ++i) {
		rmc->sessions.push_back(Renderer::Draw(makeStripe(rmc, colours, pos, i, (i + 1) % 4), rmc->gameObject));
		rmc->sessions.push_back(Renderer::Draw(makeStripe(rmc, colours, pos, 4 + i, 4 + (i + 1) % 4), rmc->gameObject));
		rmc->sessions.push_back(Renderer::Draw(makeStripe(rmc, colours, pos, i, i + 4), rmc->gameObject));
	}
}

void MonoCubeRenderer::Insert(RenderableMonoCube* rmc) {
	RCubes.insert(rmc);
	if (rmc->mode == MonoDrawable::Mode::Edge || rmc->mode == MonoDrawable::Mode::Wireframe)
		AllocStripes(rmc);
	else
		AllocSprites(rmc);
}

void MonoCubeRenderer::Erase(RenderableMonoCube* rmc)
{
	for (auto& s : rmc->sessions) {
		s.close();
	}
	RCubes.erase(rmc);
}

size_t MonoCubeRenderer::DrawCount()
{
	return RCubes.size();
}

void MonoCubeRenderer::Clear()
{
	std::deque<RenderableMonoCube*> sprites;
	for (auto s : RCubes)
		sprites.push_back(s);
	for (auto s : sprites)
		delete s;
	delete Base;
	Base = nullptr;
}
