#pragma once
#include <CubeSpriteRenderer.h>

std::map<CubeMap*, CubeSpriteRenderer::CubeMapData*> CubeSpriteRenderer::TexMap;

namespace {
	Sprite genCubeFaceSprite(Texture* tex, const Transform& transform) {
		Sprite sprite(tex);
		sprite.transform.scale = Vector3(transform.scale.x / tex->getWidth(), transform.scale.y / tex->getHeight(), 0.0f);
		sprite.transform.rotation = transform.rotation;
		sprite.transform.position = transform.position;
		return sprite;
	}
}

RenderableCubeSprite::RenderableCubeSprite(const CubeSprite& cubeSprite, const GameObject* gameObject, RenderSession::Binding* binding) 
	: Renderable(binding, gameObject), cubeSprite(cubeSprite) {
	CubeSpriteRenderer::Insert(this);
}

RenderableCubeSprite::~RenderableCubeSprite()
{
	CubeSpriteRenderer::Erase(this);
}

void RenderableCubeSprite::setPaused(bool pause)
{
	Renderable::setPaused(pause);
	for (int i = 0; i < 6; sessions[i].setPaused(pause), ++i);
}

void CubeSpriteRenderer::Insert(RenderableCubeSprite* renderable)
{
	auto& cs = renderable->cubeSprite;

	auto it = TexMap.find(renderable->cubeSprite.cubeMap);
	if (it == TexMap.end()) {
		it = TexMap.insert(std::make_pair(renderable->cubeSprite.cubeMap, new CubeMapData())).first;
		it->second->textures = {
			cs.cubeMap->createTexture(CubeFace::Left),
			cs.cubeMap->createTexture(CubeFace::Right),
			cs.cubeMap->createTexture(CubeFace::Top),
			cs.cubeMap->createTexture(CubeFace::Bottom),
			cs.cubeMap->createTexture(CubeFace::Front),
			cs.cubeMap->createTexture(CubeFace::Back)
		};
	}
	it->second->sprites.insert(renderable);

	auto& textures = it->second->textures;

	float w = cs.box.width();
	float h = cs.box.height();
	float d = cs.box.depth();

	Sprite sprites[6] = {
		genCubeFaceSprite(textures[0], Transform(Vec3(-w / 2, 0, 0), Quat(Vec3(0, 90, 0)), Vec3(d, h, 1))),
		genCubeFaceSprite(textures[1], Transform(Vec3(w / 2, 0, 0), Quat(Vec3(0, -90, 0)), Vec3(d, h, 1))),
		genCubeFaceSprite(textures[2], Transform(Vec3(0,  h / 2, 0), Quat(Vec3(90, 0, 0)), Vec3(w, d, 1))),
		genCubeFaceSprite(textures[3], Transform(Vec3(0, -h / 2, 0), Quat(Vec3(-90, 0, 0)), Vec3(w, d, 1))),
		genCubeFaceSprite(textures[4], Transform(Vec3(0, 0, -d / 2), Quat(Vec3(0, 0, 0)), Vec3(w, h, 1))),
		genCubeFaceSprite(textures[5], Transform(Vec3(0, 0,  d / 2), Quat(Vec3(0, 180, 0)), Vec3(w, h, 1)))
	};

	for (int i = 0; i < 6; ++i) {
		Matrix4 cubeLocal = Matrix4::Shift(cs.transform.position) * cs.transform.rotation.toMatrix4() * Matrix4::Scale(cs.transform.scale);
		sprites[i].facing = (Sprite::Facing)cs.facing;
		sprites[i].colour = cs.colour;
		sprites[i].material = cs.material;
		sprites[i].transform.scale = cs.transform.scale * sprites[i].transform.scale;
		sprites[i].transform.rotation = cs.transform.rotation * sprites[i].transform.rotation;
		sprites[i].transform.position = cubeLocal * sprites[i].transform.position;
		renderable->sessions[i] = Renderer::Draw(sprites[i], renderable->gameObject);
	}
}

void CubeSpriteRenderer::Erase(RenderableCubeSprite* renderable)
{
	auto it = TexMap.find(renderable->cubeSprite.cubeMap);
	if (it != TexMap.end()) {
		for (auto& rs : renderable->sessions) {
			rs.close();
		}
		it->second->sprites.erase(renderable);

		if (it->second->sprites.empty()) {
			for (auto t : it->second->textures) {
				delete t;
			}
			delete it->second;
			TexMap.erase(it);
		}
	}
}

size_t CubeSpriteRenderer::DrawCount()
{
	size_t count = 0;
	for (auto t : TexMap) {
		count += t.second->sprites.size();
	}
	return count;
}

void CubeSpriteRenderer::Clear()
{
	std::deque<RenderableCubeSprite*> sprites;
	for (auto it : TexMap) {
		for (auto s : it.second->sprites) {
			sprites.push_back(s);
		}
	}
	for (auto s : sprites) {
		delete s;
	}
}
