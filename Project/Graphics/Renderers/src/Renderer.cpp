#include <Renderer.h>
#include <vector>
#include <Engine.h>
#include <Resources.h>
#include <SpriteRenderer.h>
#include <StripeRenderer.h>
#include <TextTexturer.h>
#include <iostream>
#include <CubeSpriteRenderer.h>
#include <MonoSpriteRenderer.h>
#include <MonoCubeRenderer.h>

GameObject* Renderer::Camera = nullptr;
Renderer::Background Renderer::background;
Renderer::ProjectionSettings Renderer::projection = Renderer::ProjectionSettings(1.0f, 100000.0f, 45.0f);
Renderer::PointShadows Renderer::pointShadows;
Renderer::DirectionalShadows Renderer::dirShadows;
Renderer::Fog Renderer::fog;
Renderer::Filters Renderer::filters;
Renderer::DepthOfField Renderer::depthOfField;

namespace {
	std::map<const GameObject*, std::set<Renderable*>> GObjectMap;

	struct RenderState {
		VertexArray* vertexArray;
		VertexBuffer<StaticDraw>* vertexBuffer;
		CubeFrameBuffer* pointFrameBuffer;
		FrameBuffer* dirFrameBuffer;
		FrameBuffer* screenFrameBuffer;

		RenderState() {
			float vertices[]{
				-1.0f,-1.0f,  0.0f, 0.0f,
				 1.0f,-1.0f,  1.0f, 0.0f,
				 1.0f, 1.0f,  1.0f, 1.0f,
				-1.0f, 1.0f,  0.0f, 1.0f
			};
			vertexArray = new VertexArray();
			vertexArray->bind();
			vertexBuffer = new VertexBuffer<StaticDraw>();
			vertexBuffer->bind();
			vertexBuffer->setData(sizeof(vertices), vertices);
			vertexArray->setConfig(0, 2, GL_FLOAT, false, 4 * sizeof(float), 0 * sizeof(float));
			vertexArray->setConfig(1, 2, GL_FLOAT, false, 4 * sizeof(float), 2 * sizeof(float));

			UVec2 res = Renderer::pointShadows.resolution;
			pointFrameBuffer = new CubeFrameBuffer();

			res = Renderer::dirShadows.resolution;
			dirFrameBuffer = new FrameBuffer();

			res = UVec2(Engine::Window.getWidth(), Engine::Window.getHeight());
			screenFrameBuffer = new FrameBuffer();
		}

		~RenderState() {
			delete vertexBuffer;
			delete vertexArray;
			delete pointFrameBuffer;
			delete dirFrameBuffer;
			delete screenFrameBuffer;
		}

		void updatePointShadowBuffer() {
			UVec2 res = Renderer::pointShadows.resolution;
			pointFrameBuffer->insertDepthBuff(new CubeMap(res.x, res.y, TexFormat::DepthComponent, TexDataType::Float));
		}
	};
	RenderState* renderState = nullptr;
}

void RenderSession::setPaused(bool pause) {
	if (binding && binding->renderable) {
		binding->renderable->setPaused(pause);
	}
}

bool RenderSession::paused() const {
	if (binding && binding->renderable)
		return binding->renderable->paused();
	return true;
}

void RenderSession::close()
{
	if (binding) {
		delete binding->renderable;
	}
}

bool RenderSession::closed() const
{
	return binding ? !binding->renderable : true;
}

RenderSession::RenderSession(RenderSession::Binding* binding) : binding(binding) {
	if (binding) binding->refCount++;
}
RenderSession::RenderSession(const RenderSession& renderSession) : binding(renderSession.binding)
{
	if (binding)
		binding->refCount++;
}
RenderSession& RenderSession::operator=(const RenderSession& renderSession)
{
	if (binding && --binding->refCount == 0 && !binding->renderable) {
		delete binding;
	}
	if (binding = renderSession.binding)
		binding->refCount++;
	return *this;
}

RenderSession::~RenderSession() {

	if (binding && --binding->refCount == 0 && !binding->renderable) {
		delete binding;
	}
}

RenderSession Renderer::Draw(const Sprite& sprite, const GameObject* gameObject)
{
	if (!sprite.texture)
		return nullptr;

	gameObject = gameObject ? gameObject : &Engine::Origin;
	RenderSession session(new RenderSession::Binding());
	RenderableSprite* rsprite = new RenderableSprite(sprite, gameObject, session.binding);
	GObjectMap[gameObject].insert(rsprite);
	return session;
}

RenderSession Renderer::Draw(const Stripe& stripe, const GameObject* gameObject)
{
	gameObject = gameObject ? gameObject : &Engine::Origin;
	RenderSession session(new RenderSession::Binding());
	RenderableStripe* rstripe = new RenderableStripe(stripe, gameObject, session.binding);
	GObjectMap[gameObject].insert(rstripe);
	return session;
}

RenderSession Renderer::Draw(const CubeSprite& cube, const GameObject* gameObject)
{
	if (!cube.cubeMap)
		return nullptr;

	gameObject = gameObject ? gameObject : &Engine::Origin;
	RenderSession session(new RenderSession::Binding());
	RenderableCubeSprite* rcube = new RenderableCubeSprite(cube, gameObject, session.binding);
	GObjectMap[gameObject].insert(rcube);
	return session;
}

RenderSession Renderer::Draw(const MonoSprite& monoSprite, const GameObject* gameObject)
{
	gameObject = gameObject ? gameObject : &Engine::Origin;
	RenderSession session(new RenderSession::Binding());
	RenderableMonoSprite* rsprite = new RenderableMonoSprite(monoSprite, gameObject, session.binding);
	GObjectMap[gameObject].insert(rsprite);
	return session;
}

RenderSession Renderer::Draw(const MonoCube& monoCube, const GameObject* gameObject)
{
	gameObject = gameObject ? gameObject : &Engine::Origin;
	RenderSession session(new RenderSession::Binding());
	RenderableMonoCube* rcube = new RenderableMonoCube(monoCube, gameObject, session.binding);
	GObjectMap[gameObject].insert(rcube);
	return session;
}

size_t Renderer::ObjectCount()
{
	return SpriteRenderer::DrawCount() + StripeRenderer::DrawCount();
}

namespace /* RenderToScreen() helper functions */ {

	void allocModelMatrices(MaterialContext& matCont) {
		auto modelMatrices = new std::map<const GameObject*, Matrix4>();
		for (auto g = GObjectMap.begin(); g != GObjectMap.end();) {
			if (g->first->doomed()) {
				for (auto& r : g->second) delete r;
				g = GObjectMap.erase(g);
				continue;
			}
			else if (!g->first->enabled())
				modelMatrices->insert({ g->first, Matrix4() });
			else
				modelMatrices->insert({ g->first, g->first->toGlobal.matrix() });
			++g;
		}
		matCont.modelMatrices = modelMatrices;
	}

	void allocDirShadows(MaterialContext& matCont)
	{
		auto& dirShadows = Renderer::dirShadows;
		float near = Renderer::projection.near;
		LocationContext context;
		context.camPos = matCont.camPos;
		context.modelMatrices = matCont.modelMatrices;
		renderState->dirFrameBuffer->bind();
		glViewport(0, 0, dirShadows.resolution.x, dirShadows.resolution.y);

		for (size_t i = 0; i < DirLights.size(); ++i) {
			if (!DirLights[i].enabled)
				continue;

			renderState->dirFrameBuffer->insertDepthBuff(new Texture(dirShadows.resolution.x, dirShadows.resolution.y, TexFormat::DepthComponent, TexDataType::Float));
			glClear(GL_DEPTH_BUFFER_BIT);
			context.projMatrix = Matrix4::Ortho(-dirShadows.span.x, dirShadows.span.x, -dirShadows.span.y, dirShadows.span.y, -dirShadows.span.z, dirShadows.span.z);
			context.viewMatrix = Matrix4::LookDir(-DirLights[i].direction) * Matrix4::Shift(-context.camPos);

			SpriteRenderer::RenderDirShadow(context);

			matCont.dirShadows[i].map = renderState->dirFrameBuffer->ejectDepthBuff();
			matCont.dirShadows[i].lightLocalMatrix = context.projMatrix * context.viewMatrix;
		}
	}

	void allocPointShadows(MaterialContext& matCont)
	{
		auto& pointShadows = Renderer::pointShadows;
		PointShadowContext context;
		context.camPos = matCont.camPos;
		context.modelMatrices = matCont.modelMatrices;
		renderState->pointFrameBuffer->bind();
		glViewport(0, 0, pointShadows.resolution.x, pointShadows.resolution.y);

		for (size_t i = 0; i < PointLights.size(); ++i) {
			if (!PointLights[i].enabled)
				continue;

			renderState->pointFrameBuffer->insertDepthBuff(new CubeMap(pointShadows.resolution.x, pointShadows.resolution.y, TexFormat::DepthComponent, TexDataType::Float));
			glClear(GL_DEPTH_BUFFER_BIT);

			context.camPos = PointLights[i].position;
			context.span = pointShadows.span;
			Matrix4 projMatrix = Matrix4::Persp(90.0f, float(pointShadows.resolution.x) / float(pointShadows.resolution.y), 0.1f, pointShadows.span);
			Matrix4 shift = Matrix4::Shift(-PointLights[i].position);
			context.lightSpacePVs[0] = projMatrix * Matrix4::LookDir(Vec3(-1.0f, 0.0f, 0.0f), Vec3(0.0f, 1.0f, 0.0f)) * shift;
			context.lightSpacePVs[1] = projMatrix * Matrix4::LookDir(Vec3(1.0f, 0.0f, 0.0f), Vec3(0.0f, 1.0f, 0.0f)) * shift;
			context.lightSpacePVs[2] = projMatrix * Matrix4::LookDir(Vec3(0.0f, 1.0f, 0.0f), Vec3(0.0f, 0.0f, 1.0f)) * shift;
			context.lightSpacePVs[3] = projMatrix * Matrix4::LookDir(Vec3(0.0f, -1.0f, 0.0f), Vec3(0.0f, 0.0f, -1.0f)) * shift;
			context.lightSpacePVs[4] = projMatrix * Matrix4::LookDir(Vec3(0.0f, 0.0f, -1.0f), Vec3(0.0f, 1.0f, 0.0f)) * shift;
			context.lightSpacePVs[5] = projMatrix * Matrix4::LookDir(Vec3(0.0f, 0.0f, 1.0f), Vec3(0.0f, 1.0f, 0.0f)) * shift;

			SpriteRenderer::RenderPointShadows(context);

			matCont.pointShadows[i].map = renderState->pointFrameBuffer->ejectDepthBuff();
			matCont.pointShadows[i].span = pointShadows.span;
		}
	}

	void clear(MaterialContext& matCont)
	{
		for (auto& d : matCont.dirShadows) {
			delete d.map;
		}
		for (auto& p : matCont.pointShadows) {
			delete p.map;
		}
		delete matCont.modelMatrices;
	}
}

void Renderer::RenderToScreen()
{
	UVec2 resolution = UVec2(Engine::Window.getWidth(), Engine::Window.getHeight());
	if (Texture* tex = Render(resolution)) {
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		renderState->vertexArray->bind();
		glViewport(0, 0, Engine::Window.getWidth(), Engine::Window.getHeight());
		Shader* shader = Resources::Shaders.get("assets/renderToScreen.shader");
		shader->use();
		shader->setTex("tex", tex, 1);

		glCall(glDrawArrays(GL_TRIANGLE_FAN, 0, 4));
		delete tex;
	}
}

Texture* Renderer::Render(const UVec2& resolution)
{
	if (!Renderer::Camera)
		return nullptr;
	if (!renderState)
		renderState = new RenderState();

	MaterialContext context;
	Vec2 fres = Vec2(resolution);
	if (projection.type == ProjectionType::Perspective)
		context.projMatrix = Matrix4::Persp(projection.fov, fres.x / fres.y, projection.near, projection.far);
	else
		context.projMatrix = Matrix4::Ortho(-fres.x / 2.0f, fres.x / 2.0f, -fres.y / 2.0f, fres.y / 2.0f, projection.near, projection.far);
	context.viewMatrix = Renderer::Camera->toLocal.matrix();
	context.camPos = Renderer::Camera->global.getPos();
	allocModelMatrices(context);

	SpriteRenderer::Update(context);

	allocDirShadows(context);
	allocPointShadows(context);

	renderState->screenFrameBuffer->bind();
	renderState->screenFrameBuffer->insertColourBuff(new Texture(resolution.x, resolution.y));
	renderState->screenFrameBuffer->insertDepthBuff(new Texture(resolution.x, resolution.y, TexFormat::DepthComponent, TexDataType::Float));
	glClearColor(background.colour.x, background.colour.y, background.colour.z, background.colour.w);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glViewport(0, 0, resolution.x, resolution.y);
	SpriteRenderer::Render(context);
	StripeRenderer::Render(context);

	Texture* depth = renderState->screenFrameBuffer->ejectDepthBuff();
	Texture* colour = renderState->screenFrameBuffer->ejectColourBuff();
	depth->bind();
	depth->setWrap(TexWrap::ClampToEdge);
	colour->bind();
	colour->setWrap(TexWrap::ClampToEdge);

	renderState->screenFrameBuffer->insertColourBuff(new Texture(resolution.x, resolution.y));
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	renderState->vertexArray->bind();

	if (fog.enabled) {
		Shader* shader = Resources::Shaders.get("assets/fog.shader");
		shader->use();
		shader->setMatrix4("projection", context.projMatrix);
		shader->setMatrix4("view", context.viewMatrix);
		shader->setFloat3("camPos", context.camPos);
		shader->setFloat("startDepth", fog.startDepth);
		shader->setFloat("endDepth", fog.endDepth);
		shader->setFloat4("colour", fog.colour);
		shader->setTex("depthTex", depth, 1);
		shader->setTex("tex", colour, 2);

		glCall(glDrawArrays(GL_TRIANGLE_FAN, 0, 4));
		delete colour;
		colour = renderState->screenFrameBuffer->ejectColourBuff();
		renderState->screenFrameBuffer->insertColourBuff(new Texture(resolution.x, resolution.y));
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT);
	}

	if (depthOfField.enabled) {
		Shader* shader = Resources::Shaders.get("assets/dof.shader");
		shader->use();
		shader->setMatrix4("projection", context.projMatrix);
		shader->setMatrix4("view", context.viewMatrix);
		shader->setFloat3("focalPoint", depthOfField.focalPoint);
		shader->setFloat("blurDist", depthOfField.blurDist);
		shader->setFloat("blurSize", depthOfField.blurSize);
		shader->setFloat("blurQuality", depthOfField.blurQuality);
		shader->setTex("depthTex", depth, 1);
		shader->setTex("tex", colour, 2);

		glCall(glDrawArrays(GL_TRIANGLE_FAN, 0, 4));
		delete colour;
		colour = renderState->screenFrameBuffer->ejectColourBuff();
		renderState->screenFrameBuffer->insertColourBuff(new Texture(resolution.x, resolution.y));
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT);
	}

	if (filters.enabled) {
		Shader* shader = Resources::Shaders.get("assets/filters.shader");
		shader->use();
		shader->setFloat("brightness", filters.brightness);
		shader->setFloat("contrast", filters.contrast);
		shader->setFloat("vibrance", filters.vibrance);
		shader->setFloat("saturation", filters.saturation);
		shader->setFloat4("colourise", filters.colourise);
		shader->setFloat("hue", filters.hue);
		shader->setTex("tex", colour, 2);

		glCall(glDrawArrays(GL_TRIANGLE_FAN, 0, 4));
		delete colour;
		colour = renderState->screenFrameBuffer->ejectColourBuff();
	}
	clear(context);
	delete renderState->screenFrameBuffer->ejectColourBuff();

	delete depth;
	return colour;
}

void Renderer::Reset()
{
	MonoSpriteRenderer::Clear();
	CubeSpriteRenderer::Clear();
	SpriteRenderer::Clear();
	StripeRenderer::Clear();
	delete renderState;
}

Renderable::Renderable(RenderSession::Binding* binding, const GameObject* gameObject) : binding(binding), gameObject(gameObject), _paused(false) {
	binding->renderable = this;
}

Renderable::~Renderable() {
	if (binding->refCount)
		binding->renderable = nullptr;
	else
		delete binding;

	GObjectMap[this->gameObject].erase(this);
	if (GObjectMap[this->gameObject].size() == 0) {
		GObjectMap.erase(this->gameObject);
	}
}

void Renderable::setPaused(bool pause) {
	_paused = pause;
}

bool Renderable::paused() const
{
	return _paused;
}

Renderer::DirectionalShadows::DirectionalShadows(const UVector2& resolution, const Vector3& span)
	: resolution(resolution), span(span) {}

Renderer::PointShadows::PointShadows(const UVector2& resolution, float span)
	: resolution(resolution), span(span) {}

Renderer::Fog::Fog() : Fog(0.0f, 0.0f) {
	enabled = false;
}

Renderer::Fog::Fog(float startDist, float endDist, const Vector4& colour)
	:	startDepth(startDist), endDepth(endDist), colour(colour), enabled(true) {
}

Renderer::DepthOfField::DepthOfField()
	: DepthOfField(Vector3(0.0f), 2000.0f) 
{
	enabled = false;
}

Renderer::DepthOfField::DepthOfField(const Vector3& focalPoint, float blurDist, float blurSize, float blurQuality)
	: enabled(true), focalPoint(focalPoint), blurDist(blurDist), blurSize(blurSize), blurQuality(blurQuality) {
}

Renderer::Filters::Filters() : Filters(1.0f)
{
	enabled = false;
}

Renderer::Filters::Filters(float brightness, float contrast, float hue, float vibrance, float saturation, const Vector4& colourise)
	: brightness(brightness), contrast(contrast), hue(hue), vibrance(vibrance), saturation(saturation), colourise(colourise), enabled(true) {
}

Renderer::ProjectionSettings::ProjectionSettings(float near, float far) 
	: near(near), far(far), fov(45.0f), type(ProjectionType::Orthographic) {
}

Renderer::ProjectionSettings::ProjectionSettings(float near, float far, float fov)
	: near(near), far(far), fov(fov), type(ProjectionType::Perspective) {
}
