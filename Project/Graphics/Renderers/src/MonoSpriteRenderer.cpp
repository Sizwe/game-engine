#include <MonoSpriteRenderer.h>
#include <Sprite.h>

Texture* MonoSpriteRenderer::Base = nullptr;
std::set<RenderableMonoSprite*> MonoSpriteRenderer::RSprites;

RenderableMonoSprite::RenderableMonoSprite(const MonoSprite& sprite, const GameObject* gameObject, RenderSession::Binding* binding)
	: Renderable(binding, gameObject), MonoSprite(sprite) {
	MonoSpriteRenderer::Insert(this);
}

RenderableMonoSprite::~RenderableMonoSprite()
{
	MonoSpriteRenderer::Erase(this);
}

void RenderableMonoSprite::setPaused(bool pause)
{
	Renderable::setPaused(pause);
	for (auto s : sessions)
		s.setPaused(pause);
}

void MonoSpriteRenderer::AllocSprite(RenderableMonoSprite* rms)
{
	Matrix4 spriteLocal = Matrix4::Shift(rms->transform.position) * rms->transform.rotation.toMatrix4() * Matrix4::Scale(rms->transform.scale);
	if (!Base) {
		byte white[3] = { 0xFF, 0xFF, 0xFF };
		Base = new Texture(white, 1, 1, TexFormat::RGB);
	}
	Sprite sprite = Sprite(Base);
	sprite.transform.scale = rms->transform.scale * Vector3(rms->rect.width(), rms->rect.height(), 0.0f);
	sprite.transform.rotation = rms->transform.rotation;
	sprite.transform.position = spriteLocal * Vector3(rms->rect.centre());
	sprite.facing = rms->facing;
	sprite.material = rms->material;
	sprite.lifetime = rms->lifetime;
	sprite.colour = rms->colour;

	rms->sessions.push_back(Renderer::Draw(sprite, rms->gameObject));
}

void MonoSpriteRenderer::AllocStripes(RenderableMonoSprite* rms)
{
	Matrix4 spriteLocal = Matrix4::Shift(rms->transform.position) * rms->transform.rotation.toMatrix4() * Matrix4::Scale(rms->transform.scale);
	Vector3 pos[4] = {
	Vec3(rms->rect.bl()), Vec3(rms->rect.br()), Vec3(rms->rect.tr()), Vec3(rms->rect.tl())
	};
	for (int i = 0; i < 4; ++i)
		pos[i] = spriteLocal * pos[i];

	if (rms->mode == MonoDrawable::Mode::Wireframe) {
		rms->sessions.reserve(6);
		for (int i = 0; i < 2; ++i) {
			Stripe stripe = Stripe(Line(pos[i], pos[i + 2]), rms->colour);
			stripe.lifetime = rms->lifetime;
			rms->sessions.push_back(Renderer::Draw(stripe, rms->gameObject));
		}
	}
	else
		rms->sessions.reserve(4);

	for (int i = 0; i < 4; ++i) {
		Stripe stripe = Stripe(Line(pos[i], pos[(i + 1) % 4]), rms->colour);
		stripe.lifetime = rms->lifetime;
		rms->sessions.push_back(Renderer::Draw(stripe, rms->gameObject));
	}
}

void MonoSpriteRenderer::Insert(RenderableMonoSprite* rms) {
	RSprites.insert(rms);
	if (rms->mode == MonoDrawable::Mode::Edge || rms->mode == MonoDrawable::Mode::Wireframe)
		AllocStripes(rms);
	else 
		AllocSprite(rms);
}

void MonoSpriteRenderer::Erase(RenderableMonoSprite* rms)
{
	for (auto s : rms->sessions)
		s.close();
	RSprites.erase(rms);
}

size_t MonoSpriteRenderer::DrawCount()
{
	return RSprites.size();
}

void MonoSpriteRenderer::Clear()
{
	std::deque<RenderableMonoSprite*> sprites;
	for (auto s : RSprites)
		sprites.push_back(s);
	for (auto s : sprites)
		delete s;
	delete Base;
	Base = nullptr;
}
