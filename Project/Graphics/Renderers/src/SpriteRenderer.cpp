#pragma once
#include <SpriteRenderer.h>
#include <Resources.h>
#include <iostream>
#include <algorithm>
#include <PointLights.h>
#include <DirLights.h>
#include <Reflections.h>

std::array<SpriteRenderer::_RenderData, 3> SpriteRenderer::RenderData;

namespace {
	const UVec2 MAX_ATLAS_RES = UVec2(2048, 2048);
	const size_t MAX_VERTICES_PER_RENDER = 128;

	template<typename T>
	void updateBuffer(T* buff, size_t size, void* data) {
		buff->bind();
		if (buff->getSize() < size)
			buff->setData(size, data);
		else
			buff->setData(size, data);
	}
}

void SpriteRenderer::Insert(RenderableSprite* renderable) {
	auto& rData = RenderData[renderable->translucent ? 2 : (size_t)renderable->lifetime];

	if (rData.texMap.count(renderable->texture)) {
		rData.texMap[renderable->texture].second.insert(renderable);
		RenderState& state = *rData.texMap[renderable->texture].first;
		state.resetVertices = true;
		state.vertexCount++;
	}
	else {
		for (auto& state : rData.renderStates) {
			if (state->atlas.resolution() < MAX_ATLAS_RES && state->vertexCount < MAX_VERTICES_PER_RENDER) {
				state->atlas.insert(renderable->texture);
				delete state->atlasTex;
				state->atlasTex = nullptr;
				state->resetVertices = true;
				state->vertexCount++;
				rData.texMap[renderable->texture].first = state;
				rData.texMap[renderable->texture].second.insert(renderable);
				return;
			}
		}
		RenderState* state = renderable->translucent? (RenderState*)new TranslucentRenderState() : (RenderState*)new OpaqueRenderState(renderable->lifetime);
		state->resetVertices = true;
		state->vertexCount++;
		state->atlas.insert(renderable->texture);
		rData.renderStates.insert(state);
		rData.texMap[renderable->texture].first = state;
		rData.texMap[renderable->texture].second.insert(renderable);
	}
}

void SpriteRenderer::Erase(RenderableSprite* renderable) 
{
	auto& rData = RenderData[renderable->translucent ? 2 : (size_t)renderable->lifetime];
	auto& texInfo = rData.texMap[renderable->texture];

	RenderState* state = rData.texMap[renderable->texture].first;
	texInfo.second.erase(renderable);
	state->resetVertices = true;
	state->vertexCount--;

	if (texInfo.second.size() == 0) {
		rData.texMap.erase(renderable->texture);
		if (state->atlas.size() > 1) {
			state->atlas.erase(renderable->texture);
			delete state->atlasTex;
			state->atlasTex = nullptr;
		}
		else {
			rData.renderStates.erase(state);
			delete state;
		}
	}
}

size_t SpriteRenderer::DrawCount()
{
	size_t count = 0;
	for (auto& rd : RenderData) {
		for (auto& t : rd.texMap) {
			count += t.second.second.size();
		}
	}
	return count;
}

void SpriteRenderer::Clear() {
	std::vector<RenderableSprite*> sprites;
	for (auto& rd : RenderData) {
		for (auto& t : rd.texMap) {
			sprites.reserve(sprites.size() + t.second.second.size());
			for (auto s : t.second.second) {
				sprites.push_back(s);
			}
		}
	}
	for (auto s : sprites) {
		delete s;
	}
}

void SpriteRenderer::Update(const LocationContext& context) 
{
	for(auto& rd : RenderData){
		for (auto s : rd.renderStates)
			s->update(context);
	}
}

void SpriteRenderer::RenderPointShadows(const PointShadowContext& context)
{
	Shader* shader = Resources::Shaders.get("assets/renderPointShadows.shader");
	shader->use();
	shader->setMatrix4("lightPVs", (float*)context.lightSpacePVs.data(), 6);
	shader->setFloat3("lightPos", context.camPos);
	shader->setFloat("span", context.span);

	for (auto& rd : RenderData) {
		for (auto s : rd.renderStates)
			s->render(shader);
	}
}

void SpriteRenderer::RenderDirShadow(const LocationContext& context)
{
	Shader* shader = Resources::Shaders.get("assets/renderDirShadows.shader");
	shader->use();
	shader->setMatrix4("projection", context.projMatrix);
	shader->setMatrix4("view", context.viewMatrix);

	for (auto& rd : RenderData) {
		for (auto s : rd.renderStates)
			s->render(shader);
	}
}

void SpriteRenderer::Render(const MaterialContext& context) {
	Shader* shader = Resources::Shaders.get("assets/renderSprites.shader");
	shader->use();
	shader->setMatrix4("projection", context.projMatrix);
	shader->setMatrix4("view", context.viewMatrix);
	shader->setFloat3("camPos", context.camPos);

	uint unit = 2;
	int lFlags = 0; //reflections ->pointLights ->dirLights ->pointShadowMaps ->dirShadowMaps
	int flagPos = 0;

	for (size_t i = 0; i < Reflections.size(); ++i, ++unit, ++flagPos) {
		if (Reflections[i]) {
			shader->setCubeMap("reflections[" + std::to_string(i) + "]", Reflections[i], unit);
			lFlags |= 1 << flagPos;
		}
	}
	for (size_t i = 0; i < PointLights.size(); ++i, ++flagPos) {
		if (PointLights[i].enabled) {
			auto s = std::to_string(i);
			shader->setFloat3("pointLights[" + s + "].position", PointLights[i].position);
			shader->setFloat3("pointLights[" + s + "].ambient", PointLights[i].ambient.x);
			shader->setFloat3("pointLights[" + s + "].diffuse", PointLights[i].diffuse.x);
			shader->setFloat3("pointLights[" + s + "].specular", PointLights[i].specular.x);
			shader->setFloat3("pointLights[" + s + "].attenuation", PointLights[i].attenuation.constant, PointLights[i].attenuation.linear, PointLights[i].attenuation.quadratic);
			lFlags |= 1 << flagPos;
		}
	}
	for (size_t i = 0; i < DirLights.size(); ++i, ++flagPos) {
		if (DirLights[i].enabled) {
			auto s = std::to_string(i);
			shader->setFloat3("dirLights[" + s + "].direction", DirLights[i].direction);
			shader->setFloat3("dirLights[" + s + "].ambient", DirLights[i].ambient);
			shader->setFloat3("dirLights[" + s + "].diffuse", DirLights[i].diffuse);
			shader->setFloat3("dirLights[" + s + "].specular", DirLights[i].specular);
			lFlags |= 1 << flagPos;
		}
	}
	for (size_t i = 0; i < context.pointShadows.size(); ++i, ++unit, ++flagPos) {
		if (context.pointShadows[i].map) {
			auto s = std::to_string(i);
			shader->setCubeMap("pointShads[" + s + "].map", context.pointShadows[i].map, unit);
			shader->setFloat("pointShads[" + s + "].span", context.pointShadows[i].span);
			lFlags |= 1 << flagPos;
		}
	}
	for (size_t i = 0; i < context.dirShadows.size(); ++i, ++unit, ++flagPos) {
		if (context.dirShadows[i].map) {
			auto s = std::to_string(i);
			shader->setTex("dirShads[" + s + "].map", context.dirShadows[i].map, unit);
			shader->setMatrix4("dirShads[" + s + "].lightLocal", context.dirShadows[i].lightLocalMatrix);
			lFlags |= 1 << flagPos;
		}
	}
	shader->setInt("lFlags", lFlags);

	for (auto& rd : RenderData) {
		for (auto s : rd.renderStates)
			s->render(shader);
	}
}

SpriteRenderer::RenderState::RenderState() {
	atlasTex = nullptr;
	vertexArray = new VertexArray();
	vertexArray->bind();

	dynamicVertBuff = new VertexBuffer<DynamicDraw>();
	dynamicVertBuff->bind();
	vertexArray->setConfig(0, 4, GL_FLOAT, GL_FALSE, 16 * sizeof(float), 0 * sizeof(float));
	vertexArray->setConfig(1, 4, GL_FLOAT, GL_FALSE, 16 * sizeof(float), 4 * sizeof(float));
	vertexArray->setConfig(2, 4, GL_FLOAT, GL_FALSE, 16 * sizeof(float), 8 * sizeof(float));
	vertexArray->setConfig(3, 4, GL_FLOAT, GL_FALSE, 16 * sizeof(float), 12 * sizeof(float));

	staticVertBuff = new VertexBuffer<StaticDraw>();
	staticVertBuff->bind();
	vertexArray->setConfig(4, 4, GL_FLOAT,	GL_FALSE, 1 * sizeof(int) + 25 * sizeof(float), 0 * sizeof(int) + 0 * sizeof(float));
	vertexArray->setConfig(5, 4, GL_FLOAT,	GL_FALSE, 1 * sizeof(int) + 25 * sizeof(float), 0 * sizeof(int) + 4 * sizeof(float));
	vertexArray->setConfig(6, 4, GL_FLOAT,	GL_FALSE, 1 * sizeof(int) + 25 * sizeof(float), 0 * sizeof(int) + 8 * sizeof(float));
	vertexArray->setConfig(7, 1, GL_INT,	GL_FALSE, 1 * sizeof(int) + 25 * sizeof(float), 0 * sizeof(int) + 12 * sizeof(float));
	vertexArray->setConfig(8, 3, GL_FLOAT,	GL_FALSE, 1 * sizeof(int) + 25 * sizeof(float), 1 * sizeof(int) + 12 * sizeof(float));
	vertexArray->setConfig(9, 3, GL_FLOAT,	GL_FALSE, 1 * sizeof(int) + 25 * sizeof(float), 1 * sizeof(int) + 15 * sizeof(float));
	vertexArray->setConfig(10, 3, GL_FLOAT,	GL_FALSE, 1 * sizeof(int) + 25 * sizeof(float), 1 * sizeof(int) + 18 * sizeof(float));
	vertexArray->setConfig(11, 3, GL_FLOAT,	GL_FALSE, 1 * sizeof(int) + 25 * sizeof(float), 1 * sizeof(int) + 21 * sizeof(float));
	vertexArray->setConfig(12, 1, GL_FLOAT,	GL_FALSE, 1 * sizeof(int) + 25 * sizeof(float), 1 * sizeof(int) + 24 * sizeof(float));
}

SpriteRenderer::RenderState::~RenderState() {
	delete atlasTex;
	delete dynamicVertBuff;
	delete staticVertBuff;
	delete vertexArray;
}

namespace {
	float calcDist(const Rect& rect, const Matrix4& transform, const Vector3& point) {
		Vector3 wPoints[4] = { (Vec3)rect.bl(), (Vec3)rect.br(), (Vec3)rect.tr(), (Vec3)rect.tl() };
		for (int i = 0; i < 4; wPoints[i] = transform * wPoints[i], ++i);
		float maxDist = -FLT_MAX;
		for (int i = 0; i < 4; ++i) {
			maxDist = std::fmaxf(maxDist, point.dist(wPoints[i]));
		}
		return maxDist;
	}
}

void SpriteRenderer::OpaqueRenderState::update(const LocationContext& context) {
	if (!atlasTex) {
		if (!(atlasTex = atlas.create(TexFormat::RGB)))
			return;
	}
	vertexArray->bind();

	auto& rd = RenderData[(size_t)lifetime];
	Vertex::DynamicSeg* dSegs = new Vertex::DynamicSeg[vertexCount];
	size_t s_count = 0;

	if (resetVertices) {
		Vertex::StaticSeg* sSegs = new Vertex::StaticSeg[vertexCount];
		Vector2 fRes = Vector2((float)atlasTex->getWidth(), (float)atlasTex->getHeight());
		for (auto t : atlas) {
			for (auto* s : rd.texMap[t].second) {
				Rect crop = atlas.getCrop(t, s->crop);
				Vector2 tMin = Vector2(crop.min().x / fRes.x, crop.min().y / fRes.y);
				Vector2 tMax = Vector2(crop.max().x / fRes.x, crop.max().y / fRes.y);
				Rect wRect = Rect(crop.width(), crop.height());

				int vFlags = (int)s->material.enabled;
				vFlags |= (1 << (int)s->facing) << 1;
				vFlags |= (1 << s->material.distortion.imageIndex) << 4;
				sSegs[s_count] = 
				{{	wRect.bl(), wRect.tr()}, { tMin, tMax }, s->colour, vFlags,
					Vector3(s->material.distortion.reflectiveness, s->material.distortion.refractiveness, s->material.distortion.refractionRatio),
					s->material.ambient, s->material.diffuse, s->material.specular, s->material.shininess
				};
				dSegs[s_count] = { s->paused() ? Matrix4() : context.modelMatrices->at(s->gameObject) * s->tMatrix };
				++s_count;
			}
		}
		updateBuffer(staticVertBuff, sizeof(Vertex::StaticSeg) * vertexCount, sSegs);
		delete[] sSegs;
		resetVertices = false;
	}
	else {
		for (auto t : atlas) {
			for (auto* s : rd.texMap[t].second) {
				dSegs[s_count] = { s->paused() ? Matrix4() : context.modelMatrices->at(s->gameObject) * s->tMatrix };
				++s_count;
			}
		}
	}
	updateBuffer(dynamicVertBuff, sizeof(Vertex::DynamicSeg) * vertexCount, dSegs);
	delete[] dSegs;
}

void SpriteRenderer::OpaqueRenderState::render(Shader* shader) {
	shader->setTex("tex", atlasTex, 1);
	vertexArray->bind();
	glCall(glDrawArrays(GL_POINTS, 0, (GLsizei)vertexCount));
}

RenderableSprite::RenderableSprite(const Sprite& sprite, const GameObject* gameObject, RenderSession::Binding* binding) : Renderable(binding, gameObject), Sprite(sprite) {
	tMatrix = Matrix4::Shift(sprite.transform.position) * sprite.transform.rotation.toMatrix4() * Matrix4::Scale(sprite.transform.scale);
	translucent = sprite.texture->getFormat() == TexFormat::RGBA || sprite.texture->getFormat() == TexFormat::BGRA || sprite.colour.w < 0.999f;
	SpriteRenderer::Insert(this);
}

RenderableSprite::~RenderableSprite() {
	SpriteRenderer::Erase(this);
}

SpriteRenderer::TranslucentRenderState::TranslucentRenderState() : RenderState()
{
	elementBuffer = new ElementBuffer<DynamicDraw>();
	elementBuffer->bind();
}

SpriteRenderer::TranslucentRenderState::~TranslucentRenderState()
{
	delete elementBuffer;
}

void SpriteRenderer::TranslucentRenderState::update(const LocationContext& context)
{
	if (!atlasTex) {
		if (!(atlasTex = atlas.create(TexFormat::RGBA)))
			return;
	}
	vertexArray->bind();
	Vertex::DynamicSeg* dSegs = new Vertex::DynamicSeg[vertexCount];
	std::vector<std::pair<uint, float>> dists(vertexCount);
	size_t s_count = 0;

	if (resetVertices) {
		Vertex::StaticSeg* sSegs = new Vertex::StaticSeg[vertexCount];
		Vector2 fRes = Vector2((float)atlasTex->getWidth(), (float)atlasTex->getHeight());
		for (auto t : atlas) {
			for (auto* s : RenderData[2].texMap[t].second) {
				dSegs[s_count] = { s->paused() ? Matrix4() : context.modelMatrices->at(s->gameObject) * s->tMatrix };

				Rect crop = atlas.getCrop(t, s->crop);
				Vector2 tMin = Vector2(crop.min().x / fRes.x, crop.min().y / fRes.y);
				Vector2 tMax = Vector2(crop.max().x / fRes.x, crop.max().y / fRes.y);

				Rect wRect = Rect(crop.width(), crop.height());

				int vFlags = (int)s->material.enabled;
				vFlags |= (1 << (int)s->facing) << 1;
				vFlags |= (1 << s->material.distortion.imageIndex) << 4;
				sSegs[s_count] =
				{ {	wRect.bl(), wRect.tr()}, { tMin, tMax }, s->colour, vFlags,
					Vector3(s->material.distortion.reflectiveness, s->material.distortion.refractiveness, s->material.distortion.refractionRatio),
					s->material.ambient, s->material.diffuse, s->material.specular, s->material.shininess
				};
				dists[s_count] = { s_count, calcDist(wRect, dSegs[s_count].model, context.camPos) };

				++s_count;
			}
		}
		updateBuffer(staticVertBuff, sizeof(Vertex::StaticSeg) * vertexCount, sSegs);
		delete[] sSegs;
		resetVertices = false;
	}
	else {
		for (auto t : atlas) {
			for (auto* s : RenderData[2].texMap[t].second) {
				dSegs[s_count] = { s->paused() ? Matrix4() : context.modelMatrices->at(s->gameObject) * s->tMatrix };

				Rect crop = atlas.getCrop(t, s->crop);
				Rect wRect = Rect(crop.width(), crop.height());
				dists[s_count] = { s_count, calcDist(wRect, dSegs[s_count].model, context.camPos) };

				++s_count;
			}
		}
	}
	updateBuffer(dynamicVertBuff, sizeof(Vertex::DynamicSeg) * vertexCount, dSegs);
	delete[] dSegs;

	uint* indices = new uint[vertexCount];
	std::sort(dists.begin(), dists.end(), [](std::pair<uint, float> a, std::pair<uint, float> b) { return a.second > b.second; });
	for (size_t i = 0; i < vertexCount; ++i) {
		indices[i] = dists[i].first;
	}
	updateBuffer(elementBuffer, sizeof(uint) * vertexCount, indices);
	delete[] indices;
}

void SpriteRenderer::TranslucentRenderState::render(Shader* shader)
{
	shader->setTex("tex", atlasTex, 1);
	vertexArray->bind();
	glCall(glDrawElements(GL_POINTS, (GLsizei)vertexCount, GL_UNSIGNED_INT, 0));
}

SpriteRenderer::OpaqueRenderState::OpaqueRenderState(Drawable::Lifetime lifetime) : lifetime(lifetime) {
}
