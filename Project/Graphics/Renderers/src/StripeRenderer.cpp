#pragma once
#include <StripeRenderer.h>
#include <Resources.h>

std::map<RenderableStripe*, StripeRenderer::RenderState*> StripeRenderer::StripeMap;
std::set<StripeRenderer::RenderState*> StripeRenderer::States;

namespace {
	const size_t MAX_STRIPE_COUNT = 256;

	template<typename T>
	void updateBuffer(T* buff, size_t size, void* data) {
		buff->bind();
		if (buff->getSize() < size)
			buff->setData(size, data);
		else
			buff->setData(size, data);
	}
}

void StripeRenderer::Insert(RenderableStripe* renderable) {
	for (auto& s : States) {
		if (s->stripes.size() < MAX_STRIPE_COUNT && s->lifetime == renderable->lifetime) {
			s->stripes.insert(renderable);
			s->vertexCount++;
			s->resetVertices = true;
			StripeMap[renderable] = s;
			return;
		}
	}
	RenderState* state = new RenderState(renderable->lifetime);
	state->vertexCount++;
	state->resetVertices = true;
	state->stripes.insert(renderable);
	StripeMap[renderable] = state;
	States.insert(state);
}

void StripeRenderer::Erase(RenderableStripe* renderable) {
	RenderState* state = StripeMap[renderable];
	if (state->vertexCount == 1) {
		delete state;
		States.erase(state);
	}
	else {
		state->stripes.erase(renderable);
		state->resetVertices = true;
		state->vertexCount--;
	}
	StripeMap.erase(renderable);
}

size_t StripeRenderer::DrawCount()
{
	return StripeMap.size();
}

void StripeRenderer::Clear() {
	std::vector<RenderableStripe*> stripes;
	stripes.resize(StripeMap.size());
	size_t i = 0;
	for (auto s : StripeMap) {
		stripes[i] = s.first;
		++i;
	}
	for (auto s : stripes) {
		delete s;
	}
}

void StripeRenderer::Render(const LocationContext& context) {
	Shader* shader = Resources::Shaders.get("assets/renderStripes.shader");
	shader->use();
	shader->setMatrix4("projection", context.projMatrix);
	shader->setMatrix4("view", context.viewMatrix);

	for (auto s : States) {
		s->render(context);
	}
}

StripeRenderer::RenderState::RenderState(Drawable::Lifetime lifetime) : lifetime(lifetime) {
	vertexArray = new VertexArray();
	vertexArray->bind();

	dynamicVertBuff = new VertexBuffer<DynamicDraw>();
	dynamicVertBuff->bind();
	vertexArray->setConfig(0, 4, GL_FLOAT, GL_FALSE, 16 * sizeof(float), 0 * sizeof(float));
	vertexArray->setConfig(1, 4, GL_FLOAT, GL_FALSE, 16 * sizeof(float), 4 * sizeof(float));
	vertexArray->setConfig(2, 4, GL_FLOAT, GL_FALSE, 16 * sizeof(float), 8 * sizeof(float));
	vertexArray->setConfig(3, 4, GL_FLOAT, GL_FALSE, 16 * sizeof(float), 12 * sizeof(float));

	staticVertBuff = new VertexBuffer<StaticDraw>();
	staticVertBuff->bind();
	vertexArray->setConfig(4, 3, GL_FLOAT, GL_FALSE, 10 * sizeof(float), 0 * sizeof(float));
	vertexArray->setConfig(5, 3, GL_FLOAT, GL_FALSE, 10 * sizeof(float), 3 * sizeof(float));
	vertexArray->setConfig(6, 4, GL_FLOAT, GL_FALSE, 10 * sizeof(float), 6 * sizeof(float));
}

StripeRenderer::RenderState::~RenderState() {
	delete dynamicVertBuff;
	delete staticVertBuff;
	delete vertexArray;
}

void StripeRenderer::RenderState::render(const LocationContext& context)
{
	vertexArray->bind();
	Vertex::DynamicSeg* dSegs = new Vertex::DynamicSeg[vertexCount];
	size_t s_count = 0;

	if (resetVertices) {
		Vertex::StaticSeg* sSegs = new Vertex::StaticSeg[vertexCount];
		for (auto s : stripes) {
			dSegs[s_count] = { s->paused() ? Matrix4() : context.modelMatrices->at(s->gameObject) };
			sSegs[s_count] = { s->line.u, s->line.v, s->colour };
			++s_count;
		}
		updateBuffer(staticVertBuff, sizeof(Vertex::StaticSeg) * vertexCount, sSegs);
		delete[] sSegs;
		resetVertices = false;
	}
	else {
		for (auto s : stripes) {
			dSegs[s_count] = { s->paused() ? Matrix4() : context.modelMatrices->at(s->gameObject) };
			++s_count;
		}
	}
	updateBuffer(dynamicVertBuff, sizeof(Vertex::DynamicSeg) * vertexCount, dSegs);
	delete[] dSegs;

	glCall(glDrawArrays(GL_POINTS, 0, (GLsizei)vertexCount));
}

RenderableStripe::RenderableStripe(const Stripe& stripe, const GameObject* gameObject, RenderSession::Binding* binding) : Renderable(binding, gameObject), Stripe(stripe) {
	StripeRenderer::Insert(this);
}

RenderableStripe::~RenderableStripe() {
	StripeRenderer::Erase(this);
}