#pragma once
#include <Font.h>
#include <Transform.h>
#include <Rect.h>

class Text {
public:
	enum class Align {
		Left,
		Centre,
		Right
	};

	Font* font;
	std::wstring string;
	uint size;
	Align align;

	Transform transform;
	Rect rect;
	Vector4 colour;

	Text();
	Text(const std::wstring& string, Font* font);
	Text(const std::string& string, Font* font);
};