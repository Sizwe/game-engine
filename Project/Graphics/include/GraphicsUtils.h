#pragma once
#include <SDL.h>
#include <Macros.h>
#include <cstring>
#include <vector>
#include <map>
#include <iostream>

namespace {
	inline size_t calcPitch(size_t width, uint bytesPerPixel, uint byteAlignment = 4) {
		return ((width * bytesPerPixel + byteAlignment - 1) / byteAlignment) * byteAlignment;
	}

	inline size_t calcImageSize(size_t width, size_t height, uint bytesPerPixel, uint byteAlignment = 4) {
		return height * calcPitch(width, bytesPerPixel, byteAlignment);
	}

	inline void flipImageX(byte* data, size_t width, size_t height, uint bytesPerPixel, uint byteAlignment = 4) {
		size_t pitch = calcPitch(width, bytesPerPixel, byteAlignment);
		byte* l1 = data;
		byte* l2 = l1 + pitch * (height - 1);
		byte* temp = new byte[pitch];
		for (; l1 < l2; l1 += pitch, l2 -= pitch) {
			std::memcpy(temp, l1, pitch);
			std::memcpy(l1, l2, pitch);
			std::memcpy(l2, temp, pitch);
		}
		delete[] temp;
	}

	inline void flipImageY(byte* data, size_t width, size_t height, uint bytesPerPixel, uint byteAlignment = 4) {
		size_t pitch = calcPitch(width, bytesPerPixel, byteAlignment);
		byte* l = data;
		byte* end = l + pitch * height;
		byte* temp = new byte[bytesPerPixel];
		for (; l < end; l += pitch) {
			byte* a = l;
			byte* b = l + pitch - bytesPerPixel;
			for (; a < b; a += bytesPerPixel, b -= bytesPerPixel) {
				std::memcpy(temp, a, bytesPerPixel);
				std::memcpy(a, b, bytesPerPixel);
				std::memcpy(b, temp, bytesPerPixel);
			}
		}
		delete[] temp;
	}

	inline void flipImageXY(byte* data, size_t width, size_t height, uint bytesPerPixel, uint byteAlignment = 4) {
		size_t pitch = calcPitch(width, bytesPerPixel, byteAlignment);
		byte* a = data;
		byte* b = a + pitch * (height - 1) - bytesPerPixel;
		byte* temp = new byte[bytesPerPixel];
		for (; a < b; a += bytesPerPixel, b -= bytesPerPixel) {
			std::memcpy(temp, a, bytesPerPixel);
			std::memcpy(a, b, bytesPerPixel);
			std::memcpy(b, temp, bytesPerPixel);
		}
		delete[] temp;
	}

	inline void flipSurfaceX(SDL_Surface* surface) {
		if (SDL_MUSTLOCK(surface)) {
			if (SDL_LockSurface(surface) < 0)
				return;
		}
		byte* l1 = (byte*)surface->pixels;
		byte* l2 = l1 + surface->pitch * (size_t(surface->h) - 1);
		byte* temp = new byte[surface->pitch];
		for (; l1 < l2; l1 += surface->pitch, l2 -= surface->pitch) {
			std::memcpy(temp, l1, surface->pitch);
			std::memcpy(l1, l2, surface->pitch);
			std::memcpy(l2, temp, surface->pitch);
		}
		delete[] temp;
		if (SDL_MUSTLOCK(surface))
			SDL_UnlockSurface(surface);
	}

	inline void flipSurfaceY(SDL_Surface* surface) {
		if (SDL_MUSTLOCK(surface)) {
			if (SDL_LockSurface(surface) < 0) 
				return;
		}
		u8 bps = surface->format->BytesPerPixel;
		byte* l = (byte*)surface->pixels;
		byte* end = l + surface->pitch * size_t(surface->h);
		byte* temp = new byte[bps];
		for (; l < end; l += surface->pitch) {
			byte* a = l;
			byte* b = l + surface->pitch - bps;
			for (; a < b; a += bps, b -= bps) {
				std::memcpy(temp, a, bps);
				std::memcpy(a, b, bps);
				std::memcpy(b, temp, bps);
			}
		}
		delete[] temp;
		if (SDL_MUSTLOCK(surface))
			SDL_UnlockSurface(surface);
	}

	inline void flipSurfaceXY(SDL_Surface* surface) {
		if (SDL_MUSTLOCK(surface)) {
			if (SDL_LockSurface(surface) < 0)
				return;
		}
		const u8 bps = surface->format->BytesPerPixel;
		byte* a = (byte*)surface->pixels;
		byte* b = a + surface->pitch * (size_t(surface->h) - 1) - bps;
		byte* temp = new byte[bps];
		for (; a < b; a += bps, b -= bps) {
			std::memcpy(temp, a, bps);
			std::memcpy(a, b, bps);
			std::memcpy(b, temp, bps);
		}
		delete[] temp;
		if (SDL_MUSTLOCK(surface))
			SDL_UnlockSurface(surface);
	}

	TexFormat toTexFormat(const SDL_PixelFormat& sdlFormat) {
		auto bytesPerPixel = sdlFormat.BytesPerPixel;
		if (bytesPerPixel == 4) {
			if (sdlFormat.Rmask == 0x000000ff)
				return TexFormat::RGBA;
			else
				return TexFormat::BGRA;
		}
		else if (bytesPerPixel == 3) {
			if (sdlFormat.Rmask == 0x000000ff)
				return TexFormat::RGB;
			else
				return TexFormat::BGR;
		}
		else if (bytesPerPixel == 2) {
			return TexFormat::RG;
		}
		else if (bytesPerPixel == 1) {
			return TexFormat::R;
		}
		else {
			std::cerr << "unable to convert SDL_PixelFormat to TexFormat";
			return TexFormat::RGBA;
		}
	}

	inline static uint bytesPerPixel(TexFormat texFormat) {
		switch (texFormat) {
		case TexFormat::R: return 1;
		case TexFormat::RG: return 2;
		case TexFormat::RGB: case TexFormat::BGR: return 3;
		case TexFormat::RGBA: case TexFormat::BGRA: return 4;
		default: return 0;
		}
	}

	unsigned int* getQuadIndices(size_t quadCount) {
		static std::vector<unsigned int> buffer;
		const size_t reqSize = quadCount * 6;
		const size_t currSize = buffer.size();
		if (reqSize > currSize) {
			buffer.resize(reqSize);
			for (size_t i = currSize / 6; i < quadCount; ++i) {
				size_t ii = i * 6;
				unsigned int iv = unsigned int(i * 4);
				buffer[ii + 0] = iv;
				buffer[ii + 1] = iv + 1;
				buffer[ii + 2] = iv + 2;
				buffer[ii + 3] = iv + 2;
				buffer[ii + 4] = iv + 3;
				buffer[ii + 5] = iv;
			}
		}
		return buffer.data();
	}
}
