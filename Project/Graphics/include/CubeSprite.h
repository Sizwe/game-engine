#pragma once
#include <Drawable.h>
#include <CubeMap.h>
#include <Box.h>

class CubeSprite : public DrawablePlane {
public:
	CubeMap* cubeMap;
	Box box;
	Vector4 colour;

	CubeSprite(CubeMap* cubeMap = nullptr, const Box& box = Box(100.0f), const Vector4& colour = Vector4(1.0f));
};