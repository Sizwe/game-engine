#pragma once
#include <Transform.h>
#include <Material.h>

class Drawable {
public:
	enum class Lifetime {
		Long = 0,
		Short = 1
	};
	Lifetime lifetime;
	Drawable() : lifetime(Lifetime::Short) {}
};

class DrawablePlane : public Drawable {
public:
	enum class Facing {
		Front = 0,
		Back = 1,
		FrontAndBack = 2
	};
	Facing facing;
	Transform transform;
	Material material;
	DrawablePlane() : facing(Facing::Front) {}
};

class MonoDrawable : public DrawablePlane {
public:
	enum class Mode {
		Solid = 0,
		Edge = 1,
		Wireframe = 2
	};
	Mode mode;	
	MonoDrawable() : mode(Mode::Solid) {}
};