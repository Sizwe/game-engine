#pragma once

#include <TextureEnums.h>
#include <Macros.h>
#include <string>
#include <vector>
#include <initializer_list>
#include <Vector4.h>

class Texture {
private:
	uint ID;
	int width;
	int height;
	TexFormat format;
	TexWrap wrap[2];
	TexFilter minFilter;
	TexFilter magFilter;

public:
	Texture();
	Texture(const std::string& directory);
	Texture(int width, int height, TexFormat format = TexFormat::RGBA, TexDataType type = TexDataType::UByte);
	Texture(const byte* data, int width, int height, TexFormat format = TexFormat::RGBA, TexDataType type = TexDataType::UByte);
	Texture& operator= (const Texture&) = delete;
	Texture(const Texture&) = delete;
	~Texture();

	uint getID() const;
	void bind() const;
	void bindToUnit(int index) const;
	void setWrap(const TexWrap mode);
	void setWrap(const TexWrapAxis wrapAxis, const TexWrap mode);
	void setFilter(const TexFilter mode);
	void setFilter(const TexFilterOperation operation, const TexFilter mode);
	TexFilter getFilter(const TexFilterOperation operation) const;
	TexWrap getWrap(const TexWrapAxis wrapAxis) const;
	TexFormat getFormat() const;

	int getWidth() const;
	int getHeight() const;
	std::vector<byte> getData() const;
};