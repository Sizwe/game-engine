#pragma once

#include <Macros.h>
#include <glad/glad.h>
#include <CubeMap.h>
#include <Vector.h>
#include <Matrix.h>
#include <string>

class Shader {
private:
	uint ID;
public:
	Shader();
	Shader(const std::string& directory);
	Shader& operator= (const Shader&) = delete;
	Shader(const Shader&) = delete;
	~Shader();

	Shader& compile(const std::string& code);
	uint getID() const { return ID; }
	void use();

	void setMatrix4(const std::string& name, const float* matrix, size_t count = 1);
	void setMatrix3(const std::string& name, const float* matrix, size_t count = 1);
	void setMatrix2(const std::string& name, const float* matrix, size_t count = 1);
	void setFloat4(const std::string& name, float v1, float v2, float v3, float v4);
	void setFloat3(const std::string& name, float v1, float v2, float v3);
	void setFloat2(const std::string& name, float v1, float v2);
	void setFloat(const std::string& name, float value);
	void setInt(const std::string& name, int value);
	void setInt2(const std::string& name, int v1, int v2);
	void setInt3(const std::string& name, int v1, int v2, int v3);
	void setInt4(const std::string& name, int v1, int v2, int v3, int v4);
	void setTex(const std::string& name, const Texture* texture, int unit);
	void setCubeMap(const std::string& name, const CubeMap* cubeMap, int unit);
	void setBool(const std::string& name, bool value);

	void setFloat(const std::string& name, const float* values, size_t count);
	void setFloat2(const std::string& name, const float* values, size_t count);
	void setFloat3(const std::string& name, const float* values, size_t count);
	void setFloat4(const std::string& name, const float* values, size_t count);
	void setInt(const std::string& name, const int* values, size_t count);
	void setInt2(const std::string& name, const int* values, size_t count);
	void setInt3(const std::string& name, const int* values, size_t count);
	void setInt4(const std::string& name, const int* values, size_t count);

	void setMatrix4(const std::string& name, const Matrix4& matrix);
	void setMatrix3(const std::string& name, const Matrix3& matrix);
	void setMatrix2(const std::string& name, const Matrix2& matrix);
	void setFloat4(const std::string& name, const Vector4& vector);
	void setFloat3(const std::string& name, const Vector3& vector);
	void setFloat2(const std::string& name, const Vector2& vector);

	void setUniformBlockBinding(const std::string& name, uint index);
};