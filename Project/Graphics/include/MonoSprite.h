#pragma once
#include <Drawable.h>
#include <Rect.h>

class MonoSprite : public MonoDrawable {
public:
	Vector4 colour;
	Rect rect;

	MonoSprite(const Rect& rect, const Vector4& colour = Vector4(0.5f));
};