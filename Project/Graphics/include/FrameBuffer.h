#pragma once

#include <Macros.h>
#include <CubeMap.h>
#include <glad/glad.h>
#include <vector>

template <class TexT>
class BaseFrameBuffer {
private:
	uint ID;
	std::vector<TexT*> colourBuffs;
	TexT* depthBuff;
	TexT* stencilBuff;
public:
	BaseFrameBuffer();
	BaseFrameBuffer(uint width, uint height, TexFormat format);
	~BaseFrameBuffer();

	uint getID() const;
	void bind() const;

	TexT* getColourBuff(uint attachment = 0);
	TexT*  getDepthBuff();
	TexT*  getStencilBuff();
	const TexT* getColourBuff(uint attachment = 0) const;
	const TexT* getDepthBuff() const;
	const TexT* getStencilBuff() const;
	void insertColourBuff(TexT* texture, uint attachment = 0);
	void insertDepthBuff(TexT* texture);
	void insertStencilBuff(TexT* texture);

	TexT* ejectColourBuff(uint attachment = 0);
	TexT* ejectDepthBuff();
	TexT* ejectStencilBuff();
	void clear();

	bool complete();
};

using FrameBuffer = BaseFrameBuffer<Texture>;
using CubeFrameBuffer = BaseFrameBuffer<CubeMap>;


template<class TexT>
inline BaseFrameBuffer<TexT>::BaseFrameBuffer() : ID(0) {
	glGenFramebuffers(1, &ID);
	depthBuff = nullptr;
	stencilBuff = nullptr;
};

template<class TexT>
inline BaseFrameBuffer<TexT>::BaseFrameBuffer(uint width, uint height, TexFormat format) : BaseFrameBuffer<TexT>::BaseFrameBuffer()
{
	bind();
	colourBuffs.push_back(new TexT(width, height, format));
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, colourBuffs[0]->getID(), 0);

	GLenum drawBuffer = GL_COLOR_ATTACHMENT0;
	glDrawBuffers(1, &drawBuffer);
}

template<class TexT>
inline BaseFrameBuffer<TexT>::~BaseFrameBuffer()
{
	glDeleteFramebuffers(1, &ID);
	delete depthBuff;
	for (auto colour : colourBuffs) {
		delete colour;
	}
	delete stencilBuff;
}

template<class TexT>
inline uint BaseFrameBuffer<TexT>::getID() const
{
	return ID;
}

template<class TexT>
inline void BaseFrameBuffer<TexT>::bind() const
{
	glBindFramebuffer(GL_FRAMEBUFFER, ID);
}

template<class TexT>
inline TexT* BaseFrameBuffer<TexT>::getColourBuff(uint attachment)
{
	if (attachment < colourBuffs.size()) {
		return colourBuffs[attachment];
	}
	return nullptr;
}

template<class TexT>
inline TexT* BaseFrameBuffer<TexT>::getDepthBuff()
{
	return depthBuff;
}

template<class TexT>
inline TexT* BaseFrameBuffer<TexT>::getStencilBuff()
{
	return stencilBuff;
}

template<class TexT>
inline const TexT* BaseFrameBuffer<TexT>::getColourBuff(uint attachment) const
{
	if (attachment < colourBuffs.size()) {
		return colourBuffs[attachment];
	}
}

template<class TexT>
inline const TexT* BaseFrameBuffer<TexT>::getDepthBuff() const
{
	return depthBuff;
}

template<class TexT>
inline const TexT* BaseFrameBuffer<TexT>::getStencilBuff() const
{
	return stencilBuff;
}

template<class TexT>
inline void BaseFrameBuffer<TexT>::insertColourBuff(TexT* texture, uint attachment)
{
	if (colourBuffs.size() <= attachment) {
		colourBuffs.resize(size_t(attachment + 1));
	}
	colourBuffs[attachment] = texture;
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + attachment, texture->getID(), 0);
	GLenum* drawBuffers = new GLenum[colourBuffs.size()];
	for (size_t i = 0; i < colourBuffs.size(); ++i) {
		drawBuffers[i] = GLenum(GL_COLOR_ATTACHMENT0 + i);
	}
	glDrawBuffers((GLsizei)colourBuffs.size(), drawBuffers);
	delete[] drawBuffers;
}

template<class TexT>
inline void BaseFrameBuffer<TexT>::insertDepthBuff(TexT* texture)
{
	depthBuff = texture;
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, texture->getID(), 0);
}

template<class TexT>
inline void BaseFrameBuffer<TexT>::insertStencilBuff(TexT* texture)
{
	stencilBuff = texture;
	glFramebufferTexture(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, texture->getID(), 0);
}

template<class TexT>
inline TexT* BaseFrameBuffer<TexT>::ejectColourBuff(uint attachment)
{
	if (colourBuffs.size() > attachment) {
		TexT* retTex = colourBuffs[attachment];

		colourBuffs[attachment] = nullptr;
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + attachment, 0, 0);
		return retTex;
	}
	return nullptr;
}

template<class TexT>
inline TexT* BaseFrameBuffer<TexT>::ejectDepthBuff()
{
	TexT* retTex = depthBuff;
	depthBuff = nullptr;
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, 0, 0);
	return retTex;
}

template<class TexT>
inline TexT* BaseFrameBuffer<TexT>::ejectStencilBuff()
{
	TexT* retTex = stencilBuff;
	stencilBuff = nullptr;
	glFramebufferTexture(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, 0, 0);
	return retTex;
}

template<class TexT>
inline void BaseFrameBuffer<TexT>::clear()
{
	for (uint i = 0; i < colourBuffs.size(); ++i) {
		delete ejectColourBuff(i);
	}
	delete ejectDepthBuff;
	delete ejectStencilBuff;
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
}

template<class TexT>
inline bool BaseFrameBuffer<TexT>::complete()
{
	return glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE;
}
