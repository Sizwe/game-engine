#pragma once
#include <Texture.h>
#include <Rect.h>
#include <Drawable.h>

class Sprite : public DrawablePlane {
public:
	Texture* texture;
	Rect crop;
	Vector4 colour;

	Sprite(Texture* texture = nullptr, const Vector4& colour = Vector4(1.0f));
};