#pragma once
#include <Macros.h>
#include <vector>
#include <string>

class Font {
private:
	std::vector<byte> data;

public:
	Font(const std::string& directory);
	Font(const byte* data, size_t size);

	const std::vector<byte>& getData() const;
};