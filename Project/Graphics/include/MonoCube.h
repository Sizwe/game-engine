#pragma once
#include <Drawable.h>
#include <Box.h>

class MonoCube : public MonoDrawable {
public:
	struct Colour {
		Vector4 left;
		Vector4 right;
		Vector4 top;
		Vector4 bottom;
		Vector4 front;
		Vector4 back;

		Colour(const Vector4& left, const Vector4& right, const Vector4& up, const Vector4& down, const Vector4& front, const Vector4& back);
		Colour(const Vector4& x, const Vector4& y, const Vector4& z) : Colour(x, x, y, y, z, z) {}
		Colour(const Vector4& colour = Vector4(0.5f)) : Colour(colour, colour, colour) {}
	};
	Box box;
	Colour colour;

	MonoCube(const Box& box, const Colour& colour = Vector4(0.5f));
};