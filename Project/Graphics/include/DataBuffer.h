#pragma once

#include <Macros.h>
#include <glad/glad.h>
#include <BufferEnums.h>
#include <Error.h>

template <GLenum GLType, BufferUsage vBuffUsage = BufferUsage::StaticDraw>
class BaseBuffer {
protected:
	uint ID;
	size_t size;

public:
	BaseBuffer();
	~BaseBuffer();

	void setData(size_t size, const void* data);
	void subData(size_t byteOffset, size_t size, const void* data);
	size_t getSize() const;
	uint getID() const;
	void bind() const;
};

template <GLenum GLType, BufferUsage vBuffUsage = BufferUsage::StaticDraw>
class AdvancedBuffer : public BaseBuffer<GLType, vBuffUsage> {
public:
	void bindBlock(uint index);
	void bindBlockRanged(uint index, size_t offset, size_t size);
};

template<BufferUsage vBuffUsage = BufferUsage::StaticDraw>
using VertexBuffer = BaseBuffer<GL_ARRAY_BUFFER, vBuffUsage>;

template<BufferUsage vBuffUsage = BufferUsage::StaticDraw>
using ElementBuffer = BaseBuffer<GL_ELEMENT_ARRAY_BUFFER, vBuffUsage>;

template<BufferUsage vBuffUsage = BufferUsage::StaticDraw>
using UniformBuffer = AdvancedBuffer<GL_UNIFORM_BUFFER, vBuffUsage>;

template <GLenum GLType, BufferUsage vBuffUsage>
inline BaseBuffer<GLType, vBuffUsage>::BaseBuffer() {
	glCall(glGenBuffers(1, &ID));
	size = 0;
}

template<GLenum GLType, BufferUsage vBuffUsage>
inline BaseBuffer<GLType, vBuffUsage>::~BaseBuffer()
{
	glDeleteBuffers(1, &ID);
}

template <GLenum GLType, BufferUsage vBuffUsage>
inline void BaseBuffer<GLType, vBuffUsage>::setData(size_t size, const void* data)
{
	glCall(glBufferData(GLType, size, data, (GLenum)vBuffUsage));
	this->size = size;
}

template <GLenum GLType, BufferUsage vBuffUsage>
inline void BaseBuffer<GLType, vBuffUsage>::subData(size_t byteOffset, size_t size, const void* data)
{
	glBufferSubData(GLType, byteOffset, size, data);
}

template<GLenum GLType, BufferUsage vBuffUsage>
inline size_t BaseBuffer<GLType, vBuffUsage>::getSize() const
{
	return size;
}

template<GLenum GLType, BufferUsage vBuffUsage>
inline uint BaseBuffer<GLType, vBuffUsage>::getID() const
{
	return ID;
}

template<GLenum GLType, BufferUsage vBuffUsage>
inline void BaseBuffer<GLType, vBuffUsage>::bind() const
{
	glCall(glBindBuffer(GLType, ID));
}

template<GLenum GLType, BufferUsage vBuffUsage>
inline void AdvancedBuffer<GLType, vBuffUsage>::bindBlock(uint index)
{
	glBindBufferBase(GLType, 0, this->ID);
}
template<GLenum GLType, BufferUsage vBuffUsage>
inline void AdvancedBuffer<GLType, vBuffUsage>::bindBlockRanged(uint index, size_t offset, size_t size)
{
	glBindBufferRange(GLType, 0, this->ID, offset, size);
}