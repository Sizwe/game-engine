#pragma once
#include <Line.h>
#include <Drawable.h>

class Stripe : public Drawable {
public:
	Line line;
	Vector4 colour;

	Stripe(const Line& line, const Vector4& colour = Vector4(0.5f)) : line(line), colour(colour) {}
};