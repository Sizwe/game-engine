#pragma once
#include <Texture.h>

class CubeMap {
private:
	struct FaceData {
		int width = 0;
		int height = 0;
		TexFormat format = TexFormat::RGB;
	};
	uint ID;
	FaceData faceData[6];
	TexWrap wrap[3];
	TexFilter minFilter;
	TexFilter magFilter;

public:
	CubeMap();
	CubeMap(std::string directory);
	CubeMap(int width, int height, TexFormat format = TexFormat::RGBA, TexDataType type = TexDataType::UByte);
	CubeMap(const byte* data, int width, int height, TexFormat format = TexFormat::RGBA, TexDataType type = TexDataType::UByte);
	CubeMap(std::initializer_list<std::string> textureDirectories);
	CubeMap& operator= (const CubeMap&) = delete;
	CubeMap(const CubeMap&) = delete;
	~CubeMap();

	void setFace(CubeFace face, const std::string& directory);
	void setFace(CubeFace face, int width, int height, TexFormat format, TexDataType type = TexDataType::UByte);
	void setFace(CubeFace face, const byte* data, int width, int height, TexFormat format = TexFormat::RGBA, TexDataType type = TexDataType::UByte);
	void setFace(CubeFace face, const Texture& texture);

	static void Bind(uint ID);
	static void SetActive(int index);

	uint getID() const;
	void bind() const;
	void bindToUnit(uint index) const;

	void setWrap(const TexWrap mode);
	void setWrap(const TexWrapAxis wrapAxis, const TexWrap mode);
	void setFilter(const TexFilter mode);
	void setFilter(const TexFilterOperation operation, const TexFilter mode);
	TexWrap getWrap(const TexWrapAxis wrapAxis) const;
	TexFilter getFilter(const TexFilterOperation operation) const;

	TexFormat getFormat(CubeFace face) const;
	int getWidth(CubeFace face = CubeFace::Front) const;
	int getHeight(CubeFace face = CubeFace::Front) const;
	std::vector<byte> getData(CubeFace face) const;
	Texture* createTexture(CubeFace face) const;
};