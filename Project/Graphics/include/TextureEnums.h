#pragma once

#include <glad/glad.h>
#include <SDL.h>

enum class TexFilterOperation {
	Magnify = GL_TEXTURE_MAG_FILTER,
	Minimize = GL_TEXTURE_MIN_FILTER
};

enum class TexFilter {
	Nearest = GL_NEAREST,
	Linear = GL_LINEAR,
	NearestMipMapNearest = GL_NEAREST_MIPMAP_NEAREST,
	NearestMipMapLinear = GL_NEAREST_MIPMAP_LINEAR,
	LinearMipMapNearest = GL_LINEAR_MIPMAP_NEAREST,
	LinearMipMapLinear = GL_LINEAR_MIPMAP_LINEAR
};

enum class TexWrapAxis {
	S = GL_TEXTURE_WRAP_S,
	T = GL_TEXTURE_WRAP_T,
	R = GL_TEXTURE_WRAP_R,
	X = S,
	Y = T,
	Z = R,
};

enum class TexWrap {
	ClampToEdge = GL_CLAMP_TO_EDGE,
	ClampToBorder = GL_CLAMP_TO_BORDER,
	MirroredRepeat = GL_MIRRORED_REPEAT,
	Repeat = GL_REPEAT
};

enum class TexFormat {
	R = GL_RED,
	RG = GL_RG,
	RGB = GL_RGB,
	BGR = GL_BGR,
	RGBA = GL_RGBA,
	BGRA = GL_BGRA,
	DepthComponent = GL_DEPTH_COMPONENT,
	DepthStencil = GL_DEPTH_STENCIL,
	StencilIndex = GL_STENCIL_INDEX
};

enum class TexDataType {
	UByte = GL_UNSIGNED_BYTE,
	Byte = GL_BYTE,
	UShort = GL_UNSIGNED_SHORT,
	Short = GL_SHORT,
	UInt = GL_UNSIGNED_INT,
	Int = GL_INT,
	Float = GL_FLOAT,
	HalfFloat = GL_HALF_FLOAT
};

enum class CubeFace {
	Right = GL_TEXTURE_CUBE_MAP_POSITIVE_X,
	Left = GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
	Top = GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
	Bottom = GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
	Back = GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
	Front = GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
};
