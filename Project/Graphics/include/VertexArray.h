#pragma once

#include <Macros.h>
#include <glad/glad.h>

class VertexArray {
private:
	uint ID;
public:
	VertexArray();
	~VertexArray();
	uint getID() const;
	void bind() const;
	void setConfig(uint index, int size, GLenum type, bool normalised, int stride, size_t offset);
	void setDivisor(uint index, uint divisor);
};
