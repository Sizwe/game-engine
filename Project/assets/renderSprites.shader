#VERTEX
#version 330 core
layout(location = 0) in mat4 aModel;
layout(location = 4) in vec4 aWorldRect;
layout(location = 5) in vec4 aTexRect;
layout(location = 6) in vec4 aColour;
layout(location = 7) in int aVFlags;
layout(location = 8) in vec3 aDistortion;
layout(location = 9) in vec3 aAmbient;
layout(location = 10) in vec3 aDiffuse;
layout(location = 11) in vec3 aSpecular;
layout(location = 12) in float aShininess;

uniform mat4 view;
uniform mat4 projection;

out VS_OUT{
    mat4 projView;
    mat4 model;
    vec2 wMin;
    vec2 wMax;
    vec2 tMin;
    vec2 tMax;
    vec4 colour;
    flat int vFlags;
    vec3 distortion; // reflectiveness, refractiveness, refractionRatio
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
} vs_out;

void main()
{
    vs_out.projView = projection * view;
    vs_out.model = aModel;
    vs_out.wMin = aWorldRect.xy;
    vs_out.wMax = aWorldRect.zw;
    vs_out.tMin = aTexRect.xy;
    vs_out.tMax = aTexRect.zw;
    vs_out.colour = aColour;
    vs_out.vFlags = aVFlags;
    vs_out.distortion = aDistortion; // reflectiveness, refractiveness, refractionRatio
    vs_out.ambient = aAmbient;
    vs_out.diffuse = aDiffuse;
    vs_out.specular = aSpecular;
    vs_out.shininess = aShininess;
}

#GEOMETRY
#version 330 core
layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

#define flagged(bitField, flag) (((1 << (flag)) & bitField) != 0)
#define BACK_FACE_FLAG 2


in VS_OUT {
    mat4 projView;
    mat4 model;
    vec2 wMin;
    vec2 wMax;
    vec2 tMin;
    vec2 tMax;
    vec4 colour;
    flat int vFlags;
    vec3 distortion;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
} gs_in[];

out GS_OUT{
    vec2 texCoord;
    vec3 worldPos;
    vec3 normal;
    vec4 colour;
    flat int vFlags;
    vec3 distortion;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
} gs_out;

void main() {
    vec4 worldCoords[4] = { 
        gs_in[0].model * vec4(gs_in[0].wMin.x, gs_in[0].wMax.y, 0.0, 1.0),
        gs_in[0].model * vec4(gs_in[0].wMin.x, gs_in[0].wMin.y, 0.0, 1.0),
        gs_in[0].model * vec4(gs_in[0].wMax.x, gs_in[0].wMax.y, 0.0, 1.0),
        gs_in[0].model * vec4(gs_in[0].wMax.x, gs_in[0].wMin.y, 0.0, 1.0)
    };
    vec2 texCoords[4] = {
        vec2(gs_in[0].tMin.x, gs_in[0].tMax.y),
        vec2(gs_in[0].tMin.x, gs_in[0].tMin.y),
        vec2(gs_in[0].tMax.x, gs_in[0].tMax.y),
        vec2(gs_in[0].tMax.x, gs_in[0].tMin.y)
    };

    gs_out.normal = normalize(cross(worldCoords[1].xyz - worldCoords[0].xyz, worldCoords[2].xyz - worldCoords[0].xyz));
    if (flagged(gs_in[0].vFlags, BACK_FACE_FLAG)) {
        gs_out.normal = -gs_out.normal;
    }

    gs_out.colour = gs_in[0].colour;
    gs_out.vFlags = gs_in[0].vFlags;
    gs_out.distortion = gs_in[0].distortion;
    gs_out.ambient = gs_in[0].ambient;
    gs_out.diffuse = gs_in[0].diffuse;
    gs_out.specular = gs_in[0].specular;
    gs_out.shininess = gs_in[0].shininess;

    for (int i = 0; i < 4; ++i) {
        gl_Position = gs_in[0].projView * worldCoords[i];
        gs_out.worldPos = worldCoords[i].xyz;
        gs_out.texCoord = texCoords[i];
        EmitVertex();
    }
    EndPrimitive();
}

#FRAGMENT
#version 330 core
out vec4 fragColour;

#define flagged(bitField, flag) (((1 << (flag)) & bitField) != 0)
#define clamped(x, min, max) bool(step(min, x) - step(max, x))
#define PS_CIRCLE_RADIUS 0.5

#define REFLECTION_COUNT 4
#define POINT_LIGHT_COUNT 4
#define DIR_LIGHT_COUNT 2

//lightFlags
#define REFLECTION_ENABLED_FLAG 0
#define POINT_LIGHT_FLAG 4
#define DIR_LIGHT_FLAG 8
#define POINT_SHAD_FLAG 10
#define DIR_SHAD_FLAG 14

//vertexFlags
#define MATERIAL_ENABLED_FLAG 0
#define FRONT_FACE_FLAG 1
#define BACK_FACE_FLAG 2
#define REFLECTION_INDEX_FLAG 4

struct PointLight {
    vec3 position;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    vec3 attenuation;
};

struct DirLight {
    vec3 direction;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

struct DirShad {
    sampler2D map;
    mat4 lightLocal;
};

struct PointShad {
    samplerCube map;
    float span;
};

in GS_OUT {
    vec2 texCoord;
    vec3 worldPos;
    vec3 normal;
    vec4 colour;
    flat int vFlags;
    vec3 distortion;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
} fs_in;

uniform samplerCube reflections[REFLECTION_COUNT];

uniform DirLight dirLights[DIR_LIGHT_COUNT];
uniform PointLight pointLights[POINT_LIGHT_COUNT];

uniform DirShad dirShads[DIR_LIGHT_COUNT];
uniform PointShad pointShads[POINT_LIGHT_COUNT];

uniform int lFlags;

uniform sampler2D tex;
uniform vec3 camPos;

float dirSamples[DIR_LIGHT_COUNT * 4];
const vec2 dirSampleOffsets[4] = vec2[4]( vec2(1, 1), vec2(-1, -1), vec2(1, -1), vec2(-1, 1) );

float pointSamples[POINT_LIGHT_COUNT * 8];
const vec3 pointSampleOffsets[8] = vec3[8](
    vec3(1, 1, 1) * PS_CIRCLE_RADIUS, vec3(-1, -1, -1) * PS_CIRCLE_RADIUS, vec3(1, -1, -1) * PS_CIRCLE_RADIUS, vec3(-1, 1, 1) * PS_CIRCLE_RADIUS,
    vec3(1, -1, 1) * PS_CIRCLE_RADIUS, vec3(-1, 1, -1) * PS_CIRCLE_RADIUS, vec3(-1, -1, 1) * PS_CIRCLE_RADIUS, vec3(1, 1, -1) * PS_CIRCLE_RADIUS
);

void allocDirShadows() {
    for (int i = 0; i < DIR_LIGHT_COUNT; ++i) {
        if (!flagged(lFlags, DIR_SHAD_FLAG + i))
            continue;

        vec4 lightLocalPos = dirShads[i].lightLocal * vec4(fs_in.worldPos, 1.0f);
        vec3 lightProjCoords = lightLocalPos.xyz / lightLocalPos.w;
        lightProjCoords = lightProjCoords * 0.5 + 0.5;

        int mapIndex = i * 4;
        if (lightProjCoords.z > 1.0) {
            for (int j = 0; j < 4; ++j) {
                dirSamples[mapIndex + j] = -10000.0;
            }
        }
        else {
            float closestDepth = texture(dirShads[i].map, lightProjCoords.xy).r;
            float currentDepth = lightProjCoords.z;
            vec2 texelSize = 1.0 / textureSize(dirShads[i].map, 0);
            for (int j = 0; j < 4; ++j) {
                vec2 mapTexCoords = lightProjCoords.xy + dirSampleOffsets[j] * texelSize;
                if (!clamped(mapTexCoords.x, 0.0, 1.0) || !clamped(mapTexCoords.y, 0.0, 1.0)) {
                    dirSamples[mapIndex + j] = -10000.0;
                }
                else {
                    float pcfDepth = texture(dirShads[i].map, mapTexCoords).r;
                    dirSamples[mapIndex + j] = currentDepth - pcfDepth;
                }
            }
        }
    }
}

void allocPointShadows() {
    for (int i = 0; i < POINT_LIGHT_COUNT; ++i) {
        if(!flagged(lFlags, POINT_SHAD_FLAG + i))
            continue;

        vec3 fragToLight = fs_in.worldPos - pointLights[i].position;
        if (length(fragToLight) > pointShads[i].span) {
            for (int j = 0; j < 8; ++j) {
                pointSamples[i * 8 + j] = 10000.0;
            }
        }
        else {
            for (int j = 0; j < 8; ++j) {
                vec3 offset = fragToLight + pointSampleOffsets[j];
                float currentDepth = length(offset);
                if (currentDepth > pointShads[i].span) {
                    pointSamples[i * 8 + j] = 10000.0;
                }
                else {
                    float closestDepth = texture(pointShads[i].map, offset).r * pointShads[i].span;
                    pointSamples[i * 8 + j] = currentDepth - closestDepth - 0.005 * currentDepth;
                }
            }
        }
    }
}

vec3 calcDirLights() {
    vec3 sum;
    for (int i = 0; i < DIR_LIGHT_COUNT; ++i) {
        if (!flagged(lFlags, DIR_LIGHT_FLAG + i))
            continue;

        // ambient
        vec3 ambient = dirLights[i].ambient * fs_in.ambient;

        // diffuse 
        vec3 lightDir = normalize(-dirLights[i].direction);
        float diff = max(dot(fs_in.normal, lightDir), 0.0);
        vec3 diffuse = dirLights[i].diffuse * (diff * fs_in.diffuse);

        // specular
        vec3 viewDir = normalize(camPos - fs_in.worldPos);
        float spec = 0.0;
        if (fs_in.shininess <= 1.0) {
            vec3 halfwayDir = normalize(lightDir + viewDir);
            spec = pow(max(dot(fs_in.normal, halfwayDir), 0.0), fs_in.shininess);
        }
        else {
            vec3 reflectDir = reflect(-lightDir, fs_in.normal);
            spec = pow(max(dot(viewDir, reflectDir), 0.0), fs_in.shininess);
        }
        vec3 specular = dirLights[i].specular * (spec * fs_in.specular);

        //shadow
        float shadow = 0.0;
        float bias = max(0.05 * (1.0 - dot(fs_in.normal, lightDir)), 0.005);

        if (flagged(lFlags, DIR_SHAD_FLAG + i)) {
            int mapIndex = i * 4;
            for (int j = 0; j < 4; ++j) {
                shadow += (dirSamples[mapIndex + j] - bias >= 0.0) ? 1.0 / 4.0 : 0.0;
            }
        }
       // shadow = 0.0f;
        sum += ambient + (1.0 - shadow) * (diffuse + specular);
    }
    return sum;
}

vec3 calcPointLights() {
    vec3 sum;
    for (int i = 0; i < POINT_LIGHT_COUNT; ++i) {
        if (!flagged(lFlags, POINT_LIGHT_FLAG + i))
            continue;

        // ambient
        vec3 ambient = pointLights[i].ambient * fs_in.ambient;

        // diffuse 
        vec3 lightDir = normalize(pointLights[i].position - fs_in.worldPos);
        float diff = max(dot(fs_in.normal, lightDir), 0.0);
        vec3 diffuse = pointLights[i].diffuse * (diff * fs_in.diffuse);

        // specular
        vec3 viewDir = normalize(camPos - fs_in.worldPos);
        float spec = 0.0;
        if (fs_in.shininess <= 1.0) {
            vec3 halfwayDir = normalize(lightDir + viewDir);
            spec = pow(max(dot(fs_in.normal, halfwayDir), 0.0), fs_in.shininess);
        }
        else {
            vec3 reflectDir = reflect(-lightDir, fs_in.normal);
            spec = pow(max(dot(viewDir, reflectDir), 0.0), fs_in.shininess);
        }
        vec3 specular = pointLights[i].specular * spec * fs_in.specular;

        // attenuation
        float distance = length(pointLights[i].position - fs_in.worldPos);
        float attenuation = 1.0 / (pointLights[i].attenuation.x + pointLights[i].attenuation.y * distance + pointLights[i].attenuation.z * (distance * distance));

        //shadow 
        float shadow = 0.0;
        float bias = max(10.0f * (1.0 - dot(fs_in.normal, lightDir)), 0.005);
        
        if (flagged(lFlags, POINT_SHAD_FLAG + i)) {
            int mapIndex = i * 8;
            for (int j = 0; j < 8; ++j) {
                shadow += (pointSamples[mapIndex + j] - bias >= 0.0f) ? 1.0 / 4.0 : 0.0f;
            }
            shadow = min(shadow, 1.0f);
        }
        sum += (ambient + (1.0 - shadow) * (diffuse + specular)) * attenuation;
    }
    return sum;
}

void applyDistortions() {
    for (int i = 0; i < REFLECTION_COUNT; ++i) {
        if (!flagged(lFlags, REFLECTION_ENABLED_FLAG + i) || !flagged(fs_in.vFlags, REFLECTION_INDEX_FLAG + i))
            continue;
        vec3 toCam = normalize(fs_in.worldPos - camPos);
        if (fs_in.distortion.x > 0.0f) {
            vec3 reflection = reflect(toCam, normalize(fs_in.normal));
            fragColour = vec4(mix(fragColour.rgb, texture(reflections[i], reflection).rgb, fs_in.distortion.x), fragColour.a);
        }
        else if (fs_in.distortion.y > 0.0f) {
            vec3 refraction = refract(toCam, fs_in.normal, fs_in.distortion.z);
            fragColour = vec4(mix(fragColour.rgb, texture(reflections[i], refraction).rgb, fs_in.distortion.y), fragColour.a);
        }
    }
}

void main()
{
    if (gl_FrontFacing && flagged(fs_in.vFlags, BACK_FACE_FLAG) || !gl_FrontFacing && flagged(fs_in.vFlags, FRONT_FACE_FLAG))
        discard;

    fragColour = texture(tex, fs_in.texCoord) * fs_in.colour;
    if (fragColour.a <= 0.00001f) discard;

    if (lFlags != 0 && flagged(fs_in.vFlags, MATERIAL_ENABLED_FLAG)) {
        applyDistortions();
        allocDirShadows();
        allocPointShadows();
        vec3 fDirLight = calcDirLights();
        vec3 fPointLight = calcPointLights();
        fragColour = vec4((fDirLight + fPointLight) * fragColour.rgb, fragColour.a);
    }
}
