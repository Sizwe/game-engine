#VERTEX
#version 330 core
layout(location = 0) in mat4 aModel;
layout(location = 4) in vec3 aStart;
layout(location = 5) in vec3 aEnd;
layout(location = 6) in vec4 aColour;

uniform mat4 view;
uniform mat4 projection;

out VS_OUT {
    mat4 trans;
    vec3 start;
    vec3 end;
    vec4 colour;
} vs_out;

void main()
{
    vs_out.trans = projection * view * aModel;
    vs_out.start = aStart;
    vs_out.end = aEnd;
    vs_out.colour = aColour;
}

#GEOMETRY
#version 330 core
layout(points) in;
layout(line_strip, max_vertices = 2) out;

in VS_OUT {
    mat4 trans;
    vec3 start;
    vec3 end;
    vec4 colour;
} gs_in[];

out vec4 colour;

void main() {
    gl_Position = gs_in[0].trans * vec4(gs_in[0].start, 1.0f);
    colour = gs_in[0].colour;
    EmitVertex();

    gl_Position = gs_in[0].trans * vec4(gs_in[0].end, 1.0f);
    colour = gs_in[0].colour;
    EmitVertex();

    EndPrimitive();
}

#FRAGMENT
#version 330 core
out vec4 fragColour;

in vec4 colour;

void main()
{
    fragColour = colour;
}
