#VERTEX
#version 330 core
layout(location = 0) in vec2 aPos;
layout(location = 1) in vec2 aTexCoord;

uniform mat4 projection;
uniform mat4 view;

out vec2 texCoord;
out mat4 viewProjInv;

void main()
{
    gl_Position = vec4(aPos, 0.0f, 1.0f);
    texCoord = aTexCoord;
    viewProjInv = inverse(projection * view);
}

#FRAGMENT
#version 330 core
out vec4 fragColour;

in vec2 texCoord;
in mat4 viewProjInv;

uniform sampler2D tex;
uniform sampler2D depthTex;
uniform vec3 focalPoint;
uniform float blurDist;
uniform float blurSize;
uniform float blurQuality;

float normdist(float x, float sigma)
{
	return 0.39894f * exp(-0.5f * x * x / (sigma * sigma)) / sigma;
}

vec4 blur(sampler2D sample, float size, float quality)
{
	const int kernelCount = 16;
	vec4 final = vec4(0.0f);
	float kernles[kernelCount];
	int mSize = int(clamp(quality, 1.0f / kernelCount, 1.0f) * kernelCount);
	int kSize = int((mSize - 1.0f) / 2.0f);

	float sigma = 7.0f, ksum = 0.0f;
	for (int j = 0; j <= kSize; ++j) {
		ksum += kernles[kSize + j] = kernles[kSize - j] = normdist(j, sigma);
	}
	ksum *= 2.0f;
	ivec2 tSize = textureSize(sample, 0);
	for (int i = -kSize; i <= kSize; ++i) {
		for (int j = -kSize; j <= kSize; ++j) {
			final += kernles[kSize + j] * kernles[kSize + i] * texture(sample, (texCoord.xy + size * vec2(i, j) / tSize));
		}
	}
	return final / (ksum * ksum);
}

vec3 getWorldPos(sampler2D depthTexture, vec2 coord)
{
    vec4 clipSpaceLocation;
    clipSpaceLocation.xy = coord * 2.0f - 1.0f;
    clipSpaceLocation.z = texture(depthTexture, coord).r * 2.0f - 1.0f;
    clipSpaceLocation.w = 1.0f;
    vec4 homogenousLocation = viewProjInv * clipSpaceLocation;
    return homogenousLocation.xyz / homogenousLocation.w;
}

void main()
{
    vec3 wpos = getWorldPos(depthTex, texCoord);
    vec4 wcol = texture2D(tex, texCoord);
	vec4 bcol = blur(tex, blurSize, blurQuality);
    float mixFactor = distance(wpos, focalPoint) / blurDist;
    mixFactor = clamp(mixFactor, 0.0, 1.0);
    fragColour = mix(wcol, bcol, mixFactor);
}
