#VERTEX
#version 330 core
layout(location = 0) in vec2 aPos;
layout(location = 1) in vec2 aTexCoord;

uniform mat4 projection;
uniform mat4 view;

out vec2 texCoord;
out mat4 viewProjInv;

void main()
{
    gl_Position = vec4(aPos, 0.0f, 1.0f);
    texCoord = aTexCoord;
    viewProjInv = inverse(projection * view);
}

#FRAGMENT

#version 330 core
out vec4 fragColour;

in vec2 texCoord;
in mat4 viewProjInv;

uniform sampler2D tex;
uniform sampler2D depthTex;
uniform float startDepth;
uniform float endDepth;
uniform vec4 colour;
uniform vec3 camPos;

vec3 getWorldPos(sampler2D depthTexture, vec2 coord)
{
    vec4 clipSpaceLocation;
    clipSpaceLocation.xy = coord * 2.0f - 1.0f;
    clipSpaceLocation.z = texture(depthTexture, coord).r * 2.0f - 1.0f;
    clipSpaceLocation.w = 1.0f;
    vec4 homogenousLocation = viewProjInv * clipSpaceLocation;
    return homogenousLocation.xyz / homogenousLocation.w;
}

void main()
{
    vec3 wpos = getWorldPos(depthTex, texCoord);
    vec4 wcol = texture2D(tex, texCoord);
    float mixFactor = 1.0f - (endDepth - distance(wpos, camPos)) / (endDepth - startDepth);
    mixFactor = clamp(mixFactor, 0.0, 1.0);
    mixFactor *= colour.a;
    fragColour = vec4(mix(wcol.rgb, colour.rgb, mixFactor), wcol.a);
}
