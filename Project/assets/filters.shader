#VERTEX
#version 330 core
layout(location = 0) in vec2 aPos;
layout(location = 1) in vec2 aTexCoord;

out vec2 TexCoord;

void main()
{
    gl_Position = vec4(aPos, 0.0f, 1.0f);
    TexCoord = aTexCoord;
}

#FRAGMENT
#version 330 core
out vec4 FragColour;
in vec2 TexCoord;

uniform sampler2D tex;

/**
			 * Brightness Contrast Saturation _hue
			 * Demo: https://www.shadertoy.com/view/MdjBRy
			 * starea @ ShaderToy
			 *
			 * Forked and remixed from:
			 * [1] https://shadertoy.com/view/llGSzK
			 * [2] https://shadertoy.com/view/MsjXRt
			 *
			 * Created 7/26/2017
			 * Updated 8/11/2017
			 **/

uniform float brightness;
uniform float saturation;
uniform float vibrance;
uniform float contrast;
uniform vec4 colourise;
uniform float hue;

void brightnessAdjust(inout vec4 color, in float b) {
	color.rgb *= b;
}

void contrastAdjust(inout vec4 color, in float c) {
	float t = 0.5 - c * 0.5;
	color.rgb = color.rgb * c + t;
}

vec4 _colourise(vec4 inColour, vec4 colour) {
	vec3 grey = vec3(inColour.r + inColour.g + inColour.b) / 3.0f;
	return vec4(mix(inColour.rgb, grey * colour.rgb, colour.a), inColour.a);
}

mat4 saturationMatrix(float saturation) {
	vec3 luminance = vec3(0.3086, 0.6094, 0.0820);
	float oneMinusSat = 1.0 - saturation;
	vec3 red = vec3(luminance.x * oneMinusSat);
	red.r += saturation;

	vec3 green = vec3(luminance.y * oneMinusSat);
	green.g += saturation;

	vec3 blue = vec3(luminance.z * oneMinusSat);
	blue.b += saturation;

	return mat4(
		red, 0,
		green, 0,
		blue, 0,
		0, 0, 0, 1);
}

int modi(int x, int y) {
	return x - y * (x / y);
}

int and(int a, int b) {
	int result = 0;
	int n = 1;
	const int BIT_COUNT = 32;

	for (int i = 0; i < BIT_COUNT; i++) {
		if ((modi(a, 2) == 1) && (modi(b, 2) == 1)) {
			result += n;
		}

		a >>= 1;
		b >>= 1;
		n <<= 1;

		if (!(a > 0 && b > 0))
			break;
	}
	return result;
}

// forked from https://www.shadertoy.com/view/llGSzK
// performance optimized by Ruofei
vec4 _vibrance(vec4 inCol, float _vibrance) //r,g,b 0.0 to 1.0,  _vibrance 1.0 no change, 0.0 image B&W.
{
	vec4 outCol;
	if (_vibrance <= 1.0)
	{
		float avg = dot(inCol.rgb, vec3(0.3, 0.6, 0.1));
		outCol.rgb = mix(vec3(avg), inCol.rgb, _vibrance);
	}
	else // _vibrance > 1.0
	{
		float hue_a, a, f, p1, p2, p3, i, h, s, v, amt, _max, _min, dlt;
		float br1, br2, br3, br4, br5, br2_or_br1, br3_or_br1, br4_or_br1, br5_or_br1;
		int use;

		_min = min(min(inCol.r, inCol.g), inCol.b);
		_max = max(max(inCol.r, inCol.g), inCol.b);
		dlt = _max - _min + 0.00001 /*Hack to fix divide zero infinities*/;
		h = 0.0;
		v = _max;

		br1 = step(_max, 0.0);
		s = (dlt / _max) * (1.0 - br1);
		h = -1.0 * br1;

		br2 = 1.0 - step(_max - inCol.r, 0.0);
		br2_or_br1 = max(br2, br1);
		h = ((inCol.g - inCol.b) / dlt) * (1.0 - br2_or_br1) + (h * br2_or_br1);

		br3 = 1.0 - step(_max - inCol.g, 0.0);

		br3_or_br1 = max(br3, br1);
		h = (2.0 + (inCol.b - inCol.r) / dlt) * (1.0 - br3_or_br1) + (h * br3_or_br1);

		br4 = 1.0 - br2 * br3;
		br4_or_br1 = max(br4, br1);
		h = (4.0 + (inCol.r - inCol.g) / dlt) * (1.0 - br4_or_br1) + (h * br4_or_br1);

		h = h * (1.0 - br1);

		hue_a = abs(h); // between h of -1 and 1 are skin tones
		a = dlt;      // Reducing enhancements on small rgb differences

		// Reduce the enhancements on skin tones.    
		a = step(1.0, hue_a) * a * (hue_a * 0.67 + 0.33) + step(hue_a, 1.0) * a;
		a *= (_vibrance - 1.0);
		s = (1.0 - a) * s + a * pow(s, 0.25);

		i = floor(h);
		f = h - i;

		p1 = v * (1.0 - s);
		p2 = v * (1.0 - (s * f));
		p3 = v * (1.0 - (s * (1.0 - f)));

		inCol.rgb = vec3(0.0);
		i += 6.0;
		//use = 1 << ((int)i % 6);
		use = int(pow(2.0, mod(i, 6.0)));
		a = float(and(use, 1)); // i == 0;
		use >>= 1;
		inCol.rgb += a * vec3(v, p3, p1);

		a = float(and(use, 1)); // i == 1;
		use >>= 1;
		inCol.rgb += a * vec3(p2, v, p1);

		a = float(and(use, 1)); // i == 2;
		use >>= 1;
		inCol.rgb += a * vec3(p1, v, p3);

		a = float(and(use, 1)); // i == 3;
		use >>= 1;
		inCol.rgb += a * vec3(p1, p2, v);

		a = float(and(use, 1)); // i == 4;
		use >>= 1;
		inCol.rgb += a * vec3(p3, p1, v);

		a = float(and(use, 1)); // i == 5;
		use >>= 1;
		inCol.rgb += a * vec3(v, p1, p2);

		outCol = inCol;
	}
	return outCol;
}

// remixed from mAlk's https://www.shadertoy.com/view/MsjXRt
vec4 shiftHue(in vec3 col, in float Shift)
{
	vec3 P = vec3(0.55735) * dot(vec3(0.55735), col);
	vec3 U = col - P;
	vec3 V = cross(vec3(0.55735), U);
	col = U * cos(Shift * 6.2832) + V * sin(Shift * 6.2832) + P;
	return vec4(col, 1.0);
}

void main()
{
	FragColour = texture(tex, TexCoord);
	float a = FragColour.a;

	FragColour = saturationMatrix(saturation) * FragColour;
	brightnessAdjust(FragColour, brightness);
	contrastAdjust(FragColour, contrast);
	FragColour = _vibrance(FragColour, vibrance);
	FragColour = shiftHue(FragColour.rgb, abs(sin(hue * 0.0174533f)));
	FragColour = _colourise(FragColour, colourise);
	FragColour.a = a;
}