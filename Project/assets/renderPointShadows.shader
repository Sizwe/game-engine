#VERTEX
#version 330 core
layout(location = 0) in mat4 aModel;
layout(location = 4) in vec4 aWorldRect;
layout(location = 5) in vec4 aTexRect;
layout(location = 6) in vec4 aColour;
layout(location = 7) in int aVFlags;

out VS_OUT{
    mat4 model;
    vec2 wMin;
    vec2 wMax;
    vec2 tMin;
    vec2 tMax;
    float alpha;
    flat int vFlags;
} vs_out;

void main()
{
    vs_out.model = aModel;
    vs_out.wMin = aWorldRect.xy;
    vs_out.wMax = aWorldRect.zw;
    vs_out.tMin = aTexRect.xy;
    vs_out.tMax = aTexRect.zw;
    vs_out.alpha = aColour.a;
    vs_out.vFlags = aVFlags;
}

#GEOMETRY
#version 330 core
layout(points) in;
layout(triangle_strip, max_vertices = 24) out;

#define MATERIAL_ENABLED_FLAG 0

in VS_OUT {
    mat4 model;
    vec2 wMin;
    vec2 wMax;
    vec2 tMin;
    vec2 tMax;
    float alpha;
    flat int vFlags;
} gs_in[];

out GS_OUT{
    vec3 worldPos;
    vec2 texCoord;
    float alpha;
    flat int vFlags;
} gs_out;

uniform mat4 lightPVs[6];

void main() {
    if ((gs_in[0].vFlags & (1 << MATERIAL_ENABLED_FLAG)) == 0)
        return;

    vec4 worldCoords[4] = { 
        vec4(gs_in[0].wMin.x, gs_in[0].wMax.y, 0.0, 1.0),
        vec4(gs_in[0].wMin.x, gs_in[0].wMin.y, 0.0, 1.0),
        vec4(gs_in[0].wMax.x, gs_in[0].wMax.y, 0.0, 1.0),
        vec4(gs_in[0].wMax.x, gs_in[0].wMin.y, 0.0, 1.0)
    };
    vec2 texCoords[4] = {
        vec2(gs_in[0].tMin.x, gs_in[0].tMax.y),
        vec2(gs_in[0].tMin.x, gs_in[0].tMin.y),
        vec2(gs_in[0].tMax.x, gs_in[0].tMax.y),
        vec2(gs_in[0].tMax.x, gs_in[0].tMin.y)
    };

    gs_out.alpha = gs_in[0].alpha;
    gs_out.vFlags = gs_in[0].vFlags;

    for (int face = 0; face < 6; ++face) {
        gl_Layer = face;
        for (int i = 0; i < 4; ++i) {
            gl_Position = lightPVs[face] * gs_in[0].model * worldCoords[i];
            gs_out.worldPos = (gs_in[0].model * worldCoords[i]).xyz;
            gs_out.texCoord = texCoords[i];
            EmitVertex();
        }
        EndPrimitive();
    }
}

#FRAGMENT
#version 330 core

#define flagged(bitField, flag) (((1 << (flag)) & bitField) != 0)
#define FRONT_FACE_FLAG 1
#define BACK_FACE_FLAG 2

in GS_OUT{
    vec3 worldPos;
    vec2 texCoord;
    float alpha;
    flat int vFlags;
} fs_in;

uniform sampler2D tex;
uniform vec3 lightPos;
uniform float span;

void main()
{
    if (gl_FrontFacing && flagged(fs_in.vFlags, BACK_FACE_FLAG)
        || !gl_FrontFacing && flagged(fs_in.vFlags, FRONT_FACE_FLAG)
        || texture(tex, fs_in.texCoord).a * fs_in.alpha <= 0.8) {
        discard;
    }
   float lightDistance = length(fs_in.worldPos - lightPos);
   gl_FragDepth = lightDistance / span;
}
