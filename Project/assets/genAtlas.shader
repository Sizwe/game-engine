#VERTEX
#version 330 core
layout(location = 0) in vec2 aPos;
layout(location = 1) in vec2 aTexCoord;
layout(location = 2) in int texIndex;

out vec2 TexCoord;
flat out int TexIndex;

void main()
{
    gl_Position = vec4(aPos, 0.0f, 1.0f);
    TexCoord = aTexCoord;
    TexIndex = texIndex;
}

#FRAGMENT

#version 330 core
out vec4 FragColour;

in vec2 TexCoord;
flat in int TexIndex;

uniform sampler2D textures[16];

void main()
{
    FragColour = texture(textures[TexIndex], TexCoord);
}
