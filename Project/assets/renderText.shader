#VERTEX
#version 330 core
layout(location = 0) in vec2 aPos;
layout(location = 1) in vec2 aTexCoord;

out vec2 texCoord;

void main()
{
    gl_Position = vec4(aPos, 0.0f, 1.0f);
    texCoord = aTexCoord;
}

#FRAGMENT
#version 330 core
out vec4 fragColour;

in vec2 texCoord;

uniform sampler2D tex;

void main()
{
    fragColour = texture(tex, texCoord);
}
