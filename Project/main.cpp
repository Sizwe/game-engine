#include <SDL.h>
#include <glad/glad.h>
#include <iostream>
#include <Engine.h>
#include <Renderer.h>
#include <Timer.h>
#include <Keyboard.h>
#include <Debug.h>
#include <Resources.h>
#include <DirLights.h>
#include <PointLights.h>
#include <Reflections.h>
#include <utility>
#include <random>
#include <cstdlib>
#include <ctime>
#include <Octree.h>
#include <Collist.h>
#include <Mouse.h>

float randf(float min, float max) {
	return min + (((float)rand()) / (float)RAND_MAX) * (max - min);
}
int main(int arc, char* argv[]) {
	Engine::Init();

	GameObject* camera = new GameObject();
	camera->local.position = Vector3(0.0f, 0.0f, 1000.0f);
	Engine::Origin.children.add(camera);
	Renderer::Camera = camera;

	DirLights[0] = DirLight(Vector3(1.0f, -1.0f, -1.0f));
	//PointLights[0] = PointLight(Vector3(0.0f, 200.0f, 200.0f), Attenuation(10000.0f));
	Renderer::background.colour = Vector4(0, 0, 0, 1);
	
	MonoSprite plane = MonoSprite(Rect(10000), Vector4(Vec3(0.1f), 1.0f));
	plane.transform.position = Vector3(0.0f, -200.0f, 0.0f);
	plane.transform.rotation = Quaternion(Vector3(-90.0f, 0.0f, 0.0f));
	plane.lifetime = MonoSprite::Lifetime::Long;
	plane.facing = MonoSprite::Facing::Front;
	Renderer::Draw(plane);

	std::srand(std::time(0));

	static Octree* oct = new Octree(Box(100.0f, 100.0f, 1.0f));
	static GameObject* mov = new GameObject();
	Engine::Origin.children.add(mov);

	Renderer::fog = Renderer::Fog(0.0f, 7500.0f);
	Renderer::fog.colour = Vec4(Vec3(1.0f, 0.0f, 0.0f), 0.75f);
	Renderer::depthOfField.enabled = true;
	Renderer::depthOfField.blurQuality = 1.0f;
	Renderer::depthOfField.blurSize = 2.0f;

	Renderer::filters.enabled = true;

	for (int i = 0; i < 1000; ++i) {
		MonoCube monoCube(Box(randf(5.0f, 500.0f)));
		monoCube.facing = MonoCube::Facing::Back;
		monoCube.lifetime = MonoCube::Lifetime::Long;

		monoCube.colour = Vector4(randf(0.0f, 1.0f), randf(0.0f, 1.0f), randf(0.0f, 1.0f), 100.0f);
		monoCube.transform.position = Vector3(randf(-5000.0f, 5000.0f), randf(-200.0f, 2500.0f), randf(-5000.0f, 5000.0f));
		
		oct->insert(new Collider(monoCube.box + monoCube.transform.position, mov));

		//monoCube.transform.rotation = Quaternion(Vector3(randf(0.0f, 360.0f), randf(0.0f, 360.0f), randf(0.0f, 360.0f)));
		Renderer::Draw(monoCube, mov);
	}
	//oct->____drawTree(INT_MAX);

	camera->scripts.add([camera]() {
		static Vector3 boxPos;
		float mult = Time::DeltaTime() * 1.0f;;
		if (Keyboard::KeyDown(Key::J)) {
			boxPos += Vector3(-1.0f, 0.0f, 0.0f) * mult;
		}
		else if (Keyboard::KeyDown(Key::L)) {
			boxPos += Vector3(1.0f, 0.0f, 0.0f) * mult;
		}
		else if (Keyboard::KeyDown(Key::U)) {
			boxPos += Vector3(0.0f, 1.0f, 0.0f) * mult;
		}
		else if (Keyboard::KeyDown(Key::O)) {
			boxPos += Vector3(0.0f, -1.0f, 0.0f) * mult;
		}
		else if (Keyboard::KeyDown(Key::I)) {
			boxPos += Vector3(0.0f, 0.0f, -1.0f) * mult;
		}
		else if (Keyboard::KeyDown(Key::K)) {
			boxPos += Vector3(0.0f, 0.0f, 1.0f) * mult;
		}

		if (Keyboard::KeyDown(Key::Y)) {
			Renderer::projection.fov += Time::DeltaTime() * 0.1f;
		}
		else if (Keyboard::KeyDown(Key::H)) {
			Renderer::projection.fov -= Time::DeltaTime() * 0.1f;
		}

		Octree::RayHit hit;
		if (oct->cast(Mouse::GetRay(), &hit)) {
			Debug::Draw(Ray(hit.point, hit.normal), 200.0f, 0, Vector4(1.0f, 1.0f, 0.0f, 1.0f));
		}

		Renderer::depthOfField.focalPoint = camera->toGlobal(Vector3(0.0f, 0.0f, -500.0f));
		/*
		Collist oct;
		oct.push_back(new Collider(Box(Vector3(0.0f, 0.0f, 0.0f), 0.25f), &Engine::Origin));
		oct.push_back(new Collider(Box(Vector3(200.0f, -1000.0f, 0.0f), 10.0f, 50.0f, 100.0f), &Engine::Origin));
		oct.push_back(new Collider(Box(boxPos, 50.0f), &Engine::Origin));
		oct.____drawList();*/

		if (Keyboard::KeyDown(Key::W) && !oct->cast(Line(camera->global.getPos(), camera->toGlobal(Vector3(0.0f, 0.0f, -100.0f))))) {
			camera->global.setPos(camera->toGlobal((float)Time::DeltaTime() * Vector3(0.0f, 0.0f, -1.0f)));
		}
		else if (Keyboard::KeyDown(Key::S)) {
			camera->global.setPos(camera->toGlobal((float)Time::DeltaTime() * Vector3(0.0f, 0.0f, 1.0f)));
		}
		else if (Keyboard::KeyDown(Key::A)) {
			camera->global.setPos(camera->toGlobal((float)Time::DeltaTime() * Vector3(-1.0f, 0.0f, 0.0f)));
		}
		else if (Keyboard::KeyDown(Key::D)) {
			camera->global.setPos(camera->toGlobal((float)Time::DeltaTime() * Vector3(1.0f, 0.0f, 0.0f)));
		}

		static Vector3 rotation;

		if (Keyboard::KeyDown(Key::Up)) {
			rotation += Time::DeltaTime() * 0.05f * Vector3(1.0f, 0.0f, 0.0f);
		}
		else if (Keyboard::KeyDown(Key::Down)) {
			rotation += Time::DeltaTime() * 0.05f * Vector3(-1.0f, 0.0f, 0.0f);
		}
		else if (Keyboard::KeyDown(Key::Left)) {
			rotation += Time::DeltaTime() * 0.05f * Vector3(0.0f, 1.0f, 0.0f);
		}
		else if (Keyboard::KeyDown(Key::Right)) {
			rotation += Time::DeltaTime() * 0.05f * Vector3(0.0f, -1.0f, 0.0f);
		}
		camera->local.rotation = Quaternion(rotation);

		if (Keyboard::KeyClicked(Key::Space)) {
			Renderer::projection.type = Renderer::ProjectionType(!bool(Renderer::projection.type));
		}

		});

	Engine::Run();

	delete oct;

	Engine::Quit();
	return 0;
}
