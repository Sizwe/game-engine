#pragma once
#include <CollisionDetector.h>

class Octree : public CollisionDetector {
	struct Node {
		Box region;
		Node* parent;
		Node* children;
		std::vector<Object> objects;

		Node(Node* parent, const Box& region);
		Node(const Node& node);
		Node& operator=(const Node& node) = delete;
		~Node();

		Node* insert(const Object& obj);
		bool erase(Collider* collider);

		bool cast(const Ray& ray, RayHit* hit = nullptr) const;
		bool cast(const Line& line, RayHit* hit = nullptr) const;
		bool cast(const Box& box, BoxHit* hit = nullptr) const;
		bool cast(const Sphere& sphere, Hit* hit = nullptr) const;
		void castAll(const Ray& ray, std::vector<RayHit>& out) const;
		void castAll(const Line& line, std::vector<RayHit>& out) const;
		void castAll(const Box& box, std::vector<BoxHit>& out) const;
		void castAll(const Sphere& sphere, std::vector<Hit>& out) const;

		//temporary unofficial function
		void ____drawTree(int duration = 0) const;
	};

public:
	class iterator {
	private:
		friend Octree;
		std::stack<int> stack;
		Node* node;

		iterator(Octree* octree);
	public:

		iterator& operator ++();
		iterator& operator --();
		iterator operator++(int);
		iterator operator--(int);
		Collider* operator->();
		Collider& operator*();
		operator Collider*();

		bool operator ==(const iterator& it) const;
		bool operator !=(const iterator& it) const;
	};

	class const_iterator {
	private:
		friend Octree;
		iterator it;
		const_iterator(const Octree* octree);	

	public:
		const_iterator& operator ++();
		const_iterator operator++(int);
		const Collider* operator->();
		const Collider& operator*();
		operator const Collider* ();

		bool operator ==(const const_iterator& const_it) const;
		bool operator !=(const const_iterator& const_it) const;
	};

private:
	Node* root;
	size_t _size;

public:
	Octree();
	Octree(const Box& region);
	Octree(const Octree& oct);
	Octree& operator=(const Octree& oct);
	~Octree();

	void insert(Collider* collider);
	void erase(Collider* collider);
	iterator erase(iterator it);
	size_t size() const;

	void update() override;

	bool cast(const Ray& ray, RayHit* hit = nullptr) const override;
	bool cast(const Line& line, RayHit* hit = nullptr) const override;
	bool cast(const Box& box, BoxHit* hit = nullptr) const override;
	bool cast(const Sphere& sphere, Hit* hit = nullptr) const override;
	std::vector<RayHit> castAll(const Ray& ray) const override;
	std::vector<RayHit> castAll(const Line& line) const override;
	std::vector<BoxHit> castAll(const Box& box) const override;
	std::vector<Hit> castAll(const Sphere& sphere) const override;

	iterator begin();
	iterator end();
	const_iterator begin() const;
	const_iterator end() const;
	const_iterator cbegin() const;
	const_iterator cend() const;

	//temporary unofficial function
	void ____drawTree(int duration = 0) const;
};