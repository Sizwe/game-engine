#pragma once
#include <Box.h>
#include <GameObject.h>

class Collider {
public:
	GameObject* gameObject;
	Box box;

	Collider(const Box& box, GameObject* gameObject = nullptr);
};