#pragma once
#include <CollisionDetector.h>

class Collist : CollisionDetector {
public:
	class iterator : public std::list<Object>::iterator {
	public:
		iterator(const std::list<Object>::iterator& super);
		Collider* operator->();
		Collider& operator*();
		operator Collider* ();
	};

	class const_iterator : public std::list<Object>::const_iterator {
	public:
		const_iterator(const std::list<Object>::const_iterator& super);
		const Collider* operator->();
		const Collider& operator*();
		operator const Collider* ();
	};

private:
	std::list<Object> objects;

public:
	Collist();
	Collist(const Collist& collist);
	Collist& operator=(const Collist& collist);
	~Collist();

	void update() override;

	bool cast(const Ray& ray, RayHit* hit = nullptr) const override;
	bool cast(const Line& line, RayHit* hit = nullptr) const override;
	bool cast(const Box& box, BoxHit* hit = nullptr) const override;
	bool cast(const Sphere& sphere, Hit* hit = nullptr) const override;
	std::vector<RayHit> castAll(const Ray& ray) const override;
	std::vector<RayHit> castAll(const Line& line) const override;
	std::vector<BoxHit> castAll(const Box& box) const override;
	std::vector<Hit> castAll(const Sphere& sphere) const override;

	size_t size() const;
	void push_back(Collider* collider);
	void push_front(Collider* collider);
	void insert(iterator pos, Collider* collider);

	iterator erase(iterator it);
	void erase(Collider* collider);
	void clear();

	iterator begin();
	iterator end();
	const_iterator begin() const;
	const_iterator end() const;
	const_iterator cbegin() const;
	const_iterator cend() const;

	//temporary unofficial function
	void ____drawList(int duration = 0) const;
};