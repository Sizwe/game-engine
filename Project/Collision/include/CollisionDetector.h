#pragma once
#include <Collider.h>

class CollisionDetector {
protected:
	struct Object {
		Collider* collider;
		Box gRegion;

		Object(Collider* collider);
	};

public:
	struct Hit {
		Collider* collider;

		Hit(Collider* collider = nullptr);
	};
	struct RayHit : Hit {
		Vector3 point;
		Vector3 normal;

		RayHit(Collider* collider = nullptr, const Vector3& point = Vector3(), const Vector3& normal = Vector3());
	};
	struct BoxHit : Hit {
		Vector3 depth;

		BoxHit(Collider* collider = nullptr, const Vector3& depth = Vector3());
	};

public:
	virtual void update() = 0;

	virtual bool cast(const Ray& ray, RayHit* hit = nullptr) const = 0;
	virtual bool cast(const Line& line, RayHit* hit = nullptr) const = 0;
	virtual bool cast(const Box& box, BoxHit* hit = nullptr) const = 0;
	virtual bool cast(const Sphere& sphere, Hit* hit = nullptr) const = 0;
	virtual std::vector<RayHit> castAll(const Ray& ray) const = 0;
	virtual std::vector<RayHit> castAll(const Line& line) const = 0;
	virtual std::vector<BoxHit> castAll(const Box& box) const = 0;
	virtual std::vector<Hit> castAll(const Sphere& sphere) const = 0;
};