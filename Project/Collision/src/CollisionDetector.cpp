#include <CollisionDetector.h>

CollisionDetector::Object::Object(Collider* collider) : collider(collider) {
	gRegion = collider->gameObject->toGlobal.matrix() * collider->box;
}

CollisionDetector::Hit::Hit(Collider* collider) : collider(collider)
{
}

CollisionDetector::RayHit::RayHit(Collider* collider, const Vector3& point, const Vector3& normal) : Hit(collider), point(point), normal(normal)
{
}

CollisionDetector::BoxHit::BoxHit(Collider* collider, const Vector3& depth) : Hit(collider), depth(depth)
{
}
