#pragma once
#include <Collist.h>
#include <Debug.h>

Collist::iterator::iterator(const std::list<Collist::Object>::iterator& super) : std::list<Collist::Object>::iterator(super) {
}

Collider* Collist::iterator::operator->() {
	return std::list<Object>::iterator::operator->()->collider;
}

Collider& Collist::iterator::operator*() {
	return *std::list<Object>::iterator::operator->()->collider;
}

Collist::iterator::operator Collider* ()
{
	return std::list<Object>::iterator::operator->()->collider;
}

Collist::const_iterator::const_iterator(const std::list<Collist::Object>::const_iterator& super) : std::list<Collist::Object>::const_iterator(super) {
}

const Collider* Collist::const_iterator::operator->() {
	return std::list<Object>::const_iterator::operator->()->collider;
}

const Collider& Collist::const_iterator::operator*() {
	return *std::list<Object>::const_iterator::operator->()->collider;
}

Collist::const_iterator::operator const Collider* ()
{
	return std::list<Object>::const_iterator::operator->()->collider;
}

Collist::iterator Collist::begin()
{
	return iterator(objects.begin());
}

Collist::iterator Collist::end()
{
	return iterator(objects.end());
}

Collist::const_iterator Collist::begin() const
{
	return const_iterator(objects.cbegin());
}

Collist::const_iterator Collist::end() const
{
	return const_iterator(objects.cend());
}

Collist::const_iterator Collist::cbegin() const
{
	return const_iterator(objects.cbegin());
}

Collist::const_iterator Collist::cend() const
{
	return const_iterator(objects.cend());
}

void Collist::____drawList(int duration) const
{
	for (auto& o : objects) {
		MonoCube cube(o.gRegion, Vector4(1.0f, 0.0f, 0.0f, 1.0f));
		cube.mode = MonoCube::Mode::Edge;
		Debug::Draw(cube, duration);
	}
}

Collist::Collist()
{
}

Collist::Collist(const Collist& collist)
{
	for (auto& o : collist.objects) {
		Object copy(o);
		copy.collider = new Collider(*copy.collider);
		objects.push_back(copy);
	}
}

Collist& Collist::operator=(const Collist& collist)
{
	for (auto& o : collist.objects) {
		Object copy(o);
		copy.collider = new Collider(*copy.collider);
		objects.push_back(copy);
	}
	return *this;
}

Collist::~Collist()
{
	for (auto& o : objects) {
		delete o.collider;
	}
}

void Collist::update()
{
	for (auto& o : objects) {
		o.gRegion = o.collider->gameObject->toGlobal.matrix() * o.collider->box;
	}
}

bool Collist::cast(const Ray& ray, RayHit* hit) const {
	for (auto& o : objects) {
		float dist = 0.0f;
		if (o.collider->gameObject->enabled() && (dist = o.gRegion.intersects(ray))) {
			if (hit) {
				hit->point = ray.start + dist * ray.dir.normalise();
				hit->normal = o.gRegion.normal(hit->point);
				hit->collider = o.collider;
			}
			return true;
		}
	}
	return false;
}

bool Collist::cast(const Line& line, RayHit* hit) const {
	for (auto& o : objects) {
		if (o.collider->gameObject->enabled() && o.gRegion.intersects(line)) {
			if (hit) {
				Vector3 dir = line.u.to(line.v).normalise();
				float dist = o.gRegion.intersects(Ray(line.u, dir));
				hit->point = line.u + dist * dir;
				hit->normal = o.gRegion.normal(hit->point);
				hit->collider = o.collider;
			}
			return true;
		}
	}
	return false;
}

bool Collist::cast(const Box& box, BoxHit* hit) const {
	for (auto& o : objects) {
		if (o.collider->gameObject->enabled() && o.gRegion.overlaps(box)) {
			if (hit) {
				auto c1 = box.centre();
				auto c2 = o.gRegion.centre();
				auto isct = box & o.gRegion;
				hit->depth.x = (c1.x > c2.x) ? -isct.width() : isct.width();
				hit->depth.y = (c1.y > c2.y) ? -isct.height() : isct.height();
				hit->depth.z = (c1.z > c2.z) ? -isct.depth() : isct.depth();
				hit->collider = o.collider;
			}
			return true;
		}
	}
	return false;
}

bool Collist::cast(const Sphere& sphere, Hit* hit) const {
	for (auto& o : objects) {
		if (o.collider->gameObject->enabled() && o.gRegion.overlaps(sphere)) {
			hit->collider = o.collider;
			return true;
		}
	}
	return false;
}

std::vector<Collist::RayHit> Collist::castAll(const Ray& ray) const 
{
	std::vector<Collist::RayHit> out;
	for (auto& o : objects) {
		float dist = 0.0f;
		if (o.collider->gameObject->enabled() && (dist = o.gRegion.intersects(ray))) {
			RayHit hit;
			hit.point = ray.start + dist * ray.dir.normalise();
			hit.normal = o.gRegion.normal(hit.point);
			hit.collider = o.collider;
			out.push_back(hit);
		}
	}
	return out;
}

std::vector<Collist::RayHit> Collist::castAll(const Line& line) const 
{
	std::vector<Collist::RayHit> out;
	for (auto& o : objects) {
		if (o.collider->gameObject->enabled() && o.gRegion.intersects(line)) {
			RayHit hit;
			Vector3 dir = line.u.to(line.v).normalise();
			float dist = o.gRegion.intersects(Ray(line.u, dir));
			hit.point = line.u + dist * dir;
			hit.normal = o.gRegion.normal(hit.point);
			hit.collider = o.collider;
			out.push_back(hit);
		}
	}
	return out;
}

std::vector<Collist::BoxHit> Collist::castAll(const Box& box) const 
{
	std::vector<Collist::BoxHit> out;
	for (auto& o : objects) {
		if (o.collider->gameObject->enabled() && o.gRegion.overlaps(box)) {
			BoxHit hit;
			auto c1 = box.centre();
			auto c2 = o.gRegion.centre();
			auto isct = box & o.gRegion;
			hit.depth.x = (c1.x > c2.x) ? -isct.width() : isct.width();
			hit.depth.y = (c1.y > c2.y) ? -isct.height() : isct.height();
			hit.depth.z = (c1.z > c2.z) ? -isct.depth() : isct.depth();
			hit.collider = o.collider;
			out.push_back(hit);
		}
	}
	return out;
}

std::vector<Collist::Hit> Collist::castAll(const Sphere& sphere) const 
{
	std::vector<Collist::Hit> out;
	for (auto& o : objects) {
		if (o.collider->gameObject->enabled() && o.gRegion.overlaps(sphere)) {
			out.push_back(Hit(o.collider));
		}
	}
	return out;
}


size_t Collist::size() const
{
	return objects.size();
}

void Collist::push_back(Collider* collider)
{
	objects.push_back(Object(collider));
}

void Collist::push_front(Collider* collider)
{
	objects.push_front(Object(collider));
}

void Collist::insert(iterator pos, Collider* collider)
{
	objects.insert(pos, Object(collider));
}

Collist::iterator Collist::erase(iterator it)
{
	delete (Collider*)it;
	return objects.erase(it);
}

void Collist::erase(Collider* collider)
{
	for (auto it = objects.begin(); it != objects.end(); ++it) {
		if (it->collider == collider) {
			delete(collider);
			objects.erase(it);
			break;
		}
	}
}

void Collist::clear()
{
	for (auto o : objects) {
		delete o.collider;
	}
	objects.clear();
}