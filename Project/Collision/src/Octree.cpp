#include "..\include\Octree.h"
#include <iostream>

namespace {
	Vector3 MIN_REGION = Vector3(1.0f);

}

Octree::Octree() : root(nullptr), _size(0) {
}

Octree::Octree(const Box& region) : _size(0)
{
	root = new Node(nullptr, region);
}

Octree::Octree(const Octree& oct) : _size(oct._size)
{	
	root = oct.root ? new Node(*oct.root) : nullptr;
}

Octree& Octree::operator=(const Octree& oct)
{
	delete root;
	root = oct.root ? new Node(*oct.root) : nullptr;
	_size = oct._size;
	return *this;
}

Octree::~Octree() {
	delete root;
}

void Octree::insert(Collider* collider)
{
	Object obj = Object(collider);
	if (!root) {
		float len = fmaxf(fmaxf(collider->box.width(), collider->box.height()), collider->box.depth()) * 2.0f;
		root = new Node(nullptr, Box(collider->box.centre(), len));
	}
	else if (!root->region.contains(obj.gRegion)) {
		if (_size == 0) {
			delete root;
			root = nullptr;
		}
		else {
			size_t surrogate_index = 0;
			Vector3 centre;
			Vector3 min_max[2] = { root->region.min(), root->region.max() };
			Vector3 objc = obj.gRegion.centre();

			float minDist = FLT_MAX;
			for (int i = 0; i < 8; ++i) {
				Vector3 corner = Vector3(min_max[1 & i >> 2].x, min_max[1 & i >> 1].y, min_max[1 & i].z);
				float tdist = corner.dist(objc);
				if (tdist < minDist) {
					minDist = tdist;
					surrogate_index = i;
					centre = corner;
				}
			}
			Vector3 extents = Vector3(root->region.width(), root->region.height(), root->region.depth()) * 2.0f;
			Node* expansion = new Node(nullptr, Box(centre, extents.x, extents.y, extents.z));
			extents /= 2.0f;
			Box centreBox = Box(centre, extents.x, extents.y, extents.z);
			extents /= 2.0f;

			expansion->children = new Node[8]{
				Node(expansion, centreBox + Vector3(extents.x, extents.y, extents.z)),
				Node(expansion, centreBox + Vector3(extents.x, extents.y, -extents.z)),
				Node(expansion, centreBox + Vector3(extents.x, -extents.y, extents.z)),
				Node(expansion, centreBox + Vector3(extents.x, -extents.y, -extents.z)),
				Node(expansion, centreBox + Vector3(-extents.x, extents.y, extents.z)),
				Node(expansion, centreBox + Vector3(-extents.x, extents.y, -extents.z)),
				Node(expansion, centreBox + Vector3(-extents.x, -extents.y, extents.z)),
				Node(expansion, centreBox + Vector3(-extents.x, -extents.y, -extents.z))
			};
			Node& surrogate = expansion->children[surrogate_index];

			surrogate.objects.swap(root->objects);
			for (auto it = surrogate.objects.begin(); it != surrogate.objects.end();) {
				if (!surrogate.region.contains(it->gRegion)) {
					expansion->objects.push_back(*it);
					it = surrogate.objects.erase(it);
				}
				else ++it;
			}

			bool empty_branch = true;
			std::swap(surrogate.children, root->children);
			if (surrogate.children) {
				for (int i = 0; i < 8; ++i) {
					surrogate.children[i].parent = &surrogate;
					empty_branch = empty_branch && !surrogate.children[i].children;
				}
			}
			if (surrogate.objects.empty() && empty_branch) {
				delete surrogate.children;
			}

			delete root;
			root = expansion;
		}
		insert(collider);
		return;
	}	
	root->insert(obj);
	_size++;
}

void Octree::erase(Collider* collider)
{
	if (root && root->erase(collider))
		_size--;
}

Octree::iterator Octree::erase(iterator it)
{
	if (root) {
		auto out = --iterator(it);
		if (it.node->erase((Collider*)it)) {
			_size--;
		}
		return ++out;
	}
	else
		return iterator(nullptr);
}

size_t Octree::size() const
{
	return _size;
}

void Octree::update()
{
	if (!root) {
		return;
	}
	std::vector<Collider*> colliders;
	colliders.reserve(_size);
	for (auto it = begin(); it != end(); ++it) {
		Object& o = it.node->objects[it.stack.top()];
		if (!o.collider->gameObject->doomed()) {
			colliders.push_back(o.collider);
			o.collider = nullptr;
		}
	}
	Box rootRegion = root->region;
	delete root;
	root = new Node(nullptr, rootRegion);
	for (auto* c : colliders) {
		insert(c);
	}
}

bool Octree::cast(const Ray& ray, RayHit* hit) const
{
	if (root)
		return root->cast(ray, hit);
	return false;
}

bool Octree::cast(const Line& line, RayHit* hit) const
{
	if (root)
		return root->cast(line, hit);
	return false;
}

bool Octree::cast(const Sphere& sphere, Hit* hit) const
{
	if (root)
		return root->cast(sphere, hit);
	return false;
}

bool Octree::cast(const Box& box, BoxHit* hit) const
{
	if (root)
		return root->cast(box, hit);
	return false;
}

std::vector<Octree::RayHit> Octree::castAll(const Ray& ray) const
{
	std::vector<RayHit> out;
	if (root)
		root->castAll(ray, out);
	return out;
}

std::vector<Octree::RayHit> Octree::castAll(const Line& line) const
{
	std::vector<RayHit> out;
	if (root)
		root->castAll(line, out);
	return out;
}

std::vector<Octree::BoxHit> Octree::castAll(const Box& box) const
{
	std::vector<BoxHit> out;
	if (root)
		root->castAll(box, out);
	return out;
}

std::vector<Octree::Hit> Octree::castAll(const Sphere& sphere) const
{
	std::vector<Hit> out;
	if (root)
		root->castAll(sphere, out);
	return out;
}

Octree::iterator Octree::begin()
{
	if(root)
		return ++Octree::iterator(this);
	return end();
}

Octree::iterator Octree::end()
{
	return Octree::iterator(this);
}

Octree::const_iterator Octree::begin() const
{
	return cbegin();
}

Octree::const_iterator Octree::end() const
{
	return cend();
}

Octree::const_iterator Octree::cbegin() const
{
	if (root)
		return ++Octree::const_iterator(this);
	return cend();
}

Octree::const_iterator Octree::cend() const
{
	return Octree::const_iterator(this);
}

void Octree::____drawTree(int time) const
{
	if (root) root->____drawTree(time);
}

Octree::Node::Node(Node* parent, const Box& region) : parent(parent), region(region), children(nullptr) {
}

Octree::Node::Node(const Node& node) : parent(node.parent), region(node.region)
{
	objects.reserve(node.objects.size());
	for (auto& o : node.objects) {
		Object copy = o;
		copy.collider = new Collider(*o.collider);
		objects.push_back(copy);
	}
	if (node.children) {
		children = new Node[8]{
			node.children[0], node.children[1], node.children[2], node.children[3],
			node.children[4], node.children[5], node.children[6], node.children[7]
		};
		for (int i = 0; i < 8; ++i) {
			children[i].parent = this;
		}
	}
	else {
		children = nullptr;
	}
}

Octree::Node::~Node() {
	for (auto& o : objects)
		delete o.collider;
	delete[] children;
}

Octree::Node* Octree::Node::insert(const Object& obj)
{
	if (!children) {
		Vector3 hext = Vector3(region.width(), region.height(), region.depth()) / 2.0f;
		if (hext.x < MIN_REGION.x || hext.y < MIN_REGION.y || hext.z < MIN_REGION.z) {
			objects.push_back(obj);
			return this;
		}
		Vector3 centre = region.centre();
		Box half = Box(hext.x, hext.y, hext.z) + centre;
		hext /= 2.0f;

		children = new Node[8]{
			Node(this, half + Vector3(hext.x, hext.y, hext.z)),
			Node(this, half + Vector3(hext.x, hext.y, -hext.z)),
			Node(this, half + Vector3(hext.x, -hext.y, hext.z)),
			Node(this, half + Vector3(hext.x, -hext.y, -hext.z)),
			Node(this, half + Vector3(-hext.x, hext.y, hext.z)),
			Node(this, half + Vector3(-hext.x, hext.y, -hext.z)),
			Node(this, half + Vector3(-hext.x, -hext.y, hext.z)),
			Node(this, half + Vector3(-hext.x, -hext.y, -hext.z)) 
		};
	}
	for (int i = 0; i < 8; ++i) {
		if (children[i].region.contains(obj.gRegion))
			return children[i].insert(obj);
	}
	objects.push_back(obj);
	return this;
}

bool Octree::Node::erase(Collider* collider)
{
	for (auto it = objects.begin(); it != objects.end(); ++it) {
		if (it->collider == collider) {
			objects.erase(it);
			for (Node* node = this; node && node->objects.empty(); node = node->parent) {
				if (node->children) {
					for (int i = 0; i < 8; ++i)
						if (!node->children[i].objects.empty() || node->children[i].children)
							return true;
				}
				delete node->children;
				node->children = nullptr;
			}
			return true;
		}
	}
	if (children) {
		for (int i = 0; i < 8; ++i)
			if (children[i].erase(collider))
				return true;
	}
	return false;
}

bool Octree::Node::cast(const Ray& ray, RayHit* hit) const
{
	for (auto& c : objects) {
		float dist = 0.0f;
		if (c.collider->gameObject->enabled() && (dist = c.gRegion.intersects(ray))) {
			if (hit) {
				hit->point = ray.start + ray.dir.normalise() * dist;
				hit->normal = c.gRegion.normal(hit->point);	
				hit->collider = c.collider;
			}
			return true;
		}
	}
	if (children) {
		for (int i = 0; i < 8; ++i) {
			if (children[i].region.intersects(ray) && children[i].cast(ray, hit)) {
				return true;
			}
		}
	}
	return false;
}

bool Octree::Node::cast(const Line& line, RayHit* hit) const
{
	for (auto& c : objects) {
		if (c.collider->gameObject->enabled() && c.gRegion.intersects(line)) {
			if (hit) {
				Vector3 dir = line.u.to(line.v).normalise();
				float dist = c.gRegion.intersects(Ray(line.u, dir));
				hit->point = line.u + dist * dir;
				hit->normal = c.gRegion.normal(hit->point);
				hit->collider = c.collider;
			}
			return true;
		}
	}
	if (children) {
		for (int i = 0; i < 8; ++i) {
			if (children[i].region.intersects(line) && children[i].cast(line, hit)) {
				return true;
			}
		}
	}
	return false;
}

bool Octree::Node::cast(const Sphere& sphere, Hit* hit) const
{
	for (auto& c : objects) {
		if (c.collider->gameObject->enabled() && c.gRegion.overlaps(sphere)) {
			hit->collider = c.collider;
			return true;
		}
	}
	if (children) {
		for (int i = 0; i < 8; ++i) {
			if (children[i].region.overlaps(sphere) && children[i].cast(sphere, hit)) {
				return true;
			}
		}
	}
	return false;
}

void Octree::Node::castAll(const Ray& ray, std::vector<RayHit>& out) const
{
	for (auto& c : objects) {
		float dist = 0.0f;
		if (c.collider->gameObject->enabled() && (dist = c.gRegion.intersects(ray))) {
			RayHit hit;
			hit.point = ray.start + dist * ray.dir.normalise();
			hit.normal = c.gRegion.normal(hit.point);
			hit.collider = c.collider;
			out.push_back(hit);
		}
	}
	if (children) {
		for (int i = 0; i < 8; ++i) {
			if (children[i].region.intersects(ray))
				children[i].castAll(ray, out);
		}
	}
}

void Octree::Node::castAll(const Line& line, std::vector<RayHit>& out) const
{
	for (auto& c : objects) {
		if (c.collider->gameObject->enabled() && c.gRegion.intersects(line)) {
			RayHit hit;
			Vector3 dir = line.u.to(line.v).normalise();
			float dist = c.gRegion.intersects(Ray(line.u, dir));
			hit.point = line.u + dist * dir;
			hit.normal = c.gRegion.normal(hit.point);
			hit.collider = c.collider;
			out.push_back(hit);
		}
	}
	if (children) {
		for (int i = 0; i < 8; ++i) {
			if (children[i].region.intersects(line))
				children[i].castAll(line, out);
		}
	}
}

void Octree::Node::castAll(const Box& box, std::vector<BoxHit>& out) const
{
	for (auto& c : objects) {
		if (c.collider->gameObject->enabled() && c.gRegion.overlaps(box)) {
			BoxHit hit;
			auto c1 = box.centre();
			auto c2 = c.gRegion.centre();
			auto isct = box & c.gRegion;
			hit.depth.x = (c1.x > c2.x) ? -isct.width() : isct.width();
			hit.depth.y = (c1.y > c2.y) ? -isct.height() : isct.height();
			hit.depth.z = (c1.z > c2.z) ? -isct.depth() : isct.depth();
			hit.collider = c.collider;
			out.push_back(hit);
		}
	}
	if (children) {
		for (int i = 0; i < 8; ++i) {
			if (children[i].region.overlaps(box))
				children[i].castAll(box, out);
		}
	}
}

void Octree::Node::castAll(const Sphere& sphere, std::vector<Hit>& out) const
{
	for (auto& c : objects) {
		if (c.collider->gameObject->enabled() && c.gRegion.overlaps(sphere)) {
			Hit hit;
			hit.collider = c.collider;
			out.push_back(hit);
		}
	}
	if (children) {
		for (int i = 0; i < 8; ++i) {
			if (children[i].region.overlaps(sphere))
				children[i].castAll(sphere, out);
		}
	}
}

#include <Debug.h>
void Octree::Node::____drawTree(int time) const
{
	MonoCube cube(region, Vector4(0.0f, 0.0f, 1.0f, 1.0f));
	cube.mode = MonoCube::Mode::Edge;
	Debug::Draw(cube, time);
	cube.colour = Vector4(1.0f, 0.0f, 0.0f, 1.0f);
	for (auto& o : objects) {
		cube.box = o.gRegion;
		Debug::Draw(cube, time);
	}
	if (children) {
		for (int i = 0; i < 8; ++i) {
			children[i].____drawTree(time);
		}
	}
}

bool Octree::Node::cast(const Box& box, BoxHit* hit) const
{
	for (auto& c : objects) {
		if (c.collider->gameObject->enabled() && c.gRegion.overlaps(box)) {
			if (hit) {
				auto c1 = box.centre();
				auto c2 = c.gRegion.centre();
				auto isct = box & c.gRegion;
				hit->depth.x = (c1.x > c2.x) ? -isct.width() : isct.width();
				hit->depth.y = (c1.y > c2.y) ? -isct.height() : isct.height();
				hit->depth.z = (c1.z > c2.z) ? -isct.depth() : isct.depth();
				hit->collider = c.collider;
			}
			return true;
		}
	}
	if (children) {
		for (int i = 0; i < 8; ++i) {
			if (children[i].region.overlaps(box))
				return children[i].cast(box, hit);
		}
	}
	return false;
}

Collider* Octree::iterator::operator->() {
	return node->objects[stack.top()].collider;
}

Collider& Octree::iterator::operator*() {
	return *node->objects[stack.top()].collider;
}

Octree::iterator::operator Collider* () {
	return node->objects[stack.top()].collider;
}

Octree::iterator::iterator(Octree* octree) {
	node = octree->root;
}

Octree::iterator& Octree::iterator::operator ++() {
	if (stack.empty())
		stack.push(0);
	else
		++stack.top();

	while (stack.top() >= node->objects.size()) {
		if (node->children) {
			stack.top() = 0;
			stack.push(0);
			node = &node->children[0];
			continue;
		}
		while (node->parent) {
			stack.pop();
			node = node->parent;
			if (++stack.top() < 8) {
				node = &node->children[stack.top()];
				stack.push(0);
				break;
			}
		}
		if (!node->parent) {
			stack.pop();
			break;
		}
	}
	return *this;
}

Octree::iterator& Octree::iterator::operator --() {
	if (stack.empty()) {
		while (node->children) {
			node = &node->children[7];
			stack.top() = 7;
			stack.push(node->objects.size() - 1);
		}
	}
	else
		--stack.top();

	while (stack.top() < 0) {
		if (node->parent) {
			stack.pop();
			node = node->parent;
			if (--stack.top() >= 0) {
				node = &node->children[stack.top()];
				stack.push(node->objects.size() - 1);
			}
		}
		else {
			stack.pop();
			break;
		}

		while (node->children) {
			node = &node->children[7];
			stack.top() = 7;
			stack.push(node->objects.size() - 1);
		}
	}
	return *this;
}


Octree::iterator Octree::iterator::operator++(int)
{
	auto copy = *this;
	operator++();
	return copy;
}

Octree::iterator Octree::iterator::operator--(int)
{
	auto copy = *this;
	operator--();
	return copy;
}


bool Octree::iterator::operator ==(const Octree::iterator& it) const {
	return node == it.node && stack == it.stack;
}

bool Octree::iterator::operator !=(const Octree::iterator& it) const {
	return node != it.node || stack != it.stack;
}

Octree::const_iterator::const_iterator(const Octree* octree) : it(const_cast<Octree*>(octree)) {}

Octree::const_iterator& Octree::const_iterator::operator++()
{
	++it;
	return *this;
}

Octree::const_iterator Octree::const_iterator::operator++(int)
{
	auto copy = *this;
	it++;
	return copy;
}

const Collider* Octree::const_iterator::operator->() {
	return it.operator->();
}

const Collider& Octree::const_iterator::operator*() {
	return it.operator*();
}

Octree::const_iterator::operator const Collider* () {
	return (Collider*)it;
}

bool Octree::const_iterator::operator==(const const_iterator& const_it) const
{
	return it == const_it.it;
}

bool Octree::const_iterator::operator!=(const const_iterator& const_it) const
{
	return it != const_it.it;
}
