#include <DirLights.h>

namespace {
	inline std::array<DirLight, 2> createDefault() {
		std::array<DirLight, 2> lights;
		DirLight dl;
		dl.enabled = false;
		lights.fill(dl);
		return lights;
	}
}

std::array<DirLight, 2> DirLights = createDefault();

DirLight::DirLight(const Vector3& direction, const Vector3& ambient, const Vector3& diffuse, const Vector3& specular)
	: enabled(true), direction(direction), ambient(ambient), diffuse(diffuse), specular(specular) {
}
