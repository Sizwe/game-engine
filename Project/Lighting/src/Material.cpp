#include <Material.h>

Distortion::Distortion(int imageIndex, float reflectiveness, float refractiveness, float refractionRatio)
	: imageIndex(imageIndex), reflectiveness(reflectiveness), refractiveness(refractiveness), refractionRatio(refractionRatio) {
}

Material::Material(float shininess) : Material() { this->shininess = shininess; }

Material::Material(const Vector3& ambient, const Vector3& diffuse, const Vector3& specular, float shininess) :
	enabled(true), ambient(ambient), diffuse(diffuse), specular(specular), shininess(shininess) {
}

Material Material::Emerald() { return Material(Vector3(0.0215f, 0.1745f, 0.0215f), Vector3(0.0215f, 0.1745f, 0.0215f), Vector3(0.0215f, 0.1745f, 0.0215f), 0.6f); }
Material Material::Jade() { return Material(Vector3(0.135f, 0.2225f, 0.1575f), Vector3(0.135f, 0.2225f, 0.1575f), Vector3(0.135f, 0.2225f, 0.1575f), 0.1f); }
Material Material::Obsidian() { return Material(Vector3(0.05375f, 0.05f, 0.06625f), Vector3(0.05375f, 0.05f, 0.06625f), Vector3(0.05375f, 0.05f, 0.06625f), 0.3f); }
Material Material::Pearl() { return Material(Vector3(0.25f, 0.20725f, 0.20725f), Vector3(0.25f, 0.20725f, 0.20725f), Vector3(0.25f, 0.20725f, 0.20725f), 0.088f); }
Material Material::Ruby() { return Material(Vector3(0.1745f, 0.01175f, 0.01175f), Vector3(0.1745f, 0.01175f, 0.01175f), Vector3(0.1745f, 0.01175f, 0.01175f), 0.6f); }
Material Material::Turquoise() { return Material(Vector3(0.1f, 0.18725f, 0.1745f), Vector3(0.1f, 0.18725f, 0.1745f), Vector3(0.1f, 0.18725f, 0.1745f), 0.1f); }
Material Material::Brass() { return Material(Vector3(0.329412f, 0.223529f, 0.027451f), Vector3(0.329412f, 0.223529f, 0.027451f), Vector3(0.329412f, 0.223529f, 0.027451f), 0.21794872f); }
Material Material::Bronze() { return Material(Vector3(0.2125f, 0.1275f, 0.054f), Vector3(0.2125f, 0.1275f, 0.054f), Vector3(0.2125f, 0.1275f, 0.054f), 0.2f); }
Material Material::Chrome() { return Material(Vector3(0.25f, 0.25f, 0.25f), Vector3(0.25f, 0.25f, 0.25f), Vector3(0.25f, 0.25f, 0.25f), 0.6f); }
Material Material::Copper() { return Material(Vector3(0.19125f, 0.0735f, 0.0225f), Vector3(0.19125f, 0.0735f, 0.0225f), Vector3(0.19125f, 0.0735f, 0.0225f), 0.1f); }
Material Material::Gold() { return Material(Vector3(0.24725f, 0.1995f, 0.0745f), Vector3(0.24725f, 0.1995f, 0.0745f), Vector3(0.24725f, 0.1995f, 0.0745f), 0.4f); }
Material Material::Silver() { return Material(Vector3(0.19225f, 0.19225f, 0.19225f), Vector3(0.19225f, 0.19225f, 0.19225f), Vector3(0.19225f, 0.19225f, 0.19225f), 0.4f); }
Material Material::BlackPlastic() { return Material(Vector3(0.0f, 0.0f, 0.0f), Vector3(0.0f, 0.0f, 0.0f), Vector3(0.0f, 0.0f, 0.0f), 0.25f); }
Material Material::CyanPlastic() { return Material(Vector3(0.0f, 0.1f, 0.06f), Vector3(0.0f, 0.1f, 0.06f), Vector3(0.0f, 0.1f, 0.06f), 0.25f); }
Material Material::GreenPlastic() { return Material(Vector3(0.0f, 0.0f, 0.0f), Vector3(0.0f, 0.0f, 0.0f), Vector3(0.0f, 0.0f, 0.0f), 0.25f); }
Material Material::RedPlastic() { return Material(Vector3(0.0f, 0.0f, 0.0f), Vector3(0.0f, 0.0f, 0.0f), Vector3(0.0f, 0.0f, 0.0f), 0.25f); }
Material Material::WhitePlastic() { return Material(Vector3(0.0f, 0.0f, 0.0f), Vector3(0.0f, 0.0f, 0.0f), Vector3(0.0f, 0.0f, 0.0f), 0.25f); }
Material Material::YellowPlastic() { return Material(Vector3(0.0f, 0.0f, 0.0f), Vector3(0.0f, 0.0f, 0.0f), Vector3(0.0f, 0.0f, 0.0f), 0.25f); }
Material Material::BlackRubber() { return Material(Vector3(0.02f, 0.02f, 0.02f), Vector3(0.02f, 0.02f, 0.02f), Vector3(0.02f, 0.02f, 0.02f), 0.078125f); }
Material Material::CyanRubber() { return Material(Vector3(0.0f, 0.05f, 0.05f), Vector3(0.0f, 0.05f, 0.05f), Vector3(0.0f, 0.05f, 0.05f), 0.078125f); }
Material Material::GreenRubber() { return Material(Vector3(0.0f, 0.05f, 0.0f), Vector3(0.0f, 0.05f, 0.0f), Vector3(0.0f, 0.05f, 0.0f), 0.078125f); }
Material Material::RedRubber() { return Material(Vector3(0.05f, 0.0f, 0.0f), Vector3(0.05f, 0.0f, 0.0f), Vector3(0.05f, 0.0f, 0.0f), 0.078125f); }
Material Material::WhiteRubber() { return Material(Vector3(0.05f, 0.05f, 0.05f), Vector3(0.05f, 0.05f, 0.05f), Vector3(0.05f, 0.05f, 0.05f), 0.078125f); }
Material Material::YellowRubber() { return Material(Vector3(0.05f, 0.05f, 0.0f), Vector3(0.05f, 0.05f, 0.0f), Vector3(0.05f, 0.05f, 0.0f), 0.078125f); }
