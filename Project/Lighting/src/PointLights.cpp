#include <PointLights.h>

namespace {
	inline std::array<PointLight, 4> createDefault() {
		std::array<PointLight, 4> lights;
		PointLight pl;
		pl.enabled = false;
		lights.fill(pl);
		return lights;
	}
}

std::array<PointLight, 4> PointLights = createDefault();

Attenuation::Attenuation() : Attenuation(1.0f, 0.01f, 0.001f) {}

Attenuation::Attenuation(float radius) {
	constant = 1.0f;
	linear = (float)(5.585736 * pow((double)radius, -1.070396));
	quadratic = (float)((3.659005 * pow((double)linear, 1.99085) + 131.3533 * pow((double)radius, -2.205319)) / 2.0);
}

Attenuation::Attenuation(float constant, float linear, float quadratic) : constant(constant), linear(linear), quadratic(quadratic) {}

PointLight::PointLight(const Vector3& position, const Attenuation& attenuation, const Vector3& ambient, const Vector3& diffuse, const Vector3& specular)
	: enabled(true), position(position), attenuation(attenuation), ambient(ambient), diffuse(diffuse), specular(specular) {
}
