#pragma once
#include <array>
#include <Vector3.h>

struct Attenuation {
	float constant;
	float linear;
	float quadratic;

	Attenuation();
	Attenuation(float radius);
	Attenuation(float constant, float linear, float quadratic);
};

struct PointLight {
	Attenuation attenuation;
	Vector3 position;
	Vector3 ambient;
	Vector3 diffuse;
	Vector3 specular;
	bool enabled;

	PointLight(const Vector3& position = Vector3(), const Attenuation& attenuation = Attenuation(), const Vector3& ambient = Vector3(1.0f), const Vector3& diffuse = Vector3(1.0f), const Vector3& specular = Vector3(1.0f));
};

extern std::array<PointLight, 4> PointLights;