#pragma once
#include <Texture.h>
#include <Vector3.h>

struct Distortion {
	int imageIndex;
	float reflectiveness;
	float refractiveness;
	float refractionRatio;

	Distortion(int imageIndex = 0, float reflectiveness = 0.0f, float refractiveness = 0.0f, float refractionRatio = 1.0f / 1.52f);
};

struct Material {
	Distortion distortion;
	Vector3 ambient;
	Vector3 diffuse;
	Vector3 specular;
	float shininess;
	bool enabled;

	Material(float shininess);
	Material(const Vector3& ambient = Vector3(0.1f), const Vector3& diffuse = Vector3(0.8f), const Vector3& specular = Vector3(0.1f), float shininess = 0.5f);

	inline static Material Emerald();
	inline static Material Jade();	
	inline static Material Obsidian();
	inline static Material Pearl();	
	inline static Material Ruby();
	inline static Material Turquoise();
	inline static Material Brass();
	inline static Material Bronze();
	inline static Material Chrome();
	inline static Material Copper();	
	inline static Material Gold();
	inline static Material Silver();
	inline static Material BlackPlastic();
	inline static Material CyanPlastic();
	inline static Material GreenPlastic();
	inline static Material RedPlastic();
	inline static Material WhitePlastic();
	inline static Material YellowPlastic();
	inline static Material BlackRubber();
	inline static Material CyanRubber();
	inline static Material GreenRubber();
	inline static Material RedRubber();
	inline static Material WhiteRubber();
	inline static Material YellowRubber();
};