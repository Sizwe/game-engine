#pragma once
#include <array>
#include <Vector3.h>

struct DirLight {
	Vector3 direction;
	Vector3 ambient;
	Vector3 diffuse;
	Vector3 specular;
	bool enabled;

	DirLight(const Vector3& direction = Vector3(-1.0f), const Vector3& ambient = Vector3(1.0f), const Vector3& diffuse = Vector3(1.0f), const Vector3& specular = Vector3(1.0f));
};

extern std::array<DirLight, 2> DirLights;
