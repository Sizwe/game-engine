#pragma once
#include <Plane.h>

class Line {
public:
	Vector3 u;
	Vector3 v;

	Line() = default;
	Line(const Vector3& u, const Vector3& v) : u(u), v(v) {};

	bool intersects(const Plane& plane) const;
	Vector3 getIntersect(const Plane& plane) const;
};
