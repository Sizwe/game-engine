#pragma once
#include <IVector3.h>
#include <Vector4.h>
#include <ostream>
#include <type_traits>

#pragma pack(push, 1)
template<typename T>
struct IntegerVector4 {
	static_assert(std::is_integral<T>::value, "Integral type required for integer_vector_4");
	T x;
	T y;
	T z;
	T w;

	IntegerVector4();
	IntegerVector4(T xyzw);
	IntegerVector4(T x, T y, T z, T w);
	explicit IntegerVector4(const IntegerVector2<T>& xy, T z = T(), T w = T());
	IntegerVector4(T x, const IntegerVector2<T>& yz, T w = T());
	IntegerVector4(T x, T y, const IntegerVector2<T>& zw);
	explicit IntegerVector4(const IntegerVector3<T>& xyz, T w = T());
	IntegerVector4(T x, const IntegerVector3<T>& yzw);
	explicit IntegerVector4(const Vector4& fvec);

	explicit operator Vector4() const;

	IntegerVector2<T> xy() const;
	IntegerVector2<T> yz() const;
	IntegerVector2<T> zw() const;
	IntegerVector3<T> xyz() const;
	IntegerVector3<T> yzw() const;

	IntegerVector4 operator + (const IntegerVector4& vec) const;
	IntegerVector4 operator - (const IntegerVector4& vec) const;
	IntegerVector4 operator * (const IntegerVector4& vec) const;
	IntegerVector4 operator / (const IntegerVector4& vec) const;
	IntegerVector4 operator % (const IntegerVector4& vec) const;
	IntegerVector4 operator | (const IntegerVector4& vec) const;
	IntegerVector4 operator & (const IntegerVector4& vec) const;
	IntegerVector4 operator ^ (const IntegerVector4& vec) const;
	template<typename T2>
	IntegerVector4 operator + (T2 num) const;
	template<typename T2>
	IntegerVector4 operator - (T2 num) const;
	template<typename T2>
	IntegerVector4 operator * (T2 num) const;
	template<typename T2>
	IntegerVector4 operator / (T2 num) const;
	template<typename T2>
	IntegerVector4 operator % (T2 num) const;
	template<typename T2>
	IntegerVector4 operator | (T2 num) const;
	template<typename T2>
	IntegerVector4 operator & (T2 num) const;
	template<typename T2>
	IntegerVector4 operator ^ (T2 num) const;
	IntegerVector4 operator - () const;
	IntegerVector4 operator ~ () const;
	IntegerVector4 operator ! () const;
	IntegerVector4& operator += (const IntegerVector4& vec);
	IntegerVector4& operator -= (const IntegerVector4& vec);
	IntegerVector4& operator *= (const IntegerVector4& vec);
	IntegerVector4& operator /= (const IntegerVector4& vec);
	IntegerVector4& operator %= (const IntegerVector4& vec);
	IntegerVector4& operator |= (const IntegerVector4& vec);
	IntegerVector4& operator &= (const IntegerVector4& vec);
	IntegerVector4& operator ^= (const IntegerVector4& vec);
	IntegerVector4& operator != (const IntegerVector4& vec);
	template<typename T2>
	IntegerVector4& operator += (T2 num);
	template<typename T2>
	IntegerVector4& operator -= (T2 num);
	template<typename T2>
	IntegerVector4& operator *= (T2 num);
	template<typename T2>
	IntegerVector4& operator /= (T2 num);
	template<typename T2>
	IntegerVector4& operator &= (T2 num);
	template<typename T2>
	IntegerVector4& operator |= (T2 num);
	template<typename T2>
	IntegerVector4& operator ^= (T2 num);

	bool operator == (const IntegerVector4& vec) const;
	bool operator != (const IntegerVector4& vec) const;
	T& operator[](int index);
	const T& operator[](int index) const;

	friend IntegerVector4 operator * (T num, const IntegerVector4<T>& vec) {
		return vec * num;
	}
	friend IntegerVector4 operator / (T num, const IntegerVector4<T>& vec) {
		return vec / num;
	}
	friend std::ostream& operator<<(std::ostream& output, const IntegerVector4<T>& vec) {
		return output << "[" << vec.x << ", " << vec.y << ", " << vec.z << ", " << vec.w << "]";
	}
	friend bool operator <(const IntegerVector4& a, const IntegerVector4<T>& b) {
		return (a.x < b.x) ? true : (b.x < a.x) ? false : (a.y < b.y) ? true : (b.y < a.y) ? false : (a.z < b.z) ? true : (b.z < a.z) ? false : a.w < b.w;
	}
};
#pragma pack(pop)

using IVector4 = IntegerVector4<int32_t>;
using UVector4 = IntegerVector4<uint32_t>;
using LVector4 = IntegerVector4<int64_t>;
using ULVector4 = IntegerVector4<uint64_t>;
using SVector4 = IntegerVector4<int16_t>;
using USVector4 = IntegerVector4<uint16_t>;
using CVector4 = IntegerVector4<int8_t>;
using UCVector4 = IntegerVector4<uint8_t>;
using BVector4 = IntegerVector4<bool>;
using size_t4 = IntegerVector4<size_t>;
using IVec4 = IVector4;
using UVec4 = UVector4;
using LVec4 = LVector4;
using ULvec4 = ULVector4;
using SVec4 = SVector4;
using USVec4 = USVector4;
using CVec4 = CVector4;
using UCVec4 = UCVector4;
using BVec4 = BVector4;

template<typename T>
inline IntegerVector4<T>::IntegerVector4() : x(T()), y(T()), z(T()), w(T()) {}
template<typename T>
inline IntegerVector4<T>::IntegerVector4(T xyzw) : x(xyzw), y(xyzw), z(xyzw), w(xyzw) {}
template<typename T>
inline IntegerVector4<T>::IntegerVector4(T x, T y, T z, T w) : x(x), y(y), z(z), w(w) {}

template<typename T>
inline IntegerVector4<T>::IntegerVector4(const IntegerVector2<T>& xy, T z, T w) : x(xy.x), y(xy.y), z(z), w(w) {}
template<typename T>
inline IntegerVector4<T>::IntegerVector4(T x, const IntegerVector2<T>& yz, T w) : x(x), y(yz.x), z(yz.y), w(w) {}
template<typename T>
inline IntegerVector4<T>::IntegerVector4(T x, T y, const IntegerVector2<T>& zw) : x(x), y(y), z(zw.x), w(zw.y) {}
template<typename T>
inline IntegerVector4<T>::IntegerVector4(const IntegerVector3<T>& xyz, T w) : x(xyz.x), y(xyz.y), z(xyz.z), w(w) {}
template<typename T>
inline IntegerVector4<T>::IntegerVector4(T x, const IntegerVector3<T>& yzw) : x(x), y(yzw.x), z(yzw.y), w(yzw.z) {}
template<typename T>
inline IntegerVector4<T>::IntegerVector4(const Vector4& fvec) : x(T(fvec.x)), y(T(fvec.y)), z(T(fvec.z)), w(T(fvec.w)) {}
template<typename T>
inline IntegerVector4<T>::operator Vector4() const {
	return Vector4((float)x, (float)y, (float)z, (float)w);
}

template<typename T>
inline IntegerVector2<T> IntegerVector4<T>::xy() const {
	return IntegerVector2<T>(x, y);
}
template<typename T>
inline IntegerVector2<T> IntegerVector4<T>::yz() const {
	return IntegerVector2<T>(y, z);
}
template<typename T>
inline IntegerVector2<T> IntegerVector4<T>::zw() const {
	return IntegerVector2<T>(z, w);
}
template<typename T>
inline IntegerVector3<T> IntegerVector4<T>::xyz() const {
	return IntegerVector3<T>(x, y, z);
}
template<typename T>
inline IntegerVector3<T> IntegerVector4<T>::yzw() const {
	return IntegerVector3<T>(y, z, w);
}

template<typename T>
inline IntegerVector4<T> IntegerVector4<T>::operator + (const IntegerVector4& vec) const {
	return IntegerVector4(x + vec.x, y + vec.y, z + vec.z, w + vec.w);
}
template<typename T>
inline IntegerVector4<T> IntegerVector4<T>::operator - (const IntegerVector4& vec) const {
	return IntegerVector4(x - vec.x, y - vec.y, z - vec.z, w - vec.w);
}
template<typename T>
inline IntegerVector4<T> IntegerVector4<T>::operator * (const IntegerVector4& vec) const {
	return IntegerVector4(x * vec.x, y * vec.y, z * vec.z, w * vec.w);
}
template<typename T>
inline IntegerVector4<T> IntegerVector4<T>::operator / (const IntegerVector4& vec) const {
	return IntegerVector4(x / vec.x, y / vec.y, z / vec.z, w / vec.w);
}
template<typename T>
inline IntegerVector4<T> IntegerVector4<T>::operator%(const IntegerVector4& vec) const
{
	return IntegerVector4(x % vec.x, y % vec.y, z % vec.z, w % vec.w);
}
template<typename T>
inline IntegerVector4<T> IntegerVector4<T>::operator|(const IntegerVector4& vec) const
{
	return IntegerVector4(x | vec.x, y | vec.y, z | vec.z, w | vec.w);
}
template<typename T>
inline IntegerVector4<T> IntegerVector4<T>::operator&(const IntegerVector4& vec) const
{
	return IntegerVector4(x & vec.x, y & vec.y, z & vec.z, w & vec.w);
}
template<typename T>
inline IntegerVector4<T> IntegerVector4<T>::operator^(const IntegerVector4& vec) const
{
	return IntegerVector4(x ^ vec.x, y ^ vec.y, z ^ vec.z, w ^ vec.w);
}
template<typename T>
template<typename T2>
inline IntegerVector4<T> IntegerVector4<T>::operator+(T2 num) const
{
	return IntegerVector4(x + num, y + num, z + num, w + num);
}
template<typename T>
template<typename T2>
inline IntegerVector4<T> IntegerVector4<T>::operator-(T2 num) const
{
	return IntegerVector4(x - num, y - num, z - num, w - num);
}
template<typename T>
template<typename T2>
inline IntegerVector4<T> IntegerVector4<T>::operator * (T2 num) const {
	return IntegerVector4(x * num, y * num, z * num, w * num);
}
template<typename T>
template<typename T2>
inline IntegerVector4<T> IntegerVector4<T>::operator / (T2 num) const {
	return IntegerVector4(x / num, y / num, z / num, w / num);
}
template<typename T>
template<typename T2>
inline IntegerVector4<T> IntegerVector4<T>::operator%(T2 num) const
{
	return IntegerVector4(x % num, y % num, z % num, w % num);
}
template<typename T>
template<typename T2>
inline IntegerVector4<T> IntegerVector4<T>::operator|(T2 num) const
{
	return IntegerVector4(x | num, y | num, z | num, w | num);
}
template<typename T>
template<typename T2>
inline IntegerVector4<T> IntegerVector4<T>::operator&(T2 num) const
{
	return IntegerVector4(x & num, y & num, z & num, w & num);
}
template<typename T>
template<typename T2>
inline IntegerVector4<T> IntegerVector4<T>::operator^(T2 num) const
{
	return IntegerVector4(x ^ num, y ^ num, z ^ num, w ^ num);
}
template<typename T>
inline IntegerVector4<T> IntegerVector4<T>::operator-() const {
	return IntegerVector4(-x, -y, -z, -w);
}

template<typename T>
inline IntegerVector4<T> IntegerVector4<T>::operator~() const
{
	return IntegerVector4(~x, ~y, ~z, ~w);
}

template<typename T>
inline IntegerVector4<T> IntegerVector4<T>::operator!() const
{
	return IntegerVector4(!x, !y, !z, !w);
}

template<typename T>
inline IntegerVector4<T>& IntegerVector4<T>::operator += (const IntegerVector4<T>& vec) {
	x += vec.x;
	y += vec.y;
	z += vec.z;
	w += vec.w;
	return *this;
}
template<typename T>
inline IntegerVector4<T>& IntegerVector4<T>::operator -= (const IntegerVector4<T>& vec) {
	x -= vec.x;
	y -= vec.y;
	z -= vec.z;
	w -= vec.w;
	return *this;
}
template<typename T>
inline IntegerVector4<T>& IntegerVector4<T>::operator *= (const IntegerVector4<T>& vec) {
	x *= vec.x;
	y *= vec.y;
	z *= vec.z;
	w *= vec.w;
	return *this;
}
template<typename T>
inline IntegerVector4<T>& IntegerVector4<T>::operator /= (const IntegerVector4<T>& vec) {
	x /= vec.x;
	y /= vec.y;
	z /= vec.z;
	w /= vec.w;
	return *this;
}
template<typename T>
inline IntegerVector4<T>& IntegerVector4<T>::operator%=(const IntegerVector4& vec)
{
	x %= vec.x;
	y %= vec.y;
	z %= vec.z;
	w %= vec.w;
	return *this;
}
template<typename T>
inline IntegerVector4<T>& IntegerVector4<T>::operator|=(const IntegerVector4& vec)
{
	x |= vec.x;
	y |= vec.y;
	z |= vec.z;
	w |= vec.w;
	return *this;
}
template<typename T>
inline IntegerVector4<T>& IntegerVector4<T>::operator&=(const IntegerVector4& vec)
{
	x &= vec.x;
	y &= vec.y;
	z &= vec.z;
	w &= vec.w;
	return *this;
}
template<typename T>
inline IntegerVector4<T>& IntegerVector4<T>::operator^=(const IntegerVector4& vec)
{
	x ^= vec.x;
	y ^= vec.y;
	z ^= vec.z;
	w ^= vec.w;
	return *this;
}
template<typename T>
inline IntegerVector4<T>& IntegerVector4<T>::operator!=(const IntegerVector4& vec)
{
	x != vec.x;
	y != vec.y;
	z != vec.z;
	w != vec.w;
	return *this;
}
template<typename T>
template<typename T2>
inline IntegerVector4<T>& IntegerVector4<T>::operator+=(T2 num)
{
	x += num;
	y += num;
	z += num;
	w += num;
	return *this;
}

template<typename T>
template<typename T2>
inline IntegerVector4<T>& IntegerVector4<T>::operator-=(T2 num)
{
	x -= num;
	y -= num;
	z -= num;
	w -= num;
	return *this;
}
template<typename T>
template<typename T2>
inline IntegerVector4<T>& IntegerVector4<T>::operator *= (T2 num) {
	x *= num;
	y *= num;
	z *= num;
	w *= num;
	return *this;
}
template<typename T>
template<typename T2>
inline IntegerVector4<T>& IntegerVector4<T>::operator /= (T2 num) {
	x /= num;
	y /= num;
	z /= num;
	w /= num;
	return *this;
}

template<typename T>
template<typename T2>
inline IntegerVector4<T>& IntegerVector4<T>::operator&=(T2 num)
{
	x &= num;
	y &= num;
	z &= num;
	w &= num;
	return *this;
}

template<typename T>
template<typename T2>
inline IntegerVector4<T>& IntegerVector4<T>::operator|=(T2 num)
{
	x |= num;
	y |= num;
	z |= num;
	w |= num;
	return *this;
}

template<typename T>
template<typename T2>
inline IntegerVector4<T>& IntegerVector4<T>::operator^=(T2 num)
{
	x ^= num;
	y ^= num;
	z ^= num;
	w ^= num;
	return *this;
}

template<typename T>
inline bool IntegerVector4<T>::operator == (const IntegerVector4<T>& vec) const {
	return x == vec.x && y == vec.y && z == vec.z && w == vec.w;
}
template<typename T>
inline bool IntegerVector4<T>::operator != (const IntegerVector4<T>& vec) const {
	return x != vec.x || y != vec.y || z != vec.z || w != vec.w;
}
template<typename T>
inline T& IntegerVector4<T>::operator[](int index) {
	switch (index) {
	case 0: return x;
	case 1: return y;
	case 2: return z;
	case 3: return w;
	default: throw std::out_of_range("Integer vector4 index out of range exception");
	}
}
template<typename T>
inline const T& IntegerVector4<T>::operator[](int index) const {
	switch (index) {
	case 0: return x;
	case 1: return y;
	case 2: return z;
	case 3: return w;
	default: throw std::out_of_range("Integer vector4 index out of range exception");
	}
}