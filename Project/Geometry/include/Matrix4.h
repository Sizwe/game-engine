#pragma once

#include <Vector2.h>
#include <Vector3.h>
#include <Vector4.h>
#include <Matrix2.h>
#include <Matrix3.h>

struct Matrix4 {
private:
	struct Row {
		friend Matrix4;
	private:
		float* row;

		Row() = delete;
		Row(Matrix4* mat, int rowIndex);
		Row(const Row& matrix);
		Row& operator = (const Row&) = delete;
	public:
		float& operator[](int collIndex);
		const float& operator[](int collIndex) const;
	};

public:
	float data[16];

	Matrix4();
	Matrix4(float diagonal);
	explicit Matrix4(const Matrix3& mat);
	explicit Matrix4(const Matrix2& mat);

	explicit operator Matrix2() const;
	explicit operator Matrix3() const;

	const float& get(int rowIndex, int collIndex) const;
	float& get(int rowIndex, int collIndex);

	Matrix4 transpose() const;

	float det() const;
	Matrix4 inv() const;
	bool valid() const;

	Matrix4 operator * (float num) const;
	friend Matrix4 operator * (float num, const Matrix4& mat);

	Matrix4 operator + (const Matrix4& mat) const;
	Matrix4 operator - (const Matrix4& mat) const;
	Matrix4 operator * (const Matrix4& mat) const;

	Vector4 operator * (const Vector4& vec) const;
	Vector3 operator * (const Vector3& vec) const;
	Vector2 operator * (const Vector2& vec) const;

	Matrix4& operator *= (float num);
	Matrix4& operator += (const Matrix4& mat);
	Matrix4& operator -= (const Matrix4& mat);
	Matrix4& operator *= (const Matrix4& mat);

	bool operator == (const Matrix4& mat) const;

	Row operator[](int collIndex);
	const Row operator[](int collIndex) const;

	friend std::ostream& operator<<(std::ostream& output, const Matrix4& mat);

	static Matrix4 Identity();

	static Matrix4 Scale(float xFactor, float yFactor = 1.0f, float zFactor = 1.0f, float wFactor = 1.0f);
	static Matrix4 Shear(float xFactor, float yFactor = 0.0f, float zFactor = 0.0f);
	static Matrix4 Shift(float xDist, float yDist = 0.0f, float zDist = 0.0f);
	static Matrix4 Reflect(float xAxis, float yAxis = 0.0f);

	static Matrix4 RotateXYZ(float xDeg, float yDeg = 0.0f, float zDeg = 0.0f);
	static Matrix4 RotateXZY(float xDeg, float yDeg = 0.0f, float zDeg = 0.0f);
	static Matrix4 RotateYXZ(float xDeg, float yDeg = 0.0f, float zDeg = 0.0f);
	static Matrix4 RotateYZX(float xDeg, float yDeg = 0.0f, float zDeg = 0.0f);
	static Matrix4 RotateZXY(float xDeg, float yDeg = 0.0f, float zDeg = 0.0f);
	static Matrix4 RotateZYX(float xDeg, float yDeg = 0.0f, float zDeg = 0.0f);

	static Matrix4 Scale(const Vector4& scale) { return Scale(scale.x, scale.y, scale.z, scale.w); }
	static Matrix4 Scale(const Vector3& scale) { return Scale(scale.x, scale.y, scale.z); }
	static Matrix4 Scale(const Vector2& scale) { return Scale(scale.x, scale.y); }
	static Matrix4 Shear(const Vector3& shear) { return Shear(shear.x, shear.y, shear.z); }
	static Matrix4 Shear(const Vector2& shear) { return Shear(shear.x, shear.y); }
	static Matrix4 Shift(const Vector3& shift) { return Shift(shift.x, shift.y, shift.z); }
	static Matrix4 Shift(const Vector2& shift) { return Shift(shift.x, shift.y); }
	static Matrix4 Reflect(const Vector2& reflect) { return Reflect(reflect.x, reflect.y); }

	static Matrix4 RotateXYZ(const Vector3& rotation) { return RotateXYZ(rotation.x, rotation.y, rotation.z); }
	static Matrix4 RotateXZY(const Vector3& rotation) { return RotateXZY(rotation.x, rotation.y, rotation.z); }
	static Matrix4 RotateYXZ(const Vector3& rotation) { return RotateYXZ(rotation.x, rotation.y, rotation.z); }
	static Matrix4 RotateYZX(const Vector3& rotation) { return RotateYZX(rotation.x, rotation.y, rotation.z); }
	static Matrix4 RotateZXY(const Vector3& rotation) { return RotateZXY(rotation.x, rotation.y, rotation.z); }
	static Matrix4 RotateZYX(const Vector3& rotation) { return RotateZYX(rotation.x, rotation.y, rotation.z); }

	static Matrix4 Ortho(float left, float right, float bottom, float top, float near, float far);
	static Matrix4 Persp(float fov, float aspect, float near, float far);
	static Matrix4 LookAt(const Vector3& position, const Vector3& target, const Vector3& up = Vector3(0.0f, 1.0f, 0.0f));
	static Matrix4 LookDir(const Vector3& direction, const Vector3& up = Vector3(0.0f, 1.0f, 0.0f));
	static Matrix4 Invalid() { Matrix4 m; std::fill(m.data, m.data + 16, std::numeric_limits<float>::quiet_NaN()); return m; }
};
using Mat4 = Matrix4;