#pragma once

#include <Vector2.h>

struct Matrix2 {
private:
	struct Row {
		friend Matrix2;
	private:
		float* row;

		Row() = delete;
		Row(Matrix2* mat, int rowIndex);
		Row(const Row& matrix);
		Row& operator = (const Row&) = delete;
	public:
		float& operator[](int collIndex);
		const float& operator[](int collIndex) const;
	};

public:
	float data[4];

	Matrix2();
	Matrix2(float diagonal);

	const float& get(int rowIndex, int collIndex) const;
	float& get(int rowIndex, int collIndex);

	Matrix2 transpose() const;

	float det() const;
	Matrix2 inv() const;
	bool valid() const;

	Matrix2 operator * (float num) const;
	friend Matrix2 operator * (float num, const Matrix2& matrix);

	Matrix2 operator + (const Matrix2& mat) const;
	Matrix2 operator - (const Matrix2& mat) const;
	Matrix2 operator * (const Matrix2& mat) const;
	Vector2 operator * (const Vector2& vec) const;

	Matrix2& operator *= (float num);
	Matrix2& operator += (const Matrix2& mat);
	Matrix2& operator -= (const Matrix2& mat);
	Matrix2& operator *= (const Matrix2& mat);

	bool operator == (const Matrix2& mat) const;

	Row operator[](int collIndex);
	const Row operator[](int collIndex) const;

	friend std::ostream& operator<<(std::ostream& output, const Matrix2& mat);

	static Matrix2 Identity();

	static Matrix2 Scale(float xFactor, float yFactor = 1.0f);
	static Matrix2 Shear(float xFactor, float yFactor = 0.0f);
	static Matrix2 Scale(const Vector2& scale) { return Scale(scale.x, scale.y); }
	static Matrix2 Shear(const Vector2& shear) { return Shear(shear.x, shear.y); }
	static Matrix2 Invalid() { Matrix2 m; std::fill(m.data, m.data + 4, std::numeric_limits<float>::quiet_NaN()); return m; }
};
using Mat2 = Matrix2;
