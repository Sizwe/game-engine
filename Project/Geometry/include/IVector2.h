#pragma once
#include <initializer_list>
#include <stdexcept>
#include <ostream>
#include <array>
#include <cstdint>
#include <Vector2.h>

#pragma pack(push, 1)
template<typename T>
struct IntegerVector2 {
	static_assert(std::is_integral<T>::value, "Integral type required for integer_vector_2");
	T x;
	T y;

	IntegerVector2();
	IntegerVector2(T xy);
	IntegerVector2(T x, T y);
	IntegerVector2(const std::initializer_list<T>& values);
	explicit IntegerVector2(const Vector2& fvec);

	explicit operator Vector2() const;

	IntegerVector2 operator + (const IntegerVector2& vec) const;
	IntegerVector2 operator - (const IntegerVector2& vec) const;
	IntegerVector2 operator * (const IntegerVector2& vec) const;
	IntegerVector2 operator / (const IntegerVector2& vec) const;
	IntegerVector2 operator % (const IntegerVector2& vec) const;
	IntegerVector2 operator | (const IntegerVector2& vec) const;
	IntegerVector2 operator & (const IntegerVector2& vec) const;
	IntegerVector2 operator ^ (const IntegerVector2& vec) const;
	template<typename T2>
	IntegerVector2 operator + (T2 num) const;
	template<typename T2>
	IntegerVector2 operator - (T2 num) const;
	template<typename T2>
	IntegerVector2 operator * (T2 num) const;
	template<typename T2>
	IntegerVector2 operator / (T2 num) const;
	template<typename T2>
	IntegerVector2 operator % (T2 num) const;
	template<typename T2>
	IntegerVector2 operator | (T2 num) const;
	template<typename T2>
	IntegerVector2 operator & (T2 num) const;
	template<typename T2>
	IntegerVector2 operator ^ (T2 num) const;
	IntegerVector2 operator - () const;
	IntegerVector2 operator ~ () const;
	IntegerVector2& operator += (const IntegerVector2& vec);
	IntegerVector2& operator -= (const IntegerVector2& vec);
	IntegerVector2& operator *= (const IntegerVector2& vec);
	IntegerVector2& operator /= (const IntegerVector2& vec);
	IntegerVector2& operator %= (const IntegerVector2& vec);
	IntegerVector2& operator |= (const IntegerVector2& vec);
	IntegerVector2& operator &= (const IntegerVector2& vec);
	IntegerVector2& operator ^= (const IntegerVector2& vec);
	template<typename T2>
	IntegerVector2& operator += (T2 num);
	template<typename T2>
	IntegerVector2& operator -= (T2 num);
	template<typename T2>
	IntegerVector2& operator *= (T2 num);
	template<typename T2>
	IntegerVector2& operator /= (T2 num);
	template<typename T2>
	IntegerVector2& operator &= (T2 num);
	template<typename T2>
	IntegerVector2& operator |= (T2 num);
	template<typename T2>
	IntegerVector2& operator ^= (T2 num);

	bool operator == (const IntegerVector2& vec) const;
	bool operator != (const IntegerVector2& vec) const;
	T& operator[](int index);
	const T& operator[](int index) const;

	friend IntegerVector2 operator * (T num, const IntegerVector2<T>& vec) {
		return vec * num;
	}
	friend IntegerVector2 operator / (T num, const IntegerVector2<T>& vec) {
		return vec / num;
	}
	friend std::ostream& operator<<(std::ostream& output, const IntegerVector2<T>& vec) {
		return output << "[" << vec.x << ", " << vec.y << "]";
	}
	friend bool operator <(const IntegerVector2& a, const IntegerVector2<T>& b) {
		return (a.x < b.x) ? true : (b.x < a.x) ? false : a.y < b.y;
	}
};
#pragma pack(pop)

using IVector2 = IntegerVector2<int32_t>;
using UVector2 = IntegerVector2<uint32_t>;
using LVector2 = IntegerVector2<int64_t>;
using ULVector2 = IntegerVector2<uint64_t>;
using SVector2 = IntegerVector2<int16_t>;
using USVector2 = IntegerVector2<uint16_t>;
using CVector2 = IntegerVector2<int8_t>;
using UCVector2 = IntegerVector2<uint8_t>;
using BVector2 = IntegerVector2<bool>;
using size_t2 = IntegerVector2<size_t>;
using IVec2 = IVector2;
using UVec2 = UVector2;
using LVec2 = LVector2;
using ULVec2 = ULVector2;
using SVec2 = SVector2;
using USVec2 = USVector2;
using CVec2 = CVector2;
using UCVec2 = UCVector2;
using BVec2 = BVector2;

template<typename T>
inline IntegerVector2<T>::IntegerVector2() : x(T()), y(T()) {}
template<typename T>
inline IntegerVector2<T>::IntegerVector2(T xy) : x(xy), y(xy) {}
template<typename T>
inline IntegerVector2<T>::IntegerVector2(T x, T y) : x(x), y(y) {}
template<typename T>
inline IntegerVector2<T>::IntegerVector2(const std::initializer_list<T>& values) {
	auto it = values.begin();
	switch (values.size()) {
	case 0:
		break;
	case 1:
		x = *(it++);
		break;
	default:
		x = *(it++);
		y = *(it++);
	}
}
template<typename T>
inline IntegerVector2<T>::IntegerVector2(const Vector2& fvec) : x(T(fvec.x)), y(T(fvec.y)) {}
template<typename T>
inline IntegerVector2<T>::operator Vector2() const {
	return Vector2((float)x, (float)y);
}

template<typename T>
inline IntegerVector2<T> IntegerVector2<T>::operator + (const IntegerVector2& vec) const {
	return IntegerVector2(x + vec.x, y + vec.y);
}
template<typename T>
inline IntegerVector2<T> IntegerVector2<T>::operator - (const IntegerVector2& vec) const {
	return IntegerVector2(x - vec.x, y - vec.y);
}
template<typename T>
inline IntegerVector2<T> IntegerVector2<T>::operator * (const IntegerVector2& vec) const {
	return IntegerVector2(x * vec.x, y * vec.y);
}
template<typename T>
inline IntegerVector2<T> IntegerVector2<T>::operator / (const IntegerVector2& vec) const {
	return IntegerVector2(x / vec.x, y / vec.y);
}
template<typename T>
inline IntegerVector2<T> IntegerVector2<T>::operator%(const IntegerVector2& vec) const
{
	return IntegerVector2(x % vec.x, y % vec.y);
}
template<typename T>
inline IntegerVector2<T> IntegerVector2<T>::operator|(const IntegerVector2& vec) const
{
	return IntegerVector2(x | vec.x, y | vec.y);
}
template<typename T>
inline IntegerVector2<T> IntegerVector2<T>::operator&(const IntegerVector2& vec) const
{
	return IntegerVector2(x & vec.x, y & vec.y);
}
template<typename T>
inline IntegerVector2<T> IntegerVector2<T>::operator^(const IntegerVector2& vec) const
{
	return IntegerVector2(x ^ vec.x, y ^ vec.y);
}
template<typename T>
template<typename T2>
inline IntegerVector2<T> IntegerVector2<T>::operator+(T2 num) const
{
	return IntegerVector2(x + num, y + num);
}
template<typename T>
template<typename T2>
inline IntegerVector2<T> IntegerVector2<T>::operator-(T2 num) const
{
	return IntegerVector2(x - num, y - num);
}
template<typename T>
template<typename T2>
inline IntegerVector2<T> IntegerVector2<T>::operator * (T2 num) const {
	return IntegerVector2(x * num, y * num);
}
template<typename T>
template<typename T2>
inline IntegerVector2<T> IntegerVector2<T>::operator / (T2 num) const {
	return IntegerVector2(x / num, y / num);
}
template<typename T>
template<typename T2>
inline IntegerVector2<T> IntegerVector2<T>::operator%(T2 num) const
{
	return IntegerVector2(x % num, y % num);
}
template<typename T>
template<typename T2>
inline IntegerVector2<T> IntegerVector2<T>::operator|(T2 num) const
{
	return IntegerVector2(x | num, y | num);
}
template<typename T>
template<typename T2>
inline IntegerVector2<T> IntegerVector2<T>::operator&(T2 num) const
{
	return IntegerVector2(x & num, y & num);
}
template<typename T>
template<typename T2>
inline IntegerVector2<T> IntegerVector2<T>::operator^(T2 num) const
{
	return IntegerVector2(x ^ num, y ^ num);
}
template<typename T>
inline IntegerVector2<T> IntegerVector2<T>::operator-() const {
	return IntegerVector2(-x, -y);
}

template<typename T>
inline IntegerVector2<T> IntegerVector2<T>::operator~() const
{
	return IntegerVector2(~x, ~y);
}

template<typename T>
inline IntegerVector2<T>& IntegerVector2<T>::operator += (const IntegerVector2& vec) {
	x += vec.x;
	y += vec.y;
	return *this;
}
template<typename T>
inline IntegerVector2<T>& IntegerVector2<T>::operator -= (const IntegerVector2& vec) {
	x -= vec.x;
	y -= vec.y;
	return *this;
}
template<typename T>
inline IntegerVector2<T>& IntegerVector2<T>::operator *= (const IntegerVector2& vec) {
	x *= vec.x;
	y *= vec.y;
	return *this;
}
template<typename T>
inline IntegerVector2<T>& IntegerVector2<T>::operator /= (const IntegerVector2& vec) {
	x /= vec.x;
	y /= vec.y;
	return *this;
}
template<typename T>
inline IntegerVector2<T>& IntegerVector2<T>::operator%=(const IntegerVector2& vec)
{
	x %= vec.x;
	y %= vec.y;
	return *this;
}
template<typename T>
inline IntegerVector2<T>& IntegerVector2<T>::operator|=(const IntegerVector2& vec)
{
	x |= vec.x;
	y |= vec.y;
	return *this;
}
template<typename T>
inline IntegerVector2<T>& IntegerVector2<T>::operator&=(const IntegerVector2& vec)
{
	x &= vec.x;
	y &= vec.y;
	return *this;
}
template<typename T>
inline IntegerVector2<T>& IntegerVector2<T>::operator^=(const IntegerVector2& vec)
{
	x ^= vec.x;
	y ^= vec.y;
	return *this;
}

template<typename T>
template<typename T2>
inline IntegerVector2<T>& IntegerVector2<T>::operator+=(T2 num)
{
	x += num;
	y += num;
	return *this;
}
template<typename T>
template<typename T2>
inline IntegerVector2<T>& IntegerVector2<T>::operator-=(T2 num)
{
	x -= num;
	y -= num;
	return *this;
}
template<typename T>
template<typename T2>
inline IntegerVector2<T>& IntegerVector2<T>::operator *= (T2 num) {
	x *= num;
	y *= num;
	return *this;
}
template<typename T>
template<typename T2>
inline IntegerVector2<T>& IntegerVector2<T>::operator /= (T2 num) {
	x /= num;
	y /= num;
	return *this;
}

template<typename T>
template<typename T2>
inline IntegerVector2<T>& IntegerVector2<T>::operator&=(T2 num)
{
	x &= num;
	y &= num;
	return *this;
}

template<typename T>
template<typename T2>
inline IntegerVector2<T>& IntegerVector2<T>::operator|=(T2 num)
{
	x |= num;
	y |= num;
	return *this;
}

template<typename T>
template<typename T2>
inline IntegerVector2<T>& IntegerVector2<T>::operator^=(T2 num)
{
	x ^= num;
	y ^= num;
	return *this;
}

template<typename T>
inline bool IntegerVector2<T>::operator == (const IntegerVector2<T>& vec) const {
	return x == vec.x && y == vec.y;
}
template<typename T>
inline bool IntegerVector2<T>::operator != (const IntegerVector2<T>& vec) const {
	return x != vec.x || y != vec.y;
}
template<typename T>
inline T& IntegerVector2<T>::operator[](int index) {
	switch (index) {
	case 0: return x;
	case 1: return y;
	default: throw std::out_of_range("Integer vector index out of range exception");
	}
}
template<typename T>
inline const T& IntegerVector2<T>::operator[](int index) const {
	switch (index) {
	case 0: return x;
	case 1: return y;
	default: throw std::out_of_range("Integer vector index out of range exception");
	}
}