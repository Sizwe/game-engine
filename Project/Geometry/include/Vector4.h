#pragma once

#include <Vector2.h>
#include <Vector3.h>
#include <array>

#pragma pack(push, 1)
struct Vector4 {
	float x;
	float y;
	float z;
	float w;

	Vector4();
	Vector4(float xyzw);
	Vector4(float x, float y, float z, float w);
	Vector4(float x, const Vector2& vr2, float w = 0.0f);
	Vector4(float x, float y, const Vector2& vr2);
	Vector4(float x, const Vector3& vr3);
	Vector4(const Vector2& v1, const Vector2& v2);
	explicit Vector4(const Vector2& vr2, float z = 0.0f, float w = 0.0f);
	explicit Vector4(const Vector3& vr3, float w = 0.0f);

	float dist(const Vector4& vector) const;
	float dot(const Vector4& vector) const;
	float magnitude() const;
	Vector4 to(const Vector4& vector) const;
	Vector4 normalise() const;
	bool valid() const;

	Vector4 towards(const Vector4& vector, float step) const;
	Vector4 lerp(const Vector4& vector, float frac) const;

	Vector4 operator + (const Vector4& obj) const;
	Vector4 operator - (const Vector4& obj) const;
	Vector4 operator * (const Vector4& obj) const;
	Vector4 operator * (float num) const;
	Vector4 operator / (float num) const;
	Vector4 operator - () const;

	Vector4& operator += (const Vector4& obj);
	Vector4& operator -= (const Vector4& obj);
	Vector4& operator *= (const Vector4& obj);
	Vector4& operator *= (float num);
	Vector4& operator /= (float num);

	Vector2 xy() const;
	Vector2 yz() const;
	Vector2 zw() const;
	Vector3 xyz() const;
	Vector3 yzw() const;

	friend Vector4 operator * (float num, const Vector4& vect);
	friend Vector4 operator / (float num, const Vector4& vect);

	bool operator == (const Vector4& obj) const;
	bool operator != (const Vector4& obj) const;
	float& operator[](int index);
	const float& operator[](int index) const;

	friend std::ostream& operator<<(std::ostream& output, const Vector4& vect);
	friend bool operator <(const Vector4& a, const Vector4& b);

	static Vector4 Invalid() { constexpr float NaN = std::numeric_limits<float>::quiet_NaN(); return Vector4(NaN, NaN, NaN, NaN); }
};
#pragma pack(pop)
using Point4 = Vector4;
using Vec4 = Vector4;