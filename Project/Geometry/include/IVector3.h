#pragma once
#include <IVector2.h>
#include <Vector3.h>

#pragma pack(push, 1)
template<typename T>
struct IntegerVector3 {
	static_assert(std::is_integral<T>::value, "Integral type required for integer_vector_3");
	T x;
	T y;
	T z;

	IntegerVector3();
	IntegerVector3(T xyz);
	IntegerVector3(T x, T y, T z);
	IntegerVector3(const std::initializer_list<T>& values);
	explicit IntegerVector3(const IntegerVector2<T>& xy, T z = T());
	IntegerVector3(T x, const IntegerVector2<T>& yz);
	explicit IntegerVector3(const Vector3& fvec);

	explicit operator Vector3() const;

	IntegerVector2<T> xy() const;
	IntegerVector2<T> yz() const;

	IntegerVector3 operator + (const IntegerVector3& vec) const;
	IntegerVector3 operator - (const IntegerVector3& vec) const;
	IntegerVector3 operator * (const IntegerVector3& vec) const;
	IntegerVector3 operator / (const IntegerVector3& vec) const;
	IntegerVector3 operator % (const IntegerVector3& vec) const;
	IntegerVector3 operator | (const IntegerVector3& vec) const;
	IntegerVector3 operator & (const IntegerVector3& vec) const;
	IntegerVector3 operator ^ (const IntegerVector3& vec) const;
	template<typename T2>
	IntegerVector3 operator + (T2 num) const;
	template<typename T2>
	IntegerVector3 operator - (T2 num) const;
	template<typename T2>
	IntegerVector3 operator * (T2 num) const;
	template<typename T2>
	IntegerVector3 operator / (T2 num) const;
	template<typename T2>
	IntegerVector3 operator % (T2 num) const;
	template<typename T2>
	IntegerVector3 operator | (T2 num) const;
	template<typename T2>
	IntegerVector3 operator & (T2 num) const;
	template<typename T2>
	IntegerVector3 operator ^ (T2 num) const;
	IntegerVector3 operator - () const;
	IntegerVector3 operator ~ () const;
	IntegerVector3 operator ! () const;
	IntegerVector3& operator += (const IntegerVector3& vec);
	IntegerVector3& operator -= (const IntegerVector3& vec);
	IntegerVector3& operator *= (const IntegerVector3& vec);
	IntegerVector3& operator /= (const IntegerVector3& vec);
	IntegerVector3& operator %= (const IntegerVector3& vec);
	IntegerVector3& operator |= (const IntegerVector3& vec);
	IntegerVector3& operator &= (const IntegerVector3& vec);
	IntegerVector3& operator ^= (const IntegerVector3& vec);
	IntegerVector3& operator != (const IntegerVector3& vec);
	template<typename T2>
	IntegerVector3& operator += (T2 num);
	template<typename T2>
	IntegerVector3& operator -= (T2 num);
	template<typename T2>
	IntegerVector3& operator *= (T2 num);
	template<typename T2>
	IntegerVector3& operator /= (T2 num);
	template<typename T2>
	IntegerVector3& operator &= (T2 num);
	template<typename T2>
	IntegerVector3& operator |= (T2 num);
	template<typename T2>
	IntegerVector3& operator ^= (T2 num);

	bool operator == (const IntegerVector3& vec) const;
	bool operator != (const IntegerVector3& vec) const;
	T& operator[](int index);
	const T& operator[](int index) const;

	friend IntegerVector3 operator * (T num, const IntegerVector3<T>& vec) {
		return vec * num;
	}
	friend IntegerVector3 operator / (T num, const IntegerVector3<T>& vec) {
		return vec / num;
	}
	friend std::ostream& operator<<(std::ostream& output, const IntegerVector3<T>& vec) {
		return output << "[" << vec.x << ", " << vec.y << ", " << vec.z << "]";
	}
	friend bool operator <(const IntegerVector3& a, const IntegerVector3<T>& b) {
		return (a.x < b.x) ? true : (b.x < a.x) ? false : (a.y < b.y) ? true : (b.y < a.y) ? false : a.z < b.z;
	}
};
#pragma pack(pop)

using IVector3 = IntegerVector3<int32_t>;
using UVector3 = IntegerVector3<uint32_t>;
using LVector3 = IntegerVector3<int64_t>;
using ULVector3 = IntegerVector3<uint64_t>;
using SVector3 = IntegerVector3<int16_t>;
using USVector3 = IntegerVector3<uint16_t>;
using CVector3 = IntegerVector3<int8_t>;
using UCVector3 = IntegerVector3<uint8_t>;
using BVector3 = IntegerVector3<bool>;
using size_t3 = IntegerVector3<size_t>;
using IVec3 = IVector3;
using UVec3 = UVector3;
using LVec3 = LVector3;
using ULVec3 = ULVector3;
using SVec3 = SVector3;
using USVec3 = USVector3;
using CVec3 = CVector3;
using UCVec3 = UCVector3;
using BVec3 = BVector3;

template<typename T>
inline IntegerVector3<T>::IntegerVector3() : x(T()), y(T()), z(T()) {}
template<typename T>
inline IntegerVector3<T>::IntegerVector3(T xyz) : x(xyz), y(xyz), z(xyz) {}
template<typename T>
inline IntegerVector3<T>::IntegerVector3(T x, T y, T z) : x(x), y(y), z(z) {}
template<typename T>
inline IntegerVector3<T>::IntegerVector3(const std::initializer_list<T>& values) {
	auto it = values.begin();
	switch (values.size()) {
	case 0:
		break;
	case 1:
		x = *(it++);
		break;
	case 2:
		x = *(it++);
		y = *(it++);
		break;
	default:
		x = *(it++);
		y = *(it++);
		z = *(it++);
	}
}

template<typename T>
inline IntegerVector3<T>::IntegerVector3(const IntegerVector2<T>& xy, T z) : x(xy.x), y(xy.y), z(z) {}
template<typename T>
inline IntegerVector3<T>::IntegerVector3(T x, const IntegerVector2<T>& yz) : x(x), y(yz.x), z(yz.y) {}
template<typename T>
inline IntegerVector3<T>::IntegerVector3(const Vector3& fvec) : x(T(fvec.x)), y(T(fvec.y)), z(T(fvec.z)) {}
template<typename T>
inline IntegerVector3<T>::operator Vector3() const {
	return Vector3((float)x, (float)y, (float)z);
}

template<typename T>
inline IntegerVector2<T> IntegerVector3<T>::xy() const {
	return IntegerVector2<T>(x, y);
}
template<typename T>
inline IntegerVector2<T> IntegerVector3<T>::yz() const {
	return IntegerVector2<T>(y, z);
}

template<typename T>
inline IntegerVector3<T> IntegerVector3<T>::operator + (const IntegerVector3& vec) const {
	return IntegerVector3(x + vec.x, y + vec.y, z + vec.z);
}
template<typename T>
inline IntegerVector3<T> IntegerVector3<T>::operator - (const IntegerVector3& vec) const {
	return IntegerVector3(x - vec.x, y - vec.y, z - vec.z);
}
template<typename T>
inline IntegerVector3<T> IntegerVector3<T>::operator * (const IntegerVector3& vec) const {
	return IntegerVector3(x * vec.x, y * vec.y, z * vec.z);
}
template<typename T>
inline IntegerVector3<T> IntegerVector3<T>::operator / (const IntegerVector3& vec) const {
	return IntegerVector3(x / vec.x, y / vec.y, z / vec.z);
}
template<typename T>
inline IntegerVector3<T> IntegerVector3<T>::operator%(const IntegerVector3& vec) const
{
	return IntegerVector3(x % vec.x, y % vec.y, z % vec.z);
}
template<typename T>
inline IntegerVector3<T> IntegerVector3<T>::operator|(const IntegerVector3& vec) const
{
	return IntegerVector3(x | vec.x, y | vec.y, z | vec.z);
}
template<typename T>
inline IntegerVector3<T> IntegerVector3<T>::operator&(const IntegerVector3& vec) const
{
	return IntegerVector3(x & vec.x, y & vec.y, z & vec.z);
}
template<typename T>
inline IntegerVector3<T> IntegerVector3<T>::operator^(const IntegerVector3& vec) const
{
	return IntegerVector3(x ^ vec.x, y ^ vec.y, z ^ vec.z);
}
template<typename T>
template<typename T2>
inline IntegerVector3<T> IntegerVector3<T>::operator+(T2 num) const
{
	return IntegerVector3(x + num, y + num, z + num);
}
template<typename T>
template<typename T2>
inline IntegerVector3<T> IntegerVector3<T>::operator-(T2 num) const
{
	return IntegerVector3(x - num, y - num, z - num);
}
template<typename T>
template<typename T2>
inline IntegerVector3<T> IntegerVector3<T>::operator * (T2 num) const {
	return IntegerVector3(x * num, y * num, z * num);
}
template<typename T>
template<typename T2>
inline IntegerVector3<T> IntegerVector3<T>::operator / (T2 num) const {
	return IntegerVector3(x / num, y / num, z / num);
}
template<typename T>
template<typename T2>
inline IntegerVector3<T> IntegerVector3<T>::operator%(T2 num) const
{
	return IntegerVector3(x % num, y % num, z % num);
}
template<typename T>
template<typename T2>
inline IntegerVector3<T> IntegerVector3<T>::operator|(T2 num) const
{
	return IntegerVector3(x | num, y | num, z | num);
}
template<typename T>
template<typename T2>
inline IntegerVector3<T> IntegerVector3<T>::operator&(T2 num) const
{
	return IntegerVector3(x & num, y & num, z & num);
}
template<typename T>
template<typename T2>
inline IntegerVector3<T> IntegerVector3<T>::operator^(T2 num) const
{
	return IntegerVector3(x ^ num, y ^ num, z ^ num);
}
template<typename T>
inline IntegerVector3<T> IntegerVector3<T>::operator-() const {
	return IntegerVector3(-x, -y, -z);
}

template<typename T>
inline IntegerVector3<T> IntegerVector3<T>::operator~() const
{
	return IntegerVector3(~x, ~y, ~z);
}

template<typename T>
inline IntegerVector3<T> IntegerVector3<T>::operator!() const
{
	return IntegerVector3(!x, !y, !z);
}

template<typename T>
inline IntegerVector3<T>& IntegerVector3<T>::operator += (const IntegerVector3& vec) {
	x += vec.x;
	y += vec.y;
	z += vec.z;
	return *this;
}
template<typename T>
inline IntegerVector3<T>& IntegerVector3<T>::operator -= (const IntegerVector3& vec) {
	x -= vec.x;
	y -= vec.y;
	z -= vec.z;
	return *this;
}
template<typename T>
inline IntegerVector3<T>& IntegerVector3<T>::operator *= (const IntegerVector3& vec) {
	x *= vec.x;
	y *= vec.y;
	z *= vec.z;
	return *this;
}
template<typename T>
inline IntegerVector3<T>& IntegerVector3<T>::operator /= (const IntegerVector3& vec) {
	x /= vec.x;
	y /= vec.y;
	z /= vec.z;
	return *this;
}
template<typename T>
inline IntegerVector3<T>& IntegerVector3<T>::operator%=(const IntegerVector3& vec)
{
	x %= vec.x;
	y %= vec.y;
	z %= vec.z;
	return *this;
}
template<typename T>
inline IntegerVector3<T>& IntegerVector3<T>::operator|=(const IntegerVector3& vec)
{
	x |= vec.x;
	y |= vec.y;
	z |= vec.z;
	return *this;
}
template<typename T>
inline IntegerVector3<T>& IntegerVector3<T>::operator&=(const IntegerVector3& vec)
{
	x &= vec.x;
	y &= vec.y;
	z &= vec.z;
	return *this;
}
template<typename T>
inline IntegerVector3<T>& IntegerVector3<T>::operator^=(const IntegerVector3& vec)
{
	x ^= vec.x;
	y ^= vec.y;
	z ^= vec.z;
	return *this;
}
template<typename T>
inline IntegerVector3<T>& IntegerVector3<T>::operator!=(const IntegerVector3& vec)
{
	x != vec.x;
	y != vec.y;
	z != vec.z;
	return *this;
}
template<typename T>
template<typename T2>
inline IntegerVector3<T>& IntegerVector3<T>::operator+=(T2 num)
{
	x += num;
	y += num;
	z += num;
	return *this;
}
template<typename T>
template<typename T2>
inline IntegerVector3<T>& IntegerVector3<T>::operator-=(T2 num)
{
	x -= num;
	y -= num;
	z -= num;
	return *this;
}
template<typename T>
template<typename T2>
inline IntegerVector3<T>& IntegerVector3<T>::operator *= (T2 num) {
	x *= num;
	y *= num;
	z *= num;
	return *this;
}
template<typename T>
template<typename T2>
inline IntegerVector3<T>& IntegerVector3<T>::operator /= (T2 num) {
	x /= num;
	y /= num;
	z /= num;
	return *this;
}

template<typename T>
template<typename T2>
inline IntegerVector3<T>& IntegerVector3<T>::operator&=(T2 num)
{
	x &= num;
	y &= num;
	z &= num;
	return *this;
}

template<typename T>
template<typename T2>
inline IntegerVector3<T>& IntegerVector3<T>::operator|=(T2 num)
{
	x |= num;
	y |= num;
	z |= num;
	return *this;
}

template<typename T>
template<typename T2>
inline IntegerVector3<T>& IntegerVector3<T>::operator^=(T2 num)
{
	x ^= num;
	y ^= num;
	z ^= num;
	return *this;
}

template<typename T>
inline bool IntegerVector3<T>::operator == (const IntegerVector3& vec) const {
	return x == vec.x && y == vec.y && z == vec.z;
}
template<typename T>
inline bool IntegerVector3<T>::operator != (const IntegerVector3& vec) const {
	return x != vec.x || y != vec.y || z != vec.z;
}
template<typename T>
inline T& IntegerVector3<T>::operator[](int index) {
	switch (index) {
	case 0: return x;
	case 1: return y;
	case 2: return z;
	default: throw std::out_of_range("Integer vector3 index out of range exception");
	}
}
template<typename T>
inline const T& IntegerVector3<T>::operator[](int index) const {
	switch (index) {
	case 0: return x;
	case 1: return y;
	case 2: return z;
	default: throw std::out_of_range("Integer vector3 index out of range exception");
	}
}
