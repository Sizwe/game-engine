#pragma once
#include <Vector3.h>

class Plane {
public:
	Vector3 point;
	Vector3 normal;

	Plane() = default;
	Plane(const Vector3& point, const Vector3& normal) : point(point), normal(normal) {};
};