#pragma once
#include <array>

#pragma pack(push, 1)
struct Vector2 {
	float x;
	float y;

	Vector2();
	Vector2(float xy);
	Vector2(float x, float y);

	float dist(const Vector2& vector) const;
	float dot(const Vector2& vector) const;
	float magnitude() const;
	Vector2 to(const Vector2& vector) const;
	Vector2 normalise() const;
	bool valid() const;

	Vector2 towards(const Vector2& target, float step) const;
	Vector2 lerp(const Vector2& target, float frac) const;

	Vector2 operator + (const Vector2& obj) const;
	Vector2 operator - (const Vector2& obj) const;
	Vector2 operator * (const Vector2& obj) const;
	Vector2 operator * (float num) const;
	Vector2 operator / (float num) const;
	Vector2 operator - () const;

	Vector2& operator += (const Vector2& obj);
	Vector2& operator -= (const Vector2& obj);
	Vector2& operator *= (const Vector2& obj);
	Vector2& operator *= (float num);
	Vector2& operator /= (float num);

	friend Vector2 operator * (float num, const Vector2& vect);
	friend Vector2 operator / (float num, const Vector2& vect);

	bool operator == (const Vector2& obj) const;
	bool operator != (const Vector2& obj) const;
	float& operator[](int index);
	const float& operator[](int index) const;

	friend std::ostream& operator<<(std::ostream& output, const Vector2& vect);
	friend bool operator <(const Vector2& a, const Vector2& b);

	static Vector2 Invalid() { constexpr float NaN = std::numeric_limits<float>::quiet_NaN(); return Vector2(NaN, NaN); }
};
#pragma pack(pop)
using Point2 = Vector2;
using Vec2 = Vector2;
