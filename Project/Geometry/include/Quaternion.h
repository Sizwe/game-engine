#pragma once

#include <Vector3.h>
#include <Vector4.h>
#include <Matrix3.h>
#include <Matrix4.h>

struct Quaternion {
	float s;
	float x;
	float y;
	float z;

	Quaternion();
	Quaternion(float s, float x, float y, float z);
	Quaternion(const Vector3& axis, float angle);
	Quaternion(float angle, const Vector3& axis);
	explicit Quaternion(const Vector3& eulerAngles);
	explicit Quaternion(const Matrix3& rotationMx);
	explicit Quaternion(const Matrix4& rotationMx);

	Vector3 toEuler() const;
	Matrix3 toMatrix3() const;
	Matrix4 toMatrix4() const;
	std::pair<Vector3, float> toAxisAngle() const;
	std::pair<float, Vector3> toAngleAxis() const;
	explicit operator Vector3() const;
	explicit operator Vector4() const;
	explicit operator Matrix3() const;
	explicit operator Matrix4() const;

	float magnitude() const;
	float dot(const Quaternion& q) const;
	float angle(const Quaternion& q) const;
	Quaternion norm() const;
	Quaternion conj() const;
	Quaternion inv() const;
	Quaternion lerp(const Quaternion& q, float percentage) const;
	Quaternion slerp(const Quaternion& q, float percentage) const;
	Quaternion to(const Quaternion& q) const;
	Quaternion towards(const Quaternion& q, float angleStep) const;
	Quaternion stowards(const Quaternion& q, float angleStep) const;
	Quaternion exp() const;
	Quaternion ln() const;
	Quaternion pow(float exp) const;
	bool valid() const;

	Quaternion operator-() const;
	Quaternion operator + (const Quaternion& q) const;
	Quaternion operator - (const Quaternion& q) const;
	Quaternion operator * (const Quaternion& q) const;
	Quaternion operator / (const Quaternion& q) const;
	bool operator== (const Quaternion& q) const;
	bool operator!= (const Quaternion& q) const;

	Quaternion& operator= (float f);
	Quaternion operator + (float f) const;
	Quaternion operator - (float f) const;
	Quaternion operator * (float f) const;
	Quaternion operator / (float f) const;
	friend Quaternion operator+ (float f, const Quaternion& q);
	friend Quaternion operator- (float f, const Quaternion& q);
	friend Quaternion operator* (float f, const Quaternion& q);
	friend Quaternion operator/ (float f, const Quaternion& q);

	Quaternion& operator += (const Quaternion& q);
	Quaternion& operator -= (const Quaternion& q);
	Quaternion& operator *= (const Quaternion& q);
	Quaternion& operator /= (const Quaternion& q);
	Quaternion& operator += (float f);
	Quaternion& operator -= (float f);
	Quaternion& operator *= (float f);
	Quaternion& operator /= (float f);

	Vector3 operator * (const Vector3& q) const;
	friend Vector3 operator * (const Vector3& v, const Quaternion& q);
	friend Vector3& operator *= (Vector3& v, const Quaternion& q);

	friend std::ostream& operator<<(std::ostream& output, const Quaternion& q);
	friend bool operator <(const Quaternion& a, const Quaternion& b);

	static Quaternion LookAt(const Vector3& srcPoint, const Vector3& destPoint);
	static Quaternion LookAt(const Vector3& dir);
	static Quaternion I() { return Quaternion(1.0f, 0.0f, 0.0f, 0.0f); }
	static Quaternion Invalid() { constexpr float NaN = std::numeric_limits<float>::quiet_NaN(); return Quaternion(NaN, NaN, NaN, NaN); }
};
using Quat = Quaternion;
