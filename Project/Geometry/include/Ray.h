#pragma once
#include <Vector3.h>
#include <Plane.h>

class Ray {
public:
	Vector3 start;
	Vector3 dir;

	Ray() = default;
	Ray(const Vector3& start, const Vector3& dir) : start(start), dir(dir.normalise()) {};

	bool intersects(const Plane& plane) const;
	Vector3 getIntersect(const Plane& plane) const;
};