#pragma once

#include <Vector2.h>

struct Rect {
private:
	Vector2 _min;
	Vector2 _max;

public:
	Rect();
	Rect(const Vector2& corner, const Vector2& opposite);
	Rect(const Vector2& center, float length) : Rect(center, length, length) {}
	Rect(const Vector2& center, float width, float height);
	Rect(float width, float height) : Rect(Vector2(), width, height) {}
	Rect(float radius) : Rect(Vector2(), radius, radius) {}
	Rect(float left, float right, float bottom, float top);
	Rect(float width, float height, int row, int collumn, const Vector2& offset = Vector2());

	inline const Vector2& min() const { return _min; }
	inline const Vector2& max() const { return _max; }
	inline const Vector2 centre() const { return Vector2((_min.x + _max.x) * 0.5f, (_min.y + _max.y) * 0.5f); }

	inline Vector2 bl() const { return _min; }
	inline Vector2 br() const { return Vector2(_max.x, _min.y); }
	inline Vector2 tr() const { return _max; }
	inline Vector2 tl() const { return Vector2(_min.x, _max.y); }

	inline float width() const { return fabsf(_min.x - _max.x); }
	inline float height() const { return fabsf(_min.y - _max.y); }

	bool overlaps(const Vector2& point) const;
	bool overlaps(const Rect& rect) const;
	bool intersects(const Rect& rect) const;
	bool within(const Rect& rect) const;

	Rect operator&(const Rect& rect) const; //intersect
	Rect operator|(const Rect& rect) const; //union
	Rect& operator&=(const Rect& rect); //intersect
	Rect& operator|=(const Rect& rect); //union

	Rect operator+(const Vector2& vec2) const;
	Rect operator-(const Vector2& vec2) const;
	Rect& operator+=(const Vector2& vec2);
	Rect& operator-=(const Vector2& vec2);

	friend bool operator< (const Rect& a, const Rect& b);
	bool operator == (const Rect& rect) const { return _min == rect._min && _max == rect._max; }
	bool operator != (const Rect& rect) const { return _min != rect._min || _max != rect._max; }
	friend std::ostream& operator<<(std::ostream& output, const Rect& rect);

	bool valid() const;

	static Rect Invalid();
};
