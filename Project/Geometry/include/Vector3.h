#pragma once

#include <Vector2.h>
#include <array>

#pragma pack(push, 1)
struct Vector3 {
	float x;
	float y;
	float z;

	Vector3();
	Vector3(float xyz);
	Vector3(float x, float y, float z);
	Vector3(float x, const Vector2& vr2);
	explicit Vector3(const Vector2& vr2, float z = 0.0f);

	float dist(const Vector3& vector) const;
	float dot(const Vector3& vector) const;
	float magnitude() const;
	Vector3 cross(const Vector3& vector) const;
	Vector3 cross(const Vector3& vect1, const Vector3& vect2) const;
	Vector3 mult(const Vector3& vector) const;
	Vector3 to(const Vector3& vector) const;
	Vector3 normalise() const;
	bool valid() const;

	Vector3 towards(const Vector3& vector, float step) const;
	Vector3 lerp(const Vector3& vector, float frac) const;

	Vector3 operator + (const Vector3& obj) const;
	Vector3 operator - (const Vector3& obj) const;
	Vector3 operator * (const Vector3& obj) const;
	Vector3 operator * (float num) const;
	Vector3 operator / (float num) const;
	Vector3 operator - () const;

	Vector3& operator += (const Vector3& obj);
	Vector3& operator -= (const Vector3& obj);
	Vector3& operator *= (const Vector3& obj);
	Vector3& operator *= (float num);
	Vector3& operator /= (float num);

	Vector2 xy() const;
	Vector2 yz() const;

	friend Vector3 operator * (float num, const Vector3& vect);
	friend Vector3 operator / (float num, const Vector3& vect);

	bool operator == (const Vector3& obj) const;
	bool operator != (const Vector3& obj) const;
	float& operator[](int index);
	const float& operator[](int index) const;

	friend std::ostream& operator<<(std::ostream& output, const Vector3& vect);
	friend bool operator <(const Vector3& a, const Vector3& b);

	static Vector3 Invalid() { constexpr float NaN = std::numeric_limits<float>::quiet_NaN(); return Vector3(NaN, NaN, NaN); }
};
#pragma pack(pop)
using Vector3 = Vector3;
using Vec3 = Vector3;