#pragma once

#include <Vector3.h>
#include <Matrix4.h>
#include <Line.h>
#include <Ray.h>
#include <Sphere.h>

struct Box {
private:
	Vector3 _min;
	Vector3 _max;

public:
	Box();
	Box(const Vector3& corner, const Vector3& opposite);
	Box(const Vector3& center, float radius) : Box(center, radius, radius, radius) {}
	Box(const Vector3& center, float width, float height, float depth);
	Box(float width, float height, float depth);
	Box(float length);

	inline const Vector3& min() const { return _min; }
	inline const Vector3& max() const { return _max; }
	inline const Vector3 centre() const { return Vector3((_min.x + _max.x) * 0.5f, (_min.y + _max.y) * 0.5f, (_min.z + _max.z) * 0.5f); }

	inline Vector3 bbl() const { return _min; }
	inline Vector3 bbr() const { return Vector3(_max.x, _min.y, _min.z); }
	inline Vector3 btr() const { return Vector3(_max.x, _max.y, _min.z); }
	inline Vector3 btl() const { return Vector3(_min.x, _max.y, _min.z); }
	inline Vector3 fbl() const { return Vector3(_min.x, _min.y, _max.z); }
	inline Vector3 fbr() const { return Vector3(_max.x, _min.y, _max.z); }
	inline Vector3 ftr() const { return _max; }
	inline Vector3 ftl() const { return Vector3(_min.x, _max.y, _max.z); }

	inline float width() const { return fabsf(_min.x - _max.x); }
	inline float height() const { return fabsf(_min.y - _max.y); }
	inline float depth() const { return fabsf(_min.z - _max.z); }

	bool contains(const Vector3& point) const;
	bool contains(const Box& box) const;
	bool overlaps(const Box& box) const;
	bool overlaps(const Line& line) const;
	bool overlaps(const Sphere& sphere) const;
	bool intersects(const Line& line) const;
	bool intersects(const Box& box) const;
	float intersects(const Ray& ray) const;

	Box operator&(const Box& box) const; //intersect
	Box operator|(const Box& box) const; //union
	Box& operator&=(const Box& box); //intersect
	Box& operator|=(const Box& box); //union

	float dist(const Box box) const;
	Vector3 normal(const Vector3& surface_point) const;

	Box operator+(const Vector3& vec3) const;
	Box operator-(const Vector3& vec3) const;
	Box& operator+=(const Vector3& vec3);
	Box& operator-=(const Vector3& vec3);

	friend Box operator*(const Matrix4& mat, const Box& box);

	friend bool operator< (const Box& a, const Box& b);
	bool operator == (const Box& box) const { return _min == box._min && _max == box._max; }
	bool operator != (const Box& box) const { return _min != box._min || _max != box._max; }
	friend std::ostream& operator<<(std::ostream& output, const Box& box);

	bool valid() const;

	static Box Invalid();
};