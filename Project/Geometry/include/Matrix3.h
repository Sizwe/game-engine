#pragma once

#include <Matrix2.h>
#include <Vector2.h>
#include <Vector3.h>

struct Matrix3 {
private:
	struct Row {
		friend Matrix3;
	private:
		float* row;

		Row() = delete;
		Row(Matrix3* mat, int rowIndex);
		Row(const Row& matrix);
		Row& operator = (const Row&) = delete;
	public:
		float& operator[](int collIndex);
		const float& operator[](int collIndex) const;
	};

public:
	float data[9];

	Matrix3();
	Matrix3(float diagonal);
	explicit Matrix3(const Matrix2& matrix);

	explicit operator Matrix2() const;

	const float& get(int rowIndex, int collIndex) const;
	float& get(int rowIndex, int collIndex);

	Matrix3 transpose() const;

	float det() const;
	Matrix3 inv() const;
	bool valid() const;

	Matrix3 operator * (float num) const;
	friend Matrix3 operator * (float num, const Matrix3& matrix3);

	Matrix3 operator + (const Matrix3& mat) const;
	Matrix3 operator - (const Matrix3& mat) const;
	Matrix3 operator * (const Matrix3& mat) const;
	Vector3 operator * (const Vector3& vec) const;
	Vector2 operator * (const Vector2& vec) const;

	Matrix3& operator *= (float num);
	Matrix3& operator += (const Matrix3& mat);
	Matrix3& operator -= (const Matrix3& mat);
	Matrix3& operator *= (const Matrix3& mat);

	bool operator == (const Matrix3& mat) const;

	Row operator[](int collIndex);
	const Row operator[](int collIndex) const;

	friend std::ostream& operator<<(std::ostream& output, const Matrix3& matrix3);

	static Matrix3 Identity();

	static Matrix3 Scale(float xFactor, float yFactor = 1.0f, float zFactor = 1.0f);
	static Matrix3 Shear(float xFactor, float yFactor = 0.0f, float zFactor = 0.0f);
	static Matrix3 Shift(float xDist, float yDist = 0.0f);
	static Matrix3 Reflect(float xAxis, float yAxis = 0.0f);
	static Matrix3 Rotate(float deg);
	static Matrix3 Scale(const Vector3& scale) { return Scale(scale.x, scale.y, scale.z); }
	static Matrix3 Scale(const Vector2& scale) { return Scale(scale.x, scale.y); }
	static Matrix3 Shear(const Vector3& shear) { return Shear(shear.x, shear.y, shear.z); }
	static Matrix3 Shear(const Vector2& shear) { return Shear(shear.x, shear.y); }
	static Matrix3 Shift(const Vector2& shift) { return Shift(shift.x, shift.y); }
	static Matrix3 Reflect(const Vector2& reflect) { return Reflect(reflect.x, reflect.y); }
	static Matrix3 Invalid() { Matrix3 m; std::fill(m.data, m.data + 9, std::numeric_limits<float>::quiet_NaN()); return m; }
};
using Mat3 = Matrix3;