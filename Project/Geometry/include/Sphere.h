#pragma once
#include <Vector3.h>

class Sphere {
public:
	Vector3 centre;
	float radius;

	Sphere(const Vector3& centre, float radius);
};