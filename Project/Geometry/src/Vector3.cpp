#include <Vector3.h>
#include <string>

namespace {
	template <typename T>
	T clamp(T n, T lower, T upper) {
		return std::max(lower, std::min(n, upper));
	}
}

#define PI 3.141592741f

#define feq(a,b) (fabsf(a - b) <= FLT_EPSILON)
#define fle(a,b) (a - b <= FLT_EPSILON)
#define fge(a,b) (b - a <= FLT_EPSILON)

#define toRad(deg) (deg * PI / 180.0f)

Vector3::Vector3()
{
	x = y = z = 0.0f;
}

Vector3::Vector3(float xyz)
{
	x = y = z = xyz;
}

Vector3::Vector3(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

Vector3::Vector3(const Vector2 & vr2, float z)
{
	x = vr2.x;
	y = vr2.y;
	this->z = z;
}

Vector3::Vector3(float x, const Vector2 & vr2)
{
	this->x = x;
	y = vr2.x;
	z = vr2.y;
}

float Vector3::dist(const Vector3 & vector) const
{
	float a = x - vector.x;
	float b = y - vector.y;
	float c = z - vector.z;
	return sqrtf(a * a + b * b + c * c);
}

float Vector3::dot(const Vector3 & vector) const
{
	return x * vector.x + y * vector.y + z * vector.z;
}

float Vector3::magnitude() const
{
	return sqrtf(x * x + y * y + z * z);
}

Vector3 Vector3::cross(const Vector3 & v2) const
{
	return Vector3(
		y * v2.z - z * v2.y,
		z * v2.x - x * v2.z,
		x * v2.y - y * v2.x );
}

Vector3 Vector3::cross(const Vector3 & b, const Vector3 & c) const
{
	return b * (dot(c)) - *this * (b.dot(c));
}

Vector3 Vector3::mult(const Vector3 & vector) const
{
	return Vector3(x * vector.x, y * vector.y, z * vector.z);
}

Vector3 Vector3::to(const Vector3 & vector) const
{
	return Vector3(vector.x - x, vector.y - y, vector.z - z);
}

Vector3 Vector3::normalise() const
{
	float m = magnitude();
	if (m == 0.0f)
		return Vector3();
	m = 1.0f / m;
	return Vector3(x * m, y * m, z * m);
}

bool Vector3::valid() const
{
	if (isnan(x))
		return false;
	else if (isnan(y))
		return false;
	else if (isnan(z))
		return false;
	return true;
}

Vector3 Vector3::towards(const Vector3 & vector, float step) const
{
	if (fabsf(step) > vector.dist(*this))
		return vector;
	return *this + vector.to(*this).normalise() * step;
}

Vector3 Vector3::lerp(const Vector3 & vector, float frac) const
{
	return *this * (1.0f - frac) + vector * frac;
}

Vector3 Vector3::operator+(const Vector3 & obj) const
{
	return Vector3(x + obj.x, y + obj.y, z + obj.z);
}

Vector3 Vector3::operator-(const Vector3 & obj) const
{
	return Vector3(x - obj.x, y - obj.y, z - obj.z);
}

Vector3 Vector3::operator*(const Vector3 & obj) const
{
	return Vector3(x * obj.x, y * obj.y, z * obj.z);
}

Vector3 Vector3::operator*(float num) const
{
	return Vector3(x * num, y * num, z * num);
}

Vector3 Vector3::operator/(float num) const
{
	float i = 1.0f / num;
	return Vector3(x * i, y * i, z * i);
}

Vector3 Vector3::operator-() const
{
	return Vector3(-x, -y, -z);
}

Vector3 & Vector3::operator+=(const Vector3 & obj)
{
	x = x + obj.x;
	y = y + obj.y;
	z = z + obj.z;
	return *this;
}

Vector3 & Vector3::operator-=(const Vector3 & obj)
{
	x = x - obj.x;
	y = y - obj.y;
	z = z - obj.z;
	return *this;
}

Vector3 & Vector3::operator*=(const Vector3 & obj)
{
	x = x * obj.x;
	y = y * obj.y;
	z = z * obj.z;
	return *this;
}

Vector3 & Vector3::operator*=(float num)
{
	x = x * num;
	y = y * num;
	z = z * num;
	return *this;
}

Vector3 & Vector3::operator/=(float num)
{
	float i = 1.0f / num;
	x = x * i;
	y = y * i;
	z = z * i;
	return *this;
}

Vector2 Vector3::xy() const
{
	return Vector2(x, y);
}

Vector2 Vector3::yz() const
{
	return Vector2(y, z);
}

bool Vector3::operator==(const Vector3 & obj) const
{
	return feq(x, obj.x) && feq(y, obj.y) && feq(z, obj.z);
}

bool Vector3::operator!=(const Vector3 & obj) const
{
	return !(*this == obj);
}

float & Vector3::operator[](int index){
	switch (index) {
	case 0: return x;
	case 1: return y;
	case 2: return z;
	default: throw std::out_of_range("Vector3 index out of range exception");
	}
}

const float& Vector3::operator[](int index) const{
	switch (index) {
	case 0: return x;
	case 1: return y;
	case 2: return z;
	default: throw std::out_of_range("Vector3 index out of range exception");
	}
}

Vector3 operator*(float num, const Vector3 & vect)
{
	return Vector3(num * vect.x, num * vect.y, num * vect.z);
}

Vector3 operator/(float num, const Vector3 & vect)
{
	return Vector3(num / vect.x, num / vect.y, num / vect.z);
}

std::ostream & operator<<(std::ostream & output, const Vector3 & vect)
{
	return output << "[" << vect.x << ", " << vect.y << ", " << vect.z << "]";
}

bool operator<(const Vector3 & a, const Vector3 & b)
{
	if (a.x < b.x)
		return true;
	else if (b.x < a.x)
		return false;
	else if (a.y < b.y)
		return true;
	else if (b.y < a.y) 
		return false;
	else 
		return a.z < b.z;
}
