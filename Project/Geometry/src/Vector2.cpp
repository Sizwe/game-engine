#include <Vector2.h>
#include <string>

#define PI 3.141592741f

#define feq(a,b) (fabsf(a - b) <= std::numeric_limits<float>::epsilon())
#define fle(a,b) (a - b <= std::numeric_limits<float>::epsilon())
#define fge(a,b) (b - a <= std::numeric_limits<float>::epsilon())

#define toRad(deg) (deg * PI / 180.0f)

namespace {
	template <typename T>
	T clamp(T n, T lower, T upper) {
		return std::max(lower, std::min(n, upper));
	}
}

Vector2::Vector2()
{
	x = y = 0.0f;
}

Vector2::Vector2(float xy)
{
	x = y = xy;
}

Vector2::Vector2(float x, float y)
{
	this->x = x;
	this->y = y;
}

float Vector2::dist(const Vector2 & vector) const
{
	float a = x - vector.x;
	float b = y - vector.y;
	return sqrtf(a * a + b * b);
}

float Vector2::dot(const Vector2 & vector) const
{
	return x * vector.x + y * vector.y;
}

float Vector2::magnitude() const
{
	return sqrtf(x * x + y * y);
}

Vector2 Vector2::to(const Vector2 & vector) const
{
	return Vector2(vector.x - x, vector.y - y);
}

Vector2 Vector2::normalise() const
{
	float m = magnitude();
	if (m == 0.0f)
		return Vector2();
	return Vector2(x / m, y / m);
}

bool Vector2::valid() const
{
	if (isnan(x))
		return false;
	else if (isnan(y))
		return false;
	return true;
}

Vector2 Vector2::towards(const Vector2 & vector, float step) const
{
	if (fabsf(step) > vector.dist(*this))
		return vector;
	return *this + vector.to(*this).normalise() * step;
}

Vector2 Vector2::lerp(const Vector2 & vector, float frac) const
{
	return *this * (1.0f - frac) + vector * frac;
}

Vector2 Vector2::operator+(const Vector2 & obj) const
{
	return Vector2(x + obj.x, y + obj.y);
}

Vector2 Vector2::operator-(const Vector2 & obj) const
{
	return Vector2(x - obj.x, y - obj.y);
}

Vector2 Vector2::operator*(const Vector2 & obj) const
{
	return Vector2(x * obj.x, y * obj.y);
}

Vector2 Vector2::operator*(float num) const
{
	return Vector2(x * num, y * num);
}

Vector2 Vector2::operator/(float num) const
{
	return Vector2(x / num, y / num);
}

Vector2 & Vector2::operator+=(const Vector2 & obj)
{
	x = x + obj.x;
	y = y + obj.y;
	return *this;
}

Vector2 & Vector2::operator-=(const Vector2 & obj)
{
	x = x - obj.x;
	y = y - obj.y;
	return *this;
}

Vector2 & Vector2::operator*=(const Vector2 & obj)
{
	x = x * obj.x;
	y = y * obj.y;
	return *this;
}

Vector2 & Vector2::operator*=(float num)
{
	x = x * num;
	y = y * num;
	return *this;
}

Vector2 & Vector2::operator/=(float num)
{
	x = x / num;
	y = y / num;
	return *this;
}

Vector2 Vector2::operator-() const
{
	return Vector2(-x, -y);
}

bool Vector2::operator==(const Vector2 & obj) const
{
	return feq(x, obj.x) && feq(y, obj.y);
}

bool Vector2::operator!=(const Vector2 & obj) const
{
	return !(*this == obj);
}

float & Vector2::operator[](int index) {
	switch (index) {
	case 0: return x;
	case 1: return y;
	default: throw std::out_of_range("Vector2 index out of range exception");
	}
}

const float& Vector2::operator[](int index) const {
	switch (index) {
	case 0: return x;
	case 1: return y;
	default: throw std::out_of_range("Vector2 index out of range exception");
	}
}

Vector2 operator*(float num, const Vector2 & vect)
{
	return Vector2(num * vect.x, num * vect.y);
}

Vector2 operator/(float num, const Vector2 & vect)
{
	return Vector2(num / vect.x, num / vect.y);
}

std::ostream & operator<<(std::ostream & output, const Vector2 & vect)
{
	return output << "[" << vect.x << ", " << vect.y << "]";
}

bool operator<(const Vector2 & a, const Vector2 & b)
{
	if (a.x < b.x)
		return true;
	else if (b.x < a.x)
		return false;
	else
		return a.y < b.y;
}
