#include <Rect.h>
#include <math.h>

namespace {
	inline Vector2 fabs(Vector2 v) {
		v.x = fabsf(v.x);
		v.y = fabsf(v.y);
		return v;
	}
}

Rect::Rect() { 
	_min = _max = Vector2::Invalid();
}

Rect::Rect(const Vector2 & corner, const Vector2 & opposite) {
	Vector2 rect[4];
	rect[0] = corner;
	rect[1] = Vector2(opposite.x, corner.y);
	rect[2] = opposite;
	rect[3] = Vector2(corner.x, opposite.y);
	_min = Vector2(fminf(fminf(rect[0].x, rect[1].x), fminf(rect[2].x, rect[3].x)), fminf(fminf(rect[0].y, rect[1].y), fminf(rect[2].y, rect[3].y)));
	_max = Vector2(fmaxf(fmaxf(rect[0].x, rect[1].x), fmaxf(rect[2].x, rect[3].x)), fmaxf(fmaxf(rect[0].y, rect[1].y), fmaxf(rect[2].y, rect[3].y)));
}

Rect::Rect(const Vector2 & center, float width, float height)
{
	const float hWidth = width * 0.5f;
	const float hHeight = height * 0.5f;
	_min = Vector2(center.x - hWidth, center.y - hHeight);
	_max = Vector2(center.x + hWidth, center.y + hHeight);
}

Rect::Rect(float left, float right, float bottom, float top)
{
	if (left < right) {
		_min.x = left;
		_max.x = right;
	}
	else {
		_min.x = right;
		_max.x = left;
	}
	if (bottom < top) {
		_min.y = bottom;
		_max.y = top;
	}
	else {
		_min.y = top;
		_max.y = bottom;
	}
}

Rect::Rect(float width, float height, int rowIndex, int collIndex, const Vector2& offset)
{
	width = std::fabsf(width);
	height = std::fabsf(height);
	_min = Vector2(collIndex * width + offset.x, rowIndex * height + offset.y);
	_max = Vector2((collIndex + 1) * width + offset.x, (rowIndex + 1) * height + offset.y);
}

bool Rect::overlaps(const Vector2 & point) const
{
	return point.x >= _min.x && point.x <= _max.x && point.y >= _min.y && point.y <= _max.y;
}

bool Rect::overlaps(const Rect & box) const
{
	return box._max.x >= _min.x && box._min.x <= _max.x && box._max.y >= _min.y && box._min.y <= _max.y;
}

bool Rect::intersects(const Rect & box) const
{
	const Vector2 c1 = centre();
	const Vector2 c2 = box.centre();
	return (fabsf(c1.x - c2.x) * 2.0f < (width() + box.width())) && (fabsf(c1.y - c2.y) * 2.0f < (height() + box.height()));
}

bool Rect::within(const Rect& rect) const
{
	return rect._min.x >= _min.x && rect._min.y <= _min.y && rect._max.x <= _max.x && rect._max.y <= _max.y;
}

Rect Rect::operator&(const Rect& rect) const
{
	Rect out;
	out._min = Vector2(fmaxf(_min.x, rect._min.x), fmaxf(_min.y, rect._min.y));
	out._max = Vector2(fminf(_max.x, rect._max.x), fminf(_max.y, rect._max.y));
	if (out._min.x > out._max.x || out._min.y > out._max.y) {
		out._min = out._max = Vector2::Invalid();
	}
	return out;
}

Rect Rect::operator|(const Rect& rect) const
{
	if (!rect.valid()) return *this;
	if (!valid()) return rect;
	Rect out;
	out._min = Vector2(fminf(_min.x, rect._min.x), fminf(_min.y, rect._min.y));
	out._max = Vector2(fmaxf(_max.x, rect._max.x), fmaxf(_max.y, rect._max.y));
	return out;
}

Rect& Rect::operator&=(const Rect& rect)
{
	return *this = *this & rect;
}

Rect& Rect::operator|=(const Rect& rect)
{
	return *this = *this | rect;
}

Rect Rect::operator+(const Vector2& vec2) const {
	Rect Rect;
	Rect._min = _min + vec2;
	Rect._max = _max + vec2;
	return Rect;
}

Rect Rect::operator-(const Vector2& vec2) const {
	Rect Rect;
	Rect._min = _min - vec2;
	Rect._max = _max - vec2;
	return Rect;
}

Rect& Rect::operator+=(const Vector2& vec2) {
	_min += vec2;
	_max += vec2;
	return *this;
}

Rect& Rect::operator-=(const Vector2& vec2) {
	_min -= vec2;
	_max -= vec2;
	return *this;
}

bool Rect::valid() const
{
	return _min.valid() && _max.valid();
}

Rect Rect::Invalid()
{
	Rect out;
	out._min = out._max = Vector2::Invalid();
	return out;
}
bool operator<(const Rect & a, const Rect & b)
{
	return (a._min < b._min)? true : (b._min < a._min) ? false : b._max < a._max;
}

std::ostream& operator<<(std::ostream& output, const Rect& rect)
{
	return output << "[ " << rect.min() << ", " << rect.max() << " ]";
}

