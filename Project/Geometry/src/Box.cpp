#include <Box.h>
#include <math.h>

namespace {
	inline Vector3 fabs(Vector3 v) {
		v.x = fabsf(v.x);
		v.y = fabsf(v.y);
		v.z = fabsf(v.z);
		return v;
	}
	inline float sq(float num) {
		return num * num;
	}
}

Box::Box() {
	 _min = _max = Vector3::Invalid();
}

Box::Box(const Vector3 & a, const Vector3 & b) {	
	Vector3 p[8];
	p[0] = a;
	p[1] = Vector3(a.x, a.y, b.z);
	p[2] = Vector3(a.x, b.y, a.z);
	p[3] = Vector3(a.x, b.y, b.z);
	p[4] = Vector3(b.x, a.y, a.z);
	p[5] = Vector3(b.x, a.y, b.z);
	p[6] = Vector3(b.x, b.y, a.z);
	p[7] = a;
	_min = Vector3(
		std::min({ p[0].x, p[1].x, p[2].x, p[3].x, p[4].x, p[5].x, p[6].x, p[7].x }),
		std::min({ p[0].y, p[1].y, p[2].y, p[3].y, p[4].y, p[5].y, p[6].y, p[7].y }),
		std::min({ p[0].z, p[1].z, p[2].z, p[3].z, p[4].z, p[5].z, p[6].z, p[7].z })
	);
	_max = Vector3(
		std::max({ p[0].x, p[1].x, p[2].x, p[3].x, p[4].x, p[5].x, p[6].x, p[7].x }),
		std::max({ p[0].y, p[1].y, p[2].y, p[3].y, p[4].y, p[5].y, p[6].y, p[7].y }),
		std::max({ p[0].z, p[1].z, p[2].z, p[3].z, p[4].z, p[5].z, p[6].z, p[7].z })
	);
}

Box::Box(const Vector3 & center, float width, float height, float depth)
{
	float hWidth = width / 2.0f, hHeight = height / 2.0f, hDepth = depth / 2.0f;
	_min = Vector3(center.x - hWidth, center.y - hHeight, center.z - hDepth);
	_max = Vector3(center.x + hWidth, center.y + hHeight, center.z + hDepth);
}

Box::Box(float width, float height, float depth)
{
	float hWidth = width / 2.0f, hHeight = height / 2.0f, hDepth = depth / 2.0f;
	_min = Vector3(- hWidth, - hHeight, - hDepth);
	_max = Vector3(hWidth, hHeight, hDepth);
}

Box::Box(float length)
{
	float hl = length / 2.0f;
	_min = Vector3(-hl);
	_max = Vector3(hl);
}

bool Box::contains(const Vector3 & point) const
{
	return point.x >= _min.x && point.x <= _max.x && point.y >= _min.y && point.y <= _max.y && point.z >= _min.z && point.z <= _max.z;
}

bool Box::overlaps(const Box & box) const
{
	return box._max.x >= _min.x && box._min.x <= _max.x && box._max.y >= _min.y && box._min.y <= _max.y && box._max.z >= _min.z && box._min.z <= _max.z;
}

bool Box::overlaps(const Line& line) const
{
	return intersects(line) || contains(line.u) || contains(line.v);
}

bool Box::overlaps(const Sphere& sphere) const
{
	float r2 = sq(sphere.radius);
	if (sphere.centre.x < _min.x)		r2 -= sq(sphere.centre.x - _min.x);
	else if (sphere.centre.x > _max.x)	r2 -= sq(sphere.centre.x - _max.x);

	if (sphere.centre.y < _min.y)		r2 -= sq(sphere.centre.y - _min.y);
	else if (sphere.centre.y > _max.y)	r2 -= sq(sphere.centre.y - _max.y);

	if (sphere.centre.z < _min.z)		r2 -= sq(sphere.centre.z - _min.z);
	else if (sphere.centre.z > _max.z)	r2 -= sq(sphere.centre.z - _max.z);
	return r2 > 0;
}

bool Box::intersects(const Line& line) const
{
	Vector3 d = (line.u - line.v) / 2.0f;
	Vector3 e = (_max - _min) / 2.0f;
	Vector3 c = line.u + d - (_min + _max) / 2.0f;
	Vector3 ad = Vector3(fabsf(d.x), fabsf(d.y), fabsf(d.z));

	if (fabsf(c[0]) > e[0] + ad[0])
		return false;
	if (fabsf(c[1]) > e[1] + ad[1])
		return false;
	if (fabsf(c[2]) > e[2] + ad[2])
		return false;

	if (fabsf(d[1] * c[2] - d[2] * c[1]) > e[1] * ad[2] + e[2] * ad[1] + FLT_EPSILON)
		return false;
	if (fabsf(d[2] * c[0] - d[0] * c[2]) > e[2] * ad[0] + e[0] * ad[2] + FLT_EPSILON)
		return false;
	if (fabsf(d[0] * c[1] - d[1] * c[0]) > e[0] * ad[1] + e[1] * ad[0] + FLT_EPSILON)
		return false;

	return true;
}

float Box::intersects(const Ray& ray) const
{
	Vector3 invdir = 1.0f / ray.dir;
	Vector3 to = (_min - ray.start) * invdir;
	Vector3 te = (_max - ray.start) * invdir;
	float tmin = std::fmaxf(std::fmaxf(std::fminf(to.x, te.x), std::fminf(to.y, te.y)), std::fminf(to.z, te.z));
	float tmax = std::fminf(std::fminf(std::fmaxf(to.x, te.x), std::fmaxf(to.y, te.y)), std::fmaxf(to.z, te.z));
	return (tmax < 0 || tmin > tmax) ? 0.0f : tmin;
}

bool Box::intersects(const Box& box) const
{
	const Vector3 c1 = centre();
	const Vector3 c2 = box.centre();
	return (fabsf(c1.x - c2.x) * 2.0f < (width() + box.width())) 
		&& (fabsf(c1.y - c2.y) * 2.0f < (height() + box.height()))
		&& (fabsf(c1.z - c2.z) * 2.0f < (depth() + box.depth()));
}

bool Box::contains(const Box& box) const 
{
	return _min.x <= box._min.x && _max.x >= box._max.x &&
		_min.y <= box._min.y && _max.y >= box._max.y &&
		_min.z <= box._min.z && _max.z >= box._max.z;
}

Box Box::operator&(const Box& box) const
{
	Box out;
	out._min = Vector3(fmaxf(_min.x, box._min.x), fmaxf(_min.y, box._min.y), fmaxf(_min.z, box._min.z));
	out._max = Vector3(fminf(_max.x, box._max.x), fminf(_max.y, box._max.y), fminf(_max.z, box._max.z));;

	if (out._min.x > out._max.x || out._min.y > out._max.y || out._min.z > out._max.z) {
		out._min = out._max = Vector3::Invalid();
	}
	return out;
}

Box Box::operator|(const Box& box) const
{
	if (!box.valid()) return *this;
	if (!valid()) return box;
	Box out;
	out._min = Vector3(fminf(_min.x, box._min.x), fminf(_min.y, box._min.y), fminf(_min.z, box._min.z));
	out._max = Vector3(fmaxf(_max.x, box._max.x), fmaxf(_max.y, box._max.y), fmaxf(_max.z, box._max.z));
	return out;
}

Box& Box::operator&=(const Box& box)
{
	return *this = *this & box;
}

Box& Box::operator|=(const Box& box)
{
	return *this = *this | box;
}

float Box::dist(const Box box) const
{
	Vector3 halfExtents1 = Vector3(width() / 2.0f, height() / 2.0f, depth() / 2.0f);
	Vector3 halfExtents2 = Vector3(box.width() / 2.0f, box.height() / 2.0f, box.depth() / 2.0f);
	Vector3 d = fabs(box.centre() - centre()) - (halfExtents1 + halfExtents2);
	if (d.x >= 0.0f || d.y >= 0.0f || d.z >= 0.0f) {
		d.x = fmaxf(0.0f, d.x);
		d.y = fmaxf(0.0f, d.y);
		d.z = fmaxf(0.0f, d.z);
		return d.magnitude();
	}
	return fmaxf(fmaxf(d.x, d.y), d.z);
}

Vector3 Box::normal(const Vector3& point) const
{
	Vector3 c = (_min + _max) * 0.5f;
	Vector3 p = point - c;
	Vector3 d = (_min - _max) * 0.5f;
	float bias = 1.0001f;

	Vector3 out;
	out.x = float(int(p.x / abs(d.x) * bias));
	out.y = float(int(p.y / abs(d.y) * bias));
	out.z = float(int(p.z / abs(d.z) * bias));
	return out;
}

Box Box::operator+(const Vector3& vec3) const {
	Box box;
	box._min = _min + vec3;
	box._max = _max + vec3;
	return box;
}

Box Box::operator-(const Vector3& vec3) const {
	Box box;
	box._min = _min - vec3;
	box._max = _max - vec3;
	return box;
}

Box& Box::operator+=(const Vector3& vec3) {
	_min += vec3;
	_max += vec3;
	return *this;
}

Box& Box::operator-=(const Vector3& vec3) {
	_min -= vec3;
	_max -= vec3;
	return *this;
}

bool Box::valid() const
{
	return _min.valid() && _max.valid();
}

Box Box::Invalid()
{
	Box out;
	out._min = out._max = Vector3::Invalid();
	return out;
}

Box operator*(const Matrix4& mat, const Box& box)
{
	Box out;
	Vector3 p[8];
	p[0] = mat * box._min;
	p[1] = mat * Vector3(box._min.x, box._min.y, box._max.z);
	p[2] = mat * Vector3(box._min.x, box._max.y, box._min.z);
	p[3] = mat * Vector3(box._min.x, box._max.y, box._max.z);
	p[4] = mat * Vector3(box._max.x, box._min.y, box._min.z);
	p[5] = mat * Vector3(box._max.x, box._min.y, box._max.z);
	p[6] = mat * Vector3(box._max.x, box._max.y, box._min.z);
	p[7] = mat * box._max;
	out._min = Vector3(
		std::min({ p[0].x, p[1].x, p[2].x, p[3].x, p[4].x, p[5].x, p[6].x, p[7].x }),
		std::min({ p[0].y, p[1].y, p[2].y, p[3].y, p[4].y, p[5].y, p[6].y, p[7].y }),
		std::min({ p[0].z, p[1].z, p[2].z, p[3].z, p[4].z, p[5].z, p[6].z, p[7].z })
		);
	out._max = Vector3(
		std::max({ p[0].x, p[1].x, p[2].x, p[3].x, p[4].x, p[5].x, p[6].x, p[7].x }),
		std::max({ p[0].y, p[1].y, p[2].y, p[3].y, p[4].y, p[5].y, p[6].y, p[7].y }),
		std::max({ p[0].z, p[1].z, p[2].z, p[3].z, p[4].z, p[5].z, p[6].z, p[7].z })
	);
	return out;
}

bool operator<(const Box & a, const Box & b)
{
	return (a._min < b._min) ? true : (b._min < a._min) ? false : b._max < a._max;
}

std::ostream& operator<<(std::ostream& output, const Box& box)
{
	return output << "[ " << box.min() << ", " << box.max() << " ]";
}

