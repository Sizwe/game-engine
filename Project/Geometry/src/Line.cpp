#include <Line.h>
#include <math.h>

bool Line::intersects(const Plane& plane) const
{
    Vector3 dir = u.to(v);
    float denom = plane.normal.dot(dir);
    if (abs(denom) > FLT_EPSILON) {
        float t = (plane.point - u).dot(plane.normal) / denom;
        if (t >= FLT_EPSILON) return true;
    }
    return false;
}

Vector3 Line::getIntersect(const Plane& plane) const
{
    Vector3 dir = u.to(v);
    Vector3 diff = u - plane.point;
    float p1 = diff.dot(plane.normal);
    float p2 = dir.dot(plane.normal);
    float p3 = p1 / p2;
    return u - dir * p3;
}