#include "Ray.h"

bool Ray::intersects(const Plane& plane) const
{
    float denom = plane.normal.dot(dir);
    if (abs(denom) > FLT_EPSILON) {
        float t = (plane.point - start).dot(plane.normal) / denom;
        if (t >= FLT_EPSILON) return true;
    }
    return false;
}

Vector3 Ray::getIntersect(const Plane& plane) const
{
    Vector3 diff = start - plane.point;
    float p1 = diff.dot(plane.normal);
    float p2 = dir.dot(plane.normal);
    float p3 = p1 / p2;
    return start - dir * p3;
}
