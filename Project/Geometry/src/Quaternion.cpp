#include <Quaternion.h>

#define PI 3.14159265358979323f
#define toRad 0.0174533f
#define toDeg 57.2958f
#define feq(a, b) (fabsf(a - b) <= FLT_EPSILON)
#define fneq(a, b) (fabsf(a - b) > FLT_EPSILON)

namespace {
	inline float sgn(float val) {
		return float((0.0f < val) - (val < 0.0f));
	}
}

Quaternion::Quaternion() : s(1.0f), x(0.0f), y(0.0f), z(0.0f) {}

Quaternion::Quaternion(float s, float x, float y, float z) : s(s), x(x), y(y), z(z){}

Quaternion::Quaternion(const Vector3& axis, float angle)
{
	Vector3 n = axis.normalise();
	angle *= 0.00872664625997164788461845f; //(180/pi)/2
	float sn = sinf(angle);
	s = cosf(angle);
	x = sn * n.x;
	y = sn * n.y;
	z = sn * n.z;
}

Quaternion::Quaternion(float angle, const Vector3& axis) : Quaternion(axis, angle) {}

Quaternion::Quaternion(const Vector3& angle)
{
	auto ha = angle * 0.00872664625997164788461845f; //(180/pi)/2
	const float sx = sinf(ha.x);
	const float sy = sinf(ha.y);
	const float sz = sinf(ha.z);

	const float cx = cosf(ha.x);
	const float cy = cosf(ha.y);
	const float cz = cosf(ha.z);

	s = cx * cy * cz + sx * sy * sz;
	x = sx * cy * cz - cx * sy * sz;
	y = cx * sy * cz + sx * cy * sz;
	z = cx * cy * sz - sx * sy * cz;
}

Quaternion::Quaternion(const Matrix3& m)
{	
	enum indices { i00, i10, i20, i01, i11, i21, i02, i12, i22 };
	s = (m.data[i00] + m.data[i11] + m.data[i22] + 1.0f) * 0.25f;
	x = (m.data[i00] - m.data[i11] - m.data[i22] + 1.0f) * 0.25f;
	y = (-m.data[i00] + m.data[i11] - m.data[i22] + 1.0f) * 0.25f;
	z = (-m.data[i00] - m.data[i11] + m.data[i22] + 1.0f) * 0.25f;
	s = (s < 0.0f) ? 0.0f : sqrt(s);
	x = (x < 0.0f) ? 0.0f : sqrt(x);
	y = (y < 0.0f) ? 0.0f : sqrt(y);
	z = (z < 0.0f) ? 0.0f : sqrt(z);
	if (s >= x && s >= y && s >= z) {
		x *= sgn(m.data[i21] - m.data[i12]);
		y *= sgn(m.data[i02] - m.data[i20]);
		z *= sgn(m.data[i10] - m.data[i01]);
	}
	else if (x >= s && x >= y && x >= z) {
		s *= sgn(m.data[i21] - m.data[i12]);
		y *= sgn(m.data[i10] + m.data[i01]);
		z *= sgn(m.data[i02] + m.data[i20]);
	}
	else if (y >= s && y >= x && y >= z) {
		s *= sgn(m.data[i02] - m.data[i20]);
		x *= sgn(m.data[i10] + m.data[i01]);
		z *= sgn(m.data[i21] + m.data[i12]);
	}
	else if (z >= s && z >= x && z >= y) {
		s *= sgn(m.data[i10] - m.data[i01]);
		x *= sgn(m.data[i20] + m.data[i02]);
		y *= sgn(m.data[i21] + m.data[i12]);
	}
	else {
		*this = Invalid();
		return;
	}
	float r = 1.0f / (s * s + x * x + y * y + z * z);
	s *= r;
	x *= r;
	y *= r;
	z *= r;
}

Quaternion::Quaternion(const Matrix4& m)
{
	enum indices { i00, i10, i20, i30, i01, i11, i21, i31, i02, i12, i22, i32, i03, i13, i23, i33 };
	s = (m.data[i00] + m.data[i11] + m.data[i22] + 1.0f) * 0.25f;
	x = (m.data[i00] - m.data[i11] - m.data[i22] + 1.0f) * 0.25f;
	y = (-m.data[i00] + m.data[i11] - m.data[i22] + 1.0f) * 0.25f;
	z = (-m.data[i00] - m.data[i11] + m.data[i22] + 1.0f) * 0.25f;
	s = (s < 0.0f) ? 0.0f : sqrt(s);
	x = (x < 0.0f) ? 0.0f : sqrt(x);
	y = (y < 0.0f) ? 0.0f : sqrt(y);
	z = (z < 0.0f) ? 0.0f : sqrt(z);
	if (s >= x && s >= y && s >= z) {
		x *= sgn(m.data[i21] - m.data[i12]);
		y *= sgn(m.data[i02] - m.data[i20]);
		z *= sgn(m.data[i10] - m.data[i01]);
	}
	else if (x >= s && x >= y && x >= z) {
		s *= sgn(m.data[i21] - m.data[i12]);
		y *= sgn(m.data[i10] + m.data[i01]);
		z *= sgn(m.data[i02] + m.data[i20]);
	}
	else if (y >= s && y >= x && y >= z) {
		s *= sgn(m.data[i02] - m.data[i20]);
		x *= sgn(m.data[i10] + m.data[i01]);
		z *= sgn(m.data[i21] + m.data[i12]);
	}
	else if (z >= s && z >= x && z >= y) {
		s *= sgn(m.data[i10] - m.data[i01]);
		x *= sgn(m.data[i20] + m.data[i02]);
		y *= sgn(m.data[i21] + m.data[i12]);
	}
	else {
		*this = Invalid();
		return;
	}
	float r = 1.0f / (s * s + x * x + y * y + z * z);
	s *= r;
	x *= r;
	y *= r;
	z *= r;
}

Quaternion Quaternion::norm() const
{
	float mag = magnitude();
	if (mag != 0.0f) {
		float invMag = 1.0f / mag;
		return Quaternion(s * invMag, x * invMag, y * invMag, z * invMag);
	}
	return Quaternion();
}

float Quaternion::dot(const Quaternion& q) const
{
	return s * q.s + x * q.x + y * q.y + z * q.z;
}

float Quaternion::magnitude() const
{
	return sqrt(s * s + x * x + y * y + z * z);
}

Quaternion Quaternion::conj() const
{
	return Quaternion(s, -x, -y, -z);
}

Quaternion Quaternion::inv() const
{
	float invDot2 = 1.0f / (s * s + x * x + y * y + z * z);
	return Quaternion(s * invDot2, -x * invDot2, -y * invDot2, -z * invDot2);
}

Quaternion Quaternion::lerp(const Quaternion& q, float p) const
{
	Quaternion out;
	float invp = 1.0f - p;
	out.x = invp * x + p * q.x;
	out.y = invp * y + p * q.y;
	out.z = invp * z + p * q.z;
	out.s = invp * s + p * q.s;
	return out.norm();
}

Quaternion Quaternion::slerp(const Quaternion& q, float p) const
{
	Quaternion q1 = norm();
	Quaternion q2 = q.norm();
	float d = q1.dot(q2);
	if (d < 0.0f) {
		q2 = -q2;
		d = -d;
	}
	constexpr float tolerance = 1.0f - FLT_EPSILON;
	if (d > tolerance) {
		return Quaternion(q1 + p * (q2 - q1)).norm();
	}
	float t1 = acosf(d);
	float t2 = t1 * p;
	float s1 = sinf(t2) / sinf(t1);
	float s2 = cosf(t2) - d * s1;
	return s2 * q1 + s1 * q2;
}

Quaternion Quaternion::to(const Quaternion& q) const
{
	return q * inv();
}

Quaternion Quaternion::towards(const Quaternion& q, float angleStep) const
{
	float num = angle(q);
	if (num == 0) return q;
	float t = fminf(1.0f, angleStep / num);
	return lerp(q, t);
}

Quaternion Quaternion::stowards(const Quaternion& q, float angleStep) const
{
	float num = angle(q);
	if (num == 0) return q;
	float t = fminf(1.0f, angleStep / num);
	return slerp(q, t);
}

float Quaternion::angle(const Quaternion& q) const
{
	return acosf(fminf(fabsf(dot(q)), 1.0f)) * 2.0f * toDeg;
}

Quaternion Quaternion::exp() const
{
	float r = sqrtf(x * x + y * y + z * z);
	float ew = expf(s);
	float sn = (r >= FLT_EPSILON) ? ew * sinf(r) / r : 0.0f;
	return Quaternion(cosf(r) * ew, x * sn, y * sn, z * sn);
}

Quaternion Quaternion::ln() const
{
	float r = sqrtf(x * x + y * y + z * z);
	float t = (r >= FLT_EPSILON) ? atan2f(r, s) / r : 0.0f;
	return Quaternion(logf(dot(*this)) * 0.5f, x * t, y * t, z * t);
}

Quaternion Quaternion::pow(float e) const
{
	return (ln() * e).exp();
}

bool Quaternion::valid() const
{
	if (isnan(x))
		return false;
	else if (isnan(y))
		return false;
	else if (isnan(z))
		return false;
	else if (isnan(s))
		return false;
	return true;
}

Vector3 Quaternion::toEuler() const
{
	Vector3 out(toDeg, toDeg, toDeg);
	float sp = 2.0f * (s * y - z * x);
	out.x *= atan2f(2.0f * (s * x + y * z), 1.0f - 2.0f * (x * x + y * y));
	out.y *= ((fabsf(sp) >= 1)? copysignf(1.57079632679f, sp) : asinf(sp));
	out.z *= atan2f(2.0f * (s * z + x * y), 1.0f - 2.0f * (y * y + z * z));
	return out;
}

Matrix3 Quaternion::toMatrix3() const
{
	Matrix3 m;
	m.data[0] = 1.0f - 2.0f * y * y - 2.0f * z * z;
	m.data[1] = 2.0f * x * y + 2.0f * s * z;
	m.data[2] = 2.0f * x * z - 2.0f * s * y;
	m.data[3] = 2.0f * x * y - 2.0f * s * z;
	m.data[4] = 1.0f - 2.0f * x * x - 2.0f * z * z;
	m.data[5] = 2.0f * y * z + 2.0f * s * x;
	m.data[6] = 2.0f * x * z + 2.0f * s * y;
	m.data[7] = 2.0f * y * z - 2.0f * s * x;
	m.data[8] = 1.0f - 2.0f * x * x - 2.0f * y * y;
	return m;
}

Matrix4 Quaternion::toMatrix4() const
{
	Matrix4 m;
	m.data[0] = 1.0f - 2.0f * y * y - 2.0f * z * z;
	m.data[1] = 2.0f * x * y + 2.0f * s * z;
	m.data[2] = 2.0f * x * z - 2.0f * s * y;
	m.data[4] = 2.0f * x * y - 2.0f * s * z;
	m.data[5] = 1.0f - 2.0f * x * x - 2.0f * z * z;
	m.data[6] = 2.0f * y * z + 2.0f * s * x;
	m.data[8] = 2.0f * x * z + 2.0f * s * y;
	m.data[9] = 2.0f * y * z - 2.0f * s * x;
	m.data[10] = 1.0f - 2.0f * x * x - 2.0f * y * y;
	m.data[15] = 1.0f;
	return m;
}

std::pair<Vector3, float> Quaternion::toAxisAngle() const
{
	std::pair<Vector3, float> out;
	Quaternion q = this->norm();
	out.second = 2.0f * acosf(q.s) * toDeg;
	float d = sqrtf(1.0f - q.s * q.s);
	out.first = (d > FLT_EPSILON) ? Vector3(q.x, q.y, q.z) / d : Vector3(0.0f, 0.0f, -1.0f);
	return out;
}

std::pair<float, Vector3> Quaternion::toAngleAxis() const
{
	auto aa = toAxisAngle();
	return std::pair<float, Vector3>(aa.second, aa.first);
}

Quaternion::operator Vector3() const
{
	return toEuler();
}

Quaternion::operator Vector4() const
{
	return Vector4(x, y, z, s);
}

Quaternion::operator Matrix3() const
{
	return toMatrix3();
}

Quaternion::operator Matrix4() const
{
	return toMatrix4();
}

Quaternion Quaternion::operator-() const
{
	return Quaternion(-s, -x, -y, -z);
}

Quaternion Quaternion::operator+(const Quaternion& q) const
{
	return Quaternion(s + q.s, x + q.x, y + q.y, z + q.z);
}

Quaternion Quaternion::operator-(const Quaternion& q) const
{
	return Quaternion(s - q.s, x - q.x, y - q.y, z - q.z);
}

Quaternion Quaternion::operator+(float f) const
{
	return Quaternion(s + f, x, y, z);
}

Quaternion Quaternion::operator-(float f) const
{
	return Quaternion(s - f, x, y, z);
}

Quaternion Quaternion::operator*(const Quaternion& q) const
{
	Quaternion out;
	out.s = s * q.s - x * q.x - y * q.y - z * q.z;
	out.x = s * q.x + x * q.s + y * q.z - z * q.y;
	out.y = s * q.y - x * q.z + y * q.s + z * q.x;
	out.z = s * q.z + x * q.y - y * q.x + z * q.s;
	return out;
}

Quaternion Quaternion::operator*(float f) const
{
	return Quaternion(s * f, x * f, y * f, z * f);
}

Quaternion Quaternion::operator/(const Quaternion& q) const
{
	float invDot2 = 1.0f / q.dot(q);
	return *this * Quaternion(q.s * invDot2, -q.x * invDot2, -q.y * invDot2, -q.z * invDot2);
}

Quaternion Quaternion::operator/(float f) const
{
	return *this* (1.0f / f);
}

Quaternion& Quaternion::operator+=(const Quaternion& q)
{
	s += q.s;
	x += q.x;
	y += q.y;
	z += q.z;
	return *this;
}

Quaternion& Quaternion::operator-=(const Quaternion& q)
{
	s -= q.s;
	x -= q.x;
	y -= q.y;
	z -= q.z;
	return *this;
}

Quaternion& Quaternion::operator+=(float f)
{
	return *this = *this + f;
}

Quaternion& Quaternion::operator-=(float f)
{
	return *this = *this - f;
}

Quaternion& Quaternion::operator*=(const Quaternion& q)
{
	return *this = *this * q;
}

Quaternion& Quaternion::operator*=(float f)
{
	return *this = *this * f;
}

Quaternion& Quaternion::operator/=(const Quaternion& q)
{
	return *this = *this / q;
}

Quaternion& Quaternion::operator/=(float f)
{
	return *this = *this / f;
}

Vector3 Quaternion::operator*(const Vector3& v) const
{
	Vector3 u(x, y, z);
	Vector3 t = u.cross(v) * 2.0f;
	return v + s * t + u.cross(t);
}

Quaternion Quaternion::LookAt(const Vector3& src, const Vector3& dest)
{
	return LookAt(src.to(dest));
}

Quaternion Quaternion::LookAt(const Vector3& dir)
{
	Vector3 fv = dir.normalise();
	float dot = Vector3(0.0f , 0.0f, -1.0f).dot(fv);
	if (fabsf(dot + 1.0f) < FLT_EPSILON){
		return Quaternion(PI, 0.0f, 1.0f, 0.0f);
	}
	if (fabsf(dot - 1.0f) < FLT_EPSILON){
		return Quaternion(1.0f, 0.0f, 0.0f, 0.0f);
	}
	float angle = acosf(dot) * toDeg;
	Vector3 axis = Vector3(0.0f, 0.0f, -1.0f).cross(fv).normalise();
	return Quaternion(axis, angle);
}

Quaternion& Quaternion::operator=(float f)
{
	s = f;
	x = y = z = 0;
	return *this;
}

bool Quaternion::operator==(const Quaternion& q) const
{
	return feq(s, q.s) && feq(x, q.x) && feq(y, q.y) && feq(z, q.z);
}

bool Quaternion::operator!=(const Quaternion& q) const
{
	return fneq(s, q.s) || fneq(x, q.x) || fneq(y, q.y) || fneq(z, q.z);
}

Quaternion operator+(float f, const Quaternion& q)
{
	return q + f;
}

Quaternion operator-(float f, const Quaternion& q)
{
	return q - f;
}

Quaternion operator*(float f, const Quaternion& q)
{
	return Quaternion(q.s * f, q.x * f, q.y * f, q.z * f);
}

Quaternion operator/(float f, const Quaternion& q)
{
	return Quaternion(f / q.s, f / q.x, f / q.y, f / q.z);
}

Vector3 operator*(const Vector3& v, const Quaternion& q)
{
	return q * v;
}

Vector3& operator*=(Vector3& v, const Quaternion& q)
{
	return v = q * v;
}

std::ostream& operator<<(std::ostream& output, const Quaternion& q)
{
	return output << "{ " << q.s << " + " << q.x << "i + " << q.y << "j + " << q.z << "k }" << std::endl;
}

bool operator<(const Quaternion& a, const Quaternion& b)
{
	return (a.s < b.s) ? true : (b.s < a.s) ? false : (a.x < b.x) ? true : (b.x < a.x) ? false : (a.y < b.y) ? true : (b.y < a.y) ? false : a.z < b.z;
}
