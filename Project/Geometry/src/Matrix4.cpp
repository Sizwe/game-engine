#include <Matrix4.h>

#include <iostream>
#include <string>

#include <limits>
#include <math.h>

#define fneq(a,b) (fabsf(a - b) > std::numeric_limits<float>::epsilon())
#define PI 3.141592741f
#define toRad(deg) (deg * 0.0174533f)
#define toDeg(rad) (rad * 57.2958f)

namespace {
	enum indices { i00, i10, i20, i30, i01, i11, i21, i31, i02, i12, i22, i32, i03, i13, i23, i33 };
}

Matrix4::Matrix4() : data{ 0.0f } {}

Matrix4::Matrix4(float diagonal) : data{ 0.0f }
{
	data[i00] = diagonal;
	data[i11] = diagonal;
	data[i22] = diagonal;
	data[i33] = diagonal;
}

Matrix4::Matrix4(const Matrix3 & matrix)
{
	data[i00] = matrix.data[0];
	data[i10] = matrix.data[1];
	data[i20] = matrix.data[2];
	data[i30] = 0.0f;
	data[i01] = matrix.data[3];
	data[i11] = matrix.data[4];
	data[i21] = matrix.data[5];
	data[i31] = 0.0f;
	data[i02] = matrix.data[6];
	data[i12] = matrix.data[7];
	data[i22] = matrix.data[8];
	data[i32] = 0.0f;
	data[i03] = 0.0f;
	data[i13] = 0.0f;
	data[i23] = 0.0f;
	data[i33] = 1.0f;
}

Matrix4::Matrix4(const Matrix2 & matrix)
{
	data[i00] = matrix.data[0];
	data[i10] = matrix.data[1];
	data[i20] = 0.0f;
	data[i30] = 0.0f;
	data[i01] = matrix.data[2];
	data[i11] = matrix.data[3];
	data[i21] = 0.0f;
	data[i31] = 0.0f;
	data[i02] = 0.0f;
	data[i12] = 0.0f;
	data[i22] = 1.0f;
	data[i32] = 0.0f;
	data[i03] = 0.0f;
	data[i13] = 0.0f;
	data[i23] = 0.0f;
	data[i33] = 1.0f;
}

Matrix4::operator Matrix2() const
{
	Matrix2 m;
	m.data[0] = data[0];
	m.data[1] = data[1];
	m.data[2] = data[4];
	m.data[3] = data[5];
	return m;
}

Matrix4::operator Matrix3() const
{
	Matrix3 m;
	m.data[0] = data[i00];
	m.data[1] = data[i10];
	m.data[2] = data[i20];
	m.data[3] = data[i01];
	m.data[4] = data[i11];
	m.data[5] = data[i21];
	m.data[6] = data[i02];
	m.data[7] = data[i12];
	m.data[8] = data[i22];
	return m;
}

const float& Matrix4::get(int rowIndex, int collIndex) const
{
	return data[collIndex * 4 + rowIndex];
}

float& Matrix4::get(int rowIndex, int collIndex)
{
	return data[collIndex * 4 + rowIndex];
}

Matrix4 Matrix4::transpose() const
{
	Matrix4 newMx;
	for (int i = 0; i < 4; ++i)
		for (int j = 0; j < 4; ++j)
			newMx.get(i, j) = get(j, i);
	return newMx;
}

Vector3 Matrix4::operator*(const Vector3& vec) const
{
	Vector3 finalVr;
	finalVr.x = data[i00] * vec.x + data[i01] * vec.y + data[i02] * vec.z + data[i03] * 1.0f;
	finalVr.y = data[i10] * vec.x + data[i11] * vec.y + data[i12] * vec.z + data[i13] * 1.0f;
	finalVr.z = data[i20] * vec.x + data[i21] * vec.y + data[i22] * vec.z + data[i23] * 1.0f;
	return finalVr;
}

Vector2 Matrix4::operator*(const Vector2& vec) const
{
	Vector2 finalVr;
	finalVr.x = data[i00] * vec.x + data[i01] * vec.y + data[i03] * 1.0f;
	finalVr.y = data[i10] * vec.x + data[i11] * vec.y + data[i13] * 1.0f;
	return finalVr;
}

float Matrix4::det() const
{
	return	data[i03] * data[i12] * data[i21] * data[i30] - data[i02] * data[i13] * data[i21] * data[i30] -
			data[i03] * data[i11] * data[i22] * data[i30] + data[i01] * data[i13] * data[i22] * data[i30] +
			data[i02] * data[i11] * data[i23] * data[i30] - data[i01] * data[i12] * data[i23] * data[i30] -
			data[i03] * data[i12] * data[i20] * data[i31] + data[i02] * data[i13] * data[i20] * data[i31] +
			data[i03] * data[i10] * data[i22] * data[i31] - data[i00] * data[i13] * data[i22] * data[i31] -
			data[i02] * data[i10] * data[i23] * data[i31] + data[i00] * data[i12] * data[i23] * data[i31] +
			data[i03] * data[i11] * data[i20] * data[i32] - data[i01] * data[i13] * data[i20] * data[i32] -
			data[i03] * data[i10] * data[i21] * data[i32] + data[i00] * data[i13] * data[i21] * data[i32] +
			data[i01] * data[i10] * data[i23] * data[i32] - data[i00] * data[i11] * data[i23] * data[i32] -
			data[i02] * data[i11] * data[i20] * data[i33] + data[i01] * data[i12] * data[i20] * data[i33] +
			data[i02] * data[i10] * data[i21] * data[i33] - data[i00] * data[i12] * data[i21] * data[i33] -
			data[i01] * data[i10] * data[i22] * data[i33] + data[i00] * data[i11] * data[i22] * data[i33];
}

Matrix4 Matrix4::inv() const
{
	Matrix4 finalMx;

	finalMx.data[0] =data[5] * data[10] * data[15] -
		data[5] * data[11] * data[14] -
		data[9] * data[6] * data[15] +
		data[9] * data[7] * data[14] +
		data[13] * data[6] * data[11] -
		data[13] * data[7] * data[10];

	finalMx.data[4] = -data[4] * data[10] * data[15] +
		data[4] * data[11] * data[14] +
		data[8] * data[6] * data[15] -
		data[8] * data[7] * data[14] -
		data[12] * data[6] * data[11] +
		data[12] * data[7] * data[10];

	finalMx.data[8] = data[4] * data[9] * data[15] -
		data[4] * data[11] * data[13] -
		data[8] * data[5] * data[15] +
		data[8] * data[7] * data[13] +
		data[12] * data[5] * data[11] -
		data[12] * data[7] * data[9];

	finalMx.data[12] = -data[4] * data[9] * data[14] +
		data[4] * data[10] * data[13] +
		data[8] * data[5] * data[14] -
		data[8] * data[6] * data[13] -
		data[12] * data[5] * data[10] +
		data[12] * data[6] * data[9];

	finalMx.data[1] = -data[1] * data[10] * data[15] +
		data[1] * data[11] * data[14] +
		data[9] * data[2] * data[15] -
		data[9] * data[3] * data[14] -
		data[13] * data[2] * data[11] +
		data[13] * data[3] * data[10];

	finalMx.data[5] = data[0] * data[10] * data[15] -
		data[0] * data[11] * data[14] -
		data[8] * data[2] * data[15] +
		data[8] * data[3] * data[14] +
		data[12] * data[2] * data[11] -
		data[12] * data[3] * data[10];

	finalMx.data[9] = -data[0] * data[9] * data[15] +
		data[0] * data[11] * data[13] +
		data[8] * data[1] * data[15] -
		data[8] * data[3] * data[13] -
		data[12] * data[1] * data[11] +
		data[12] * data[3] * data[9];

	finalMx.data[13] = data[0] * data[9] * data[14] -
		data[0] * data[10] * data[13] -
		data[8] * data[1] * data[14] +
		data[8] * data[2] * data[13] +
		data[12] * data[1] * data[10] -
		data[12] * data[2] * data[9];

	finalMx.data[2] = data[1] * data[6] * data[15] -
		data[1] * data[7] * data[14] -
		data[5] * data[2] * data[15] +
		data[5] * data[3] * data[14] +
		data[13] * data[2] * data[7] -
		data[13] * data[3] * data[6];

	finalMx.data[6] = -data[0] * data[6] * data[15] +
		data[0] * data[7] * data[14] +
		data[4] * data[2] * data[15] -
		data[4] * data[3] * data[14] -
		data[12] * data[2] * data[7] +
		data[12] * data[3] * data[6];

	finalMx.data[10] = data[0] * data[5] * data[15] -
		data[0] * data[7] * data[13] -
		data[4] * data[1] * data[15] +
		data[4] * data[3] * data[13] +
		data[12] * data[1] * data[7] -
		data[12] * data[3] * data[5];

	finalMx.data[14] = -data[0] * data[5] * data[14] +
		data[0] * data[6] * data[13] +
		data[4] * data[1] * data[14] -
		data[4] * data[2] * data[13] -
		data[12] * data[1] * data[6] +
		data[12] * data[2] * data[5];

	finalMx.data[3] = -data[1] * data[6] * data[11] +
		data[1] * data[7] * data[10] +
		data[5] * data[2] * data[11] -
		data[5] * data[3] * data[10] -
		data[9] * data[2] * data[7] +
		data[9] * data[3] * data[6];

	finalMx.data[7] = data[0] * data[6] * data[11] -
		data[0] * data[7] * data[10] -
		data[4] * data[2] * data[11] +
		data[4] * data[3] * data[10] +
		data[8] * data[2] * data[7] -
		data[8] * data[3] * data[6];

	finalMx.data[11] = -data[0] * data[5] * data[11] +
		data[0] * data[7] * data[9] +
		data[4] * data[1] * data[11] -
		data[4] * data[3] * data[9] -
		data[8] * data[1] * data[7] +
		data[8] * data[3] * data[5];

	finalMx.data[15] = data[0] * data[5] * data[10] -
		data[0] * data[6] * data[9] -
		data[4] * data[1] * data[10] +
		data[4] * data[2] * data[9] +
		data[8] * data[1] * data[6] -
		data[8] * data[2] * data[5];

	float determinant = data[0] * finalMx.data[0] + data[1] * finalMx.data[4] + data[2] * finalMx.data[8] + data[3] * finalMx.data[12];
	if (determinant == 0.0f) {
		return Matrix4::Invalid();
	}
	float invDet = 1.0f / determinant;

	for (int i = 0; i < 16; ++i)
		finalMx.data[i] = finalMx.data[i] * invDet;

	return finalMx;
}

bool Matrix4::valid() const
{
	for (int i = 0; i < 16; ++i)
		if (std::isnan(data[i]))
			return false;
	return true;
}

Matrix4 Matrix4::operator*(float num) const
{
	Matrix4 finalMx;
	for (int i = 0; i < 16; ++i) {
		finalMx.data[i] = data[i] * num;
	}
	return finalMx;
}

Matrix4 Matrix4::operator+(const Matrix4 & obj) const
{
	Matrix4 finalMx;
	for (int i = 0; i < 16; ++i) {
		finalMx.data[i] = data[i] + obj.data[i];
	}
	return finalMx;
}

Matrix4 Matrix4::operator-(const Matrix4 & obj) const
{
	Matrix4 finalMx;
	for (int i = 0; i < 16; ++i) {
		finalMx.data[i] = data[i] - obj.data[i];
	}
	return finalMx;
}

Matrix4 Matrix4::operator*(const Matrix4 & obj) const
{
	Matrix4 finalMx;
	finalMx.data[i00] = data[i00] * obj.data[i00] + data[i01] * obj.data[i10] + data[i02] * obj.data[i20] + data[i03] * obj.data[i30];
	finalMx.data[i01] = data[i00] * obj.data[i01] + data[i01] * obj.data[i11] + data[i02] * obj.data[i21] + data[i03] * obj.data[i31];
	finalMx.data[i02] = data[i00] * obj.data[i02] + data[i01] * obj.data[i12] + data[i02] * obj.data[i22] + data[i03] * obj.data[i32];
	finalMx.data[i03] = data[i00] * obj.data[i03] + data[i01] * obj.data[i13] + data[i02] * obj.data[i23] + data[i03] * obj.data[i33];

	finalMx.data[i10] = data[i10] * obj.data[i00] + data[i11] * obj.data[i10] + data[i12] * obj.data[i20] + data[i13] * obj.data[i30];
	finalMx.data[i11] = data[i10] * obj.data[i01] + data[i11] * obj.data[i11] + data[i12] * obj.data[i21] + data[i13] * obj.data[i31];
	finalMx.data[i12] = data[i10] * obj.data[i02] + data[i11] * obj.data[i12] + data[i12] * obj.data[i22] + data[i13] * obj.data[i32];
	finalMx.data[i13] = data[i10] * obj.data[i03] + data[i11] * obj.data[i13] + data[i12] * obj.data[i23] + data[i13] * obj.data[i33];

	finalMx.data[i20] = data[i20] * obj.data[i00] + data[i21] * obj.data[i10] + data[i22] * obj.data[i20] + data[i23] * obj.data[i30];
	finalMx.data[i21] = data[i20] * obj.data[i01] + data[i21] * obj.data[i11] + data[i22] * obj.data[i21] + data[i23] * obj.data[i31];
	finalMx.data[i22] = data[i20] * obj.data[i02] + data[i21] * obj.data[i12] + data[i22] * obj.data[i22] + data[i23] * obj.data[i32];
	finalMx.data[i23] = data[i20] * obj.data[i03] + data[i21] * obj.data[i13] + data[i22] * obj.data[i23] + data[i23] * obj.data[i33];

	finalMx.data[i30] = data[i30] * obj.data[i00] + data[i31] * obj.data[i10] + data[i32] * obj.data[i20] + data[i33] * obj.data[i30];
	finalMx.data[i31] = data[i30] * obj.data[i01] + data[i31] * obj.data[i11] + data[i32] * obj.data[i21] + data[i33] * obj.data[i31];
	finalMx.data[i32] = data[i30] * obj.data[i02] + data[i31] * obj.data[i12] + data[i32] * obj.data[i22] + data[i33] * obj.data[i32];
	finalMx.data[i33] = data[i30] * obj.data[i03] + data[i31] * obj.data[i13] + data[i32] * obj.data[i23] + data[i33] * obj.data[i33];
	return finalMx;
}

Vector4 Matrix4::operator*(const Vector4 & obj) const
{
	Vector4 finalVr;
	finalVr.x = data[i00] * obj.x + data[i01] * obj.y + data[i02] * obj.z + data[i03] * obj.w;
	finalVr.y = data[i10] * obj.x + data[i11] * obj.y + data[i12] * obj.z + data[i13] * obj.w;
	finalVr.z = data[i20] * obj.x + data[i21] * obj.y + data[i22] * obj.z + data[i23] * obj.w;
	finalVr.w = data[i30] * obj.x + data[i31] * obj.y + data[i32] * obj.z + data[i33] * obj.w;
	return finalVr;
}

Matrix4 & Matrix4::operator*=(float num)
{
	for (int i = 0; i < 16; ++i)
		data[i] = data[i] * num;
	return *this;
}

Matrix4 & Matrix4::operator+=(const Matrix4 & obj)
{
	for (int i = 0; i < 16; ++i)
		data[i] = data[i] + obj.data[i];
	return *this;
}

Matrix4 & Matrix4::operator-=(const Matrix4 & obj)
{
	for (int i = 0; i < 16; ++i)
		data[i] = data[i] - obj.data[i];
	return *this;
}

Matrix4 & Matrix4::operator*=(const Matrix4 & obj)
{
	Matrix4 m = *this * obj;
	for (int i = 0; i < 16; ++i) {
		data[i] = m.data[i];
	}
	return *this;
}

bool Matrix4::operator==(const Matrix4 & obj) const
{
	for (int i = 0; i < 16; ++i)
		if (fneq(data[i], obj.data[i]))
			return false;
	return true;
}

Matrix4::Row Matrix4::operator[](int collIndex)
{
	return Row(this, collIndex);
}

const Matrix4::Row Matrix4::operator[](int collIndex) const
{
	return Row(const_cast<Matrix4*>(this), collIndex);
}

Matrix4 operator*(float num, const Matrix4 & mx4)
{
	Matrix4 finalMx;
	for (int i = 0; i < 9; ++i)
		finalMx.data[i] = num * mx4.data[i];
	return finalMx;
}

std::ostream & operator<<(std::ostream & output, const Matrix4 & matrix)
{
	output << "[ ";
	for (int i = 0; i < 4; ++i) {
		output << "{ ";
		for (int j = 0; j < 4; ++j) {
			output << matrix.get(i, j) << ((j < 3) ? ", " : " ");
		}
		output << "}" << ((i < 3) ? ", " : " ");
	}
	return output << "]";
}

#define IMX(m)\
	m.data[i00] = 1.0f;\
	m.data[i11] = 1.0f;\
	m.data[i22] = 1.0f;\
	m.data[i33] = 1.0f;\

Matrix4 Matrix4::Identity()
{
	Matrix4 m;
	IMX(m)
	return m;
}

Matrix4 Matrix4::Scale(float xFactor, float yFactor, float zFactor, float wFactor)
{
	Matrix4 m;
	m.data[i00] = xFactor;
	m.data[i11] = yFactor;
	m.data[i22] = zFactor;
	m.data[i33] = wFactor;
	return m;
}

Matrix4 Matrix4::Shear(float xFactor, float yFactor, float zFactor)
{
	Matrix4 m;
	IMX(m)
	m.data[i01] = xFactor;
	m.data[i10] = yFactor;
	m.data[i02] = zFactor;
	return m;
}

Matrix4 Matrix4::Shift(float xDist, float yDist, float zDist)
{
	Matrix4 m;
	IMX(m)
	m.data[i03] = xDist;
	m.data[i13] = yDist;
	m.data[i23] = zDist;
	return m;
}

Matrix4 Matrix4::Reflect(float xFactor, float yFactor)
{
	Matrix4 m;
	m.data[i00] = xFactor;
	m.data[i11] = yFactor;
	m.data[i22] = 1.0f;
	m.data[i33] = 1.0f;
	return m;
}

#define genRotVals()\
	xDeg = toRad(xDeg);\
	yDeg = toRad(yDeg);\
	zDeg = toRad(zDeg);\
	float sinX = sinf(xDeg);\
	float sinY = sinf(yDeg);\
	float sinZ = sinf(zDeg);\
	float cosX = cosf(xDeg);\
	float cosY = cosf(yDeg);\
	float cosZ = cosf(zDeg)\

Matrix4 Matrix4::RotateXYZ(float xDeg, float yDeg, float zDeg)
{
	Matrix4 m;
	genRotVals();

	m.data[i00] = cosY * cosZ;
	m.data[i10] = cosZ * sinX * sinY + cosX * sinZ;
	m.data[i20] = sinX * sinZ - cosX * cosZ * sinY;

	m.data[i01] = -cosY * sinZ;
	m.data[i11] = cosX * cosZ - sinX * sinY * sinZ;
	m.data[i21] = cosZ * sinX + cosX * sinY * sinZ;

	m.data[i02] = sinY;
	m.data[i12] = -cosY * sinX;
	m.data[i22] = cosX * cosY;

	m.data[i33] = 1.0f;
	return m;
}

Matrix4 Matrix4::RotateXZY(float xDeg, float yDeg, float zDeg)
{
	Matrix4 m;
	genRotVals();

	m.data[i00] = cosY * cosZ;
	m.data[i10] = sinX * sinY + cosX * cosY * sinZ;
	m.data[i20] = cosY * sinX * sinZ - cosX * sinY;

	m.data[i01] = -sinZ;
	m.data[i11] = cosX * cosZ;
	m.data[i21] = cosZ * sinX;

	m.data[i02] = cosZ * sinY;
	m.data[i12] = cosX * sinY * sinZ - cosY * sinX;
	m.data[i22] = cosX * cosY + sinX * sinY * sinZ;

	m.data[i33] = 1.0f;
	return m;
}

Matrix4 Matrix4::RotateYXZ(float xDeg, float yDeg, float zDeg)
{
	Matrix4 m;
	genRotVals();

	m.data[i00] = cosY * cosZ + sinX * sinY * sinZ;
	m.data[i10] = cosX * sinZ;
	m.data[i20] = cosY * sinX * sinZ - cosZ * sinY;

	m.data[i01] = cosZ * sinX * sinY - cosY * sinZ;
	m.data[i11] = cosX * cosZ;
	m.data[i21] = cosY * cosZ * sinX + sinY * sinZ;

	m.data[i02] = cosX * sinY;
	m.data[i12] = -sinX;
	m.data[i22] = cosX * cosY;

	m.data[i33] = 1.0f;
	return m;
}

Matrix4 Matrix4::RotateYZX(float xDeg, float yDeg, float zDeg)
{
	Matrix4 m;
	genRotVals();

	m.data[i00] = cosY * cosZ;
	m.data[i10] = sinZ;
	m.data[i20] = -cosZ * sinY;

	m.data[i01] = sinX * sinY - cosX * cosY * sinZ;
	m.data[i11] = cosX * cosZ;
	m.data[i21] = cosY * sinX + cosX * sinY * sinZ;

	m.data[i02] = cosX * sinY + cosY * sinX * sinZ;
	m.data[i12] = -cosZ * sinX;
	m.data[i22] = cosX * cosY - sinX * sinY * sinZ;

	m.data[i33] = 1.0f;
	return m;
}

Matrix4 Matrix4::RotateZXY(float xDeg, float yDeg, float zDeg)
{
	Matrix4 m;
	genRotVals();

	m.data[i00] = cosY * cosZ - sinX * sinY * sinZ;
	m.data[i10] = cosZ * sinX * sinY + cosY * sinZ;
	m.data[i20] = -cosX * sinY;

	m.data[i01] = -cosX * sinZ;
	m.data[i11] = cosX * cosZ;
	m.data[i21] = sinX;

	m.data[i02] = cosZ * sinY + cosY * sinX * sinZ;
	m.data[i12] = sinY * sinZ - cosY * cosZ * sinX;
	m.data[i22] = cosX * cosY;

	m.data[i33] = 1.0f;
	return m;
}

Matrix4 Matrix4::RotateZYX(float xDeg, float yDeg, float zDeg)
{
	Matrix4 m;
	genRotVals();

	m.data[i00] = cosZ * cosY;
	m.data[i10] = sinZ * cosY;
	m.data[i20] = -sinY;

	m.data[i01] = -sinZ * cosX + cosZ * sinY * sinX;
	m.data[i11] = cosZ * cosX + sinZ * sinY * sinX;
	m.data[i21] = cosY * sinX;

	m.data[i02] = sinZ * sinX + cosZ * sinY * cosX;
	m.data[i12] = -cosZ * sinX + sinZ * sinY * cosX;
	m.data[i22] = cosY * cosX;

	m.data[i33] = 1.0f;
	return m;
}

Matrix4 Matrix4::Ortho(float left, float right, float bottom, float top, float near, float far)
{
	Matrix4 m;
	m.data[i00] = 2.0f / (right - left);
	m.data[i10] = 0.0f;
	m.data[i20] = 0.0f;
	m.data[i30] = 0.0f;

	m.data[i01] = 0.0f;
	m.data[i11] = 2.0f / (top - bottom);
	m.data[i21] = 0.0f;
	m.data[i31] = 0.0f;

	m.data[i02] = 0.0f;
	m.data[i12] = 0.0f;
	m.data[i22] = -2.0f / (far - near);
	m.data[i32] = 0.0f;

	m.data[i03] = -(right + left) / (right - left);
	m.data[i13] = -(top + bottom) / (top - bottom);
	m.data[i23] = -(far + near) / (far - near);
	m.data[i33] = 1.0f;
	return m;
}

Matrix4 Matrix4::Persp(float fov, float aspect, float near, float far)
{
	float h = 1.0f / tanf(toRad(fov) / 2.0f);
	float nearNegFar = near - far;
	Matrix4 m;
	m.data[i00] = h / aspect;
	m.data[i10] = 0.0f;
	m.data[i20] = 0.0f;
	m.data[i30] = 0.0f;

	m.data[i01] = 0.0f;
	m.data[i11] = h;
	m.data[i21] = 0.0f;
	m.data[i31] = 0.0f;

	m.data[i02] = 0.0f;
	m.data[i12] = 0.0f;
	m.data[i22] = (far + near) / nearNegFar;
	m.data[i32] = -1.0f;

	m.data[i03] = 0.0f;
	m.data[i13] = 0.0f;
	m.data[i23] = (2.0f * near * far) / nearNegFar;
	m.data[i33] = 0.0f;
	return m;
}


Matrix4 Matrix4::LookAt(const Vector3& p, const Vector3& t, const Vector3& u)
{
	Matrix4 m;
	Vector3 dir = p.to(t).normalise();
	Vector3 right = u.normalise().cross(dir).normalise();
	Vector3 up = dir.cross(right).normalise();
	m.data[i00] = right.x;
	m.data[i10] = right.y;
	m.data[i20] = right.z;
	m.data[i01] = up.x;
	m.data[i11] = up.y;
	m.data[i21] = up.z;
	m.data[i02] = dir.x;
	m.data[i12] = dir.y;
	m.data[i22] = dir.z;
	m.data[i33] = 1.0f;
	return m;
}

Matrix4 Matrix4::LookDir(const Vector3& direction, const Vector3& up)
{
	Mat4 out;
	Vec3 xaxis = -up.normalise().cross(direction).normalise();
	Vec3 yaxis = direction.cross(xaxis).normalise();
	out.data[i00] = xaxis.x;
	out.data[i10] = yaxis.x;
	out.data[i20] = direction.x;
	out.data[i01] = xaxis.y;
	out.data[i11] = yaxis.y;
	out.data[i21] = direction.y;
	out.data[i02] = xaxis.z;
	out.data[i12] = yaxis.z;
	out.data[i22] = direction.z;
	out.data[i33] = 1.0f;
	return out.transpose();
}

Matrix4::Row::Row(Matrix4* mat, int rowIndex)
{
	this->row = mat->data + rowIndex;
}

Matrix4::Row::Row(const Row& matrix) : row(matrix.row) {}

float& Matrix4::Row::operator[](int collIndex)
{
	return row[collIndex << 2];
}

const float& Matrix4::Row::operator[](int collIndex) const
{
	return row[collIndex << 2];
}
