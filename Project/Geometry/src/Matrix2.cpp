#include <Matrix2.h>
#include <Vector2.h>
#include <Matrix3.h>
#include <Matrix4.h>

#include <iostream>
#include <string>

#include <limits>
#include <math.h>

#define fneq(a,b) (fabsf(a - b) > std::numeric_limits<float>::epsilon())

Matrix2::Matrix2() : data{0.0f} {}

Matrix2::Matrix2(float diagonal)
{
	data[1] = data[2] = 0.0f;
	data[0] = data[3] = diagonal;
}

const float& Matrix2::get(int rowIndex, int collIndex) const
{
	return data[collIndex * 2 + rowIndex];
}

float& Matrix2::get(int rowIndex, int collIndex)
{
	return data[collIndex * 2 + rowIndex];
}

Matrix2 Matrix2::transpose() const
{
	Matrix2 newMx;
	newMx.data[0] = data[0];
	newMx.data[1] = data[2];
	newMx.data[2] = data[1];
	newMx.data[3] = data[3];
	return newMx;
}

float Matrix2::det() const
{
	return data[0] * data[3] - data[2] * data[1];
}

Matrix2 Matrix2::inv() const
{
	Matrix2 finalMx;
	float det = data[0] * data[3] - data[2] * data[1];
	if (det == 0.0f) {
		return Matrix2::Invalid();
	}
	det = 1.0f / det;
	finalMx.data[0] = det * data[3];
	finalMx.data[1] = det * -data[1];
	finalMx.data[2] = det * -data[2];
	finalMx.data[3] = det * data[0];
	return finalMx;
}

bool Matrix2::valid() const
{
	for (int i = 0; i < 4; ++i)
		if (std::isnan(data[i]))
			return false;
	return true;
}

Matrix2 Matrix2::operator*(float num) const
{
	Matrix2 finalMx;
	for (int i = 0; i < 4; ++i)
		finalMx.data[i] = data[i] * num;
	return finalMx;
}

Matrix2 Matrix2::operator+(const Matrix2 & obj) const
{
	Matrix2 finalMx;
	for (int i = 0; i < 4; ++i)
		finalMx.data[i] = data[i] + obj.data[i];
	return finalMx;
}

Matrix2 Matrix2::operator-(const Matrix2 & obj) const
{
	Matrix2 finalMx;
	for (int i = 0; i < 4; ++i)
		finalMx.data[i] = data[i] - obj.data[i];
	return finalMx;
}

Matrix2 Matrix2::operator*(const Matrix2 & obj) const
{
	Matrix2 finalMx;
	finalMx.data[0] = data[0] * obj.data[0] + data[2] * obj.data[1];
	finalMx.data[1] = data[1] * obj.data[0] + data[3] * obj.data[1];
	finalMx.data[2] = data[0] * obj.data[2] + data[2] * obj.data[3];
	finalMx.data[3] = data[1] * obj.data[2] + data[3] * obj.data[3];
	return finalMx;
}

Vector2 Matrix2::operator*(const Vector2 & obj) const
{
	Vector2 finalVr;
	finalVr.x = data[0] * obj.x + data[2] * obj.y;
	finalVr.y = data[1] * obj.x + data[3] * obj.y;
	return finalVr;
}

Matrix2 & Matrix2::operator*=(float num)
{
	for (int i = 0; i < 4; ++i)
		data[i] = data[i] * num;
	return *this;
}

Matrix2 & Matrix2::operator+=(const Matrix2 & obj)
{
	for (int i = 0; i < 4; ++i)
		data[i] = data[i] + obj.data[i];
	return *this;
}

Matrix2 & Matrix2::operator-=(const Matrix2 & obj)
{
	for (int i = 0; i < 4; ++i)
		data[i] = data[i] - obj.data[i];
	return *this;
}

Matrix2 & Matrix2::operator*=(const Matrix2 & obj)
{
	float content[4];
	content[0] = data[0] * obj.data[0] + data[2] * obj.data[1];
	content[1] = data[1] * obj.data[0] + data[3] * obj.data[1];
	content[2] = data[0] * obj.data[2] + data[2] * obj.data[3];
	content[3] = data[1] * obj.data[2] + data[3] * obj.data[3];
	memcpy(data, content, sizeof(content));
	return *this;
}

bool Matrix2::operator==(const Matrix2 & obj) const
{
	for (int i = 0; i < 4; ++i)
		if (fneq(data[i], obj.data[i]))
			return false;
	return true;
}

Matrix2 operator*(float num, const Matrix2 & matrix)
{
	Matrix2 finalMx;
	for (int i = 0; i < 4; ++i)
		finalMx.data[i] = num * matrix.data[i];
	return finalMx;
}

std::ostream & operator<<(std::ostream & output, const Matrix2 & matrix)
{
	output << "[ ";
	for (int i = 0; i < 2; ++i) {
		output << "{ ";
		for (int j = 0; j < 2; ++j) {
			output << matrix.get(i, j) << ((j == 0) ? ", " : " ");
		}
		output <<  "}" << ((i == 0) ? ", " : " ");
	}
	return output << "]";
}

Matrix2 Matrix2::Identity()
{
	Matrix2 finalMx;
	finalMx.data[0] = 1.0f;
	finalMx.data[3] = 1.0f;
	return finalMx;
}

Matrix2 Matrix2::Scale(float xFactor, float yFactor)
{
	Matrix2 m;
	m.data[0] = xFactor;
	m.data[3] = yFactor;
	return m;
}

Matrix2 Matrix2::Shear(float xFactor, float yFactor)
{
	Matrix2 m;
	m.data[0] = 1.0f;
	m.data[3] = 1.0f;

	m.data[2] = xFactor;
	m.data[1] = yFactor;
	return m;
}

Matrix2::Row::Row(Matrix2* mat, int rowIndex)
{
	this->row = mat->data + rowIndex;
}

Matrix2::Row::Row(const Row& matrix) : row(matrix.row) {}

float& Matrix2::Row::operator[](int collIndex)
{
	return row[collIndex << 1];
}

const float& Matrix2::Row::operator[](int collIndex) const
{
	return row[collIndex << 1];
}

Matrix2::Row Matrix2::operator[](int collIndex)
{
	return Row(this, collIndex);
}

const Matrix2::Row Matrix2::operator[](int collIndex) const
{
	return Row(const_cast<Matrix2*>(this), collIndex);
}
