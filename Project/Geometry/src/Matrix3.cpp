#include <Matrix3.h>
#include <Matrix4.h>

#include <iostream>
#include <string>

#include <limits>
#include <math.h>

#define fneq(a,b) (fabsf(a - b) > std::numeric_limits<float>::epsilon())
#define PI 3.141592741f
#define toRad(deg) (deg * 0.0174533f)
#define toDeg(rad) (rad * 57.2958f)

namespace {
	enum indices {i00, i10, i20, i01, i11, i21, i02, i12, i22};
}

Matrix3::Matrix3() : data{ 0.0f } {}

Matrix3::Matrix3(float diagonal) : data{ 0.0f }
{
	data[i00] = diagonal;
	data[i11] = diagonal;
	data[i22] = diagonal;
}

Matrix3::Matrix3(const Matrix2 & matrix)
{
	data[i00] = matrix.data[0];
	data[i10] = matrix.data[1];
	data[i20] = 0.0f;
	data[i01] = matrix.data[2];
	data[i11] = matrix.data[3];
	data[i21] = 0.0f;
	data[i02] = 0.0f;
	data[i12] = 0.0f;
	data[i22] = 1.0f;
}

Matrix3::operator Matrix2() const
{
	Matrix2 m;
	m.data[0] = data[0];
	m.data[1] = data[1];
	m.data[2] = data[3];
	m.data[3] = data[4];
	return m;
}

const float& Matrix3::get(int rowIndex, int collIndex) const
{
	return data[collIndex * 3 + rowIndex];
}

float& Matrix3::get(int rowIndex, int collIndex)
{
	return data[collIndex * 3 + rowIndex];
}

Matrix3 Matrix3::transpose() const
{
	Matrix3 newMx;
	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 3; ++j)
			newMx.get(i, j) = get(j, i);
	return newMx;
}

float Matrix3::det() const
{ //https://uk.mathworks.com/help/aeroblks/determinantof3x3matrix.html
	float a = data[i00] * (data[i11] * data[i22] - data[i12] * data[i21]);
	float b = data[i01] * (data[i10] * data[i22] - data[i12] * data[i20]);
	float c = data[i02] * (data[i10] * data[i21] - data[i11] * data[i20]);
	return a - b + c;
}

Matrix3 Matrix3::inv() const
{
	Matrix3 finalMx;
	float determinant = det();
	if (determinant == 0.0f) {
		return Matrix3::Invalid();
	}
	float invdet = 1.0f / determinant;
	finalMx.data[i00] = (data[i11] * data[i22] - data[i21] * data[i12]) * invdet;
	finalMx.data[i01] = (data[i02] * data[i21] - data[i01] * data[i22]) * invdet;
	finalMx.data[i02] = (data[i01] * data[i12] - data[i02] * data[i11]) * invdet;
	finalMx.data[i10] = (data[i12] * data[i20] - data[i10] * data[i22]) * invdet;
	finalMx.data[i11] = (data[i00] * data[i22] - data[i02] * data[i20]) * invdet;
	finalMx.data[i12] = (data[i10] * data[i02] - data[i00] * data[i12]) * invdet;
	finalMx.data[i20] = (data[i10] * data[i21] - data[i20] * data[i11]) * invdet;
	finalMx.data[i21] = (data[i20] * data[i01] - data[i00] * data[i21]) * invdet;
	finalMx.data[i22] = (data[i00] * data[i11] - data[i10] * data[i01]) * invdet;
	return finalMx;
}

bool Matrix3::valid() const
{
	for (int i = 0; i < 9; ++i)
		if (std::isnan(data[i]))
			return false;
	return true;
}

Matrix3 Matrix3::operator*(float num) const
{
	Matrix3 finalMx;
	for (int i = 0; i < 9; ++i)
		finalMx.data[i] = data[i] * num;
	return finalMx;
}

Matrix3 Matrix3::operator+(const Matrix3 & obj) const
{
	Matrix3 finalMx;
	for (int i = 0; i < 9; ++i)
		finalMx.data[i] = data[i] * obj.data[i];
	return finalMx;
}

Matrix3 Matrix3::operator-(const Matrix3 & obj) const
{
	Matrix3 finalMx;
	for (int i = 0; i < 9; ++i)
		finalMx.data[i] = data[i] - obj.data[i];
	return finalMx;
}

Matrix3 Matrix3::operator*(const Matrix3 & obj) const
{
	Matrix3 finalMx;	
	finalMx.data[i00] = data[i00] * obj.data[i00] + data[i01] * obj.data[i10] + data[i02] * obj.data[i20];
	finalMx.data[i01] = data[i00] * obj.data[i01] + data[i01] * obj.data[i11] + data[i02] * obj.data[i21];
	finalMx.data[i02] = data[i00] * obj.data[i02] + data[i01] * obj.data[i12] + data[i02] * obj.data[i22];

	finalMx.data[i10] = data[i10] * obj.data[i00] + data[i11] * obj.data[i10] + data[i12] * obj.data[i20];
	finalMx.data[i11] = data[i10] * obj.data[i01] + data[i11] * obj.data[i11] + data[i12] * obj.data[i21];
	finalMx.data[i12] = data[i10] * obj.data[i02] + data[i11] * obj.data[i12] + data[i12] * obj.data[i22];

	finalMx.data[i20] = data[i20] * obj.data[i00] + data[i21] * obj.data[i10] + data[i22] * obj.data[i20];
	finalMx.data[i21] = data[i20] * obj.data[i01] + data[i21] * obj.data[i11] + data[i22] * obj.data[i21];
	finalMx.data[i22] = data[i20] * obj.data[i02] + data[i21] * obj.data[i12] + data[i22] * obj.data[i22];
	return finalMx;
}

Vector3 Matrix3::operator*(const Vector3 & obj) const
{
	Vector3 finalVr;
	finalVr.x = data[i00] * obj.x + data[i01] * obj.y + data[i02] * obj.z;
	finalVr.y = data[i10] * obj.x + data[i11] * obj.y + data[i12] * obj.z;
	finalVr.z = data[i20] * obj.x + data[i21] * obj.y + data[i22] * obj.z;
	return finalVr;
}

Vector2 Matrix3::operator*(const Vector2& vec) const
{
	Vector2 finalVr;
	finalVr.x = data[i00] * vec.x + data[i01] * vec.y + data[i02] * 1.0f;
	finalVr.y = data[i10] * vec.x + data[i11] * vec.y + data[i12] * 1.0f;
	return finalVr;
}

Matrix3 & Matrix3::operator*=(float num)
{
	for (int i = 0; i < 9; ++i)
		data[i] = data[i] * num;
	return *this;
}

Matrix3 & Matrix3::operator+=(const Matrix3 & obj)
{
	for (int i = 0; i < 9; ++i)
		data[i] = data[i] + obj.data[i];
	return *this;
}

Matrix3 & Matrix3::operator-=(const Matrix3 & obj)
{
	for (int i = 0; i < 9; ++i)
		data[i] = data[i] - obj.data[i];
	return *this;
}

Matrix3 & Matrix3::operator*=(const Matrix3 & obj)
{
	Matrix3 m = *this * obj;
	for (int i = 0; i < 9; ++i) {
		data[i] = m.data[i];
	}
	return *this;
}

bool Matrix3::operator==(const Matrix3 & obj) const
{
	for (int i = 0; i < 9; ++i)
		if (fneq(data[i], obj.data[i]))
			return false;
	return true;
}

Matrix3 operator*(float num, const Matrix3 & mx3)
{
	Matrix3 finalMx;
	for (int i = 0; i < 9; ++i)
		finalMx.data[i] = num * mx3.data[i];
	return finalMx;
}

std::ostream & operator<<(std::ostream & output, const Matrix3 & matrix)
{
	output << "[ ";
	for (int i = 0; i < 3; ++i) {
		output << "{ ";
		for (int j = 0; j < 3; ++j) {
			output << matrix.get(i, j) << ((j < 2) ? ", " : " ");
		}
		output << "}" << ((i < 2) ? ", " : " ");
	}
	return output << "]";
}

Matrix3 Matrix3::Identity()
{
	Matrix3 m;
	m.data[i00] = 1.0f;
	m.data[i11] = 1.0f;
	m.data[i22] = 1.0f;
	return m;
}

#define IMX(m)\
	m.data[i00] = 1.0f;\
	m.data[i11] = 1.0f;\
	m.data[i22] = 1.0f;\

Matrix3 Matrix3::Scale(float xFactor, float yFactor, float zFactor)
{
	Matrix3 m;
	m.data[i00] = xFactor;
	m.data[i11] = yFactor;
	m.data[i22] = zFactor;
	return m;
}

Matrix3 Matrix3::Shear(float xFactor, float yFactor, float zFactor)
{
	Matrix3 m;
	IMX(m)
	m.data[i01] = xFactor;
	m.data[i10] = yFactor;
	m.data[i02] = zFactor;
	return m;
}

Matrix3 Matrix3::Shift(float xDist, float yDist)
{
	Matrix3 m;
	IMX(m)
	m.data[i02] = xDist;
	m.data[i12] = yDist;
	return m;
}

Matrix3 Matrix3::Reflect(float xFactor, float yFactor)
{
	Matrix3 m;
	m.data[i00] = xFactor;
	m.data[i11] = yFactor;
	m.data[i22] = 1.0f;
	return m;
}

Matrix3 Matrix3::Rotate(float deg)
{
	Matrix3 m;
	float rad = toRad(deg);
	float sinTheta = sinf(rad);
	float cosTheta = cosf(rad);

	m.data[i00] = cosTheta;
	m.data[i01] = -sinTheta;
	m.data[i10] = sinTheta;
	m.data[i11] = cosTheta;
	m.data[i22] = 1.0f;
	return m;
}

Matrix3::Row::Row(Matrix3* mat, int rowIndex)
{
	row = mat->data + rowIndex;
}

Matrix3::Row::Row(const Row& matrix) : row(matrix.row) {}

float& Matrix3::Row::operator[](int collIndex)
{
	return row[collIndex * 4];
}

const float& Matrix3::Row::operator[](int collIndex) const
{
	return row[collIndex * 4];
}

Matrix3::Row Matrix3::operator[](int collIndex)
{
	return Row(this, collIndex);
}

const Matrix3::Row Matrix3::operator[](int collIndex) const
{
	return Row(const_cast<Matrix3*>(this), collIndex);
}