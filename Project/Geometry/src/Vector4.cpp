#include <Vector4.h>
#include <string>
#include <limits>

namespace {
	template <typename T>
	T clamp(T n, T lower, T upper) {
		return std::max(lower, std::min(n, upper));
	}
}

#define PI 3.141592741f

#define feq(a,b) (fabsf(a - b) <= std::numeric_limits<float>::epsilon())
#define fle(a,b) (a - b <= std::numeric_limits<float>::epsilon())
#define fge(a,b) (b - a <= std::numeric_limits<float>::epsilon())

#define toRad(deg) (deg * PI / 180.0f)

Vector4::Vector4()
{
	x = y = z = w = 0.0f;
}

Vector4::Vector4(float xyzw)
{
	x = y = z = w = xyzw;
}

Vector4::Vector4(float x, float y, float z, float w)
{
	this->x = x;
	this->y = y;
	this->z = z;
	this->w = w;
}

Vector4::Vector4(const Vector2 & vr2, float z, float w)
{
	x = vr2.x;
	y = vr2.y;
	this->z = z;
	this->w = w;
}

Vector4::Vector4(float x, const Vector2 & vr2, float w)
{
	this->x = x;
	y = vr2.x;
	z = vr2.y;
	this->w = w;
}

Vector4::Vector4(float x, float y, const Vector2 & vr2)
{
	this->x = x;
	this->y = y;
	z = vr2.x;
	w = vr2.y;
}

Vector4::Vector4(const Vector3 & vr3, float w)
{
	x = vr3.x;
	y = vr3.y;
	z = vr3.z;
	this->w = w;
}

Vector4::Vector4(float x, const Vector3 & vr3)
{
	this->x = x;
	y = vr3.x;
	z = vr3.y;
	w = vr3.z;
}

Vector4::Vector4(const Vector2& v1, const Vector2& v2) : x(v1.x), y(v1.y), z(v2.x), w(v2.y) {}

float Vector4::dist(const Vector4 & vector) const
{
	float a = x - vector.x;
	float b = y - vector.y;
	float c = z - vector.z;
	float d = w - vector.w;
	return sqrtf(a * a + b * b + c * c + d * d);
}

float Vector4::dot(const Vector4 & vector) const
{
	return x * vector.x + y * vector.y + z * vector.z + w * vector.w;
}

float Vector4::magnitude() const
{
	return sqrtf(x * x + y * y + z * z + w * w);
}

Vector4 Vector4::to(const Vector4 & vector) const
{
	return Vector4(vector.x - x, vector.y - y, vector.z - z, vector.w - w);
}

Vector4 Vector4::normalise() const
{
	float m = magnitude();
	if (m == 0.0f)
		return Vector4();
	return Vector4(x / m, y / m, z / m, w / m);
}

bool Vector4::valid() const
{
	if (isnan(x))
		return false;
	else if (isnan(y))
		return false;
	else if (isnan(z))
		return false;
	else if (isnan(w))
		return false;
	return true;
}

Vector4 Vector4::towards(const Vector4 & vector, float step) const
{
	if (fabsf(step) > vector.dist(*this))
		return vector;
	return *this + vector.to(*this).normalise() * step;
}

Vector4 Vector4::lerp(const Vector4 & vector, float frac) const
{
	return *this * (1.0f - frac) + vector * frac;
}

Vector4 Vector4::operator+(const Vector4 & obj) const
{
	return Vector4(x + obj.x, y + obj.y, z + obj.z, w + obj.w);
}

Vector4 Vector4::operator-(const Vector4 & obj) const
{
	return Vector4(x - obj.x, y - obj.y, z - obj.z, w - obj.w);
}

Vector4 Vector4::operator*(const Vector4 & obj) const
{
	return Vector4(x * obj.x, y * obj.y, z * obj.z, w * obj.w);
}

Vector4 Vector4::operator*(float num) const
{
	return Vector4(x * num, y * num, z * num, w * num);
}

Vector4 Vector4::operator/(float num) const
{
	float i = 1.0f / num;
	return Vector4(x * i, y * i, z * i, w * i);
}

Vector4 Vector4::operator-() const
{
	return Vector4(-x, -y, -z, -w);
}

Vector4 & Vector4::operator+=(const Vector4 & obj)
{
	x = x + obj.x;
	y = y + obj.y;
	z = z + obj.z;
	w = w + obj.w;
	return *this;
}

Vector4 & Vector4::operator-=(const Vector4 & obj)
{
	x = x - obj.x;
	y = y - obj.y;
	z = z - obj.z;
	w = w - obj.w;
	return *this;
}

Vector4 & Vector4::operator*=(const Vector4 & obj)
{
	x = x * obj.x;
	y = y * obj.y;
	z = z * obj.z;
	w = w * obj.w;
	return *this;
}

Vector4 & Vector4::operator*=(float num)
{
	x = x * num;
	y = y * num;
	z = z * num;
	w = w * num;
	return *this;
}

Vector4 & Vector4::operator/=(float num)
{
	float i = 1.0f / num;
	x = x * i;
	y = y * i;
	z = z * i;
	w = w * i;
	return *this;
}

Vector2 Vector4::xy() const
{
	return Vector2(x, y);
}

Vector2 Vector4::yz() const
{
	return Vector2(y, z);
}

Vector2 Vector4::zw() const
{
	return Vector2(z, w);
}

Vector3 Vector4::xyz() const
{
	return Vector3(x, y, z);
}

Vector3 Vector4::yzw() const
{
	return Vector3(y, z, w);
}

bool Vector4::operator==(const Vector4 & obj) const
{
	return feq(x, obj.x) && feq(y, obj.y) && feq(z, obj.z) && feq(w, obj.w);
}

bool Vector4::operator!=(const Vector4 & obj) const
{
	return !(*this == obj);
}

float & Vector4::operator[](int index) {
	switch (index) {
	case 0: return x;
	case 1: return y;
	case 2: return z;
	case 3: return w;
	default: throw std::out_of_range("Vector4 index out of range exception");
	}
}

const float& Vector4::operator[](int index) const {
	switch (index) {
	case 0: return x;
	case 1: return y;
	case 2: return z;
	case 3: return w;
	default: throw std::out_of_range("Vector4 index out of range exception");
	}
}

Vector4 operator*(float num, const Vector4 & vect)
{
	return Vector4(num * vect.x, num * vect.y, num * vect.z, num * vect.w);
}

Vector4 operator/(float num, const Vector4 & vect)
{
	return Vector4(num / vect.x, num / vect.y, num / vect.z, num / vect.w);
}

std::ostream & operator<<(std::ostream & output, const Vector4 & vect)
{
	return output << "[" << vect.x << ", " << vect.y << ", " << vect.z << ", " << vect.w << "]";
}

bool operator<(const Vector4 & a, const Vector4 & b)
{
	if (a.x < b.x) 
		return true;
	else if (b.x < a.x)
		return false;
	else if (a.y < b.y)
		return true;
	else if (b.y < a.y)
		return false;
	else if (a.z < b.z)
		return true;
	else if (b.z < a.z)
		return false;
	else
		return a.w < b.w;
}